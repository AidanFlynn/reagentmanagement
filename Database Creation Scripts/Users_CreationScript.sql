USE [master]
GO

/*******************************/
/****** DATABASE CREATION ******/
/*******************************/ 


/****** Object:  Database [Users]    Script Date: 21/03/2017 6:48:16 PM ******/
CREATE DATABASE [Users]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UsersNew', FILENAME = N'C:\Database\Signout\Users.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UsersNew_log', FILENAME = N'C:\Database\Signout\Users_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Users] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Users].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Users] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Users] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Users] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Users] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Users] SET ARITHABORT OFF 
GO
ALTER DATABASE [Users] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Users] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Users] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Users] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Users] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Users] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Users] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Users] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Users] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Users] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Users] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Users] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Users] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Users] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Users] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Users] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Users] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Users] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Users] SET  MULTI_USER 
GO
ALTER DATABASE [Users] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Users] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Users] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Users] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Users]
GO
USE [Users]
GO


/*******************************/
/***** TABLES AND FUNCTIONS ****/
/*******************************/ 


/****** Object:  UserDefinedFunction [dbo].[fn_ConcatLabs]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fn_ConcatLabs](@UserID uniqueidentifier, @ApplicationName varchar (255))
returns varchar(200)
as
begin
	declare @out varchar(200)
	select	@out = coalesce(@out + ',' + convert(varchar,LabID), convert(varchar,LabID))
	from	user_app_lab_role_rel
	where	UserID = @UserID

	return @out
end

GO
/****** Object:  Table [dbo].[user_app_lab_role_rel]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_app_lab_role_rel](
	[UserID] [uniqueidentifier] NOT NULL,
	[ApplicationID] [tinyint] NOT NULL,
	[RoleID] [int] NULL,
	[LabID] [tinyint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[applications]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[applications](
	[ApplicationID] [tinyint] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [varchar](100) NOT NULL,
	[DescriptiveName] [varchar](400) NOT NULL,
	[DefaultPreferences] [xml] NULL,
	[MasterPassword] [varchar](128) NULL,
 CONSTRAINT [PK__applications__060DEAE8] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[UserID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](128) NOT NULL,
	[Password] [varchar](128) NOT NULL,
	[Extension] [varchar](12) NULL,
	[LabID] [tinyint] NULL,
	[RFID] [varchar](20) NULL,
	[IsLabHead] [bit] NOT NULL,
 CONSTRAINT [PK__Users__1367E606] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [FullName] UNIQUE NONCLUSTERED 
(
	[FirstName] ASC,
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[AuthenticationView]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AuthenticationView]
AS
SELECT     dbo.applications.ApplicationName, dbo.user_app_lab_role_rel.ApplicationID, dbo.users.FirstName, dbo.users.LastName, dbo.users.UserID, 
                      dbo.users.Password, dbo.users.RFID
FROM         dbo.applications INNER JOIN
                      dbo.user_app_lab_role_rel ON dbo.applications.ApplicationID = dbo.user_app_lab_role_rel.ApplicationID INNER JOIN
                      dbo.users ON dbo.user_app_lab_role_rel.UserID = dbo.users.UserID

GO
/****** Object:  Table [dbo].[labs]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[labs](
	[LabID] [tinyint] IDENTITY(1,1) NOT NULL,
	[LabName] [varchar](300) NOT NULL,
	[InstituteID] [tinyint] NOT NULL,
 CONSTRAINT [PK__labs__03317E3D] PRIMARY KEY CLUSTERED 
(
	[LabID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UniqueLabName] UNIQUE NONCLUSTERED 
(
	[LabName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__labs__0425A276] UNIQUE NONCLUSTERED 
(
	[LabName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_application_preferences]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_application_preferences](
	[UserID] [uniqueidentifier] NOT NULL,
	[ApplicationID] [tinyint] NOT NULL,
	[Preferences] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[roles]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[UserProfileView]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UserProfileView]
AS
SELECT     dbo.applications.ApplicationName, dbo.user_app_lab_role_rel.ApplicationID, dbo.user_app_lab_role_rel.RoleID, dbo.user_app_lab_role_rel.LabID, 
                      dbo.users.FirstName, dbo.users.LastName, dbo.users.UserID, dbo.users.Email, dbo.users.Password, dbo.users.LabID AS HomeLab, 
                      dbo.labs.LabName, dbo.users.Extension, dbo.roles.RoleName, dbo.user_application_preferences.Preferences
FROM         dbo.applications INNER JOIN
                      dbo.user_app_lab_role_rel ON dbo.applications.ApplicationID = dbo.user_app_lab_role_rel.ApplicationID INNER JOIN
                      dbo.users ON dbo.user_app_lab_role_rel.UserID = dbo.users.UserID INNER JOIN
                      dbo.labs ON dbo.user_app_lab_role_rel.LabID = dbo.labs.LabID INNER JOIN
                      dbo.roles ON dbo.user_app_lab_role_rel.RoleID = dbo.roles.RoleID INNER JOIN
                      dbo.user_application_preferences ON dbo.applications.ApplicationID = dbo.user_application_preferences.ApplicationID AND 
                      dbo.users.UserID = dbo.user_application_preferences.UserID

GO
/****** Object:  Table [dbo].[app_role_relations]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_role_relations](
	[ApplicationID] [tinyint] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Description] [varchar](250) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[cost_centres]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cost_centres](
	[CostCentreID] [char](10) NOT NULL,
	[Description] [varchar](100) NULL,
	[LabID] [tinyint] NOT NULL,
	[Retired] [bit] NOT NULL,
 CONSTRAINT [PK__cost_centres__0CBAE877] PRIMARY KEY CLUSTERED 
(
	[CostCentreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Institutes]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Institutes](
	[InstituteID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DeliveryAddress] [varchar](500) NOT NULL,
	[BillingAddress] [varchar](500) NOT NULL,
	[IsCurrentSite] [bit] NOT NULL,
 CONSTRAINT [PK_Institutes] PRIMARY KEY CLUSTERED 
(
	[InstituteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[SettingName] [varchar](50) NOT NULL,
	[Value] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Index [UNQ_user_app_lab_role_rel_Cred]    Script Date: 21/03/2017 6:48:16 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNQ_user_app_lab_role_rel_Cred] ON [dbo].[user_app_lab_role_rel]
(
	[UserID] ASC,
	[ApplicationID] ASC,
	[LabID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cost_centres] ADD  CONSTRAINT [DF_cost_centres_Retired]  DEFAULT ((0)) FOR [Retired]
GO
ALTER TABLE [dbo].[Institutes] ADD  CONSTRAINT [DF_Institutes_IsCurrentSite]  DEFAULT ((0)) FOR [IsCurrentSite]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_IsLabHead]  DEFAULT ((0)) FOR [IsLabHead]
GO
ALTER TABLE [dbo].[app_role_relations]  WITH CHECK ADD  CONSTRAINT [app_role_relations_AppID_fk] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[applications] ([ApplicationID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[app_role_relations] CHECK CONSTRAINT [app_role_relations_AppID_fk]
GO
ALTER TABLE [dbo].[app_role_relations]  WITH CHECK ADD  CONSTRAINT [app_role_relations_RID_fk] FOREIGN KEY([RoleID])
REFERENCES [dbo].[roles] ([RoleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[app_role_relations] CHECK CONSTRAINT [app_role_relations_RID_fk]
GO
ALTER TABLE [dbo].[cost_centres]  WITH CHECK ADD  CONSTRAINT [FK_cost_centres_labid] FOREIGN KEY([LabID])
REFERENCES [dbo].[labs] ([LabID])
GO
ALTER TABLE [dbo].[cost_centres] CHECK CONSTRAINT [FK_cost_centres_labid]
GO
ALTER TABLE [dbo].[labs]  WITH CHECK ADD  CONSTRAINT [FK_labs_InstituteID] FOREIGN KEY([InstituteID])
REFERENCES [dbo].[Institutes] ([InstituteID])
GO
ALTER TABLE [dbo].[labs] CHECK CONSTRAINT [FK_labs_InstituteID]
GO
ALTER TABLE [dbo].[user_app_lab_role_rel]  WITH CHECK ADD  CONSTRAINT [user_app_role_rel_AppID_fk] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[applications] ([ApplicationID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_app_lab_role_rel] CHECK CONSTRAINT [user_app_role_rel_AppID_fk]
GO
ALTER TABLE [dbo].[user_app_lab_role_rel]  WITH CHECK ADD  CONSTRAINT [user_app_role_rel_LID_fk] FOREIGN KEY([LabID])
REFERENCES [dbo].[labs] ([LabID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_app_lab_role_rel] CHECK CONSTRAINT [user_app_role_rel_LID_fk]
GO
ALTER TABLE [dbo].[user_app_lab_role_rel]  WITH CHECK ADD  CONSTRAINT [user_app_role_rel_RID_fk] FOREIGN KEY([RoleID])
REFERENCES [dbo].[roles] ([RoleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_app_lab_role_rel] CHECK CONSTRAINT [user_app_role_rel_RID_fk]
GO
ALTER TABLE [dbo].[user_app_lab_role_rel]  WITH CHECK ADD  CONSTRAINT [user_app_role_rel_UID_fk] FOREIGN KEY([UserID])
REFERENCES [dbo].[users] ([UserID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_app_lab_role_rel] CHECK CONSTRAINT [user_app_role_rel_UID_fk]
GO
ALTER TABLE [dbo].[user_application_preferences]  WITH CHECK ADD  CONSTRAINT [FK_user_application_preferences_applicationid] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[applications] ([ApplicationID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_application_preferences] CHECK CONSTRAINT [FK_user_application_preferences_applicationid]
GO
ALTER TABLE [dbo].[user_application_preferences]  WITH CHECK ADD  CONSTRAINT [FK_user_application_preferences_userID] FOREIGN KEY([UserID])
REFERENCES [dbo].[users] ([UserID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_application_preferences] CHECK CONSTRAINT [FK_user_application_preferences_userID]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_LabID] FOREIGN KEY([LabID])
REFERENCES [dbo].[labs] ([LabID])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_LabID]
GO
/****** Object:  StoredProcedure [dbo].[p_AddCredential]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Aidan Flynn>
-- Create date: <11/11/2011>
-- Description:	<Add or Update a User Credential >
-- =============================================
CREATE PROCEDURE [dbo].[p_AddCredential](@UserID uniqueidentifier, @LabID int, @RoleID int, @ApplicationID int)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRANSACTION
		IF EXISTS (SELECT * FROM  user_app_lab_role_rel WHERE UserID=@UserID AND ApplicationID=@ApplicationID AND LabID=@LabID) 
			--Credential Exists, update required
			BEGIN
				UPDATE user_app_lab_role_rel SET RoleID=@RoleID WHERE UserID=@UserID AND ApplicationID=@ApplicationID AND LabID=@LabID
			END
		ELSE
			--Credential does not exist, creation required
			BEGIN
				INSERT INTO user_app_lab_role_rel (UserID, ApplicationID, RoleID,LabID) VALUES (@UserID, @ApplicationID, @RoleID, @LabID);
				
				IF NOT EXISTS (SELECT Preferences FROM user_application_preferences WHERE UserID=@UserID AND ApplicationID=@ApplicationID)
				--User does not have prefence profile for application, creation required
				BEGIN
					DECLARE @DefaultPreferences XML;
					SELECT @DefaultPreferences=DefaultPreferences FROM applications WHERE ApplicationID=@ApplicationID;
					INSERT INTO user_application_preferences (UserID, ApplicationID, Preferences) VALUES (@UserID, @ApplicationID, @DefaultPreferences);
				END
		END
		COMMIT TRANSACTION
	END TRY
    BEGIN CATCH
      		ROLLBACK TRANSACTION
      		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE() + 'Parameters:LabID='+CONVERT(varchar(10),@LabID)+',RoleID='+CONVERT(varchar(10),@RoleID)+',ApplicationID='+CONVERT(varchar(10),@ApplicationID)+',UserID='+CONVERT(varchar(36),@UserID),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

		    
			RAISERROR (@ErrorMessage,
					   @ErrorSeverity,
					   @ErrorState
					   );
		END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[p_AddUser]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_AddUser](@FirstName VarChar(50), @LastName VarChar(50), @Password VarChar(128), @Email VarChar(128), @RFID VarChar(30), @IsLabHead bit, @LabID tinyint, @Extension VarChar(15),
                       		@ApplicationName VarChar(255) = NULL, @RoleID int = NULL)
AS
DECLARE @ErrMsg varchar(100);
DECLARE @UserID uniqueidentifier;

IF @ApplicationName IS NULL AND @RoleID IS NULL
BEGIN;
				IF NOT EXISTS (SELECT UserID FROM users WHERE FirstName=@FirstName AND LastName=@LastName)
                    BEGIN
                        SET @UserID = NEWID();
                        INSERT INTO users  (UserID, FirstName, LastName, LabID, [Password], Email, RFID, IsLabHead, Extension)
                                    VALUES
                                            (@UserID, @FirstName, @LastName, @LabID, @Password, @Email, @RFID, @IsLabHead, @Extension);
                    END
				ELSE
					BEGIN
						Set @ErrMsg = 'AccountExists';
						RAISERROR(@ErrMsg,16,1,'Account Exists');
					END;
                
END;
ELSE
BEGIN
--CONVERT APPLICATION NAME TO APPLICATIONID
DECLARE @ApplicationID tinyint;
SELECT @ApplicationID = applications.ApplicationID FROM applications WHERE applications.ApplicationName=@ApplicationName;

IF @ApplicationID IS NOT NULL
	BEGIN;
		BEGIN TRY
			BEGIN TRANSACTION
				--INSERT NEW USER INTO USERS TABLE
				IF NOT EXISTS (SELECT UserID FROM users WHERE FirstName=@FirstName AND LastName=@LastName)
                    BEGIN
                        SET @UserID = NEWID();
                        INSERT INTO users  (UserID, FirstName, LastName, LabID, [Password], Email, RFID, IsLabHead, Extension)
                                    VALUES
                                            (@UserID, @FirstName, @LastName, @LabID, @Password, @Email, @RFID, @IsLabHead, @Extension);
                    END
				ELSE
				SELECT @UserID=UserID FROM users WHERE FirstName=@FirstName AND LastName=@LastName;
                
                --CREATE RELATIONSHIP BETWEEN USER AND APPLICATION
                IF NOT EXISTS(SELECT * FROM user_app_lab_role_rel WHERE UserID=@UserID AND ApplicationID=@ApplicationID AND LabID=@LabID)
					BEGIN
						DECLARE @DefaultPreferences XML;
						SELECT @DefaultPreferences=DefaultPreferences FROM applications WHERE ApplicationID=@ApplicationID;
						INSERT INTO user_app_lab_role_rel (UserID, ApplicationID, RoleID,LabID) VALUES (@UserID, @ApplicationID, @RoleID, @LabID);
						INSERT INTO user_application_preferences (UserID, ApplicationID, Preferences) VALUES (@UserID, @ApplicationID, @DefaultPreferences);
					END
				ELSE 
					BEGIN
						Set @ErrMsg = 'AccountExists';
						RAISERROR(@ErrMsg,16,1,'Account Exists');
					END;
			COMMIT TRANSACTION
		END TRY
    	BEGIN CATCH
      		ROLLBACK TRANSACTION
      		DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT @ErrorMessage = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState = ERROR_STATE();

		    
			RAISERROR (@ErrorMessage,
					   @ErrorSeverity,
					   @ErrorState
					   );
		END CATCH
  	END;
ELSE 
  	BEGIN;
      Set @ErrMsg = 'InvalidApplicationName:' + @ApplicationName;
      RAISERROR(@ErrMsg,16,1,'ApplicationID');
	END;
	
END;



GO
/****** Object:  StoredProcedure [dbo].[p_DeleteUser]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DeleteUser](@Username VarChar(255), @ApplicationName VarChar(255) = NULL)
AS

DECLARE @ApplicationID tinyint;
IF @ApplicationName IS NOT NULL 
SELECT @ApplicationID = applications.ApplicationID FROM applications WHERE applications.ApplicationName=@ApplicationName;

--CONVERT USERNAME TO USERID
DECLARE @UserID uniqueidentifier;
SELECT @UserID = users.UserID FROM users WHERE (users.FirstName+' '+users.LastName)=@UserName;
IF @UserID IS NULL
RAISERROR('Invalid UserName',16,1,'UserID');

--DELETE USER APPLICATION RELATIONSHIP
BEGIN TRY  	
	  	IF @ApplicationID IS NULL
        DELETE FROM users WHERE UserID=@UserID;
        ELSE
        DELETE FROM user_app_lab_role_rel WHERE UserID=@UserID AND ApplicationID=@ApplicationID;  	
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;
	RETURN @@ERROR;
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[p_GetSetting]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_GetSetting] @Name varchar(50)
AS
BEGIN	
IF (EXISTS (SELECT Value FROM settings WHERE SettingName=@Name)) SELECT Value FROM settings WHERE SettingName=@Name;
ELSE RAISERROR ('InvalidSettingName',16,1,'InvalidSettingName')
END

GO
/****** Object:  StoredProcedure [dbo].[p_SetSetting]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_SetSetting] @Name varchar(50), @Value varchar(200)
AS
BEGIN	
IF (EXISTS (SELECT Value FROM settings WHERE SettingName=@Name))
UPDATE settings SET Value=@Value WHERE SettingName=@Name;
ELSE
INSERT INTO settings (SettingName,Value) VALUES (@Name,@Value);	
END

GO
/****** Object:  StoredProcedure [dbo].[p_UnLinkUser]    Script Date: 21/03/2017 6:48:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_UnLinkUser](@Username VarChar(255), @ApplicationName VarChar(255), @LabID tinyint)
AS

--CONVERT APPLICATION NAME TO APPLICATIONID
DECLARE @ApplicationID tinyint;
SELECT @ApplicationID = applications.ApplicationID FROM applications WHERE applications.ApplicationName=@ApplicationName;
IF @ApplicationID IS NULL
RAISERROR('Invalid Application Name',16,1,'ApplicationID');

--CONVERT USERNAME TO USERID
DECLARE @UserID uniqueidentifier;
SELECT @UserID = users.UserID FROM users WHERE (users.FirstName+' '+users.LastName)=@UserName;
IF @UserID IS NULL
RAISERROR('Invalid UserName',16,1,'UserID');

--DELETE USER APPLICATION RELATIONSHIP
DELETE FROM user_app_lab_role_rel WHERE UserID=@UserID AND LabID=@LabID AND ApplicationID=@ApplicationID;

GO
USE [master]
GO
ALTER DATABASE [Users] SET  READ_WRITE 
GO
