USE Users;

DECLARE @APPLICATIONNAME AS VARCHAR(50);
DECLARE @APPLICATIONDESC AS VARCHAR(150)
DECLARE @MASTERPASS AS VARCHAR(50);
DECLARE @MASTERPASSHASH AS VARCHAR(40); 
DECLARE @APPID AS INT;
DECLARE @ADMINROLEID AS INT;
DECLARE @USERROLEID AS INT;
DECLARE @ADMINID AS uniqueidentifier;
DECLARE @InstituteID as INT;
DECLARE @LabID as INT;
DECLARE @InstituteName AS VARCHAR(100);
DECLARE @LabName AS VARCHAR(300);
DECLARE @CostCentreCode AS VARCHAR(15);
DECLARE @CostCentreDescription AS VARCHAR(300);
DECLARE @FirstName AS VARCHAR(50);
DECLARE @LastName AS VARCHAR(50);
DECLARE @Email AS VARCHAR(100);
DECLARE @UserPassword AS VARCHAR(100); 
DECLARE @UserPasswordHash AS VARCHAR(40); 
DECLARE @Extension AS VARCHAR(15);

/* APPLICATION DETAILS */
SET @APPLICATIONNAME = ''; /* REQUIRED */
SET @APPLICATIONDESC = ''; /* REQUIRED */
SET @MASTERPASS = 'masterpass'; /* REQUIRED */
SELECT @MASTERPASSHASH=convert(varchar(40), HASHBYTES('SHA1', @MASTERPASS), 2);

/* INSTITUTE DETAILS */
SET @InstituteName='Inst' /* REQUIRED */


SET @LabName='' /* REQUIRED */
SET @CostCentreCode ='' /* REQUIRED */
SET @CostCentreDescription = '' /* REQUIRED */

/* ADMINISTRATOR DETAILS */
SET @FirstName = ''; /* REQUIRED */
SET @LastName = ''; /* REQUIRED */
SET @Email = ''; /* REQUIRED */
SET @Extension = ''; /* REQUIRED */
SET @UserPassword = 'password'; /* REQUIRED */
SELECT @UserPasswordHash=convert(varchar(40), HASHBYTES('SHA1', @UserPassword), 2);


BEGIN TRY 

BEGIN TRANSACTION


/* CREATE APPLICATION  */
INSERT INTO applications (ApplicationName, DescriptiveName, MasterPassword) VALUES (@APPLICATIONNAME, @APPLICATIONDESC, @MASTERPASSHASH)
 
/* CREATE ROLES */

INSERT INTO roles (RoleName) VALUES ('Administrator');
INSERT INTO roles (RoleName) VALUES ('User');

/* LINK ROLES AND APPLICATION */

SELECT @APPID = applications.ApplicationID FROM applications WHERE ApplicationName=@APPLICATIONNAME;
SELECT @ADMINROLEID  = roles.RoleID FROM roles WHERE RoleName='Administrator';
SELECT @USERROLEID  = roles.RoleID FROM roles WHERE RoleName='User';

INSERT INTO app_role_relations (ApplicationID, RoleID, Description) VALUES (@APPID, @ADMINROLEID, 'Administrator')
INSERT INTO app_role_relations (ApplicationID, RoleID, Description) VALUES (@APPID, @USERROLEID, 'User')


/* INSTITUTE */

INSERT INTO Institutes (Name, DeliveryAddress, BillingAddress, IsCurrentSite) VALUES (@InstituteName, '', '', 1);
SELECT @InstituteID = InstituteID FROM Institutes WHERE Institutes.Name=@InstituteName;

/* LAB */

INSERT INTO labs (LabName, InstituteID) VALUES (@LabName, @InstituteID);
SELECT @LabID=LabID FROM labs WHERE LabName = @LabName;
/* USER */

INSERT INTO Users.dbo.users (UserID, FirstName, LastName, Email, Password, Extension, LabID, IsLabHead) VALUES (NEWID(), @FirstName, @LastName, @Email, @UserPasswordHash, @Extension, @LabID, 1)
SELECT @ADMINID=UserID FROM Users WHERE LastName = @LastName;

/* CREDENTIALS */
INSERT INTO user_app_lab_role_rel (UserID, LabID, ApplicationID,RoleID) VALUES (@ADMINID, @LABID, @APPID, @ADMINROLEID)

/*Preferences*/
INSERT INTO user_application_preferences (UserID, ApplicationID, Preferences) VALUES (@ADMINID, @APPID, NULL)

/*CostCentre*/
INSERT INTO cost_centres (CostCentreID, LabID, Description, Retired) VALUES (@CostCentreCode, @LabID, @CostCentreDescription, 0) 

COMMIT TRANSACTION

END TRY

BEGIN CATCH

ROLLBACK TRANSACTION
SELECT  ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,ERROR_LINE () AS ErrorLine,ERROR_PROCEDURE() AS ErrorProcedure, ERROR_MESSAGE() AS ErrorMessage;  
END CATCH