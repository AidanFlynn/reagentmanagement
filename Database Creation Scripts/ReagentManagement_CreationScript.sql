USE [master]
GO
/****** Object:  Database [ReagentManagement]    Script Date: 21/03/2017 6:38:44 PM ******/
CREATE DATABASE [ReagentManagement]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ReagentManagement', FILENAME = N'C:\Database\Signout\ReagentManagement.mdf' , SIZE = 5312KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ReagentManagement_log', FILENAME = N'C:\Database\Signout\ReagentManagement_log.LDF' , SIZE = 3520KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ReagentManagement] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReagentManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ReagentManagement] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ReagentManagement] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ReagentManagement] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ReagentManagement] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ReagentManagement] SET ARITHABORT OFF 
GO
ALTER DATABASE [ReagentManagement] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ReagentManagement] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ReagentManagement] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ReagentManagement] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ReagentManagement] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ReagentManagement] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ReagentManagement] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ReagentManagement] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ReagentManagement] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ReagentManagement] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ReagentManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ReagentManagement] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ReagentManagement] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ReagentManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ReagentManagement] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ReagentManagement] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ReagentManagement] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ReagentManagement] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ReagentManagement] SET  MULTI_USER 
GO
ALTER DATABASE [ReagentManagement] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ReagentManagement] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ReagentManagement] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ReagentManagement] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [ReagentManagement]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CalculatePurchaseCostPerunit]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CalculatePurchaseCostPerunit] 
(
	-- Add the parameters for the function here
	@PurchaseCost decimal(10,2),
	@ReagentTypeID int
)
RETURNS decimal(10,2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @CPU decimal (10,2);

	-- Add the T-SQL statements to compute the return value here
	SELECT @CPU = @PurchaseCost / (SELECT UnitsPerItem FROM Reagent_types WHERE ReagentTypeID=@ReagentTypeID)

	-- Return the result of the function
	RETURN @CPU;

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_DaysUntilDepletion]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_DaysUntilDepletion]
(
	-- Add the parameters for the function here
	@ReagentTypeID int,
	@Date date	
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Days decimal(8,2);
	DECLARE @StockLevel decimal(8,2);
	DECLARE @WeightedAvererageUse decimal(8,2);
	
	-- Add the T-SQL statements to compute the return value here
	SELECT @StockLevel = dbo.fn_GetStockLevel(@ReagentTypeID, @Date)
	SELECT @WeightedAvererageUse = dbo.fn_GetWeightedAvererageUse(@ReagentTypeID,@Date, 7, 0.5, 30,0.3, 90, 0.2)

	IF @StockLevel = 0 OR @WeightedAvererageUse = 0 OR @StockLevel IS NULL OR @WeightedAvererageUse IS NULL
	SET @Days = 0
	ELSE
	SET @Days = CAST(ROUND(@StockLevel/@WeightedAvererageUse,0) AS int);
	
	RETURN @Days

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetActiveUsers]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create FUNCTION [dbo].[fn_GetActiveUsers]
(	
	-- Add the parameters for the function here
	@StartDate datetime,
	@EndDate datetime,
	@BillingStatus bit
)
RETURNS @ReturnTable TABLE 
(
	UserID uniqueidentifier
)
AS 
BEGIN
	IF @BillingStatus IS NOT NULL
	BEGIN
	INSERT INTO @ReturnTable SELECT DISTINCT UserID FROM ReagentSignout WHERE Date > @StartDate AND Date < @EndDate AND Billed=@BillingStatus
	END
	ELSE
	BEGIN
	INSERT INTO @ReturnTable SELECT DISTINCT UserID FROM ReagentSignout WHERE Date > @StartDate AND Date < @EndDate
	END
RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageDeliveryTime]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetAverageDeliveryTime]
(
	-- Add the parameters for the function here
	@ReagentTypeID int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @AverageDeliveryTime int

	SELECT @AverageDeliveryTime = AVG(DeliveryDays) FROM (SELECT TOP 10 DATEDIFF(DAY, OrderDate, ReceiveDate) AS DeliveryDays FROM reagent_inventory WHERE ReagentTypeID=@ReagentTypeID ORDER BY ReceiveDate DESC) A
	-- Return the result of the function
	RETURN @AverageDeliveryTime;

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageUseOverPeriod]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetAverageUseOverPeriod]
(
	-- Add the parameters for the function here
	@Period int,
	@EndDate date,
	@ReagentTypeID int
)
RETURNS decimal(8,2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @AvererageUse decimal(8,2);
	
	SELECT 	@AvererageUse = IsNull(SUM(IsNull(Quantity,0)),0)/@Period 

		FROM  reagent_inventory 
			LEFT JOIN ReagentSignout 
			ON reagent_inventory.ItemID = ReagentSignout.ItemID
			WHERE   CONVERT(Date,ReagentSignOut.Date,1)<@EndDate
					AND CONVERT(Date,ReagentSignOut.Date,1)> DATEADD(DAY,0-@Period, @EndDate)
					AND ReagentTypeID=@ReagentTypeID
	-- Return the result of the function
	RETURN @AvererageUse;

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCostCentreDetails]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetCostCentreDetails]
(
	@StartDate DateTime, 
	@EndDate DateTime,
	@BillingStatus  bit,
	@DebtorCostCentre varchar(50), 
	@CreditorCostCentre varchar(50)
)
RETURNS 
@ReturnTable TABLE 
(
	EventID int,
	ItemName varchar(100),
	UserID uniqueidentifier,
	ServiceDate datetime,
	AppliedQuantity decimal(10,2),
	AppliedCost Decimal(10,2)	
)
AS
BEGIN
	-- 0 0 0
	IF @DebtorCostCentre IS NULL AND @CreditorCostCentre IS NULL AND @BillingStatus IS NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	--AND CostCentreID=@DebtorCostCentre
	--AND CreditorCostCentre= @CreditorCostCentre
	--AND Billed=@BillingStatus;
	END
	
	
	--0	0	1

	IF @DebtorCostCentre IS NULL AND @CreditorCostCentre IS NULL AND @BillingStatus IS NOT NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	--AND CostCentreID=@DebtorCostCentre
	--AND CreditorCostCentre= @CreditorCostCentre
	AND Billed=@BillingStatus;
	END

	--0	1	0

	IF @DebtorCostCentre IS NULL AND @CreditorCostCentre IS NOT NULL AND @BillingStatus IS NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date],Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	--AND CostCentreID=@DebtorCostCentre
	AND CreditorCostCentre= @CreditorCostCentre
	--AND Billed=@BillingStatus;
	END
	

	--0	1	1

	IF @DebtorCostCentre IS NULL AND @CreditorCostCentre IS NOT NULL AND @BillingStatus IS NOT NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	--AND CostCentreID=@DebtorCostCentre
	AND CreditorCostCentre= @CreditorCostCentre
	AND Billed=@BillingStatus;
	END
	
	
	
	--1	0	0

	IF @DebtorCostCentre IS NOT NULL AND @CreditorCostCentre IS NULL AND @BillingStatus IS NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	AND CostCentreID=@DebtorCostCentre
	--AND CreditorCostCentre= @CreditorCostCentre
	--AND Billed=@BillingStatus;
	END
	
	--1	0	1
	IF @DebtorCostCentre IS NOT NULL AND @CreditorCostCentre IS NULL AND @BillingStatus IS NOT NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	AND CostCentreID=@DebtorCostCentre
	--AND CreditorCostCentre= @CreditorCostCentre
	AND Billed=@BillingStatus;
	END
	
	--1	1	0
	IF @DebtorCostCentre IS NOT NULL AND @CreditorCostCentre IS NOT NULL AND @BillingStatus IS NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	AND CostCentreID=@DebtorCostCentre
	AND CreditorCostCentre= @CreditorCostCentre
	--AND Billed=@BillingStatus;
	END
	
	--1	1	1

	IF @DebtorCostCentre IS NOT NULL AND @CreditorCostCentre IS NOT NULL AND @BillingStatus IS NOT NULL 
	BEGIN
	INSERT INTO @ReturnTable 
	SELECT EventID, Name, UserID, [Date], Quantity, CONVERT(decimal(10,2),CONVERT(decimal(10,2),PurchaseCostPerUnit)*CONVERT(decimal(10,2),Quantity)) AS Cost
	FROM ReagentSignout 
	INNER JOIN reagent_inventory 
	ON reagent_inventory.ItemID=ReagentSignout.ItemID 
	INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID
	WHERE Date > @StartDate AND Date < @EndDate 
	AND CostCentreID=@DebtorCostCentre
	AND CreditorCostCentre= @CreditorCostCentre
	AND Billed=@BillingStatus;
	END

	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCostCentreSummary]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery17.sql|0|0|C:\Users\Aidan Flynn\AppData\Local\Temp\~vsF88C.sql
CREATE FUNCTION [dbo].[fn_GetCostCentreSummary]
(
	@StartDate datetime,
	@EndDate datetime,
	@BillingStatus bit
)
RETURNS @ReturnTable TABLE 
(
	DebtorCostCentre  varchar(50),
	CreditorCostCentre varchar(50),
	Cost Decimal(10,2)
)
AS
BEGIN
	IF @BillingStatus IS NOT NULL
INSERT INTO @ReturnTable SELECT ReagentSignout.CostCentreID, CreditorCostCentre, SUM(reagent_inventory.PurchaseCostPerUnit*ReagentSignout.Quantity) AS BillableAmount
FROM reagent_inventory INNER JOIN ReagentSignout ON reagent_inventory.ItemID = ReagentSignout.ItemID
WHERE ReagentSignout.Date > @StartDate AND ReagentSignout.Date < @EndDate
AND ReagentSignout.Billed=@BillingStatus
GROUP BY CostCentreID,CreditorCostCentre
ELSE
INSERT INTO @ReturnTable SELECT ReagentSignout.CostCentreID, CreditorCostCentre, SUM(reagent_inventory.PurchaseCostPerUnit*ReagentSignout.Quantity) AS BillableAmount
FROM reagent_inventory INNER JOIN ReagentSignout ON reagent_inventory.ItemID = ReagentSignout.ItemID
WHERE ReagentSignout.Date > @StartDate AND ReagentSignout.Date < @EndDate
GROUP BY CostCentreID,CreditorCostCentre
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetStockLevel]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_GetStockLevel]
(
	-- Add the parameters for the function here
	@ReagentTypeID int,
	@Date date
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @StockCount decimal(8,2)

	SELECT @StockCount = SUM(StockUnits)	
	FROM 
		(SELECT SUM(UnitsRemaining) AS StockUnits
				
		FROM         
			reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			WHERE CONVERT(Date,ReceiveDate,1)<=@Date 
			AND ((CONVERT(Date,FinishDate,1)>=@Date AND CONVERT(Date,FinishDate,1)>=@Date) OR FinishDate IS NULL) 
			AND reagent_types.ReagentTypeID=@ReagentTypeID
			GROUP BY Reagent_Types.ReagentTypeID
		UNION
		SELECT 	IsNull(SUM(IsNull(Quantity,0)),0) AS StockUnits			
		FROM  reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			LEFT JOIN ReagentSignout 
			ON reagent_inventory.ItemID = ReagentSignout.ItemID
			WHERE   CONVERT(Date,ReagentSignOut.Date,1)>@Date
					AND CONVERT(Date,ReceiveDate,1)<=@Date 
					AND ((CONVERT(Date,FinishDate,1)>=@Date AND CONVERT(Date,FinishDate,1)>=@Date) OR FinishDate IS NULL) 
					AND reagent_types.ReagentTypeID=@ReagentTypeID) 
			CommonTable
	-- Return the result of the function
	RETURN @StockCount

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetUserCostDetails]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetUserCostDetails]
(	
	-- Add the parameters for the function here
	@UserID uniqueidentifier,
	@StartDate DateTime,
	@EndDate DateTime,
	@BillingStatus bit
)
RETURNS @ReturnTable TABLE 
(
	EventID int,  
	ItemID int, 
	Name varchar(100),
	[Date] datetime, 
	DebtorCostCentre varchar(50), 
	CreditorCostCentre varchar(50),  
	Quantity decimal(6,2), 
	Cost decimal(10,2),
	Billed bit
)
AS
BEGIN
	-- Add the SELECT statement with parameter references here
	IF(@BillingStatus IS NULL)
	BEGIN
		INSERT INTO @ReturnTable 
				SELECT  ReagentSignout.EventID,  
				ReagentSignout.ItemID, 
				reagent_types.Name,
				ReagentSignout.[Date], 
				ReagentSignout.CostCentreID, 
				ReagentSignout.CreditorCostCentre,  
				ReagentSignout.Quantity, 
				CONVERT(decimal(10,2), ReagentSignout.Quantity)*CONVERT(decimal(10,2), reagent_inventory.PurchaseCostPerUnit) AS Cost,
				ReagentSignout.Billed 
		FROM reagent_inventory 
		INNER JOIN reagent_types 
		ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
		INNER JOIN ReagentSignout 
		ON reagent_inventory.ItemID = ReagentSignout.ItemID 
		WHERE ReagentSignout.UserID=@UserID 
		AND ReagentSignout.[Date] > @StartDate 
		AND ReagentSignout.[Date] < @EndDate 
	END
	ELSE
	BEGIN
		INSERT INTO @ReturnTable
			SELECT  ReagentSignout.EventID,  
				ReagentSignout.ItemID, 
				reagent_types.Name,
				ReagentSignout.[Date], 
				ReagentSignout.CostCentreID, 
				ReagentSignout.CreditorCostCentre,  
				ReagentSignout.Quantity, 
				CONVERT(decimal(10,2), ReagentSignout.Quantity)*CONVERT(decimal(10,2), reagent_inventory.PurchaseCostPerUnit) AS Cost,
				ReagentSignout.Billed 
		FROM reagent_inventory 
		INNER JOIN reagent_types 
		ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
		INNER JOIN ReagentSignout 
		ON reagent_inventory.ItemID = ReagentSignout.ItemID 
		WHERE ReagentSignout.UserID=@UserID 
		AND ReagentSignout.[Date] > @StartDate 
		AND ReagentSignout.[Date] < @EndDate
		AND Billed = @BillingStatus
	END 
	RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetWeightedAvererageUse]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetWeightedAvererageUse]
(
	@ReagentTypeID int,
	@EndDate date,
	@PeriodOne int,
	@WeightOne decimal(4,2),
	@PeriodTwo int,
	@WeightTwo decimal(4,2),
	@PeriodThree int,
	@WeightThree decimal(4,2)
)
RETURNS decimal(8,2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @WeightedAverage decimal(8,2);
	
		-- Add the T-SQL statements to compute the return value here
	SELECT @WeightedAverage =
(@WeightOne * dbo.fn_GetAverageUseOverPeriod(@PeriodOne,@EndDate,@ReagentTypeID)) 
+ (@WeightTwo * dbo.fn_GetAverageUseOverPeriod(@PeriodTwo,@EndDate,@ReagentTypeID)) 
+ (@WeightThree * dbo.fn_GetAverageUseOverPeriod(@PeriodThree,@EndDate,@ReagentTypeID)) 

	-- Return the result of the function
	RETURN @WeightedAverage; 

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_StockLevelsOverTimePeriod]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_StockLevelsOverTimePeriod] 
(
	-- Add the parameters for the function here
	@StartDate date,
	@LookBackDays int,
	@ReagentTypeID int
)
RETURNS 
@StockLevel TABLE 
(
	[Date] date,
	StockLevel int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
DECLARE @Date Date;

DECLARE @COUNTBACK int = 0 - @LookBackDays;

WHILE @COUNTBACK <0
BEGIN
SET @Date = DATEADD(DAY,@COUNTBACK,@StartDate);
SET @COUNTBACK = @COUNTBACK + 1;

 
INSERT INTO @StockLevel SELECT @Date, SUM(StockUnits)	
	FROM 
		(SELECT SUM(UnitsRemaining) AS StockUnits
				
		FROM         
			reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			WHERE CONVERT(Date,ReceiveDate,1)<=@Date 
			AND ((CONVERT(Date,FinishDate,1)>=@Date AND CONVERT(Date,FinishDate,1)>=@Date) OR FinishDate IS NULL) 
			AND reagent_types.ReagentTypeID=@ReagentTypeID
			GROUP BY Reagent_Types.ReagentTypeID
		UNION
		SELECT 	IsNull(SUM(IsNull(Quantity,0)),0) AS StockUnits			
		FROM  reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			LEFT JOIN ReagentSignout 
			ON reagent_inventory.ItemID = ReagentSignout.ItemID
			WHERE   CONVERT(Date,ReagentSignOut.Date,1)>@Date
					AND CONVERT(Date,ReceiveDate,1)<=@Date 
					AND ((CONVERT(Date,FinishDate,1)>=@Date AND CONVERT(Date,FinishDate,1)>=@Date) OR FinishDate IS NULL) 
					AND reagent_types.ReagentTypeID=@ReagentTypeID) 
			CommonTable
END
	RETURN 
END

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Icon] [varbinary](8000) NULL,
	[ParentCategoryID] [int] NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Category] UNIQUE NONCLUSTERED 
(
	[Description] ASC,
	[ParentCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reagent_inventory]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reagent_inventory](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[ReagentTypeID] [smallint] NOT NULL,
	[LotNumber] [varchar](50) NULL,
	[UnitsRemaining] [decimal](10, 3) NULL,
	[Status] [tinyint] NOT NULL,
	[OrderDate] [datetime] NULL,
	[ReceiveDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[Comment] [varchar](300) NULL,
	[ReceivedBy] [varchar](50) NULL,
	[ExpiryDate] [datetime] NULL,
	[Location] [varchar](50) NULL,
	[PurchaseCost] [money] NOT NULL,
	[PurchaseCostPerUnit]  AS ([dbo].[fn_CalculatePurchaseCostPerUnit]([PurchaseCost],[ReagentTypeID])),
	[OrderedBy] [varchar](50) NULL,
	[PurchaseCostCentre] [varchar](20) NULL,
 CONSTRAINT [reagent_inventory_pk] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reagent_types]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reagent_types](
	[ReagentTypeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](300) NOT NULL,
	[Manufacturer] [varchar](200) NULL,
	[Supplier] [varchar](200) NULL,
	[PartNumber] [varchar](50) NOT NULL,
	[LastOrdered] [datetime] NULL,
	[Status] [tinyint] NULL,
	[OrderThreshold] [smallint] NOT NULL,
	[UnitsPerItem] [int] NOT NULL,
	[Cost] [decimal](9, 2) NULL,
	[CostPerUnit]  AS ([Cost]/[UnitsPerItem]),
	[UnitOfSale] [varchar](50) NULL,
	[InfiniteResource] [bit] NOT NULL,
	[MarkUp] [decimal](5, 2) NULL,
	[DefaultPurchaseCostCentre] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReagentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReagentCategoryRel]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReagentCategoryRel](
	[ReagentTypeID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
 CONSTRAINT [PK_ReagentCategoryRel] PRIMARY KEY CLUSTERED 
(
	[ReagentTypeID] ASC,
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReagentSetCategoryRel]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReagentSetCategoryRel](
	[ReagentSetID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReagentSetReagentTypeRel]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReagentSetReagentTypeRel](
	[ReagentSetID] [int] NOT NULL,
	[ReagentTypeID] [smallint] NOT NULL,
	[Units] [decimal](8, 3) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReagentSets]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReagentSets](
	[SetID] [int] IDENTITY(1,1) NOT NULL,
	[SetName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ReagentSets] PRIMARY KEY CLUSTERED 
(
	[SetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReagentSignout]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReagentSignout](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[Quantity] [decimal](10, 4) NOT NULL,
	[Date] [datetime] NOT NULL,
	[CostCentreID] [varchar](50) NOT NULL,
	[Billed] [bit] NOT NULL,
	[CreditorCostCentre] [varchar](20) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[settings]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[settings](
	[SettingName] [varchar](100) NOT NULL,
	[Value] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[SettingName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[reagent_types] ADD  DEFAULT ((0)) FOR [InfiniteResource]
GO
ALTER TABLE [dbo].[reagent_types] ADD  CONSTRAINT [DF_reagent_types_MarkUp]  DEFAULT ((0)) FOR [MarkUp]
GO
ALTER TABLE [dbo].[ReagentSignout] ADD  CONSTRAINT [DF_ReagentSignout_Billed]  DEFAULT ((0)) FOR [Billed]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [FK_Catagories_SFK] FOREIGN KEY([ParentCategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [FK_Catagories_SFK]
GO
ALTER TABLE [dbo].[reagent_inventory]  WITH CHECK ADD  CONSTRAINT [reagent_inventory_fk] FOREIGN KEY([ReagentTypeID])
REFERENCES [dbo].[reagent_types] ([ReagentTypeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[reagent_inventory] CHECK CONSTRAINT [reagent_inventory_fk]
GO
ALTER TABLE [dbo].[ReagentSetCategoryRel]  WITH CHECK ADD  CONSTRAINT [FK_ReagentSetCategoryRel_CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReagentSetCategoryRel] CHECK CONSTRAINT [FK_ReagentSetCategoryRel_CategoryID]
GO
ALTER TABLE [dbo].[ReagentSetCategoryRel]  WITH CHECK ADD  CONSTRAINT [FK_ReagentSetCategoryRel_ReagentSetID] FOREIGN KEY([ReagentSetID])
REFERENCES [dbo].[ReagentSets] ([SetID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReagentSetCategoryRel] CHECK CONSTRAINT [FK_ReagentSetCategoryRel_ReagentSetID]
GO
ALTER TABLE [dbo].[ReagentSetReagentTypeRel]  WITH CHECK ADD  CONSTRAINT [FK_ReagentSetReagentTypeRel_ReagentSetID] FOREIGN KEY([ReagentSetID])
REFERENCES [dbo].[ReagentSets] ([SetID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReagentSetReagentTypeRel] CHECK CONSTRAINT [FK_ReagentSetReagentTypeRel_ReagentSetID]
GO
ALTER TABLE [dbo].[ReagentSetReagentTypeRel]  WITH CHECK ADD  CONSTRAINT [FK_ReagentSetReagentTypeRel_ReagentTypeID] FOREIGN KEY([ReagentTypeID])
REFERENCES [dbo].[reagent_types] ([ReagentTypeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReagentSetReagentTypeRel] CHECK CONSTRAINT [FK_ReagentSetReagentTypeRel_ReagentTypeID]
GO
ALTER TABLE [dbo].[ReagentSignout]  WITH CHECK ADD  CONSTRAINT [ReagentSignout_ItemID_FK] FOREIGN KEY([ItemID])
REFERENCES [dbo].[reagent_inventory] ([ItemID])
GO
ALTER TABLE [dbo].[ReagentSignout] CHECK CONSTRAINT [ReagentSignout_ItemID_FK]
GO
/****** Object:  StoredProcedure [dbo].[GetCostCentreDetails]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCostCentreDetails]
	@DebtorCostCentre varchar(20), 
	@CreditorCostCentre varchar(20),
	@StartDate varchar(10), 
	@EndDate varchar(10),
	@BillingStatus  bit
AS
BEGIN
	SET NOCOUNT ON;
DECLARE @Query varchar(1000);
SET @Query = 'SELECT EventID FROM ReagentSignout WHERE Date > ''' + @StartDate + ''' AND Date < ''' +@EndDate + '''';

IF @DebtorCostCentre IS NOT NULL
SET @Query = @Query + ' AND CostCentreID=''' + @DebtorCostCentre +'''';

IF @CreditorCostCentre IS NOT NULL
SET @Query = @Query + ' AND CreditorCostCentre=''' +  @CreditorCostCentre + ''''; 

IF @BillingStatus IS NOT NULL 
SET @Query = @Query + ' AND Billed=' + Convert(char(1),@BillingStatus);

EXEC (@Query);

END
GO
/****** Object:  StoredProcedure [dbo].[GetCostCentreSummary]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCostCentreSummary]
	
	@StartDate datetime,
	@EndDate datetime,
	@BillingStatus bit
AS
BEGIN
	SET NOCOUNT ON;

IF @BillingStatus IS NOT NULL
SELECT ReagentSignout.CostCentreID, CreditorCostCentre, CONVERT(DECIMAL(9,2),SUM(reagent_inventory.PurchaseCostPerUnit*ReagentSignout.Quantity)) AS BillableAmount
FROM reagent_inventory INNER JOIN ReagentSignout ON reagent_inventory.ItemID = ReagentSignout.ItemID
WHERE ReagentSignout.Date > @StartDate AND ReagentSignout.Date < @EndDate
AND ReagentSignout.Billed=@BillingStatus
GROUP BY CostCentreID,CreditorCostCentre
ELSE
SELECT ReagentSignout.CostCentreID, CreditorCostCentre, CONVERT(DECIMAL(9,2),SUM(reagent_inventory.PurchaseCostPerUnit*ReagentSignout.Quantity)) AS BillableAmount
FROM reagent_inventory INNER JOIN ReagentSignout ON reagent_inventory.ItemID = ReagentSignout.ItemID
WHERE ReagentSignout.Date > @StartDate AND ReagentSignout.Date < @EndDate
GROUP BY CostCentreID,CreditorCostCentre
END
GO
/****** Object:  StoredProcedure [dbo].[GetReagentDetails]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetReagentDetails] (	
	@StartDate DateTime,
	@EndDate DateTime,
	@CostCentre varchar(20),
	@Mode varchar (10)
						)
AS
BEGIN
IF @Mode = 'Order'
BEGIN
---Items On Order
SELECT     [Name] as ReagentType, SUM(UnitsRemaining) AS OrderUnits, (AVG(PurchaseCostPerUnit)*SUM(UnitsRemaining)) as PurchaseValue
FROM         reagent_inventory INNER JOIN
                      reagent_types ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID                      
WHERE (OrderDate<=@EndDate AND (ReceiveDate>=@EndDate OR ReceiveDate IS NULL)) AND PurchaseCostCentre=@CostCentre	
GROUP BY Reagent_Types.ReagentTypeID, [Name]
END


IF @Mode = 'Stock'
BEGIN
--Stock Items Value
	SELECT ReagentType, 
			SUM(StockUnits) AS StockUnits, 
			SUM(PurchaseValue) AS PurchaseValue
	FROM 
		(SELECT [Name] as ReagentType, 
				SUM(UnitsRemaining) AS StockUnits, 
				AVG(PurchaseCostPerUnit)*SUM(UnitsRemaining) as PurchaseValue
		FROM         
			reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			WHERE ReceiveDate<=@EndDate 
			AND ((FinishDate>=@StartDate AND FinishDate>=@EndDate) OR FinishDate IS NULL) 
			AND PurchaseCostCentre=@CostCentre
			GROUP BY Reagent_Types.ReagentTypeID, 
					 [Name]
		UNION
		SELECT [Name] as ReagentType, 
				IsNull(SUM(Quantity),0) AS StockUnits, 
				(AVG(PurchaseCostPerUnit)*IsNull(SUM(Quantity),0)) as PurchaseValue
		FROM  reagent_inventory 
			INNER JOIN reagent_types 
			ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
			LEFT JOIN ReagentSignout 
			ON reagent_inventory.ItemID = ReagentSignout.ItemID
			WHERE   ReceiveDate<=@EndDate 
					AND ((FinishDate>=@StartDate AND FinishDate<=@EndDate) OR FinishDate IS NULL) 
					AND (ReagentSignOut.Date>=@StartDate AND ReagentSignOut.Date<=@EndDate) 
					AND PurchaseCostCentre=@CostCentre
			GROUP BY Reagent_Types.ReagentTypeID, [Name]) 
			CommonTable
	GROUP BY ReagentType
END

IF @Mode = 'Loss'
BEGIN
SELECT       [Name] as ReagentType, SUM(UnitsRemaining) AS StockUnits, (AVG(PurchaseCostPerUnit)*(SUM(UnitsRemaining))) as PurchaseValue
FROM         reagent_inventory INNER JOIN
             reagent_types ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
WHERE PurchaseCostCentre=@CostCentre AND ((FinishDate>=@StartDate AND FinishDate<=@EndDate AND FinishDate IS NOT NULL AND UnitsRemaining>0) OR Reagent_Inventory.Status=4) --Faulty
GROUP BY Reagent_Types.ReagentTypeID, [Name]
END
END

GO
/****** Object:  StoredProcedure [dbo].[GetReagentSummary]    Script Date: 21/03/2017 6:38:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetReagentSummary] (	
	@StartDate DateTime,
	@EndDate DateTime)
AS
BEGIN
--Total Value On Order
SELECT 'Total Stock on Order' as Category , PurchaseCostCentre AS PCC, IsNull(SUM(PurchaseValue),0) AS PurchaseValue FROM 
(
SELECT       PurchaseCostCentre, SUM(UnitsRemaining) AS OrderUnits, (PurchaseCostPerUnit*UnitsRemaining) as PurchaseValue
FROM         reagent_inventory 
WHERE (OrderDate<=@EndDate AND (ReceiveDate>=@EndDate OR ReceiveDate IS NULL))	
GROUP BY PurchaseCostCentre, ReagentTypeID, UnitsRemaining, PurchaseCostPerUnit
) Orders GROUP BY PurchaseCostCentre 

UNION
--Total Stock Value
SELECT 'Total Reagent in Stock' as Category, PurchaseCostCentre AS PCC, IsNull(SUM(PurchaseValue),0) AS PurchaseValue FROM 
(
SELECT PurchaseCostCentre, 
			SUM(UnitsRemaining) AS StockUnits, 
			AVG(PurchaseCostPerUnit)*SUM(UnitsRemaining) as PurchaseValue
	FROM         
		reagent_inventory 
		INNER JOIN reagent_types 
		ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
		WHERE ReceiveDate<=@EndDate 
		AND ((FinishDate>=@StartDate AND FinishDate>=@EndDate) OR FinishDate IS NULL) 
		GROUP BY PurchaseCostCentre, 
				 Reagent_Types.ReagentTypeID				 
	UNION
	SELECT PurchaseCostCentre, 
			IsNull(SUM(Quantity),0) AS StockUnits, 
			(AVG(PurchaseCostPerUnit)*IsNull(SUM(Quantity),0)) as PurchaseValue
	FROM  reagent_inventory 
		INNER JOIN reagent_types 
		ON reagent_inventory.ReagentTypeID = reagent_types.ReagentTypeID 
		LEFT JOIN ReagentSignout 
		ON reagent_inventory.ItemID = ReagentSignout.ItemID
	    WHERE   ReceiveDate<=@EndDate 
				AND ((FinishDate>=@StartDate AND FinishDate<=@EndDate) OR FinishDate IS NULL) 
				AND (ReagentSignOut.Date>=@StartDate AND ReagentSignOut.Date<=@EndDate) 
				
		GROUP BY PurchaseCostCentre, 
				Reagent_Types.ReagentTypeID
) Stock  GROUP BY PurchaseCostCentre 
Union
--UnSigned Out Losses
SELECT 'Total UnTracked Loss' as Category,PurchaseCostCentre AS PCC, IsNull(SUM(PurchaseValue),0) AS PurchaseValue FROM 
(
SELECT       PurchaseCostCentre, (PurchaseCostPerUnit*(SUM(UnitsRemaining))) as PurchaseValue
FROM         reagent_inventory 
WHERE (FinishDate>=@StartDate AND FinishDate<=@EndDate AND FinishDate IS NOT NULL AND UnitsRemaining>0) OR Status=4 --Faulty
GROUP BY PurchaseCostCentre, ReagentTypeID, PurchaseCostPerUnit 
) Loss  GROUP BY PurchaseCostCentre 
END

CREATE TRIGGER returnStock
    ON ReagentSignout
    FOR DELETE
AS
	DECLARE @SignoutTotals TABLE
	(
		ItemID int, 
		SignOutTotal decimal(5,3)
	) 

	INSERT INTO @SignoutTotals SELECT itemID, SUM(Quantity) FROM deleted GROUP BY ItemID
	 
	UPDATE reagent_inventory SET UnitsRemaining = UnitsRemaining+SignOutTotal
    FROM reagent_inventory INNER JOIN @SignoutTotals AS ST ON reagent_inventory.ItemID=ST.ItemID

	UPDATE reagent_inventory SET reagent_inventory.Status=2
	WHERE reagent_inventory.ItemID IN (SELECT ItemID FROM @SignoutTotals)
	AND reagent_inventory.Status=3
GO

GO
USE [master]
GO
ALTER DATABASE [ReagentManagement] SET  READ_WRITE 
GO
