CREATE LOGIN ReagentLogin WITH PASSWORD='ReagentLogin', CHECK_POLICY = OFF, CHECK_EXPIRATION = OFF, DEFAULT_DATABASE=ReagentManagement
USE ReagentManagement
CREATE USER ReagentLogin FROM LOGIN ReagentLogin with DEFAULT_SCHEMA=dbo
GRANT ALL ON Categories TO ReagentLogin
GRANT ALL ON reagent_inventory TO ReagentLogin
GRANT ALL ON reagent_types TO ReagentLogin 
GRANT ALL ON ReagentCategoryRel TO ReagentLogin 
GRANT ALL ON ReagentSetCategoryRel TO ReagentLogin 
GRANT ALL ON ReagentSetReagentTypeRel TO ReagentLogin 
GRANT ALL ON ReagentSets TO ReagentLogin 
GRANT ALL ON ReagentSignout TO ReagentLogin 
GRANT ALL ON settings TO ReagentLogin 
GRANT ALL ON GetCostCentreDetails TO ReagentLogin 
GRANT ALL ON GetCostCentreSummary TO ReagentLogin 
GRANT ALL ON GetReagentDetails TO ReagentLogin 
GRANT ALL ON GetReagentSummary TO ReagentLogin 
GRANT ALL ON fn_GetActiveUsers TO ReagentLogin 
GRANT ALL ON fn_GetCostCentreDetails TO ReagentLogin 
GRANT ALL ON fn_GetCostCentreSummary TO ReagentLogin 
GRANT ALL ON fn_GetUserCostDetails TO ReagentLogin 
GRANT ALL ON fn_StockLevelsOverTimePeriod TO ReagentLogin 
GRANT ALL ON fn_CalculatePurchaseCostPerunit TO ReagentLogin 
GRANT ALL ON fn_DaysUntilDepletion TO ReagentLogin 
GRANT ALL ON fn_GetAverageDeliveryTime TO ReagentLogin 
GRANT ALL ON fn_GetAverageUseOverPeriod TO ReagentLogin 
GRANT ALL ON fn_GetStockLevel TO ReagentLogin 
GRANT ALL ON fn_GetWeightedAvererageUse TO ReagentLogin 

USE Users
CREATE USER ReagentLogin FROM LOGIN ReagentLogin with DEFAULT_SCHEMA=dbo
GRANT ALL ON app_role_relations TO ReagentLogin 
GRANT ALL ON applications TO ReagentLogin 
GRANT ALL ON cost_centres TO ReagentLogin 
GRANT ALL ON Institutes TO ReagentLogin 
GRANT ALL ON labs TO ReagentLogin 
GRANT ALL ON roles TO ReagentLogin 
GRANT ALL ON Settings TO ReagentLogin 
GRANT ALL ON user_app_lab_role_rel TO ReagentLogin 
GRANT ALL ON user_application_preferences TO ReagentLogin 
GRANT ALL ON users TO ReagentLogin 
GRANT ALL ON AuthenticationView TO ReagentLogin 
GRANT ALL ON UserProfileView TO ReagentLogin 
GRANT ALL ON p_AddUser TO ReagentLogin 
GRANT ALL ON p_DeleteUser TO ReagentLogin 
GRANT ALL ON p_AddCredential TO ReagentLogin 
GRANT ALL ON p_GetSetting TO ReagentLogin 
GRANT ALL ON p_SetSetting TO ReagentLogin 
GRANT ALL ON p_UnLinkUser TO ReagentLogin 