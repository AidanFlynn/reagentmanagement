﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReagentSignout
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void PasswordChangeConfirmButton_Click(object sender, EventArgs e)
        {
            try
            {
                Users.UserProfile User = ((CatalogForm)this.Owner).ShoppingCart.User;

                if ((Users.UserProfile.Authenticate(User.FullName, PasswordChangeCurrentTextBox.Text, ((CatalogForm)this.Owner).App)).UserID.ToString() == User.UserID.ToString())
                {

                    if (PasswordChangeConfirmTextBox.Text == PasswordChangeNewTextBox.Text)
                    {
                        User.ChangePassword(PasswordChangeNewTextBox.Text);
                        MessageBox.Show("Password Successfully Updated");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("The passwords in the new and confirm fields do not match.");
                    }
                }
                else
                {
                    MessageBox.Show("The current password you have supplied does not match the current account");
                }
            }
            catch 
            {
                MessageBox.Show("An error occurred while updating password.");                
            }

        }

        private void PasswordChangeCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
