﻿namespace ReagentSignout
{
    partial class DatabaseConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatabaseConfigUpdateButton = new System.Windows.Forms.Button();
            this.DatabasePasswordTextBox = new System.Windows.Forms.TextBox();
            this.DatabasePasswordLabel = new System.Windows.Forms.Label();
            this.DatabaseUsernameTextBox = new System.Windows.Forms.TextBox();
            this.DBLoginLabel = new System.Windows.Forms.Label();
            this.DatabaseNameTextBox = new System.Windows.Forms.TextBox();
            this.DBNameLabel = new System.Windows.Forms.Label();
            this.DatabaseHostTextBox = new System.Windows.Forms.TextBox();
            this.DBHostNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AppNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // DatabaseConfigUpdateButton
            // 
            this.DatabaseConfigUpdateButton.Location = new System.Drawing.Point(23, 211);
            this.DatabaseConfigUpdateButton.Name = "DatabaseConfigUpdateButton";
            this.DatabaseConfigUpdateButton.Size = new System.Drawing.Size(230, 23);
            this.DatabaseConfigUpdateButton.TabIndex = 6;
            this.DatabaseConfigUpdateButton.Text = "Update";
            this.DatabaseConfigUpdateButton.UseVisualStyleBackColor = true;
            this.DatabaseConfigUpdateButton.Click += new System.EventHandler(this.DatabaseConfigUpdateButton_Click);
            // 
            // DatabasePasswordTextBox
            // 
            this.DatabasePasswordTextBox.Location = new System.Drawing.Point(114, 174);
            this.DatabasePasswordTextBox.Name = "DatabasePasswordTextBox";
            this.DatabasePasswordTextBox.PasswordChar = '*';
            this.DatabasePasswordTextBox.Size = new System.Drawing.Size(139, 20);
            this.DatabasePasswordTextBox.TabIndex = 5;
            // 
            // DatabasePasswordLabel
            // 
            this.DatabasePasswordLabel.AutoSize = true;
            this.DatabasePasswordLabel.Location = new System.Drawing.Point(20, 177);
            this.DatabasePasswordLabel.Name = "DatabasePasswordLabel";
            this.DatabasePasswordLabel.Size = new System.Drawing.Size(53, 13);
            this.DatabasePasswordLabel.TabIndex = 6;
            this.DatabasePasswordLabel.Text = "Password";
            // 
            // DatabaseUsernameTextBox
            // 
            this.DatabaseUsernameTextBox.Location = new System.Drawing.Point(114, 132);
            this.DatabaseUsernameTextBox.Name = "DatabaseUsernameTextBox";
            this.DatabaseUsernameTextBox.Size = new System.Drawing.Size(139, 20);
            this.DatabaseUsernameTextBox.TabIndex = 4;
            // 
            // DBLoginLabel
            // 
            this.DBLoginLabel.AutoSize = true;
            this.DBLoginLabel.Location = new System.Drawing.Point(20, 135);
            this.DBLoginLabel.Name = "DBLoginLabel";
            this.DBLoginLabel.Size = new System.Drawing.Size(55, 13);
            this.DBLoginLabel.TabIndex = 4;
            this.DBLoginLabel.Text = "Username";
            // 
            // DatabaseNameTextBox
            // 
            this.DatabaseNameTextBox.Location = new System.Drawing.Point(114, 90);
            this.DatabaseNameTextBox.Name = "DatabaseNameTextBox";
            this.DatabaseNameTextBox.Size = new System.Drawing.Size(139, 20);
            this.DatabaseNameTextBox.TabIndex = 3;
            // 
            // DBNameLabel
            // 
            this.DBNameLabel.AutoSize = true;
            this.DBNameLabel.Location = new System.Drawing.Point(20, 93);
            this.DBNameLabel.Name = "DBNameLabel";
            this.DBNameLabel.Size = new System.Drawing.Size(84, 13);
            this.DBNameLabel.TabIndex = 2;
            this.DBNameLabel.Text = "Database Name";
            // 
            // DatabaseHostTextBox
            // 
            this.DatabaseHostTextBox.Location = new System.Drawing.Point(114, 48);
            this.DatabaseHostTextBox.Name = "DatabaseHostTextBox";
            this.DatabaseHostTextBox.Size = new System.Drawing.Size(139, 20);
            this.DatabaseHostTextBox.TabIndex = 2;
            // 
            // DBHostNameLabel
            // 
            this.DBHostNameLabel.AutoSize = true;
            this.DBHostNameLabel.Location = new System.Drawing.Point(20, 51);
            this.DBHostNameLabel.Name = "DBHostNameLabel";
            this.DBHostNameLabel.Size = new System.Drawing.Size(75, 13);
            this.DBHostNameLabel.TabIndex = 0;
            this.DBHostNameLabel.Text = "Host Name/IP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Application Name";
            // 
            // AppNameTextBox
            // 
            this.AppNameTextBox.Location = new System.Drawing.Point(114, 12);
            this.AppNameTextBox.Name = "AppNameTextBox";
            this.AppNameTextBox.Size = new System.Drawing.Size(139, 20);
            this.AppNameTextBox.TabIndex = 1;
            // 
            // DatabaseConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AppNameTextBox);
            this.Controls.Add(this.DatabaseConfigUpdateButton);
            this.Controls.Add(this.DatabasePasswordTextBox);
            this.Controls.Add(this.DatabasePasswordLabel);
            this.Controls.Add(this.DBHostNameLabel);
            this.Controls.Add(this.DatabaseUsernameTextBox);
            this.Controls.Add(this.DatabaseHostTextBox);
            this.Controls.Add(this.DBLoginLabel);
            this.Controls.Add(this.DBNameLabel);
            this.Controls.Add(this.DatabaseNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DatabaseConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DatabaseConfigUpdateButton;
        private System.Windows.Forms.TextBox DatabasePasswordTextBox;
        private System.Windows.Forms.Label DatabasePasswordLabel;
        private System.Windows.Forms.TextBox DatabaseUsernameTextBox;
        private System.Windows.Forms.Label DBLoginLabel;
        private System.Windows.Forms.TextBox DatabaseNameTextBox;
        private System.Windows.Forms.Label DBNameLabel;
        private System.Windows.Forms.TextBox DatabaseHostTextBox;
        private System.Windows.Forms.Label DBHostNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AppNameTextBox;
    }
}