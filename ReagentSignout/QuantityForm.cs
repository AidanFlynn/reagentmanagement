﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReagentSignout
{
    public partial class QuantityForm : Form
    {
        public ReagentManagement.ReagentType Reagent;
        public ReagentManagement.ReagentSet ReagentSet;

        public QuantityForm()
        {
            InitializeComponent();
            MakeButtons(1, 10);
        }

        private void MakeButtons(int Min, int Max)
        {
            Size ButtonSize = new Size(80, 80);
            int Spacing = 10;
            int Top = 15;
            int Left = 15;
            int ButtonsInRow = 0;
            for (int i = Min; i <= Max; i++)
            {
                Button NumberButton = new Button();
                NumberButton.Name = "Button_" + i.ToString();
                NumberButton.Text = i.ToString();
                NumberButton.Size = ButtonSize;
                NumberButton.Top = Top;
                NumberButton.Left = Left;
                NumberButton.Tag = i;
                NumberButton.Click += new EventHandler(NumberButton_Click);
                this.Controls.Add(NumberButton);
                ButtonsInRow++;
                Left += ButtonSize.Width + Spacing;

                if (ButtonsInRow == 5) 
                {
                    Top += ButtonSize.Height + Spacing;
                    Left = 15;
                }
            }

            Top += ButtonSize.Height + Spacing;
            Left = 15;

            Button PreviousNumberRangeButton = new Button();
            PreviousNumberRangeButton.Name = "PreviousNumberRangeButton";
            PreviousNumberRangeButton.Size = ButtonSize;
            PreviousNumberRangeButton.Top = Top;
            PreviousNumberRangeButton.Left = Left;
            PreviousNumberRangeButton.Click += new EventHandler(PreviousNumberRangeButton_Click);
            PreviousNumberRangeButton.Tag = new object[] { Min - 10, Max - 10 };
            PreviousNumberRangeButton.Text = (Min - 10).ToString() + "-" + (Max - 10).ToString();
            if (Min > 1)
            {
                this.Controls.Add(PreviousNumberRangeButton);
                Left += ButtonSize.Width + Spacing;
            }


            Button NextNumberRangeButton = new Button();
            NextNumberRangeButton.Name = "NextNumberRangeButton";
            NextNumberRangeButton.Size = ButtonSize;
            NextNumberRangeButton.Top = Top;
            NextNumberRangeButton.Left = Left;
            NextNumberRangeButton.Click += new EventHandler(NextNumberRangeButton_Click);
            NextNumberRangeButton.Tag = new object[] { Min + 10, Max + 10 };
            NextNumberRangeButton.Text = (Min + 10).ToString() + "-" + (Max + 10).ToString();
            this.Controls.Add(NextNumberRangeButton);

            Left += ButtonSize.Width + Spacing;

            Button CancelButton = new Button();
            CancelButton.Name = "CancelButton";
            CancelButton.Size = ButtonSize;
            CancelButton.Top = Top;
            CancelButton.Left = Left;
            CancelButton.Click += new EventHandler(CancelButton_Click);
            CancelButton.Text = "Cancel";
            this.Controls.Add(CancelButton);


        }

        void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void NumberButton_Click(object sender, EventArgs e)
        {
            int Quantity = (int)((Button)sender).Tag;
            if (Reagent != null)
            {
                ((CatalogForm)this.Owner).ShoppingCart.Add(Reagent, Quantity);
            }
            else if (ReagentSet != null)
            {
                foreach(KeyValuePair<ReagentManagement.ReagentType,decimal> KVP in ReagentSet.Reagents)
                {
                    ((CatalogForm)this.Owner).ShoppingCart.Add(KVP.Key, KVP.Value*Quantity);                    
                }
            }
            else
            {
                throw new Exception("Reagent/ReagentSet not specified on Quanity form");
            }
            this.Close();
        }

        void NextNumberRangeButton_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            MakeButtons((int)((object[])((Button)sender).Tag)[0], (int)((object[])((Button)sender).Tag)[1]);
        }

        void PreviousNumberRangeButton_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            MakeButtons((int)((object[])((Button)sender).Tag)[0], (int)((object[])((Button)sender).Tag)[1]);
        }
    }
}
