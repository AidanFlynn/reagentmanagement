﻿namespace ReagentSignout
{
    partial class CostCentreSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SurnameLetterGroupBox = new System.Windows.Forms.GroupBox();
            this.NameGroupBox = new System.Windows.Forms.GroupBox();
            this.NamePanel = new System.Windows.Forms.Panel();
            this.CostCentreGroupBox = new System.Windows.Forms.GroupBox();
            this.CostCentrePanel = new System.Windows.Forms.Panel();
            this.UserCancelButton = new System.Windows.Forms.Button();
            this.NameGroupBox.SuspendLayout();
            this.CostCentreGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SurnameLetterGroupBox
            // 
            this.SurnameLetterGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SurnameLetterGroupBox.Location = new System.Drawing.Point(12, 12);
            this.SurnameLetterGroupBox.Name = "SurnameLetterGroupBox";
            this.SurnameLetterGroupBox.Size = new System.Drawing.Size(264, 358);
            this.SurnameLetterGroupBox.TabIndex = 1;
            this.SurnameLetterGroupBox.TabStop = false;
            this.SurnameLetterGroupBox.Text = "Surname";
            // 
            // NameGroupBox
            // 
            this.NameGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.NameGroupBox.Controls.Add(this.NamePanel);
            this.NameGroupBox.Location = new System.Drawing.Point(282, 12);
            this.NameGroupBox.Name = "NameGroupBox";
            this.NameGroupBox.Size = new System.Drawing.Size(261, 403);
            this.NameGroupBox.TabIndex = 2;
            this.NameGroupBox.TabStop = false;
            this.NameGroupBox.Text = "Full Name";
            // 
            // NamePanel
            // 
            this.NamePanel.AutoScroll = true;
            this.NamePanel.AutoScrollMargin = new System.Drawing.Size(0, 25);
            this.NamePanel.AutoSize = true;
            this.NamePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.NamePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamePanel.Location = new System.Drawing.Point(3, 16);
            this.NamePanel.Name = "NamePanel";
            this.NamePanel.Size = new System.Drawing.Size(255, 384);
            this.NamePanel.TabIndex = 0;
            // 
            // CostCentreGroupBox
            // 
            this.CostCentreGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CostCentreGroupBox.Controls.Add(this.CostCentrePanel);
            this.CostCentreGroupBox.Location = new System.Drawing.Point(549, 12);
            this.CostCentreGroupBox.Name = "CostCentreGroupBox";
            this.CostCentreGroupBox.Size = new System.Drawing.Size(349, 400);
            this.CostCentreGroupBox.TabIndex = 3;
            this.CostCentreGroupBox.TabStop = false;
            this.CostCentreGroupBox.Text = "CostCentre";
            // 
            // CostCentrePanel
            // 
            this.CostCentrePanel.AutoScroll = true;
            this.CostCentrePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CostCentrePanel.Location = new System.Drawing.Point(3, 16);
            this.CostCentrePanel.Name = "CostCentrePanel";
            this.CostCentrePanel.Size = new System.Drawing.Size(343, 381);
            this.CostCentrePanel.TabIndex = 0;
            // 
            // CancelButton
            // 
            this.UserCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UserCancelButton.Location = new System.Drawing.Point(12, 376);
            this.UserCancelButton.Name = "CancelButton";
            this.UserCancelButton.Size = new System.Drawing.Size(264, 39);
            this.UserCancelButton.TabIndex = 4;
            this.UserCancelButton.Text = "Cancel";
            this.UserCancelButton.UseVisualStyleBackColor = true;
            this.UserCancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CostCentreSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 433);
            this.Controls.Add(this.UserCancelButton);
            this.Controls.Add(this.CostCentreGroupBox);
            this.Controls.Add(this.NameGroupBox);
            this.Controls.Add(this.SurnameLetterGroupBox);
            this.Name = "CostCentreSearch";
            this.Text = "CostCentreSearch";
            this.NameGroupBox.ResumeLayout(false);
            this.NameGroupBox.PerformLayout();
            this.CostCentreGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox SurnameLetterGroupBox;
        private System.Windows.Forms.GroupBox NameGroupBox;
        private System.Windows.Forms.GroupBox CostCentreGroupBox;
        private System.Windows.Forms.Panel NamePanel;
        private System.Windows.Forms.Panel CostCentrePanel;
        private System.Windows.Forms.Button UserCancelButton;
    }
}