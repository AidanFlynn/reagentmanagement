﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ReagentSignout
{
    public partial class SignInForm : Form
    {
        Timer RFIDTimer;
        bool TimerOn = false;
        string RFID = string.Empty;
        public Users.UserProfile CurrentUser = null;
        string[] AlphabetUpper = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        string[] AlphabetLower = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        string[] Symbols = new string[] { "!", "@", "#", "$", "%", "^", "f", "*", "(", ")", "_", "-", "+", "=", "{", "}", "[", "]", ":", ";", "'", "\"", "<", ">", ",", ".", "?", "/", "\\" }; 
        public SignInForm()
        {
            InitializeComponent();
            this.Text =  "Login";
            BuildSurnameKeyboard();
            
            this.MouseClick += new MouseEventHandler(SignInForm_MouseClick);
            //RFIDButton.KeyPress += new KeyPressEventHandler(RFIDButton_KeyPress);
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(RFIDButton_KeyPress);
            RFIDTimer = new Timer();
            RFIDTimer.Tick += new EventHandler(RFIDTimer_Tick);
            RFIDTimer.Interval = 400;
        }

        void RFIDTimer_Tick(object sender, EventArgs e)
        {
            RFIDTimer.Stop();
            TimerOn = false;
            RFID = String.Empty;
        }

        void RFIDButton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!TimerOn)
            {
                RFIDTimer.Start();
                TimerOn = true;
            }

            if ("0123456789".IndexOf(e.KeyChar) != -1)
            {
                RFID += e.KeyChar.ToString();
            }

            if (RFID.Length == 10)
            {
                try
                {
                    Users.UserProfile User = Users.UserProfile.AuthenticateByRFID(RFID, ((CatalogForm)this.Owner).App);
                    MakeCostCentreButtons(User);
                }
                catch(Exception Err)
                {
                    MessageBox.Show("An error occurred while logging in with RFID:" + Err);
                }
                
            }
        }



        void SignInForm_MouseClick(object sender, MouseEventArgs e)
        {
            //DEBUG////
            //pmci_users.PMCI_UsersSoapClient S = new ReagentSignout.pmci_users.PMCI_UsersSoapClient();
            //SetLoggedIn(S.GetUserByIDForApplication(new Guid("0758ab33-35aa-4973-9f0a-7b7285fa23c9"), APP_NAME),"Z0248");

            //END_DEBUG////
        }

        private void BuildSurnameKeyboard()
        {
            Button [] LetterButtons = new Button[26];
            int Xpos = 20;
            int Ypos = 100;
            int ColumnCounter = 0;
            for (int i = 0; i<26;i++)
            {
                Button LetterButton = new Button();
                LetterButton.Text = AlphabetUpper[i];
                LetterButton.Tag = AlphabetUpper[i];
                LetterButton.Font = new Font("Ariel", 18);
                LetterButton.Height = 70;
                LetterButton.Width = 70;
                LetterButton.Left = Xpos; 
                LetterButton.Top = Ypos;
                LetterButton.Click += new EventHandler(SurnameLetter_Click);
                LetterButtons[i] = LetterButton;
                
                Xpos+=75;
                ColumnCounter++;
                
                if(ColumnCounter == 5)
                {
                    Xpos = 20;
                    Ypos += 75;
                    ColumnCounter = 0;
                }
            }
            LastNameLettersGroupBox.Controls.AddRange(LetterButtons);
        }

        private void SurnameLetter_Click(object sender, EventArgs e)
        {
            Button LetterButton = (Button)sender;

            try
            {
                List<Users.UserProfile> Users = ((CatalogForm)this.Owner).App.GetUsersByLastName((string)LetterButton.Tag + "%");
                PopulateNameButtons(Users);
                UserNameLabel.Text = "None Selected";
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void PopulateNameButtons(List<Users.UserProfile> Users)
        {
            int Xpos = 25;
            int Ypos = 25;
            NamePanel.Controls.Clear();
            foreach (Users.UserProfile User in Users)
            {
                Button NameButton = new Button();
                NameButton.Text = User.FullName;
                NameButton.Tag = User;
                NameButton.Height = 50;
                NameButton.Width = NamePanel.Width - 50;
                //NameButton.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                NameButton.Left = Xpos;
                NameButton.Top = Ypos;
                NameButton.Font = new Font("Ariel", 12);
                NameButton.Click += new EventHandler(NameButton_Click);
                
                Ypos += 60;
                NamePanel.Controls.Add(NameButton);
            }
        }

        void NameButton_Click(object sender, EventArgs e)
        {
            CurrentUser = ((Users.UserProfile)((Button)sender).Tag);
            UserNameLabel.Text = ((Users.UserProfile)((Button)sender).Tag).FullName;
        }

       
        private void LoginButton_Click(object sender, EventArgs e)
        {

            if (CurrentUser == null)
            {
                MessageBox.Show("Please select your name or swipe your RFID card", "User Name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (PasswordTextBox.Text == string.Empty)
            {
                MessageBox.Show("Please supply your password or swipe your RFID card", "User Name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                try
                {
                    Users.UserProfile User = Users.UserProfile.Authenticate(CurrentUser.FullName, PasswordTextBox.Text, ((CatalogForm)this.Owner).App);
                    MakeCostCentreButtons(User);
                }
                catch(Exception Err)
                {
                    MessageBox.Show("An Error Occurred while Authenticating: "+Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                PasswordTextBox.Text = String.Empty;
            }

            
        }

        private void MakeCostCentreButtons(Users.UserProfile User)
        {
            CostCentrePanel.Controls.Clear();
            int Xpos = 25;
            int Ypos = 25;

            foreach (Users.Credential C in User.Credentials)
            {
                List<Users.CostCentre> CostCentres = Users.CostCentre.GetCostCentresByLabID(C.Lab.LabID, true);

                
                foreach (Users.CostCentre CostCentre in CostCentres)
                {
                    Button CostCentreButton = new Button();
                    CostCentreButton.Text = CostCentre.CostCentreID + ((CostCentre.Description == String.Empty) ? "" : " (" + CostCentre.Description + ")");
                    CostCentreButton.Tag = new object [] {User,CostCentre.CostCentreID};
                    CostCentreButton.Height = 50;
                    CostCentreButton.Width = CostCentrePanel.Width - 50;
                    //CostCentreButton.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    CostCentreButton.Left = Xpos;
                    CostCentreButton.Top = Ypos;
                    CostCentreButton.Font = new Font("Ariel", 12);
                    CostCentreButton.Click += new EventHandler(CostCentreButton_Click);

                    Ypos += 60;
                    CostCentrePanel.Controls.Add(CostCentreButton);
                }
            }
        }

        void CostCentreButton_Click(object sender, EventArgs e)
        {   
            object [] LoginDetails = (object[])((Button)sender).Tag;
            Users.UserProfile User = (Users.UserProfile)LoginDetails[0];
            string CostCentreID = (string)LoginDetails[1];
            SetLoggedIn(User, CostCentreID);
        }

        private void SetLoggedIn(Users.UserProfile User,string CostCentreID)
        {
            ((CatalogForm)this.Owner).ShoppingCart = new ReagentManagement.ShoppingCart(User, CostCentreID, DatabaseConfig.GetConnectionString());
            ((CatalogForm)this.Owner).PopulateSignoutHistory();
            ((CatalogForm)this.Owner).PopulateRecentPurchases();
            ((CatalogForm)this.Owner).SetPermissions();
            ((CatalogForm)this.Owner).Show();
            this.Hide();  
        }

        private void SignInForm_Load(object sender, EventArgs e)
        {

        }

        public void SetLoggedOut()
        {
            RFID = String.Empty;
            CurrentUser = null;
            UserNameLabel.Text = "None Selected";
            NamePanel.Controls.Clear();
            CostCentrePanel.Controls.Clear();
        }

        private void CostCentrePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HelpButton_Click(object sender, EventArgs e)
        {
            Help HelpForm = new Help();
            HelpForm.ShowDialog(this);
        }

        

    }
}
