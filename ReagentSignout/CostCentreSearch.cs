﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReagentSignout
{
    public partial class CostCentreSearch : Form
    {
        string[] AlphabetUpper = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        public string CostCentre;
        public Guid UserID;

        public CostCentreSearch()
        {
            InitializeComponent();
            BuildSurnameKeyboard();
        }

        private void BuildSurnameKeyboard()
        {
            Button[] LetterButtons = new Button[26];
            int Xpos = 20;
            int Ypos = 20;
            int ColumnCounter = 0;
            for (int i = 0; i < 26; i++)
            {
                Button LetterButton = new Button();
                LetterButton.Text = AlphabetUpper[i];
                LetterButton.Tag = AlphabetUpper[i];
                LetterButton.Font = new Font("Ariel", 18);
                LetterButton.Height = 40;
                LetterButton.Width = 40;
                LetterButton.Left = Xpos;
                LetterButton.Top = Ypos;
                LetterButton.Click += new EventHandler(SurnameLetter_Click);
                LetterButtons[i] = LetterButton;

                Xpos += 45;
                ColumnCounter++;

                if (ColumnCounter == 5)
                {
                    Xpos = 20;
                    Ypos += 45;
                    ColumnCounter = 0;
                }
            }
            SurnameLetterGroupBox.Controls.AddRange(LetterButtons);
        }

        private void SurnameLetter_Click(object sender, EventArgs e)
        {
            Button LetterButton = (Button)sender;
            List<Users.UserProfile> Users = ((AdministrationForm)this.Owner).App.GetUsersByLastName((string)LetterButton.Tag + "%");
            PopulateNameButtons(Users);
        }

        private void PopulateNameButtons(List<Users.UserProfile> Users)
        {
            int Xpos = 20;
            int Ypos = 20;
            NamePanel.Controls.Clear();
            foreach (Users.UserProfile User in Users)
            {
                Button NameButton = new Button();
                NameButton.Text = User.FullName;
                NameButton.Tag = User.UserID;
                NameButton.Height = 40;
                NameButton.Width = NamePanel.Width - 50;
                //NameButton.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                NameButton.Left = Xpos;
                NameButton.Top = Ypos;
                NameButton.Font = new Font("Ariel", 12);
                NameButton.Click += new EventHandler(NameButton_Click);

                Ypos += 45;
                NamePanel.Controls.Add(NameButton);
            }
        }

        void NameButton_Click(object sender, EventArgs e)
        {
            UserID = (Guid)((Button)sender).Tag;
            Users.UserProfile User = new Users.UserProfile(UserID, ((AdministrationForm)this.Owner).App);
            if (User.IsExternalUser)
            {
                CostCentre = User.UserID.ToString();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MakeCostCentreButtons(User);
            }
        }

        private void MakeCostCentreButtons(Users.UserProfile User)
        {
            int Xpos = 20;
            int Ypos = 20;
            CostCentrePanel.Controls.Clear();

            foreach (Users.Credential C in User.Credentials)
            {
                List<Users.CostCentre> CostCentres = Users.CostCentre.GetCostCentresByLabID(C.Lab.LabID,true);

                foreach (Users.CostCentre CostCentre in CostCentres)
                {
                    Button CostCentreButton = new Button();
                    CostCentreButton.Text = CostCentre.CostCentreID + ((CostCentre.Description == String.Empty) ? "" : " (" + CostCentre.Description + ")");
                    CostCentreButton.Tag = CostCentre.CostCentreID;
                    CostCentreButton.Height = 40;
                    CostCentreButton.Width = CostCentrePanel.Width - 50;
                    //CostCentreButton.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    CostCentreButton.Left = Xpos;
                    CostCentreButton.Top = Ypos;
                    CostCentreButton.Font = new Font("Ariel", 12);
                    CostCentreButton.Click += new EventHandler(CostCentreButton_Click);

                    Ypos += 45;
                    CostCentrePanel.Controls.Add(CostCentreButton);
                }
            }
        }

        void CostCentreButton_Click(object sender, EventArgs e)
        {   
            CostCentre = (string)((Button)sender).Tag;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
