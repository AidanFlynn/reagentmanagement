﻿namespace ReagentSignout
{
    partial class SignInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PasswordGroupBox = new System.Windows.Forms.GroupBox();
            this.LoginButton = new System.Windows.Forms.Button();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.NameGroupBox = new System.Windows.Forms.GroupBox();
            this.NamePanel = new System.Windows.Forms.Panel();
            this.LastNameLettersGroupBox = new System.Windows.Forms.GroupBox();
            this.SurnameHeader = new System.Windows.Forms.Label();
            this.CostCentreGroupBox = new System.Windows.Forms.GroupBox();
            this.CostCentrePanel = new System.Windows.Forms.Panel();
            this.UsernameHeaderLabel = new System.Windows.Forms.Label();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.UserHelpButton = new System.Windows.Forms.Button();
            this.PasswordGroupBox.SuspendLayout();
            this.NameGroupBox.SuspendLayout();
            this.LastNameLettersGroupBox.SuspendLayout();
            this.CostCentreGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // PasswordGroupBox
            // 
            this.PasswordGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordGroupBox.Controls.Add(this.UserNameLabel);
            this.PasswordGroupBox.Controls.Add(this.UsernameHeaderLabel);
            this.PasswordGroupBox.Controls.Add(this.LoginButton);
            this.PasswordGroupBox.Controls.Add(this.PasswordTextBox);
            this.PasswordGroupBox.Location = new System.Drawing.Point(844, 12);
            this.PasswordGroupBox.Name = "PasswordGroupBox";
            this.PasswordGroupBox.Size = new System.Drawing.Size(418, 134);
            this.PasswordGroupBox.TabIndex = 5;
            this.PasswordGroupBox.TabStop = false;
            this.PasswordGroupBox.Text = "Password";
            // 
            // LoginButton
            // 
            this.LoginButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginButton.Location = new System.Drawing.Point(220, 66);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(183, 40);
            this.LoginButton.TabIndex = 8;
            this.LoginButton.Text = "Log In";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordTextBox.Location = new System.Drawing.Point(11, 66);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(203, 40);
            this.PasswordTextBox.TabIndex = 0;
            // 
            // NameGroupBox
            // 
            this.NameGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.NameGroupBox.Controls.Add(this.NamePanel);
            this.NameGroupBox.Location = new System.Drawing.Point(430, 12);
            this.NameGroupBox.Name = "NameGroupBox";
            this.NameGroupBox.Size = new System.Drawing.Size(408, 729);
            this.NameGroupBox.TabIndex = 4;
            this.NameGroupBox.TabStop = false;
            this.NameGroupBox.Text = "Full Name";
            // 
            // NamePanel
            // 
            this.NamePanel.AutoScroll = true;
            this.NamePanel.AutoScrollMargin = new System.Drawing.Size(0, 25);
            this.NamePanel.AutoSize = true;
            this.NamePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.NamePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamePanel.Location = new System.Drawing.Point(3, 16);
            this.NamePanel.Name = "NamePanel";
            this.NamePanel.Size = new System.Drawing.Size(402, 710);
            this.NamePanel.TabIndex = 0;
            // 
            // LastNameLettersGroupBox
            // 
            this.LastNameLettersGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.LastNameLettersGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.LastNameLettersGroupBox.Controls.Add(this.UserHelpButton);
            this.LastNameLettersGroupBox.Controls.Add(this.SurnameHeader);
            this.LastNameLettersGroupBox.Location = new System.Drawing.Point(12, 12);
            this.LastNameLettersGroupBox.Name = "LastNameLettersGroupBox";
            this.LastNameLettersGroupBox.Size = new System.Drawing.Size(412, 729);
            this.LastNameLettersGroupBox.TabIndex = 3;
            this.LastNameLettersGroupBox.TabStop = false;
            this.LastNameLettersGroupBox.Text = "Last Name";
            // 
            // SurnameHeader
            // 
            this.SurnameHeader.AutoSize = true;
            this.SurnameHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SurnameHeader.Location = new System.Drawing.Point(62, 29);
            this.SurnameHeader.Name = "SurnameHeader";
            this.SurnameHeader.Size = new System.Drawing.Size(292, 25);
            this.SurnameHeader.TabIndex = 26;
            this.SurnameHeader.Text = "Press First Letter of Surname";
            // 
            // CostCentreGroupBox
            // 
            this.CostCentreGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CostCentreGroupBox.Controls.Add(this.CostCentrePanel);
            this.CostCentreGroupBox.Location = new System.Drawing.Point(844, 152);
            this.CostCentreGroupBox.Name = "CostCentreGroupBox";
            this.CostCentreGroupBox.Size = new System.Drawing.Size(418, 589);
            this.CostCentreGroupBox.TabIndex = 6;
            this.CostCentreGroupBox.TabStop = false;
            this.CostCentreGroupBox.Text = "Cost Centre";
            // 
            // CostCentrePanel
            // 
            this.CostCentrePanel.AutoScroll = true;
            this.CostCentrePanel.AutoScrollMargin = new System.Drawing.Size(0, 25);
            this.CostCentrePanel.AutoSize = true;
            this.CostCentrePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CostCentrePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CostCentrePanel.Location = new System.Drawing.Point(3, 16);
            this.CostCentrePanel.Name = "CostCentrePanel";
            this.CostCentrePanel.Size = new System.Drawing.Size(412, 570);
            this.CostCentrePanel.TabIndex = 0;
            this.CostCentrePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.CostCentrePanel_Paint);
            // 
            // UsernameHeaderLabel
            // 
            this.UsernameHeaderLabel.AutoSize = true;
            this.UsernameHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameHeaderLabel.Location = new System.Drawing.Point(6, 29);
            this.UsernameHeaderLabel.Name = "UsernameHeaderLabel";
            this.UsernameHeaderLabel.Size = new System.Drawing.Size(125, 25);
            this.UsernameHeaderLabel.TabIndex = 9;
            this.UsernameHeaderLabel.Text = "User Name:";
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLabel.Location = new System.Drawing.Point(137, 29);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(153, 25);
            this.UserNameLabel.TabIndex = 10;
            this.UserNameLabel.Text = "None Selected";
            // 
            // HelpButton
            // 
            this.UserHelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.UserHelpButton.Location = new System.Drawing.Point(6, 668);
            this.UserHelpButton.Name = "HelpButton";
            this.UserHelpButton.Size = new System.Drawing.Size(400, 55);
            this.UserHelpButton.TabIndex = 27;
            this.UserHelpButton.Text = "Help";
            this.UserHelpButton.UseVisualStyleBackColor = true;
            this.UserHelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // SignInForm
            // 
            this.AcceptButton = this.LoginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 764);
            this.Controls.Add(this.CostCentreGroupBox);
            this.Controls.Add(this.PasswordGroupBox);
            this.Controls.Add(this.NameGroupBox);
            this.Controls.Add(this.LastNameLettersGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "SignInForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.SignInForm_Load);
            this.PasswordGroupBox.ResumeLayout(false);
            this.PasswordGroupBox.PerformLayout();
            this.NameGroupBox.ResumeLayout(false);
            this.NameGroupBox.PerformLayout();
            this.LastNameLettersGroupBox.ResumeLayout(false);
            this.LastNameLettersGroupBox.PerformLayout();
            this.CostCentreGroupBox.ResumeLayout(false);
            this.CostCentreGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox PasswordGroupBox;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.GroupBox NameGroupBox;
        private System.Windows.Forms.GroupBox LastNameLettersGroupBox;
        private System.Windows.Forms.Label SurnameHeader;
        private System.Windows.Forms.GroupBox CostCentreGroupBox;
        private System.Windows.Forms.Panel NamePanel;
        private System.Windows.Forms.Panel CostCentrePanel;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.Label UsernameHeaderLabel;
        private System.Windows.Forms.Button UserHelpButton;

    }
}

