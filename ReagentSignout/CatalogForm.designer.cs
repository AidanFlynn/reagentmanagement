﻿namespace ReagentSignout
{
    partial class CatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CatalogForm));
            this.CatalogPanel = new System.Windows.Forms.Panel();
            this.ShoppingCartDataGrid = new System.Windows.Forms.DataGridView();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemIDs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.LogOutButton = new System.Windows.Forms.Button();
            this.CheckOutButton = new System.Windows.Forms.Button();
            this.AdministrationButton = new System.Windows.Forms.Button();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.CheckoutHistoryGroupBox = new System.Windows.Forms.GroupBox();
            this.RecentItemGroupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // CatalogPanel
            // 
            this.CatalogPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CatalogPanel.Location = new System.Drawing.Point(12, 12);
            this.CatalogPanel.Name = "CatalogPanel";
            this.CatalogPanel.Size = new System.Drawing.Size(780, 365);
            this.CatalogPanel.TabIndex = 0;
            // 
            // ShoppingCartDataGrid
            // 
            this.ShoppingCartDataGrid.AllowUserToAddRows = false;
            this.ShoppingCartDataGrid.AllowUserToDeleteRows = false;
            this.ShoppingCartDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ShoppingCartDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ShoppingCartDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ShoppingCartDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.ItemIDs,
            this.Quantity,
            this.Remove});
            this.ShoppingCartDataGrid.Location = new System.Drawing.Point(12, 541);
            this.ShoppingCartDataGrid.Name = "ShoppingCartDataGrid";
            this.ShoppingCartDataGrid.Size = new System.Drawing.Size(1095, 223);
            this.ShoppingCartDataGrid.TabIndex = 1;
            // 
            // Item
            // 
            this.Item.FillWeight = 70F;
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            // 
            // ItemIDs
            // 
            this.ItemIDs.HeaderText = "ItemIDs";
            this.ItemIDs.Name = "ItemIDs";
            // 
            // Quantity
            // 
            this.Quantity.FillWeight = 15F;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Remove
            // 
            this.Remove.FillWeight = 15F;
            this.Remove.HeaderText = "Remove";
            this.Remove.Name = "Remove";
            // 
            // LogOutButton
            // 
            this.LogOutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LogOutButton.Location = new System.Drawing.Point(1113, 724);
            this.LogOutButton.Name = "LogOutButton";
            this.LogOutButton.Size = new System.Drawing.Size(137, 40);
            this.LogOutButton.TabIndex = 2;
            this.LogOutButton.Text = "Cancel";
            this.LogOutButton.UseVisualStyleBackColor = true;
            this.LogOutButton.Click += new System.EventHandler(this.LogOutButton_Click);
            // 
            // CheckOutButton
            // 
            this.CheckOutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckOutButton.Location = new System.Drawing.Point(1113, 678);
            this.CheckOutButton.Name = "CheckOutButton";
            this.CheckOutButton.Size = new System.Drawing.Size(137, 40);
            this.CheckOutButton.TabIndex = 3;
            this.CheckOutButton.Text = "Check Out";
            this.CheckOutButton.UseVisualStyleBackColor = true;
            this.CheckOutButton.Click += new System.EventHandler(this.CheckOutButton_Click);
            // 
            // AdministrationButton
            // 
            this.AdministrationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AdministrationButton.Location = new System.Drawing.Point(1113, 586);
            this.AdministrationButton.Name = "AdministrationButton";
            this.AdministrationButton.Size = new System.Drawing.Size(137, 40);
            this.AdministrationButton.TabIndex = 4;
            this.AdministrationButton.Text = "Administration";
            this.AdministrationButton.UseVisualStyleBackColor = true;
            this.AdministrationButton.Visible = false;
            this.AdministrationButton.Click += new System.EventHandler(this.AdministrationButton_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangePasswordButton.Location = new System.Drawing.Point(1113, 632);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(137, 40);
            this.ChangePasswordButton.TabIndex = 5;
            this.ChangePasswordButton.Text = "Change Password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitButton.Location = new System.Drawing.Point(1113, 540);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(137, 40);
            this.ExitButton.TabIndex = 6;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Visible = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // CheckoutHistoryGroupBox
            // 
            this.CheckoutHistoryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckoutHistoryGroupBox.Location = new System.Drawing.Point(846, 12);
            this.CheckoutHistoryGroupBox.Name = "CheckoutHistoryGroupBox";
            this.CheckoutHistoryGroupBox.Size = new System.Drawing.Size(418, 365);
            this.CheckoutHistoryGroupBox.TabIndex = 7;
            this.CheckoutHistoryGroupBox.TabStop = false;
            this.CheckoutHistoryGroupBox.Text = "Checkout History";
            // 
            // RecentItemGroupBox
            // 
            this.RecentItemGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.RecentItemGroupBox.Location = new System.Drawing.Point(12, 383);
            this.RecentItemGroupBox.Name = "RecentItemGroupBox";
            this.RecentItemGroupBox.Size = new System.Drawing.Size(1252, 140);
            this.RecentItemGroupBox.TabIndex = 8;
            this.RecentItemGroupBox.TabStop = false;
            this.RecentItemGroupBox.Text = "Favourites";
            // 
            // CatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 776);
            this.Controls.Add(this.RecentItemGroupBox);
            this.Controls.Add(this.CheckoutHistoryGroupBox);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.AdministrationButton);
            this.Controls.Add(this.CheckOutButton);
            this.Controls.Add(this.LogOutButton);
            this.Controls.Add(this.ShoppingCartDataGrid);
            this.Controls.Add(this.CatalogPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CatalogForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Signout";
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel CatalogPanel;
        private System.Windows.Forms.DataGridView ShoppingCartDataGrid;
        private System.Windows.Forms.Button LogOutButton;
        private System.Windows.Forms.Button CheckOutButton;
        private System.Windows.Forms.Button AdministrationButton;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.GroupBox CheckoutHistoryGroupBox;
        private System.Windows.Forms.GroupBox RecentItemGroupBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemIDs;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewButtonColumn Remove;
    }
}