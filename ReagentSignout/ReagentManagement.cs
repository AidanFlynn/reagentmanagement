﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.IO;
/// <summary>
/// Summary description for ReagentManagement
/// </summary>
public static class ReagentManagement
{
    //public static string CONNECTION_STRING = "Data Source=localhost;Initial Catalog=ReagentManagement;Integrated Security=SSPI";
   //public static string CONNECTION_STRING = @"Data Source=PMC-GS3\SQLEXPRESS;Initial Catalog=ReagentManagement;User Id=ReagentManagement;Password=ReagentManagement";

    public enum ReagentStatus
    {
        Ordered = 0,
        Received = 1,
        Open = 2,
        Finished = 3,
        Faulty = 4
    }

    public static string AusToSQLDateConvert(string AusDate)
    {
        char[] DateSplitter = new char[] { '/', '\\', '-' };
        if (!String.IsNullOrEmpty(AusDate))
        {
            return AusDate.Split(DateSplitter)[1] + "/" + AusDate.Split(DateSplitter)[0] + "/" + AusDate.Split(DateSplitter)[2];
        }
        else
        {
            return String.Empty;
        }

        
    }        

    public class ShoppingCart
    {
        public List<ShoppingCartItem> Items;
        public Users.UserProfile User;
        public string CostCentre;
        private string CONNECTION_STRING;
        
        public ShoppingCart(Users.UserProfile User, string CostCentre, string ConnectionString)
        {
            this.User = User;
            this.CostCentre = CostCentre;
            Items = new List<ShoppingCartItem>(); 
            CONNECTION_STRING = ConnectionString;
        }

        public void Add(ReagentManagement.ReagentType Item, decimal Quantity)
        {
            ReagentType KeyReagent = new ReagentManagement.ReagentType(CONNECTION_STRING);

            int ItemIndex = -1;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].ItemType.Name == Item.Name)
                {
                    KeyReagent = Items[i].ItemType;
                    ItemIndex = i;
                }
            }

            if (ItemIndex == -1)
            {
                Items.Add(new ShoppingCartItem(Item, Quantity));
            }
            else
            {
                Items[ItemIndex].Quantity += Quantity;
            }
        }

        public void Remove(string ItemName, decimal Quantity)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].ItemType.Name == ItemName)
                {
                    if (Items[i].Quantity - Quantity <= 0)
                    {
                        Items.RemoveAt(i);
                    }
                    else
                    {
                        Items[i].Quantity -= Quantity;
                    }
                }
            }

           

        }

        public List<ReagentManagement.Reagent> CheckOut(string ApplicationName)
        {
            List<ReagentManagement.Reagent> UsedItems = new List<Reagent>();
            try
            {
                using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
                {
                    Connection.Open();
                    SqlTransaction Transaction = Connection.BeginTransaction();

                    try
                    {
                        foreach (ShoppingCartItem CartItem in Items)
                        {
                            List<ReagentManagement.Reagent> StockItems = new List<Reagent>();

                            if (CartItem.ItemIDs.Count == 0)
                            {
                                StockItems = CartItem.ItemType.GetStockItems(Connection, Transaction);
                            }
                            else
                            {
                                foreach (string ItemID in CartItem.ItemIDs)
                                {
                                    StockItems.Add(new Reagent(ItemID, CONNECTION_STRING));
                                }
                            }

                            double UsageQuantity = (double)CartItem.Quantity;
                            double TotalUnits = 0;

                            foreach (Reagent StockItem in StockItems)
                            {
                                TotalUnits += double.Parse(StockItem.UnitsRemaining);
                            }

                            /*if (CartItem.ItemType.GetStockLevels(false, Connection, Transaction))
                            {
                                TotalUnits = double.Parse(CartItem.ItemType.Stock.UnitsRemaining);
                            }
                            else
                            {
                                throw new Exception("Failed to fetch total stock level for reagent.");
                            }*/

                            if (!CartItem.ItemType.InfiniteResource && UsageQuantity > TotalUnits)
                            {
                                throw new ArgumentOutOfRangeException("Quantity", "Usage Quantity Exceeds Stock Level - " + CartItem.ItemType.Name);
                            }

                            while (UsageQuantity > 0)
                            {
                                if (double.Parse(StockItems[0].UnitsRemaining) > UsageQuantity || StockItems[0].InfiniteResource)
                                {
                                    StockItems[0].SignOut(User.UserID, CostCentre, UsageQuantity, Connection, Transaction, ApplicationName);
                                    UsageQuantity = 0;
                                    UsedItems.Add(StockItems[0]);
                                }
                                else if (double.Parse(StockItems[0].UnitsRemaining) == UsageQuantity)
                                {
                                    StockItems[0].SignOut(User.UserID, CostCentre, UsageQuantity, Connection, Transaction, ApplicationName);
                                    UsedItems.Add(StockItems[0]);
                                    StockItems.RemoveAt(0);
                                    UsageQuantity = 0;
                                    
                                }
                                else
                                {
                                    UsageQuantity -= double.Parse(StockItems[0].UnitsRemaining);
                                    StockItems[0].SignOut(User.UserID, CostCentre, double.Parse(StockItems[0].UnitsRemaining), Connection, Transaction, ApplicationName);
                                    UsedItems.Add(StockItems[0]);
                                    StockItems.RemoveAt(0);
                                }

                                if (UsageQuantity > 0 && StockItems.Count == 0)
                                {
                                    throw new ArgumentOutOfRangeException("Quantity", "Usage Quantity Exceeds Stock Level - " + CartItem.ItemType.Name);
                                }
                            }

                        }

                    }
                    catch (Exception Err)
                    {
                        if (Err.Message == "Stock Level Check Failed")
                        {
                            Transaction.Commit();
                            throw Err;
                        }
                        else
                        {
                            Transaction.Rollback();
                            throw Err;
                        }
                    }
                    Transaction.Commit();
                    return UsedItems;
                }
            }
            catch (ArgumentOutOfRangeException Err)
            {
                throw Err;
            }
            catch (Exception Err)
            {
                if (Err.Message == "Stock Level Check Failed")
                {
                    throw Err;
                }
                else
                {
                    throw new Exception("Reagent Signout Command Failed", Err);
                }
            }

            

        }

        public class ShoppingCartItem
        {
            public ReagentManagement.ReagentType ItemType { get; set; }
            public decimal Quantity { get; set; }
            public List<string> ItemIDs { get; set; }

            public ShoppingCartItem(ReagentType ItemType) 
            {
                this.ItemType = ItemType;
                ItemIDs = new List<string>();
            }

            public ShoppingCartItem(ReagentType ItemType, decimal Quantity)
            {
                this.ItemType = ItemType;
                ItemIDs = new List<string>();
                this.Quantity = Quantity;
            }

        }
    }
    
    public class Category
    {
        public int CategoryID;
        public int ParentCategoryID;
        public string Description;
        public System.Drawing.Image Icon;
        private string CONNECTION_STRING;

        public Category(string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
        }

        public Category(int CategoryID, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            this.CategoryID = CategoryID;
            FetchDetails();           
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ParentCategoryID, Description, Icon FROM Categories WHERE CategoryID=@CategoryID");
                Command.Parameters.AddWithValue("@CategoryID", CategoryID);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        Reader.Read();

                        ParentCategoryID = (!Reader.IsDBNull(0)) ? Reader.GetInt32(0) : -1;
                        Description = (!Reader.IsDBNull(1))?Reader.GetString(1):String.Empty;
                        if (!Reader.IsDBNull(2))
                        {
                            System.Drawing.Image.FromStream(Reader.GetSqlBytes(2).Stream);
                        }
                        else
                        {
                            Icon = null;
                        }
                    }
                }
                
                
            }
        }

        public bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO Categories (Description, ParentCategoryID) VALUES (@Description, @ParentCategoryID)");
                Command.Parameters.AddWithValue("@Description", this.Description);
                Command.Parameters.AddWithValue("@ParentCategoryID", (this.ParentCategoryID==-1)?(object)DBNull.Value:this.ParentCategoryID);
                //Command.Parameters.AddWithValue("@Icon", (this.Icon == null) ? (object)DBNull.Value : this.Icon);
                Command.Connection= Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        SqlCommand IdentityCommand = new SqlCommand("SELECT CAST(@@IDENTITY as int) AS CategoryID");
                        IdentityCommand.Connection = Connection;

                        using (SqlDataReader Reader = IdentityCommand.ExecuteReader())
                        {
                            if (Reader.HasRows)
                            {
                                Reader.Read();
                                if (!Reader.IsDBNull(0))
                                {
                                    this.CategoryID = Reader.GetInt32(0);
                                    return true;
                                }
                                else
                                {
                                    throw new Exception("CategoryID retrieval failed");
                                }
                            }
                            else
                            {
                                throw new Exception("CategoryID retrieval failed");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Category Insert Statement returned wrong number of affected rows");
                    }
                }
                catch(Exception Err)
                {
                    throw new Exception("Category Write Failed",Err);
                }
            }
        }

        public bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM Categories WHERE CategoryID=@CategoryID");
                Command.Parameters.AddWithValue("@CategoryID", this.CategoryID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Category Delete Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Category Delete Failed", Err);
                }
            }
        }

        public bool AddReagentType(ReagentManagement.ReagentType ReagentType)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO ReagentCategoryRel (ReagentTypeID, CategoryID) VALUES (@ReagentTypeID, @CategoryID)");
                Command.Parameters.AddWithValue("@ReagentTypeID", ReagentType.TypeID);
                Command.Parameters.AddWithValue("@CategoryID", this.CategoryID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Category/Reagent Link Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Category/Reagent Link Failed", Err);
                }
            }   
        }

        public bool RemoveReagentType(ReagentManagement.ReagentType ReagentType)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM ReagentCategoryRel WHERE ReagentTypeID=@ReagentTypeID AND CategoryID=@CategoryID");
                Command.Parameters.AddWithValue("@ReagentTypeID", ReagentType.TypeID);
                Command.Parameters.AddWithValue("@CategoryID", this.CategoryID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Category/Reagent UnLink Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Category/Reagent UnLink Failed", Err);
                }
            }
        }

        public bool AddReagentSet(ReagentManagement.ReagentSet reagentSet)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO ReagentSetCategoryRel (ReagentSetID, CategoryID) VALUES (@ReagentSetID, @CategoryID)");
                Command.Parameters.AddWithValue("@ReagentSetID", reagentSet.SetID);
                Command.Parameters.AddWithValue("@CategoryID", this.CategoryID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Category/ReagentSet Link Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Category/ReagentSet Link Failed", Err);
                }
            }
        }

        public bool RemoveReagentSet(ReagentManagement.ReagentSet reagentSet)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM ReagentSetCategoryRel WHERE ReagentSetID=@ReagentSetID AND CategoryID=@CategoryID");
                Command.Parameters.AddWithValue("@ReagentSetID", reagentSet.SetID);
                Command.Parameters.AddWithValue("@CategoryID", this.CategoryID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Category/ReagentSet UnLink Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Category/ReagentSet UnLink Failed", Err);
                }
            }
        }

        
        public List<Category> GetChildCategories()
        {
            List<Category> ReturnList = new List<Category>();

            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT CategoryID FROM Categories WHERE ParentCategoryID=@CategoryID ORDER BY Description");
                Command.Parameters.AddWithValue("@CategoryID", CategoryID);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Category(Reader.GetInt32(0), this.CONNECTION_STRING));
                        }
                    }
                }
            }

            return ReturnList;
        }

        public List<ReagentType> GetChildReagents()
        {
            List<ReagentType> ReturnList = new List<ReagentType>();

            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ReagentCategoryRel.ReagentTypeID FROM ReagentCategoryRel INNER JOIN reagent_types ON ReagentCategoryRel.ReagentTypeID=reagent_types.ReagentTypeID WHERE CategoryID=@CategoryID ORDER BY reagent_types.Name");
                Command.Parameters.AddWithValue("@CategoryID", CategoryID);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new ReagentType(Reader.GetInt32(0).ToString(), CONNECTION_STRING));
                        }
                    }
                }
            }

            return ReturnList;
        }

        public List<ReagentSet> GetChildReagentSets()
        {
            List<ReagentSet> ReturnList = new List<ReagentSet>();

            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ReagentSetCategoryRel.ReagentSetID FROM ReagentSetCategoryRel INNER JOIN ReagentSets ON ReagentSetCategoryRel.ReagentSetID=ReagentSets.SetID WHERE CategoryID=@CategoryID ORDER BY ReagentSets.SetName");
                Command.Parameters.AddWithValue("@CategoryID", CategoryID);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new ReagentSet(Reader.GetInt32(0), CONNECTION_STRING));
                        }
                    }
                }
            }

            return ReturnList;
        }

        public static List<Category> GetTopLevelCategories(string ConnectionString)
        {
            List<Category> ReturnList = new List<Category>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT CategoryID FROM Categories WHERE ParentCategoryID IS NULL");
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Category(Reader.GetInt32(0), ConnectionString));
                        }
                    }
                }
            }

            return ReturnList;
        }

    }

    public class ReagentSet
    {
        public int SetID { get; set; }
        public string Name { get; set; }
        public Dictionary<ReagentType, decimal> Reagents { get; set; }
        private string CONNECTION_STRING;

        public ReagentSet(string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            Reagents = new Dictionary<ReagentType, decimal>();
        }


        public ReagentSet(int SetID, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            this.SetID = SetID;
            Reagents = new Dictionary<ReagentType, decimal>();
            FetchDetails();
            FetchReagents();
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT SetName FROM ReagentSets WHERE SetID=@SetID");
                Command.Parameters.AddWithValue("@SetID", SetID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                this.Name = Reader.GetString(0);
                            }
                        }
                        else
                        {
                            throw new Exception("Suppied ReagentSetID returned no rows");
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("ReagentSet Fetch Details Failed", Err);
                }
            }
        }

        private void FetchReagents()
        {
            Reagents.Clear();
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ReagentTypeID, Units FROM ReagentSetReagentTypeRel WHERE ReagentSetID=@ReagentSetID");
                Command.Parameters.AddWithValue("@ReagentSetID", SetID);
                Command.Connection = Connection;
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            Reagents.Add(new ReagentType(Reader.GetInt16(0).ToString(), CONNECTION_STRING), Reader.GetDecimal(1));
                        }
                    }
                }
            }
        }

        public void Write()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                try
                {
                    Connection.Open();

                    SqlCommand Command = new SqlCommand("INSERT INTO ReagentSets (SetName) VALUES (@SetName)");
                    Command.Parameters.AddWithValue("@SetName", Name);
                    Command.Connection = Connection;

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        SqlCommand IdentityCommand = new SqlCommand("SELECT CAST(@@IDENTITY as int) AS SetID");
                        IdentityCommand.Connection = Connection;

                        using (SqlDataReader Reader = IdentityCommand.ExecuteReader())
                        {
                            if (Reader.HasRows)
                            {
                                Reader.Read();
                                if (!Reader.IsDBNull(0))
                                {
                                    this.SetID = Reader.GetInt32(0);
                                }
                                else
                                {
                                    throw new Exception("SetID retrieval failed");
                                }
                            }
                            else
                            {
                                throw new Exception("SetID retrieval failed");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Set Insert Statement returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Writing Set Failed", Err);
                }
            }
        }

        public void Delete()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                try
                {
                    Connection.Open();

                    SqlCommand Command = new SqlCommand("DELETE FROM ReagentSets WHERE SetID=@SetID");
                    Command.Parameters.AddWithValue("@SetID", SetID);
                    Command.Connection = Connection;

                    if (Command.ExecuteNonQuery() != 1)
                    {
                        throw new Exception("Set Delete Statement returned wrong number of affected rows");
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("Deleting Set Failed", Err);
                }
            }
        }

        public bool LinkToReagent(SqlConnection Connection, SqlTransaction Transaction, ReagentType reagentType, decimal Units)
        {
            try
            {
                SqlCommand Command = new SqlCommand("INSERT INTO ReagentSetReagentTypeRel (ReagentSetID, ReagentTypeID, Units) VALUES (@ReagentSetID, @ReagentTypeID, @Units)");
                Command.Parameters.AddWithValue("@ReagentSetID", this.SetID);
                Command.Parameters.AddWithValue("@ReagentTypeID", reagentType.TypeID);
                Command.Parameters.AddWithValue("@Units", Units);
                Command.Connection = Connection;
                Command.Transaction = Transaction;

                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Linking reagent to set returned wrong number of affected rows");
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Linking reagent to set failed", Err);
            }
        }

        public bool LinkToReagents(Dictionary<ReagentType,decimal> ReagentsList)
        {
            try
            {
                using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
                {
                    Connection.Open();
                    SqlTransaction Transaction = Connection.BeginTransaction();

                    try
                    {
                        this.UnLinkAllReagents(Connection, Transaction);

                        foreach (KeyValuePair<ReagentType, decimal> KVP in ReagentsList)
                        {
                            this.LinkToReagent(Connection, Transaction, KVP.Key, KVP.Value);
                        }
                        Transaction.Commit();
                        return true;
                    }
                    catch (Exception Err)
                    {
                        Transaction.Rollback();
                        throw Err;
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Linking reagent to set failed", Err);
            }
        }

        public bool UnLinkReagent(ReagentType reagentType)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                try
                {
                    Connection.Open();
                    SqlTransaction Transaction = Connection.BeginTransaction();
                    this.UnLinkReagent(Connection, Transaction, reagentType);
                    Transaction.Commit();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("Unlinking reagent from set failed - No Transaction", Err);
                }
            }
        }

        public bool UnLinkReagent(SqlConnection Connection, SqlTransaction Transaction, ReagentType reagentType)
        {

            try
            {
                SqlCommand Command = new SqlCommand("DELETE FROM ReagentSetReagentTypeRel WHERE ReagentSetID=@ReagentSetID AND ReagentTypeID=@ReagentTypeID");
                Command.Parameters.AddWithValue("@ReagentSetID", this.SetID);
                Command.Parameters.AddWithValue("@ReagentTypeID", reagentType.TypeID);
                Command.Connection = Connection;
                Command.Transaction = Transaction;

                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Unlinking reagent to set returned wrong number of affected rows");
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Unlinking reagent from set failed", Err);
            }
        }

        public void UnLinkAllReagents()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                try
                {
                    Connection.Open();
                    SqlTransaction Transaction = Connection.BeginTransaction();
                    try
                    {
                        this.UnLinkAllReagents(Connection, Transaction);
                        Transaction.Commit();
                    }
                    catch (Exception Err)
                    {
                        Transaction.Rollback();
                        throw Err;
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("Unlinking All reagents from set failed - No Transaction", Err);
                }
            }
        }

        public void UnLinkAllReagents(SqlConnection Connection, SqlTransaction Transaction)
        {
            try
            {
                SqlCommand Command = new SqlCommand("DELETE FROM ReagentSetReagentTypeRel WHERE ReagentSetID=@ReagentSetID");
                Command.Parameters.AddWithValue("@ReagentSetID", this.SetID);
                Command.Connection = Connection;
                Command.Transaction = Transaction;
                Command.ExecuteNonQuery();
            }
            catch (Exception Err)
            {
                throw new Exception("Unlinking All reagents from set failed - Transaction", Err);
            }
        }
        

        internal static List<ReagentSet> GetSetList(string ConnectionString)
        {
            List<ReagentSet> ReturnList = new List<ReagentSet>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT SetID FROM ReagentSets");
                Command.Connection = Connection;
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new ReagentSet(Reader.GetInt32(0),ConnectionString));
                        }
                    }
                }
                return ReturnList;
            }
        }
    }

    public class ReagentType
    {
        public string TypeID { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Supplier { get; set; }
        public string UnitsPerItem { get; set; }
        public string PartNumber { get; set; }
        public string LastOrdered { get; set; }
        public decimal Cost { get; set; }
        public decimal CostPerUnit { get; set; }
        public decimal MarkUp { get; set; }
        public string OrderThreshold { get; set; }
        public string UnitOfSale { get; set; }
        public bool InfiniteResource { get; set; }
        public string DefaultPurchaseCostCentre { get; set; }
        public StockLevel Stock;
        internal string CONNECTION_STRING;

        public ReagentType()
        {
        }

        public ReagentType(string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
        }

        public ReagentType(string ExistingReagentTypeID, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            TypeID = ExistingReagentTypeID;
            FetchDetails();
        }

        public virtual void FetchDetails()
        {
            if (String.IsNullOrEmpty(TypeID)) throw new ArgumentNullException("TypeID", "TypeID not set");
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT *, CONVERT(varchar,LastOrdered,103) AS LastOrderedFormatted FROM reagent_types WHERE ReagentTypeID=@TypeID");
                Command.Parameters.AddWithValue("@TypeID", TypeID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        Name = Reader["Name"].ToString();
                        Manufacturer = Reader["Manufacturer"].ToString();
                        Supplier = Reader["Supplier"].ToString();
                        UnitsPerItem = Reader["UnitsPerItem"].ToString();
                        PartNumber = Reader["PartNumber"].ToString();
                        Cost = Reader.GetDecimal(Reader.GetOrdinal("Cost"));
                        CostPerUnit = Reader.GetDecimal(Reader.GetOrdinal("CostPerUnit"));
                        LastOrdered = Reader["LastOrderedFormatted"].ToString();
                        OrderThreshold = Reader["OrderThreshold"].ToString();
                        UnitOfSale = Reader["UnitOfSale"].ToString();
                        InfiniteResource = Reader.GetBoolean(Reader.GetOrdinal("InfiniteResource"));
                        MarkUp = Reader.GetDecimal(Reader.GetOrdinal("MarkUp"));
                        DefaultPurchaseCostCentre = Reader.IsDBNull(Reader.GetOrdinal("DefaultPurchaseCostCentre")) ? String.Empty : Reader.GetString(Reader.GetOrdinal("DefaultPurchaseCostCentre"));

                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("TypeID", "Supplied TypeID Returned No Results");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Fetching Details Failed", Err);
                }
            }
        }

        public bool TypeExists(string PartNumber)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT * FROM reagent_types WHERE PartNumber=@PartNumber");
                Command.Parameters.AddWithValue("@PartNumber", PartNumber);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    SqlDataReader Reader = Command.ExecuteReader();
                    bool ReturnVaule = Reader.HasRows;
                    Connection.Close();
                    return ReturnVaule;
                }
                catch (Exception Err)
                {
                    throw new Exception("Existing ReagentType Check Failed", Err);
                }
            }
        }

        public bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO reagent_types (PartNumber, Name, Manufacturer,Supplier,UnitsPerItem,OrderThreshold, UnitOfSale,Cost, InfiniteResource, MarkUp, DefaultPurchaseCostCentre) VALUES (@PartNumber, @Name, @Manufacturer,@Supplier,@UnitsPerItem,@OrderThreshold,@UnitOfSale,@Cost, @InfiniteResource, @MarkUp, @DefaultPurchaseCostCentre)");
                Command.Parameters.AddWithValue("@PartNumber", PartNumber);
                Command.Parameters.AddWithValue("@Name", Name);
                Command.Parameters.AddWithValue("@Manufacturer", Manufacturer);
                Command.Parameters.AddWithValue("@Supplier", Supplier);
                Command.Parameters.AddWithValue("@Cost", Cost);
                Command.Parameters.AddWithValue("@UnitsPerItem", UnitsPerItem.Replace(",", "."));
                Command.Parameters.AddWithValue("@OrderThreshold", OrderThreshold);
                Command.Parameters.AddWithValue("@UnitOfSale", UnitOfSale);
                Command.Parameters.AddWithValue("@InfiniteResource", UnitOfSale);
                Command.Parameters.AddWithValue("@MarkUp", MarkUp);
                Command.Parameters.AddWithValue("@DefaultPurchaseCostCentre", string.IsNullOrEmpty(DefaultPurchaseCostCentre) ? (object)DBNull.Value : DefaultPurchaseCostCentre);
                
                try
                {
                    Command.Connection = Connection;
                    Connection.Open();

                    if (TypeExists(PartNumber))
                    {
                        throw new ArgumentException("A Reagent With That Part Number Already Exists");
                    }

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        SqlCommand IdentityCommand = new SqlCommand("SELECT @@IDENTITY AS ReagentTypeID");
                        IdentityCommand.Connection = Connection;

                        using (SqlDataReader Reader = IdentityCommand.ExecuteReader())
                        {
                            if (Reader.HasRows)
                            {
                                Reader.Read();
                                if (Reader["ReagentTypeID"] != DBNull.Value)
                                {
                                    TypeID = Reader["ReagentTypeID"].ToString();
                                }
                            }
                        }
                        Connection.Close();
                        return true;
                    }
                    else
                    {
                        throw new Exception("New Reagent Creation Query Returned Incorrect Number of Affected Rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("ReagentType Creation Query Failed", Err);
                }
            }
        }

        public bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("UPDATE Reagent_Types SET PartNumber=@PartNumber, Name=@Name,Manufacturer=@Manufacturer,Supplier=@Supplier,Cost=@Cost,UnitsPerItem=@UnitsPerItem, OrderThreshold=@OrderThreshold, UnitOfSale=@UnitOfSale, InfiniteResource=@InfiniteResource, MarkUp=@MarkUp, DefaultPurchaseCostCentre=@DefaultPurchaseCostCentre WHERE ReagentTypeID=@ReagentTypeID");
                Command.Parameters.AddWithValue("@ReagentTypeID", TypeID);
                Command.Parameters.AddWithValue("@PartNumber", PartNumber);
                Command.Parameters.AddWithValue("@Name", Name);
                Command.Parameters.AddWithValue("@Manufacturer", Manufacturer);
                Command.Parameters.AddWithValue("@Supplier", Supplier);
                Command.Parameters.AddWithValue("@Cost", Cost);
                Command.Parameters.AddWithValue("@UnitsPerItem", UnitsPerItem.Replace(",","."));
                Command.Parameters.AddWithValue("@OrderThreshold", OrderThreshold.Replace(",", "."));
                Command.Parameters.AddWithValue("@UnitOfSale", UnitOfSale);
                Command.Parameters.AddWithValue("@InfiniteResource", InfiniteResource);
                Command.Parameters.AddWithValue("@MarkUp", MarkUp);
                Command.Parameters.AddWithValue("@DefaultPurchaseCostCentre", DefaultPurchaseCostCentre);
                
                try
                {
                    Command.Connection = Connection;
                    Connection.Open();

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Reagent Type Update Query Returned Incorrect Number of Affected Rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Reagent Type Update Query Failed", Err);
                }
            }
        }

        public bool Order(int Quantity, string CostCentre, string OrderedBy)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlTransaction Transaction;

                try
                {
                    Connection.Open();
                    Transaction = Connection.BeginTransaction();
                    try
                    {
                        Reagent R = new Reagent(this, CONNECTION_STRING);
                        R.PurchaseCostCentre = CostCentre;
                        R.OrderedBy = OrderedBy;
                        for (int i = 0; i < Quantity; i++)
                        {
                            R.Write(Connection, Transaction);
                        }
                        SqlCommand Command = new SqlCommand("UPDATE reagent_types SET LastOrdered=GETDATE() WHERE ReagentTypeID=@ReagentTypeID");
                        Command.Parameters.AddWithValue("@ReagentTypeID", this.TypeID);
                        Command.Connection = Connection;
                        Command.Transaction = Transaction;
                        if (Command.ExecuteNonQuery() == 1)
                        {
                            Transaction.Commit();
                            return true;
                        }
                        else
                        {
                            throw new Exception("Last Ordered Date Update Query Returned Wrong Number of Affected Rows");
                        }

                    }
                    catch (Exception Err)
                    {
                        Transaction.Rollback();
                        throw Err;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Ordering of Reagent Failed", Err);
                }
            }
        }

        public static List<ReagentType> GetReagentTypes(string ConnectionString)
        {
            List<ReagentType> ReturnList = new List<ReagentType>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("Select ReagentTypeID FROM reagent_types ORDER BY Name");
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    SqlDataReader Reader = Command.ExecuteReader();
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new ReagentType(Reader["ReagentTypeID"].ToString(), ConnectionString));
                        }
                    }
                    return ReturnList;
                }
                catch (Exception Err)
                {
                    throw new Exception("Error while retrieving Reagent Types", Err);
                }
            }
        }

        public List<Reagent> GetStockItems()
        {
            List<Reagent> ReturnList = new List<Reagent>();
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ItemID FROM reagent_inventory WHERE Status IN (1,2) AND ReagentTypeID=@TypeID");
                Command.Parameters.AddWithValue("@TypeID", TypeID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Reagent(Reader["ItemID"].ToString(),CONNECTION_STRING));
                        }
                    }
                    return ReturnList;
                }
                catch (Exception Err)
                {
                    throw new Exception("Fetching ReagentType Stock Items Failed", Err);
                }
            }
        }

        public List<Reagent> GetStockItems(SqlConnection Connection, SqlTransaction Transaction)
        {
            List<Reagent> ReturnList = new List<Reagent>();

            SqlCommand Command = new SqlCommand("SELECT ItemID FROM reagent_inventory WHERE Status IN (1,2) AND ReagentTypeID=@TypeID ORDER BY ItemID ASC");
            Command.Parameters.AddWithValue("@TypeID", TypeID);
            Command.Connection = Connection;
            Command.Transaction = Transaction;

            try
            {

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Reagent(Reader["ItemID"].ToString(),CONNECTION_STRING));
                        }
                    }
                }
                return ReturnList;
            }
            catch (Exception Err)
            {
                throw new Exception("Fetching ReagentType Stock Items Failed", Err);
            }

        }

        public bool GetStockLevels(bool CountOrdersAsStock, SqlConnection Connection, SqlTransaction Transaction)
        {
            if (this.InfiniteResource)
            {
                this.Stock.ItemCount = Int32.MaxValue.ToString();
                this.Stock.UnitsRemaining = Int32.MaxValue.ToString();
                return true;
            }

            SqlCommand Command = new SqlCommand("SELECT Count(reagent_types.PartNumber) AS ItemCount, "
                                                    + "IsNull(SUM(reagent_inventory.UnitsRemaining),0) AS UnitsRemaining "
                                                    + " FROM reagent_types LEFT OUTER JOIN reagent_inventory"
                                                    + " ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID "
                                                    + " WHERE reagent_inventory.ReagentTypeID=@TypeID "
                                                    + " AND reagent_inventory.Status IN (" + ((CountOrdersAsStock) ? "0," : String.Empty) + "1,2)");
            Command.Parameters.AddWithValue("@TypeID", TypeID);
            Command.Connection = Connection;
            Command.Transaction = Transaction;

            try
            {
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        this.Stock.ItemCount = Reader["ItemCount"].ToString();
                        this.Stock.UnitsRemaining = Reader["UnitsRemaining"].ToString();
                        return true;
                    }
                    else
                    {
                        throw new Exception("Reagent Type Stock Query Returned No Rows");
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Reagent Type Stock Query Failed", Err);
            }

        }


        public bool CheckStockLevels(SqlConnection Connection, SqlTransaction Transaction, string ApplicationName)
        {
            if (this.InfiniteResource) return true;
            try
            {
                this.GetStockLevels(true, Connection, Transaction);
                if (decimal.Parse(this.Stock.UnitsRemaining) < decimal.Parse(this.OrderThreshold))
                {
                    try
                    {
                        return OrderNotify(this, ApplicationName);
                    }
                    catch
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Stock Level Check Failed",Err);
            }
        }

        public bool CheckStockLevels(string ApplicationName)
        {
            try
            {
                using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
                {
                    this.GetStockLevels(true, Connection, null);
                    if (decimal.Parse(this.Stock.UnitsRemaining) < decimal.Parse(this.OrderThreshold))
                    {
                        try
                        {
                            return OrderNotify(this, ApplicationName);
                        }
                        catch
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Stock Level Check Failed", Err);
            }
        }

        private bool OrderNotify(ReagentType reagentType, string ApplicationName)
        {

            //pmci_users.PMCI_UsersSoapClient PersonnelTool = new pmci_users.PMCI_UsersSoapClient();

            Users.ClientApplication App = new Users.ClientApplication(ApplicationName);
            List<string> Recipients = App.GetAdminEmailAddresses();
            if (Recipients.Count != 0)
            {
                string RecipientsString = string.Empty;
                foreach (string Address in Recipients)
                {
                    RecipientsString += Address + ",";
                }
                RecipientsString.TrimEnd(',');
                string MessageText = "Dear Lab Staff, <br/><br/> The reagent "
                                   + reagentType.Name + " has only "
                                   + reagentType.Stock.UnitsRemaining
                                   + " units remaining. The order threshold is "
                                   + reagentType.OrderThreshold
                                   + ". Please order additional reagents as required. <br/><br/> Kind Regards, <br/> The Reagent Management System";
                return Users.SendMail("Low Stock Level: " + reagentType.Name, RecipientsString, Settings.GetSetting("SystemAdminEmail", CONNECTION_STRING), MessageText, true);
            }
            else
            {
                return false;
            }
        }

        public static SqlDataAdapter GetDataAdapter(string ConnectionString)
        {
            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlDataAdapter DataAdapter = new SqlDataAdapter();

            //Select Command
            DataAdapter.SelectCommand = new SqlCommand("SELECT [ReagentTypeID], [Name], [Manufacturer], [Supplier], [PartNumber], CONVERT(varchar,LastOrdered,103) AS LastOrdered, [Status], [OrderThreshold], [UnitsPerItem], CONVERT(DECIMAL(9,2),[Cost]) AS Cost, CONVERT(DECIMAL(9,2),[CostPerUnit]) AS CPU, [UnitOfSale], [InfiniteResource], [MarkUp], [DefaultPurchaseCostCentre] FROM reagent_types"); 
            DataAdapter.SelectCommand.Connection = Connection;

            //Delete Command
            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM reagent_types WHERE ReagentTypeID=@ReagentTypeID");
            DataAdapter.DeleteCommand.Connection = Connection;
            DataAdapter.DeleteCommand.Parameters.Add(new SqlParameter("@ReagentTypeID", SqlDbType.Int));
            DataAdapter.DeleteCommand.Parameters["@ReagentTypeID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.DeleteCommand.Parameters["@ReagentTypeID"].SourceColumn = "ReagentTypeID";

            //Update Command
            DataAdapter.UpdateCommand = new SqlCommand("UPDATE reagent_types SET Name=@Name, Manufacturer=@Manufacturer, Supplier=@Supplier, PartNumber=@PartNumber, LastOrdered=@LastOrdered, Status=@Status, OrderThreshold=@OrderThreshold, UnitsPerItem=@UnitsPerItem, UnitOfSale=@UnitOfSale, Cost=@Cost, InfiniteResource=@InfiniteResource, MarkUp=@MarkUp, DefaultPurchaseCostCentre=@DefaultPurchaseCostCentre WHERE ReagentTypeID=@ReagentTypeID");
            DataAdapter.UpdateCommand.Connection = Connection;
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReagentTypeID", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@ReagentTypeID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.UpdateCommand.Parameters["@ReagentTypeID"].SourceColumn = "ReagentTypeID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@Name"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Name"].SourceColumn = "Name";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Manufacturer", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@Manufacturer"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Manufacturer"].SourceColumn = "Manufacturer";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Supplier", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@Supplier"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Supplier"].SourceColumn = "Supplier";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@PartNumber", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@PartNumber"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@PartNumber"].SourceColumn = "PartNumber";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@LastOrdered", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@LastOrdered"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@LastOrdered"].SourceColumn = "LastOrdered";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrderThreshold", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@OrderThreshold"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@OrderThreshold"].SourceColumn = "OrderThreshold";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.TinyInt));
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceColumn = "Status";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@UnitsPerItem", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@UnitsPerItem"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@UnitsPerItem"].SourceColumn = "UnitsPerItem";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Cost", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@Cost"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Cost"].SourceColumn = "Cost";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@UnitOfSale", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@UnitOfSale"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@UnitOfSale"].SourceColumn = "UnitOfSale";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@InfiniteResource", SqlDbType.Bit));
            DataAdapter.UpdateCommand.Parameters["@InfiniteResource"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@InfiniteResource"].SourceColumn = "InfiniteResource";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@MarkUp", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@MarkUp"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@MarkUp"].SourceColumn = "MarkUp";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@DefaultPurchaseCostCentre", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@DefaultPurchaseCostCentre"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@DefaultPurchaseCostCentre"].SourceColumn = "DefaultPurchaseCostCentre";



            DataAdapter.InsertCommand = new SqlCommand("INSERT INTO reagent_types (Name, Manufacturer, Supplier, PartNumber, LastOrdered, OrderThreshold, Status, UnitsPerItem, UnitOfSale, Cost, InfiniteResource, MarkUp, DefaultPurchaseCostCentre) VALUES (@Name,@Manufacturer,@Supplier,@PartNumber,@LastOrdered, @OrderThreshold,@Status,@UnitsPerItem,@UnitOfSale,@Cost, @InfiniteResource, @MarkUp, @DefaultPurchaseCostCentre)");
            DataAdapter.InsertCommand.Connection = Connection;
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@Name"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@Name"].SourceColumn = "Name";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Manufacturer", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@Manufacturer"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@Manufacturer"].SourceColumn = "Manufacturer";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Supplier", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@Supplier"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@Supplier"].SourceColumn = "Supplier";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@PartNumber", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@PartNumber"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@PartNumber"].SourceColumn = "PartNumber";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@LastOrdered", SqlDbType.DateTime));
            DataAdapter.InsertCommand.Parameters["@LastOrdered"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@LastOrdered"].SourceColumn = "LastOrdered";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@OrderThreshold", SqlDbType.Int));
            DataAdapter.InsertCommand.Parameters["@OrderThreshold"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@OrderThreshold"].SourceColumn = "OrderThreshold";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.TinyInt));
            DataAdapter.InsertCommand.Parameters["@Status"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@Status"].SourceColumn = "Status";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@UnitsPerItem", SqlDbType.Int));
            DataAdapter.InsertCommand.Parameters["@UnitsPerItem"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@UnitsPerItem"].SourceColumn = "UnitsPerItem";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@Cost", SqlDbType.Decimal));
            DataAdapter.InsertCommand.Parameters["@Cost"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@Cost"].SourceColumn = "Cost";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@UnitOfSale", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@UnitOfSale"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@UnitOfSale"].SourceColumn = "UnitOfSale";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@InfiniteResource", SqlDbType.Bit));
            DataAdapter.InsertCommand.Parameters["@InfiniteResource"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@InfiniteResource"].SourceColumn = "InfiniteResource";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@MarkUp", SqlDbType.Decimal));
            DataAdapter.InsertCommand.Parameters["@MarkUp"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@MarkUp"].SourceColumn = "MarkUp";
            DataAdapter.InsertCommand.Parameters.Add(new SqlParameter("@DefaultPurchaseCostCentre", SqlDbType.VarChar));
            DataAdapter.InsertCommand.Parameters["@DefaultPurchaseCostCentre"].SourceVersion = DataRowVersion.Current;
            DataAdapter.InsertCommand.Parameters["@DefaultPurchaseCostCentre"].SourceColumn = "DefaultPurchaseCostCentre";
            
            return DataAdapter;
        }

        internal static List<ReagentType> GetRecentPurchaseItems(Guid UserID, int Days, string ConnectionString)
        {
            List<ReagentType> ReturnList = new List<ReagentType>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                string SQL = "SELECT TOP 10 ReagentTypeID, SUM(ReagentSignout.Quantity) AS QuantityUsed FROM reagent_inventory ";
                SQL += " INNER JOIN ReagentSignout";
                SQL += " ON reagent_inventory.ItemID=ReagentSignout.ItemID";
                SQL += " WHERE ReagentSignout.UserID='" + UserID.ToString() + "'";
                SQL += " AND ReagentSignout.Date > '" + DateTime.Now.Subtract(new TimeSpan(Days, 0, 0, 0, 0)).ToString("MM/dd/yyyy") + "'";
                SQL += " GROUP BY reagent_inventory.ReagentTypeID";
                SQL += " ORDER BY QuantityUsed DESC";
                SqlCommand Command = new SqlCommand(SQL);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new ReagentType(Reader.GetInt16(0).ToString(), ConnectionString));
                        }
                    }
                }
            }

            return ReturnList;
        }
    }

    public struct StockLevel
    {
        public string ItemCount { get; set; }
        public string UnitsRemaining { get; set; }
    }

    public class Reagent : ReagentType
    {
        public string ItemID { get; set; }
        public string UnitsRemaining { get; set; }
        public string LotNumber { get; set; }
        public ReagentStatus Status { get; set; }
        public string OrderDate { get; set; }
        public string ReceiveDate { get; set; }
        public string FinishDate { get; set; }
        public string Comment { get; set; }
        public string ReceivedBy { get; set; }
        public string Location { get; set; }
        public string ExpiryDate { get; set; }
        public decimal PurchaseCost { get; set; }
        public decimal PurchaseCostPerUnit { get; set; }
        public string OrderedBy { get; set; }
        public string PurchaseCostCentre { get; set; }
        
        public Reagent(string ConnectionString) 
        {
            CONNECTION_STRING = ConnectionString;
        }

        public Reagent(string ExistingReagentItemID, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            ItemID = ExistingReagentItemID;
            FetchDetails();
        }

        public Reagent(ReagentType RT, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            TypeID = RT.TypeID;
            Name = RT.Name;
            Manufacturer = RT.Manufacturer;
            Supplier = RT.Supplier;
            UnitsPerItem = RT.UnitsPerItem;
            PartNumber = RT.PartNumber;
            LastOrdered = RT.LastOrdered;
            OrderThreshold = RT.OrderThreshold;
            PurchaseCost = RT.Cost + (RT.Cost * (RT.MarkUp / 100));
        }

        new private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT *, CONVERT(varchar,OrderDate,103) AS OrderDateFormatted,CONVERT(varchar,ReceiveDate,103) AS ReceiveDateFormatted, CONVERT(varchar,FinishDate,103) AS FinishDateFormatted FROM reagent_inventory INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID WHERE ItemID=@ItemID");
                Command.Parameters.AddWithValue("@ItemID", ItemID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        TypeID = Reader["ReagentTypeID"].ToString();
                        Name = Reader["Name"].ToString();
                        Manufacturer = Reader["Manufacturer"].ToString();
                        Supplier = Reader["Supplier"].ToString();
                        UnitsPerItem = Reader["UnitsPerItem"].ToString();
                        UnitOfSale = Reader["UnitOfSale"].ToString();
                        PartNumber = Reader["PartNumber"].ToString();
                        LastOrdered = Reader["LastOrdered"].ToString();
                        OrderThreshold = Reader["OrderThreshold"].ToString();
                        UnitsRemaining = Reader["UnitsRemaining"].ToString();
                        LotNumber = Reader["LotNumber"].ToString();
                        Status = (ReagentStatus)int.Parse(Reader["Status"].ToString());
                        Cost = Reader.GetDecimal(Reader.GetOrdinal("Cost"));
                        CostPerUnit = Reader.GetDecimal(Reader.GetOrdinal("CostPerUnit"));
                        OrderDate = Reader["OrderDateFormatted"].ToString();
                        ReceiveDate = Reader["ReceiveDateFormatted"].ToString();
                        FinishDate = Reader["FinishDateFormatted"].ToString();
                        Comment = Reader["Comment"].ToString();
                        ReceivedBy = Reader["ReceivedBy"].ToString();
                        Location = Reader["Location"].ToString();
                        ExpiryDate = Reader["ExpiryDate"].ToString();
                        PurchaseCost = Reader.IsDBNull(Reader.GetOrdinal("PurchaseCost")) ? 0 : Reader.GetDecimal(Reader.GetOrdinal("PurchaseCost"));
                        PurchaseCostPerUnit = Reader.GetDecimal(Reader.GetOrdinal("PurchaseCostPerUnit"));
                        PurchaseCostCentre = Reader.IsDBNull(Reader.GetOrdinal("PurchaseCostCentre")) ? String.Empty : Reader.GetString(Reader.GetOrdinal("PurchaseCostCentre"));
                        OrderedBy = Reader.IsDBNull(Reader.GetOrdinal("OrderedBy")) ? String.Empty : Reader.GetString(Reader.GetOrdinal("OrderedBy"));
                        InfiniteResource = Reader.GetBoolean(Reader.GetOrdinal("InfiniteResource"));                        
 
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("Fetching Reagent Details Failed", Err);
                }
            }
        }

        new public bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO reagent_inventory (ReagentTypeID, Status,OrderDate,UnitsRemaining, PurchaseCost, PurchaseCostCentre, OrderedBy) VALUES (@TypeID, @Status,GETDATE(),@UnitsPerItem, @PurchaseCost, @PurchaseCostCentre, @OrderedBy)");
                Command.Parameters.AddWithValue("@TypeID", TypeID);
                Command.Parameters.AddWithValue("@Status", ReagentStatus.Ordered);
                Command.Parameters.AddWithValue("@UnitsPerItem", UnitsPerItem);
                Command.Parameters.AddWithValue("@PurchaseCost", Cost);
                Command.Parameters.AddWithValue("@PurchaseCostCentre", PurchaseCostCentre);
                Command.Parameters.AddWithValue("@OrderedBy", OrderedBy);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        Connection.Close();
                        return true;
                    }
                    else
                    {
                        Connection.Close();
                        throw new Exception("Reagent Inventory Insertion Query Returned Wrong Number of Affected Values");
                    }
                }
                catch (Exception Err)
                {
                    Connection.Close();
                    throw new Exception("Reagent Inventory Insertion Query Failed", Err);
                }
            }
        }

        public bool Write(SqlConnection Connection, SqlTransaction Transaction)
        {
            SqlCommand Command = new SqlCommand("INSERT INTO reagent_inventory (ReagentTypeID, Status,OrderDate,UnitsRemaining, PurchaseCost, PurchaseCostCentre, OrderedBy) VALUES (@TypeID, @Status,GETDATE(),@UnitsPerItem, @PurchaseCost, @PurchaseCostCentre, @OrderedBy)");
            Command.Parameters.AddWithValue("@TypeID", TypeID);
            Command.Parameters.AddWithValue("@Status", ReagentStatus.Ordered);
            Command.Parameters.AddWithValue("@UnitsPerItem", UnitsPerItem);
            Command.Parameters.AddWithValue("@PurchaseCost", PurchaseCost);
            Command.Parameters.AddWithValue("@PurchaseCostCentre", PurchaseCostCentre);
            Command.Parameters.AddWithValue("@OrderedBy", OrderedBy);
            Command.Connection = Connection;
            Command.Transaction = Transaction;

            try
            {
                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Reagent Inventory Insertion Query Returned Wrong Number of Affected Values");
                }

            }
            catch (Exception Err)
            {
                throw new Exception("Reagent Inventory Insertion Query Failed", Err);
            }
        }

        new public bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("UPDATE reagent_inventory SET Status=@Status,OrderDate=@OrderDate, OrderedBy=@OrderedBy, ReceiveDate=@ReceiveDate, FinishDate=@FinishDate, LotNumber=@LotNumber, UnitsRemaining=@UnitsRemaining, Comment=@Comment, ReceivedBy=@ReceivedBy, PurchaseCost=@PurchaseCost, PurchaseCostCentre=@PurchaseCostCentre WHERE ItemID=@ItemID");
                Command.Parameters.AddWithValue("@ItemID", ItemID);
                Command.Parameters.AddWithValue("@Status", Status);
                Command.Parameters.AddWithValue("@OrderDate", String.IsNullOrEmpty(OrderDate) ? (object)DBNull.Value : AusToSQLDateConvert(OrderDate)); 
                Command.Parameters.AddWithValue("@ReceiveDate", String.IsNullOrEmpty(ReceiveDate) ? (object)DBNull.Value : AusToSQLDateConvert(ReceiveDate));
                Command.Parameters.AddWithValue("@FinishDate", String.IsNullOrEmpty(FinishDate)?(object)DBNull.Value:AusToSQLDateConvert(FinishDate));
                Command.Parameters.AddWithValue("@LotNumber", String.IsNullOrEmpty(LotNumber) ? (object)DBNull.Value : LotNumber); 
                Command.Parameters.AddWithValue("@UnitsRemaining", UnitsRemaining);
                Command.Parameters.AddWithValue("@Comment", String.IsNullOrEmpty(Comment) ? (object)DBNull.Value : Comment);
                Command.Parameters.AddWithValue("@ReceivedBy", String.IsNullOrEmpty(ReceivedBy) ? (object)DBNull.Value : ReceivedBy); 
                Command.Parameters.AddWithValue("@PurchaseCost", PurchaseCost);
                Command.Parameters.AddWithValue("@PurchaseCostCentre", PurchaseCostCentre);
                Command.Parameters.AddWithValue("@OrderedBy", OrderedBy);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                       
                        throw new Exception("Reagent Inventory Update Query Returned Wrong Number of Affected Rows");
                    }
                }
                catch (Exception Err)
                {
                    string InnerMessage = ""; 
                    while (Err != null)
                    {
                        InnerMessage += Err.Message + ":";
                        Err = Err.InnerException;
                    }
                    throw new Exception("Reagent Inventory Update Query Failed:" + InnerMessage, Err);
                }
            }        
        }

        public bool Update(SqlConnection Connection, SqlTransaction Transaction)
        {

            SqlCommand Command = new SqlCommand("UPDATE reagent_inventory SET Status=@Status,OrderDate=@OrderDate, ReceiveDate=@ReceiveDate, FinishDate=@FinishDate, LotNumber=@LotNumber, UnitsRemaining=@UnitsRemaining, Comment=@Comment, ReceivedBy=@ReceivedBy, PurchaseCost=@PurchaseCost, PurchaseCostCentre=@PurchaseCostCentre, OrderedBy=@OrderedBy WHERE ItemID=@ItemID");
            Command.Parameters.AddWithValue("@ItemID", ItemID);
            Command.Parameters.AddWithValue("@Status", Status);
            Command.Parameters.AddWithValue("@OrderDate", String.IsNullOrEmpty(OrderDate) ? (object)DBNull.Value : AusToSQLDateConvert(OrderDate));
            Command.Parameters.AddWithValue("@ReceiveDate", String.IsNullOrEmpty(ReceiveDate) ? (object)DBNull.Value : AusToSQLDateConvert(ReceiveDate));
            Command.Parameters.AddWithValue("@FinishDate", String.IsNullOrEmpty(FinishDate) ? (object)DBNull.Value : AusToSQLDateConvert(FinishDate));
            Command.Parameters.AddWithValue("@LotNumber", String.IsNullOrEmpty(LotNumber) ? (object)DBNull.Value : LotNumber);
            Command.Parameters.AddWithValue("@UnitsRemaining", UnitsRemaining.Replace(",", "."));
            Command.Parameters.AddWithValue("@Comment", String.IsNullOrEmpty(Comment) ? (object)DBNull.Value : Comment);
            Command.Parameters.AddWithValue("@ReceivedBy", String.IsNullOrEmpty(ReceivedBy) ? (object)DBNull.Value : ReceivedBy);
            Command.Parameters.AddWithValue("@PurchaseCost", PurchaseCost);
            Command.Parameters.AddWithValue("@PurchaseCostCentre", PurchaseCostCentre);
            Command.Parameters.AddWithValue("@OrderedBy", OrderedBy);


            Command.Connection = Connection;
            Command.Transaction = Transaction;

            try
            {
                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Reagent Inventory Update Query Returned Wrong Number of Affected Rows");
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Reagent Inventory Update Query Failed:"+Err.InnerException.Message, Err);
            }

        }

        public static bool CancelOrder(string ItemID, string ConnectionString)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {

                SqlCommand Command = new SqlCommand("DELETE FROM reagent_inventory WHERE ItemID=@ItemID");
                Command.Parameters.AddWithValue("@ItemID", ItemID);
                Command.Connection = Connection;

                //Kit Reagents are deleted by Foreign Key Cascade

                try
                {
                    Connection.Open();

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Cancel Reagent Order Command Returned Wrong Number of Affected Rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Cancel Reagent Order Command Failed", Err);
                }
            }
        }

        public bool Receive(string ReceivedBy)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("UPDATE reagent_inventory SET LotNumber=@LotNumber, ExpiryDate=@ExpiryDate, ReceiveDate=GETDATE(), ReceivedBy=@ReceivedBy,  Status=@Status WHERE ItemID=@ItemID");
                Command.Parameters.AddWithValue("@Status", ReagentStatus.Received);
                Command.Parameters.AddWithValue("@LotNumber", LotNumber);
                Command.Parameters.AddWithValue("@ExpiryDate", (String.IsNullOrEmpty(ExpiryDate))?(object)DBNull.Value:AusToSQLDateConvert(ExpiryDate));
                Command.Parameters.AddWithValue("@ReceivedBy", ReceivedBy);
                Command.Parameters.AddWithValue("@ItemID", ItemID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Reagent receive Query Returned Wrong number of Affected Rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Reagent Receive Query Failed", Err);
                }

            }
        }

        public static List<Reagent> GetReagentsByStatus(ReagentStatus Status, string ConnectionString)
        {
            List<Reagent> ReturnList = new List<Reagent>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand OrderedReagentsCommand = new SqlCommand("SELECT ItemID FROM reagent_inventory WHERE Status=@Status");
                OrderedReagentsCommand.Parameters.AddWithValue("@Status", (int)Status);
                OrderedReagentsCommand.Connection = Connection;

                try
                {
                    Connection.Open();

                    SqlDataReader Reader = OrderedReagentsCommand.ExecuteReader();

                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Reagent(Reader["ItemID"].ToString(),ConnectionString));
                        }
                    }

                    return ReturnList;
                }
                catch (Exception Err)
                {
                    throw new Exception("Error fetching list of reagents by status", Err);
                }
            }
        }

        public bool Use(double Units)
        {
                using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
                {
                    Connection.Open();
                    return this.Use(Units, Connection, null);
                }
        }

        public bool Use(double Units, SqlConnection Connection, SqlTransaction Transaction)
        {
            try
            {
                if (!this.InfiniteResource)
                {
                    double unitsremaining = double.Parse(this.UnitsRemaining) - Units;

                    if (unitsremaining >= 0)
                    {
                        this.UnitsRemaining = unitsremaining.ToString();
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Units", "The number of units requested exceeds the amount remaining for this item.");
                    }

                    if (Status == ReagentStatus.Finished
                      || Status == ReagentStatus.Faulty
                      || Status == ReagentStatus.Ordered)
                    {
                        throw new InvalidOperationException("The requested reagent is not available for use. It may be finished, faulty, or still on order");
                    }

                    if (Status == ReagentStatus.Received)
                    {
                        Status = ReagentStatus.Open;
                    }

                    if (Status == ReagentStatus.Open && unitsremaining == 0)
                    {
                        Status = ReagentStatus.Finished;
                    }
                }
                else
                {
                    Status = ReagentStatus.Open;
                }



                SqlCommand Command = new SqlCommand("UPDATE reagent_inventory SET UnitsRemaining=@UnitsRemaining, Status=@Status, FinishDate=@FinishDate WHERE ItemID=@ItemID");
                Command.Parameters.AddWithValue("@UnitsRemaining", UnitsRemaining.Replace(",", "."));
                Command.Parameters.AddWithValue("@ItemID", ItemID);
                Command.Parameters.AddWithValue("@Status", Status);
                Command.Parameters.AddWithValue("@FinishDate", (Status == ReagentStatus.Finished) ? DateTime.Now : (object)DBNull.Value);
                Command.Connection = Connection;
                Command.Transaction = Transaction;

                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Use Command Returned Wrong Number of Affected Rows");
                }
            }
            catch (ArgumentOutOfRangeException Err)
            {
                throw Err;
            }
            catch (Exception Err)
            {
                throw new Exception("Use Command Failed", Err);
            }
        }

        public bool SignOut(Guid UserID, string CostCentreID, double Quantity, SqlConnection Connection, SqlTransaction Transaction, string ApplicationName)
        {
            try
            {
                if (this.Use(Quantity, Connection, Transaction))
                {
                    SqlCommand Command = new SqlCommand("INSERT INTO ReagentSignout (ItemID,UserID,Quantity,Date,CostCentreID, CreditorCostCentre) VALUES (@ItemID,@UserID,@Quantity,GETDATE(),@CostCentreID, @CreditorCostCentre)");
                    Command.Parameters.AddWithValue("@ItemID", ItemID);
                    Command.Parameters.AddWithValue("@UserID", UserID);
                    Command.Parameters.AddWithValue("@Quantity", Quantity);
                    Command.Parameters.AddWithValue("@CostCentreID", CostCentreID);
                    Command.Parameters.AddWithValue("@CreditorCostCentre", PurchaseCostCentre);
                    
                    Command.Connection = Connection;
                    Command.Transaction = Transaction;

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        try
                        {
                            this.CheckStockLevels(Connection,Transaction, ApplicationName);
                        }
                        catch
                        {
                            throw new Exception("Stock Level Check Failed");
                        }
                        return true;
                    }
                    else
                    {
                        throw new Exception("SignOut Command Returned Wrong Number of Affected Rows");
                    }
                }
                else
                {
                    throw new Exception("Use command returned false");
                }
            }
            catch (ArgumentOutOfRangeException Err)
            {
                throw Err;
            }
            catch (Exception Err)
            {
                if (Err.Message == "Stock Level Check Failed")
                {
                    throw Err;
                }
                else
                {
                    Transaction.Rollback();
                    throw new Exception("Signout Command Failed", Err);
                }
            }
        }

        public bool SignOut(Guid UserID, string CostCentreID, double Quantity, string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                Connection.Open();
                SqlTransaction Transaction = Connection.BeginTransaction();
                try
                {
                    SignOut(UserID, CostCentreID, Quantity, Connection, Transaction, ApplicationName);
                }
                catch (Exception Err)
                {
                    if (Err.Message == "Stock Level Check Failed")
                    {
                        Transaction.Commit();
                        throw Err;
                    }
                    else
                    {
                        throw Err;
                    }
                }
                Transaction.Commit();
                return true;
            }
        }

        public static SqlDataAdapter GetDataAdaptor(string ConnectionString)
        {
            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            
            //Select Command
            DataAdapter.SelectCommand = new SqlCommand("SELECT [ItemID], [Name],[Manufacturer],[Supplier],[PartNumber], [PurchaseCost] AS Cost, [PurchaseCostPerUnit] AS CPU, [LotNumber], CONVERT(varchar,ExpiryDate,103) AS ExpiryDate, [UnitsRemaining],[reagent_inventory].[Status], CONVERT(varchar,OrderDate,103) AS [OrderDate],[OrderedBy],[PurchaseCostCentre] AS PCC, CONVERT(varchar,ReceiveDate,103) AS [ReceiveDate], [ReceivedBy] ,CONVERT(varchar,FinishDate,103) AS [FinishDate], [Comment] FROM reagent_inventory INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID");
            DataAdapter.SelectCommand.Connection = Connection;
            
            //Delete Command
            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM reagent_inventory WHERE ItemID=@ItemID");
            DataAdapter.DeleteCommand.Connection = Connection;
            DataAdapter.DeleteCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int));
            DataAdapter.DeleteCommand.Parameters["@ItemID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.DeleteCommand.Parameters["@ItemID"].SourceColumn = "ItemID";

            //Update Command
            DataAdapter.UpdateCommand = new SqlCommand("UPDATE reagent_inventory SET LotNumber=@LotNumber, UnitsRemaining=@UnitsRemaining, Status=@Status, OrderDate=@OrderDate, OrderedBy=@OrderedBy, ReceiveDate=@ReceiveDate, FinishDate=@FinishDate, Comment=@Comment, ReceivedBy=@ReceivedBy, ExpiryDate=@ExpiryDate, PurchaseCost=@PurchaseCost, PurchaseCostCentre=@PurchaseCostCentre WHERE ItemID=@ItemID");
            DataAdapter.UpdateCommand.Connection = Connection;
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceColumn = "ItemID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@LotNumber", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@LotNumber"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@LotNumber"].SourceColumn = "LotNumber";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@UnitsRemaining", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@UnitsRemaining"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@UnitsRemaining"].SourceColumn = "UnitsRemaining";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.TinyInt));
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceColumn = "Status";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrderDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@OrderDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@OrderDate"].SourceColumn = "OrderDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReceiveDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@ReceiveDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ReceiveDate"].SourceColumn = "ReceiveDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@FinishDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@FinishDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@FinishDate"].SourceColumn = "FinishDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Comment", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@Comment"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Comment"].SourceColumn = "Comment";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@ReceivedBy"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ReceivedBy"].SourceColumn = "ReceivedBy";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@ExpiryDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ExpiryDate"].SourceColumn = "ExpiryDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@PurchaseCost", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@PurchaseCost"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@PurchaseCost"].SourceColumn = "PurchaseCost";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@PurchaseCostCentre", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@PurchaseCostCentre"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@PurchaseCostCentre"].SourceColumn = "PCC";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrderedBy", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@OrderedBy"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@OrderedBy"].SourceColumn = "OrderedBy";
            
            return DataAdapter;            
        }


        public static SqlDataAdapter GetDataAdaptor(string Filter, string ConnectionString)
        {
            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlDataAdapter DataAdapter = new SqlDataAdapter();

            //Select Command
            DataAdapter.SelectCommand = new SqlCommand("SELECT [ItemID], [Name],[Manufacturer],[Supplier],[PartNumber], CONVERT(DECIMAL(9,2),[PurchaseCost]) AS Cost, CONVERT(DECIMAL(9,2),[PurchaseCostPerUnit]) AS CPU, [LotNumber], CONVERT(varchar,ExpiryDate,103) AS ExpiryDate, [UnitsRemaining],[reagent_inventory].[Status], CONVERT(varchar,OrderDate,103) AS [OrderDate],[OrderedBy],[PurchaseCostCentre] AS PCC, CONVERT(varchar,ReceiveDate,103) AS [ReceiveDate], [ReceivedBy] ,CONVERT(varchar,FinishDate,103) AS [FinishDate], [Comment] FROM reagent_inventory INNER JOIN reagent_types ON reagent_inventory.ReagentTypeID=reagent_types.ReagentTypeID WHERE " + Filter);
            DataAdapter.SelectCommand.Connection = Connection;

            //Delete Command
            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM reagent_inventory WHERE ItemID=@ItemID");
            DataAdapter.DeleteCommand.Connection = Connection;
            DataAdapter.DeleteCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int));
            DataAdapter.DeleteCommand.Parameters["@ItemID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.DeleteCommand.Parameters["@ItemID"].SourceColumn = "ItemID";

            //Update Command
            DataAdapter.UpdateCommand = new SqlCommand("UPDATE reagent_inventory SET LotNumber=@LotNumber, UnitsRemaining=@UnitsRemaining, Status=@Status, OrderDate=@OrderDate, OrderedBy=@OrderedBy, ReceiveDate=@ReceiveDate, FinishDate=@FinishDate, Comment=@Comment, ReceivedBy=@ReceivedBy, ExpiryDate=@ExpiryDate, PurchaseCost=@PurchaseCost, PurchaseCostCentre=@PurchaseCostCentre  WHERE ItemID=@ItemID");
            DataAdapter.UpdateCommand.Connection = Connection;
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceColumn = "ItemID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@LotNumber", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@LotNumber"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@LotNumber"].SourceColumn = "LotNumber";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@UnitsRemaining", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@UnitsRemaining"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@UnitsRemaining"].SourceColumn = "UnitsRemaining";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.TinyInt));
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Status"].SourceColumn = "Status";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrderDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@OrderDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@OrderDate"].SourceColumn = "OrderDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReceiveDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@ReceiveDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ReceiveDate"].SourceColumn = "ReceiveDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@FinishDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@FinishDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@FinishDate"].SourceColumn = "FinishDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Comment", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@Comment"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Comment"].SourceColumn = "Comment";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ReceivedBy", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@ReceivedBy"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ReceivedBy"].SourceColumn = "ReceivedBy";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ExpiryDate", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@ExpiryDate"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ExpiryDate"].SourceColumn = "ExpiryDate";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@PurchaseCost", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@PurchaseCost"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@PurchaseCost"].SourceColumn = "Cost";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@PurchaseCostCentre", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@PurchaseCostCentre"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@PurchaseCostCentre"].SourceColumn = "PCC";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@OrderedBy", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@OrderedBy"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@OrderedBy"].SourceColumn = "OrderedBy";
            
            return DataAdapter;
        }

        public bool ValidateLotNumber(string LotNumber)
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT ItemID FROM Reagent_inventory WHERE LotNumber=@LotNumber AND Status IN (1,2,3,4) ORDER BY Reagent_inventory.ReceiveDate DESC, Status ASC");
                Command.Parameters.AddWithValue("@LotNumber", LotNumber);
                Command.Connection = Connection;
                SqlDataReader Reader;

                try
                {
                    Connection.Open();
                    Reader = Command.ExecuteReader();
                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        this.ItemID = Reader["ItemID"].ToString();
                        this.LotNumber = LotNumber;
                        FetchDetails();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Lot Number Validation Failed", Err);
                }
            }
        }
    }

    public class SignOutEvent
    {
        public int EventID { get; set; }
        public Reagent Item { get; set; }
        public Guid UserID { get; set; }
        public decimal Quantity { get; set; }
        public string Date { get; set; }
        public string CostCentreID { get; set; }
        public string CreditorCostCentre { get; set; }
        public bool Billed { get; set; }
        private string CONNECTION_STRING;

        public SignOutEvent(int EventID, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            this.EventID = EventID;
            FetchDetails();    
        }

        public SignOutEvent(string ItemID, string UserID, decimal Quantity, string Date, string CostCentreID, string CreditorCostCentre, bool Billed, string ConnectionString)
        {
            CONNECTION_STRING = ConnectionString;
            this.Item = new Reagent(ItemID, ConnectionString);
            this.UserID = new Guid(UserID);
            this.Quantity = Quantity;
            this.Date = Date;
            this.CostCentreID = CostCentreID;
            this.CreditorCostCentre = CreditorCostCentre;
            this.Billed = Billed;
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT [ItemID],[UserID],[Quantity],[Date],[CostCentreID],[Billed], [CreditorCostCentre] FROM ReagentSignout WHERE EventID=@EventID");
                Command.Parameters.AddWithValue("@EventID", EventID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            this.Item = new Reagent(Reader.GetInt32(0).ToString(), CONNECTION_STRING);
                            this.UserID = Reader.GetGuid(1);
                            this.Quantity = Reader.GetDecimal(2);
                            this.Date = Reader.GetDateTime(3).ToString();
                            this.CostCentreID = Reader.GetString(4);
                            this.Billed = Reader.GetBoolean(5);
                            this.CreditorCostCentre = Reader.GetString(6);

                        }
                        else
                        {
                            throw new Exception("Supplied EventID returned no rows");
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching signout event details", Err);
                }
                
            }
        }

        public bool MarkBilled()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("UPDATE ReagentSignout SET Billed=1 WHERE EventID=@EventID");
                Command.Parameters.AddWithValue("@EventID", EventID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        Billed = true;
                        return true;
                    }
                    else
                    {
                        throw new Exception("Mark billed query return wrong number of affected rows");
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while marking signout event as billed", Err);
                }
            }
        }

        public bool MarkUnBilled()
        {
            using (SqlConnection Connection = new SqlConnection(CONNECTION_STRING))
            {
                SqlCommand Command = new SqlCommand("UPDATE ReagentSignout SET Billed=0 WHERE EventID=@EventID");
                Command.Parameters.AddWithValue("@EventID", EventID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        Billed = false;
                        return true;
                    }
                    else
                    {
                        throw new Exception("Mark unbilled query return wrong number of affected rows");
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while marking signout event as unbilled", Err);
                }
            }
        }

        public static SqlDataAdapter GetDataAdaptor(string Filter, string ConnectionString)
        {
            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlDataAdapter DataAdapter = new SqlDataAdapter();

            //Select Command
            DataAdapter.SelectCommand = new SqlCommand("SELECT [EventID],[ItemID],[UserID],[Quantity],[Date],[CostCentreID],[Billed],[CreditorCostCentre] FROM ReagentSignout WHERE " + Filter);
            DataAdapter.SelectCommand.Connection = Connection;

            //Delete Command
            DataAdapter.DeleteCommand = new SqlCommand("DELETE FROM ReagentSignout WHERE EventID=@EventID");
            DataAdapter.DeleteCommand.Connection = Connection;
            DataAdapter.DeleteCommand.Parameters.Add(new SqlParameter("@EventID", SqlDbType.Int));
            DataAdapter.DeleteCommand.Parameters["@EventID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.DeleteCommand.Parameters["@EventID"].SourceColumn = "EventID";

            //Update Command
            DataAdapter.UpdateCommand = new SqlCommand("UPDATE ReagentSignout SET [ItemID]=@ItemID,[UserID]=@UserID,[Quantity]=@Quantity,[Date]=@Date,[CostCentreID]=@CostCentreID,[Billed]=@Billed,[CreditorCostCentre]=@CreditorCostCentre  WHERE EventID=@EventID");
            DataAdapter.UpdateCommand.Connection = Connection;
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@EventID", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@EventID"].SourceVersion = DataRowVersion.Original;
            DataAdapter.UpdateCommand.Parameters["@EventID"].SourceColumn = "EventID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.Int));
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@ItemID"].SourceColumn = "ItemID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@UserID", SqlDbType.UniqueIdentifier));
            DataAdapter.UpdateCommand.Parameters["@UserID"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@UserID"].SourceColumn = "UserID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Decimal));
            DataAdapter.UpdateCommand.Parameters["@Quantity"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Quantity"].SourceColumn = "Quantity";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Date", SqlDbType.DateTime));
            DataAdapter.UpdateCommand.Parameters["@Date"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Date"].SourceColumn = "Date";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@CostCentreID", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@CostCentreID"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@CostCentreID"].SourceColumn = "CostCentreID";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@Billed", SqlDbType.Bit));
            DataAdapter.UpdateCommand.Parameters["@Billed"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@Billed"].SourceColumn = "Billed";
            DataAdapter.UpdateCommand.Parameters.Add(new SqlParameter("@CreditorCostCentre", SqlDbType.VarChar));
            DataAdapter.UpdateCommand.Parameters["@CreditorCostCentre"].SourceVersion = DataRowVersion.Current;
            DataAdapter.UpdateCommand.Parameters["@CreditorCostCentre"].SourceColumn = "CreditorCostCentre";

            return DataAdapter;
        }

        public static List<SignOutEvent> GetSignoutHistory(Guid UserID, string ConnectionString)
        {
            List<SignOutEvent> ReturnList = new List<SignOutEvent>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT [EventID] FROM ReagentSignout WHERE UserID=@UserID ORDER BY Date DESC");
                Command.Parameters.AddWithValue("@UserID", UserID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(new SignOutEvent (Reader.GetInt32(0), ConnectionString));
                            }
                        }
                       
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching signout history", Err);
                }

            }
            return ReturnList;
        }

        public static List<SignOutEvent> GetSignoutHistory(Guid UserID, string StartDate, string EndDate, string BillingStatus, string ConnectionString)
        {
            List<SignOutEvent> ReturnList = new List<SignOutEvent>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT [EventID] FROM ReagentSignout WHERE UserID=@UserID AND Date > @StartDate AND Date < @EndDate");
                if (BillingStatus == "Billed")
                {
                    Command.CommandText += " AND Billed=1";
                }
                else if (BillingStatus == "Unbilled")
                {
                    Command.CommandText += " AND Billed=0";                    
                }

                Command.Parameters.AddWithValue("@UserID", UserID);
                Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
                Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(new SignOutEvent(Reader.GetInt32(0), ConnectionString));
                            }
                        }

                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching signout history", Err);
                }

            }
            return ReturnList;
        }

        internal static List<Guid> GetActiveUsersList(string StartDate, string EndDate, string ConnectionString)
        {
            List<Guid> UserIDs = new List<Guid>();

            try
            {
                using (SqlConnection Connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand Command = new SqlCommand("SELECT DISTINCT UserID FROM ReagentSignout WHERE Date > @StartDate AND Date < @EndDate");
                    Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
                    Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
                    Command.Connection = Connection;
                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                UserIDs.Add(Reader.GetGuid(0));
                            }
                        }
                    }

                    return UserIDs;
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Fetching active user list by date range failed", Err);
            }
        }
    }

    public static List<object[]> GetCostCentreSummary(string StartDate, string EndDate, string BillingStatus, string ConnectionString)
    {
        List<object[]> ReturnList = new List<object[]>();

        using (SqlConnection Connection = new SqlConnection(ConnectionString))
        {
            SqlCommand Command = new SqlCommand("EXEC GetCostCentreSummary @StartDate, @EndDate, @BillingStatus");
            Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
            Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
            switch (BillingStatus)
            {
                case "All":
                    Command.Parameters.AddWithValue("@BillingStatus", DBNull.Value);
                    break;
                case "Billed":
                    Command.Parameters.AddWithValue("@BillingStatus", 1);
                    break;
                case "UnBilled":
                    Command.Parameters.AddWithValue("@BillingStatus", 0);
                    break;
            }
            Command.Connection = Connection;

            try
            {
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new object[] { Reader.GetString(0), Reader.GetString(1), Reader.GetDecimal(2) });
                        }
                        return ReturnList;
                    }
                    else
                    {
                        return ReturnList;
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("An Error Occurred While Fetching Cost Centre Summaries", Err);
            }
        }
    }

    public static List<SignOutEvent> GetCostCentreDetails(string DebtorCostCentre, string CreditorCostCentre, string StartDate, string EndDate, string BillingStatus, string ConnectionString)
    {
        List<SignOutEvent> ReturnList = new List<SignOutEvent>();
        using (SqlConnection Connection = new SqlConnection(ConnectionString))
        {
            SqlCommand Command = new SqlCommand("EXEC GetCostCentreDetails @DebtorCostCentre, @CreditorCostCentre, @StartDate, @EndDate, @BillingStatus");
            switch (DebtorCostCentre)
            {
                case "All":
                    Command.Parameters.AddWithValue("@DebtorCostCentre", DBNull.Value);
                    break;
                default:
                    Command.Parameters.AddWithValue("@DebtorCostCentre", DebtorCostCentre);
                    break;                        
            }

            switch (CreditorCostCentre)
            {
                case "All":
                    Command.Parameters.AddWithValue("@CreditorCostCentre", DBNull.Value);
                    break;
                default:
                    Command.Parameters.AddWithValue("@CreditorCostCentre", CreditorCostCentre);
                    break;
            }

            Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
            Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));

            switch (BillingStatus)
            {
                case "All":
                    Command.Parameters.AddWithValue("@BillingStatus", DBNull.Value);
                    break;
                case "Billed":
                    Command.Parameters.AddWithValue("@BillingStatus", 1);
                    break;
                case "UnBilled":
                    Command.Parameters.AddWithValue("@BillingStatus", 0);
                    break;
            }

            Command.Connection = Connection;

            try
            {
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new SignOutEvent(Reader.GetInt32(0), ConnectionString));
                        }
                        return ReturnList;
                    }
                    else
                    {
                        return ReturnList;
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching detailed cost centre summary", Err);
            }
            
        }
                
  
    }

    public static Dictionary<string, Dictionary<string, decimal>> GetReagentSummary(string StartDate, string EndDate, string ConnectionString)
    {
        Dictionary<string,Dictionary<string, decimal>> ReturnDictionary = new Dictionary<string,Dictionary<string, decimal>>();

        using (SqlConnection Connection = new SqlConnection(ConnectionString))
        {
            SqlCommand Command = new SqlCommand("EXEC GetReagentSummary @StartDate, @EndDate");
            Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
            Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));            
            Command.Connection = Connection;

            try
            {
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            if (!Reader.IsDBNull(0) && !Reader.IsDBNull(1))
                            {
                                string ExpenseCategory = Reader.GetString(0);
                                string CostCentre = Reader.GetString(1);
                                decimal Value = Reader.GetDecimal(2);
                                if (!ReturnDictionary.ContainsKey(ExpenseCategory))
                                {
                                    ReturnDictionary.Add(ExpenseCategory, new Dictionary<string, decimal>() { { CostCentre, Value } });
                                }
                                else
                                {
                                    ReturnDictionary[ExpenseCategory].Add(CostCentre, Value);
                                }
                            }
                            else
                            {
                                throw new Exception("Null Value in return");
                            }
                        }
                    }
                }
                return ReturnDictionary;
            }
            catch(Exception Err)
            {
                throw new Exception("An error occurred while fetching reagent summary", Err);
            }
        }
    }

    public static Dictionary<string, decimal[]> GetReagentDetails(string StartDate, string EndDate, string CostCentre, string Mode, string ConnectionString)
    {
        Dictionary<string, decimal[]> ReturnDictionary = new Dictionary<string, decimal[]>();

        using (SqlConnection Connection = new SqlConnection(ConnectionString))
        {
            SqlCommand Command = new SqlCommand("EXEC GetReagentDetails @StartDate, @EndDate, @CostCentre, @Mode");
            Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
            Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
            Command.Parameters.AddWithValue("@CostCentre", CostCentre);
            Command.Parameters.AddWithValue("@Mode", Mode);
            Command.Connection = Connection;

            try
            {
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            if (!Reader.IsDBNull(0) && !Reader.IsDBNull(1))
                            {
                                ReturnDictionary.Add(Reader.GetString(0), new decimal[] { Reader.GetDecimal(1), Reader.GetDecimal(2) });
                            }
                            else
                            {
                                throw new Exception("Null Value in return");
                            }
                        }
                    }
                }
                return ReturnDictionary;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching reagent summary", Err);
            }
        }
    }

    public static void CheckExpiryDates(DateTime MinExpiryDate, string ConnectionString, string ApplicationName)
    {
        List<Reagent> ExpiringReagents = new List<Reagent>();

        using (SqlConnection Connection = new SqlConnection(ConnectionString))
        {
            SqlCommand Command = new SqlCommand("SELECT ItemID FROM Reagent_Inventory WHERE ExpiryDate < @MinExpiryDate AND Status IN (1,2)");
            Command.Parameters.AddWithValue("@MinExpiryDate", MinExpiryDate.ToString("MM/dd/yyyy"));
            Command.Connection = Connection;

            try
            {
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ExpiringReagents.Add(new Reagent(Reader.GetInt32(0).ToString(), ConnectionString));                           
                        }

                        Users.ClientApplication App = new Users.ClientApplication(ApplicationName);
                        List<string> Recipients = App.GetAdminEmailAddresses();
                        if (Recipients.Count != 0)
                        {
                            string RecipientsString = string.Empty;
                            foreach (string Address in Recipients)
                            {
                                RecipientsString += Address + ",";
                            }
                            RecipientsString.TrimEnd(',');
                            string MessageText = "Dear Lab Staff, <br/><br/> The following reagents have already expired or will expire in the next 4 weeks: <br/><br/> "
                                                 +"<table CellPadding=\"5\"><tr><td>ItemID</td><td>ItemName</td><td>ExpiryDate</td></tr>";
                            foreach (Reagent R in ExpiringReagents)
                            {
                                MessageText += "<tr><td>" + R.ItemID + "</td><td>" + R.Name + "</td><td>" + R.ExpiryDate + "</td></tr>";
                            }
                            MessageText += "</table>. <br/><br/> This is an informational message only, no user action is required. <br/><br/> Kind Regards, <br/> The Reagent Management System";
                            Users.SendMail("Expiring Items ", RecipientsString, Settings.GetSetting("SystemAdminEmail", ConnectionString), MessageText, true);
                        }
                    }
                }
            }
            catch
            {
                //suppress
            }
        } 
    }
}
