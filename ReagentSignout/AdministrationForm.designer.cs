﻿namespace ReagentSignout
{
    partial class AdministrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdministrationForm));
            this.AdministrationTabs = new System.Windows.Forms.TabControl();
            this.ReagentTypesTab = new System.Windows.Forms.TabPage();
            this.ReagentTypesInfoPanel = new System.Windows.Forms.Panel();
            this.ReagentTypesRevertButton = new System.Windows.Forms.Button();
            this.ReagentTypesUpdateButton = new System.Windows.Forms.Button();
            this.ReagentTypesDataGridView = new System.Windows.Forms.DataGridView();
            this.OrdersTab = new System.Windows.Forms.TabPage();
            this.OrdersRevertButton = new System.Windows.Forms.Button();
            this.OrdersUpdateButton = new System.Windows.Forms.Button();
            this.OrdersInfoPanel = new System.Windows.Forms.Panel();
            this.OrdersDataGridView = new System.Windows.Forms.DataGridView();
            this.InventoryTab = new System.Windows.Forms.TabPage();
            this.InventoryRevertButton = new System.Windows.Forms.Button();
            this.InventoryUpdateButton = new System.Windows.Forms.Button();
            this.InventoryInfoPanel = new System.Windows.Forms.Panel();
            this.InventoryDataGridView = new System.Windows.Forms.DataGridView();
            this.HistoryTab = new System.Windows.Forms.TabPage();
            this.HistoryUpdateButton = new System.Windows.Forms.Button();
            this.HistoryDataGridView = new System.Windows.Forms.DataGridView();
            this.BudgetsTab = new System.Windows.Forms.TabPage();
            this.BudgetsTabControl = new System.Windows.Forms.TabControl();
            this.CostCentreSummaryTab = new System.Windows.Forms.TabPage();
            this.EmailUserReportsButton = new System.Windows.Forms.Button();
            this.ExportMasterReportButton = new System.Windows.Forms.Button();
            this.CostCentreSummaryDisplayButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CostCentreDetailsDataGridView = new System.Windows.Forms.DataGridView();
            this.EventID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Billed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CostCentreSummaryDataGridView = new System.Windows.Forms.DataGridView();
            this.DebtorCostCentre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreditorCostCentre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillableAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.UnBillButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BillingStatusGroupBox = new System.Windows.Forms.GroupBox();
            this.CostCentreSummaryAllBillingStatusRadioButton = new System.Windows.Forms.RadioButton();
            this.CostCentreSummaryBilledBillingStatusRadioButton = new System.Windows.Forms.RadioButton();
            this.CostCentreSummaryUnbilledBillingStatusRadioButton = new System.Windows.Forms.RadioButton();
            this.EndDateGroupBox = new System.Windows.Forms.GroupBox();
            this.CostCentreSummaryEndDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.StartDateGroupBox = new System.Windows.Forms.GroupBox();
            this.CostCentreSummaryStartDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.ReagentSummaryTab = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ReagentDetailsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ReagentSummaryDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReagentSummaryCostCentre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReagentSummaryDisplayButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ReagentSummaryEndDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ReagentSummaryStartDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.SignOutListTabPage = new System.Windows.Forms.TabPage();
            this.SignoutListExportButton = new System.Windows.Forms.Button();
            this.SignOutListRevertButton = new System.Windows.Forms.Button();
            this.SignOutListUpdateButton = new System.Windows.Forms.Button();
            this.SignOutListDisplayButton = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.SignOutListDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.SignoutListEndDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.SignoutListStartDateCalendar = new System.Windows.Forms.MonthCalendar();
            this.CategoriesTab = new System.Windows.Forms.TabPage();
            this.ReagentSetToolsGroupBox = new System.Windows.Forms.GroupBox();
            this.EditSetReagentsGroupBox = new System.Windows.Forms.GroupBox();
            this.EditSetDestinationDataGridView = new System.Windows.Forms.DataGridView();
            this.EditSetDestinationReagentTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditSetDestinationReagentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditSetDestinationReagentManufacturer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditSetDestinationUnits = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.EditSetComboBox = new System.Windows.Forms.ComboBox();
            this.UpdateSetReagentButton = new System.Windows.Forms.Button();
            this.RemoveSetReagentButton = new System.Windows.Forms.Button();
            this.AddSetReagentButton = new System.Windows.Forms.Button();
            this.EditSetSourceDataGridView = new System.Windows.Forms.DataGridView();
            this.EditSetSourceReagentTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditSetSourceReagentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditSetSourceManufacturer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteSetGroupBox = new System.Windows.Forms.GroupBox();
            this.DeleteSetComboBox = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.DeleteSetButton = new System.Windows.Forms.Button();
            this.CreateSetGroupBox = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.CreateSetButton = new System.Windows.Forms.Button();
            this.SetNameTextBox = new System.Windows.Forms.TextBox();
            this.CategoryToolsGroupBox = new System.Windows.Forms.GroupBox();
            this.SetCategoryRelGroupBox = new System.Windows.Forms.GroupBox();
            this.SetCategoryLinkUnLinkButton = new System.Windows.Forms.Button();
            this.SetCategoryLinkLinkButton = new System.Windows.Forms.Button();
            this.SetCategoryLinkCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.SetCategoryLinkSetComboBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.AddCategoryGroupBox = new System.Windows.Forms.GroupBox();
            this.NewCategoryCreateButton = new System.Windows.Forms.Button();
            this.NewCategoryParentComboBox = new System.Windows.Forms.ComboBox();
            this.NewCategoryParentLabel = new System.Windows.Forms.Label();
            this.NewCategoryNameTextBox = new System.Windows.Forms.TextBox();
            this.NewCategoryNameLabel = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DeleteCategoryButton = new System.Windows.Forms.Button();
            this.DeleteCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ReagentCategoryRelGroupBox = new System.Windows.Forms.GroupBox();
            this.ReagentCategoryLinkUnLinkButton = new System.Windows.Forms.Button();
            this.ReagentCategoryLinkLinkButton = new System.Windows.Forms.Button();
            this.ReagentCategoryLinkCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.ReagentCategoryLinkCategoryLabel = new System.Windows.Forms.Label();
            this.ReagentCategoryLinkReagentComboBox = new System.Windows.Forms.ComboBox();
            this.ReagentCategoryLinkReagentLabel = new System.Windows.Forms.Label();
            this.CategoriesInfoPanel = new System.Windows.Forms.Panel();
            this.ManageUsersTab = new System.Windows.Forms.TabPage();
            this.tc_ManageUsers = new System.Windows.Forms.TabControl();
            this.tab_Users = new System.Windows.Forms.TabPage();
            this.gb_UserCredentials = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btn_UserCredentialAdd = new System.Windows.Forms.Button();
            this.cmb_CreateCredentialApplication = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_ApplicationDescription = new System.Windows.Forms.Label();
            this.cmb_CreateCredentialInstitute = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmb_CreateCredentialRole = new System.Windows.Forms.ComboBox();
            this.cmb_CreateCredentialLab = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gb_UserCredentialsExisting = new System.Windows.Forms.GroupBox();
            this.pnl_UserCredentialsExisting = new System.Windows.Forms.Panel();
            this.CreateUserGroupBox = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btn_UserUpdatePassword = new System.Windows.Forms.Button();
            this.tb_UserUpdatePassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmb_UserSelect = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_UserInstitute = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_UserDelete = new System.Windows.Forms.Button();
            this.btn_UserUpdate = new System.Windows.Forms.Button();
            this.btn_UserCreate = new System.Windows.Forms.Button();
            this.cmb_UserLab = new System.Windows.Forms.ComboBox();
            this.chk_IsLabHead = new System.Windows.Forms.CheckBox();
            this.tb_UserExtension = new System.Windows.Forms.TextBox();
            this.tb_UserEmail = new System.Windows.Forms.TextBox();
            this.tb_UserLastName = new System.Windows.Forms.TextBox();
            this.tb_UserFirstName = new System.Windows.Forms.TextBox();
            this.lbl_IsLabHead = new System.Windows.Forms.Label();
            this.LabLabel = new System.Windows.Forms.Label();
            this.ExtLabel = new System.Windows.Forms.Label();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.tab_CostCentres = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkb_CostCentreRetired = new System.Windows.Forms.CheckBox();
            this.btn_CostCentreDelete = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_CostCentre = new System.Windows.Forms.ComboBox();
            this.btn_CostCentreUpdate = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cmb_CostCentreInstitute = new System.Windows.Forms.ComboBox();
            this.tb_CostCentreDescription = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_CostCentreCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmb_CostCentreLab = new System.Windows.Forms.ComboBox();
            this.btn_CostCentreCreate = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.tab_LabsInstitutes = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.pnl_LabMembers = new System.Windows.Forms.Panel();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.cmb_LabSelect = new System.Windows.Forms.ComboBox();
            this.btn_LabDelete = new System.Windows.Forms.Button();
            this.btn_LabUpdate = new System.Windows.Forms.Button();
            this.btn_LabCreate = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmb_LabInstitute = new System.Windows.Forms.ComboBox();
            this.tb_LabName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CreateLabGroupBox = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tb_InstituteDeliveryAddress = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tb_InstituteBillingAddress = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btn_InstituteDelete = new System.Windows.Forms.Button();
            this.btn_InstituteUpdate = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.chk_InstituteCurrentSite = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmb_InstituteSelect = new System.Windows.Forms.ComboBox();
            this.btn_InstituteCreate = new System.Windows.Forms.Button();
            this.tb_InstituteName = new System.Windows.Forms.TextBox();
            this.CreateLabLabNameLabel = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.DeleteCostCentreComboBox = new System.Windows.Forms.ComboBox();
            this.DeleteCostCentreButton = new System.Windows.Forms.Button();
            this.DeleteCostCentreLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.DeleteLabComboBox = new System.Windows.Forms.ComboBox();
            this.DeleteLabButton = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.ConfigTabPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.btn_SMTPUpdate = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.tb_SMTPServer = new System.Windows.Forms.TextBox();
            this.DBConfigGroupBox = new System.Windows.Forms.GroupBox();
            this.DatabaseConfigEditButton = new System.Windows.Forms.Button();
            this.AdministrationExitButton = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn4 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.monthCalendar2 = new System.Windows.Forms.MonthCalendar();
            this.monthCalendar3 = new System.Windows.Forms.MonthCalendar();
            this.ShowIDCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowNameCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowManufacturerCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowLastOrderedCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowPartNumberCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowSupplierCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowUnitofSaleCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowCPUCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowCostCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowUnitsPerItemCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowOrderThresholdCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowStatusCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowReceiveDateCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowOrderedByCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowOrderDateCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowUnitsRemainingCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowExpiryDateCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowLotNumberCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowPCCCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowCommentCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowFinishDateCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowReceivedByCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowInfiniteResourceCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowMarkUpCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowDefaultPCCCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowUseAsCheckBox = new System.Windows.Forms.CheckBox();
            this.ExportMasterFileDialogBox = new System.Windows.Forms.SaveFileDialog();
            this.ExportSignoutListFileDialogBox = new System.Windows.Forms.SaveFileDialog();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_SMTPPort = new System.Windows.Forms.TextBox();
            this.AdministrationTabs.SuspendLayout();
            this.ReagentTypesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReagentTypesDataGridView)).BeginInit();
            this.OrdersTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersDataGridView)).BeginInit();
            this.InventoryTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InventoryDataGridView)).BeginInit();
            this.HistoryTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDataGridView)).BeginInit();
            this.BudgetsTab.SuspendLayout();
            this.BudgetsTabControl.SuspendLayout();
            this.CostCentreSummaryTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreDetailsDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreSummaryDataGridView)).BeginInit();
            this.BillingStatusGroupBox.SuspendLayout();
            this.EndDateGroupBox.SuspendLayout();
            this.StartDateGroupBox.SuspendLayout();
            this.ReagentSummaryTab.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReagentDetailsDataGridView)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReagentSummaryDataGridView)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SignOutListTabPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SignOutListDataGridView)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.CategoriesTab.SuspendLayout();
            this.ReagentSetToolsGroupBox.SuspendLayout();
            this.EditSetReagentsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditSetDestinationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditSetSourceDataGridView)).BeginInit();
            this.DeleteSetGroupBox.SuspendLayout();
            this.CreateSetGroupBox.SuspendLayout();
            this.CategoryToolsGroupBox.SuspendLayout();
            this.SetCategoryRelGroupBox.SuspendLayout();
            this.AddCategoryGroupBox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.ReagentCategoryRelGroupBox.SuspendLayout();
            this.ManageUsersTab.SuspendLayout();
            this.tc_ManageUsers.SuspendLayout();
            this.tab_Users.SuspendLayout();
            this.gb_UserCredentials.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.gb_UserCredentialsExisting.SuspendLayout();
            this.CreateUserGroupBox.SuspendLayout();
            this.tab_CostCentres.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tab_LabsInstitutes.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.CreateLabGroupBox.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.ConfigTabPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.DBConfigGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // AdministrationTabs
            // 
            this.AdministrationTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AdministrationTabs.Controls.Add(this.ReagentTypesTab);
            this.AdministrationTabs.Controls.Add(this.OrdersTab);
            this.AdministrationTabs.Controls.Add(this.InventoryTab);
            this.AdministrationTabs.Controls.Add(this.HistoryTab);
            this.AdministrationTabs.Controls.Add(this.BudgetsTab);
            this.AdministrationTabs.Controls.Add(this.SignOutListTabPage);
            this.AdministrationTabs.Controls.Add(this.CategoriesTab);
            this.AdministrationTabs.Controls.Add(this.ManageUsersTab);
            this.AdministrationTabs.Controls.Add(this.ConfigTabPage);
            this.AdministrationTabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdministrationTabs.Location = new System.Drawing.Point(3, 3);
            this.AdministrationTabs.Name = "AdministrationTabs";
            this.AdministrationTabs.SelectedIndex = 0;
            this.AdministrationTabs.Size = new System.Drawing.Size(1229, 505);
            this.AdministrationTabs.TabIndex = 0;
            // 
            // ReagentTypesTab
            // 
            this.ReagentTypesTab.Controls.Add(this.ReagentTypesInfoPanel);
            this.ReagentTypesTab.Controls.Add(this.ReagentTypesRevertButton);
            this.ReagentTypesTab.Controls.Add(this.ReagentTypesUpdateButton);
            this.ReagentTypesTab.Controls.Add(this.ReagentTypesDataGridView);
            this.ReagentTypesTab.Location = new System.Drawing.Point(4, 22);
            this.ReagentTypesTab.Name = "ReagentTypesTab";
            this.ReagentTypesTab.Padding = new System.Windows.Forms.Padding(3);
            this.ReagentTypesTab.Size = new System.Drawing.Size(1221, 479);
            this.ReagentTypesTab.TabIndex = 0;
            this.ReagentTypesTab.Text = "Reagent Types";
            this.ReagentTypesTab.UseVisualStyleBackColor = true;
            // 
            // ReagentTypesInfoPanel
            // 
            this.ReagentTypesInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ReagentTypesInfoPanel.AutoScroll = true;
            this.ReagentTypesInfoPanel.Location = new System.Drawing.Point(5, 421);
            this.ReagentTypesInfoPanel.Name = "ReagentTypesInfoPanel";
            this.ReagentTypesInfoPanel.Size = new System.Drawing.Size(904, 54);
            this.ReagentTypesInfoPanel.TabIndex = 3;
            // 
            // ReagentTypesRevertButton
            // 
            this.ReagentTypesRevertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ReagentTypesRevertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReagentTypesRevertButton.Location = new System.Drawing.Point(934, 421);
            this.ReagentTypesRevertButton.Name = "ReagentTypesRevertButton";
            this.ReagentTypesRevertButton.Size = new System.Drawing.Size(133, 55);
            this.ReagentTypesRevertButton.TabIndex = 2;
            this.ReagentTypesRevertButton.Text = "Revert";
            this.ReagentTypesRevertButton.UseVisualStyleBackColor = true;
            this.ReagentTypesRevertButton.Click += new System.EventHandler(this.ReagentTypesRevertButton_Click);
            // 
            // ReagentTypesUpdateButton
            // 
            this.ReagentTypesUpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ReagentTypesUpdateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReagentTypesUpdateButton.Location = new System.Drawing.Point(1073, 421);
            this.ReagentTypesUpdateButton.Name = "ReagentTypesUpdateButton";
            this.ReagentTypesUpdateButton.Size = new System.Drawing.Size(142, 55);
            this.ReagentTypesUpdateButton.TabIndex = 1;
            this.ReagentTypesUpdateButton.Text = "Update";
            this.ReagentTypesUpdateButton.UseVisualStyleBackColor = true;
            this.ReagentTypesUpdateButton.Click += new System.EventHandler(this.ReagentTypesUpdateButton_Click);
            // 
            // ReagentTypesDataGridView
            // 
            this.ReagentTypesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReagentTypesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReagentTypesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ReagentTypesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReagentTypesDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReagentTypesDataGridView.Location = new System.Drawing.Point(3, 3);
            this.ReagentTypesDataGridView.MultiSelect = false;
            this.ReagentTypesDataGridView.Name = "ReagentTypesDataGridView";
            this.ReagentTypesDataGridView.Size = new System.Drawing.Size(1209, 412);
            this.ReagentTypesDataGridView.TabIndex = 0;
            // 
            // OrdersTab
            // 
            this.OrdersTab.Controls.Add(this.OrdersRevertButton);
            this.OrdersTab.Controls.Add(this.OrdersUpdateButton);
            this.OrdersTab.Controls.Add(this.OrdersInfoPanel);
            this.OrdersTab.Controls.Add(this.OrdersDataGridView);
            this.OrdersTab.Location = new System.Drawing.Point(4, 22);
            this.OrdersTab.Name = "OrdersTab";
            this.OrdersTab.Padding = new System.Windows.Forms.Padding(3);
            this.OrdersTab.Size = new System.Drawing.Size(1221, 479);
            this.OrdersTab.TabIndex = 1;
            this.OrdersTab.Text = "Orders";
            this.OrdersTab.UseVisualStyleBackColor = true;
            // 
            // OrdersRevertButton
            // 
            this.OrdersRevertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersRevertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrdersRevertButton.Location = new System.Drawing.Point(934, 416);
            this.OrdersRevertButton.Name = "OrdersRevertButton";
            this.OrdersRevertButton.Size = new System.Drawing.Size(133, 55);
            this.OrdersRevertButton.TabIndex = 6;
            this.OrdersRevertButton.Text = "Revert";
            this.OrdersRevertButton.UseVisualStyleBackColor = true;
            this.OrdersRevertButton.Click += new System.EventHandler(this.OrdersRevertButton_Click);
            // 
            // OrdersUpdateButton
            // 
            this.OrdersUpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersUpdateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrdersUpdateButton.Location = new System.Drawing.Point(1073, 415);
            this.OrdersUpdateButton.Name = "OrdersUpdateButton";
            this.OrdersUpdateButton.Size = new System.Drawing.Size(142, 55);
            this.OrdersUpdateButton.TabIndex = 5;
            this.OrdersUpdateButton.Text = "Update";
            this.OrdersUpdateButton.UseVisualStyleBackColor = true;
            this.OrdersUpdateButton.Click += new System.EventHandler(this.OrdersUpdateButton_Click);
            // 
            // OrdersInfoPanel
            // 
            this.OrdersInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OrdersInfoPanel.AutoScroll = true;
            this.OrdersInfoPanel.Location = new System.Drawing.Point(3, 416);
            this.OrdersInfoPanel.Name = "OrdersInfoPanel";
            this.OrdersInfoPanel.Size = new System.Drawing.Size(916, 54);
            this.OrdersInfoPanel.TabIndex = 4;
            // 
            // OrdersDataGridView
            // 
            this.OrdersDataGridView.AllowUserToAddRows = false;
            this.OrdersDataGridView.AllowUserToDeleteRows = false;
            this.OrdersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.OrdersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrdersDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.OrdersDataGridView.Location = new System.Drawing.Point(3, 3);
            this.OrdersDataGridView.MultiSelect = false;
            this.OrdersDataGridView.Name = "OrdersDataGridView";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.OrdersDataGridView.Size = new System.Drawing.Size(1212, 406);
            this.OrdersDataGridView.TabIndex = 1;
            // 
            // InventoryTab
            // 
            this.InventoryTab.Controls.Add(this.InventoryRevertButton);
            this.InventoryTab.Controls.Add(this.InventoryUpdateButton);
            this.InventoryTab.Controls.Add(this.InventoryInfoPanel);
            this.InventoryTab.Controls.Add(this.InventoryDataGridView);
            this.InventoryTab.Location = new System.Drawing.Point(4, 22);
            this.InventoryTab.Name = "InventoryTab";
            this.InventoryTab.Size = new System.Drawing.Size(1221, 479);
            this.InventoryTab.TabIndex = 2;
            this.InventoryTab.Text = "Inventory";
            this.InventoryTab.UseVisualStyleBackColor = true;
            // 
            // InventoryRevertButton
            // 
            this.InventoryRevertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InventoryRevertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryRevertButton.Location = new System.Drawing.Point(937, 421);
            this.InventoryRevertButton.Name = "InventoryRevertButton";
            this.InventoryRevertButton.Size = new System.Drawing.Size(133, 55);
            this.InventoryRevertButton.TabIndex = 6;
            this.InventoryRevertButton.Text = "Revert";
            this.InventoryRevertButton.UseVisualStyleBackColor = true;
            this.InventoryRevertButton.Click += new System.EventHandler(this.InventoryRevertButton_Click);
            // 
            // InventoryUpdateButton
            // 
            this.InventoryUpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InventoryUpdateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryUpdateButton.Location = new System.Drawing.Point(1076, 421);
            this.InventoryUpdateButton.Name = "InventoryUpdateButton";
            this.InventoryUpdateButton.Size = new System.Drawing.Size(142, 55);
            this.InventoryUpdateButton.TabIndex = 5;
            this.InventoryUpdateButton.Text = "Update";
            this.InventoryUpdateButton.UseVisualStyleBackColor = true;
            this.InventoryUpdateButton.Click += new System.EventHandler(this.InventoryUpdateButton_Click);
            // 
            // InventoryInfoPanel
            // 
            this.InventoryInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.InventoryInfoPanel.AutoScroll = true;
            this.InventoryInfoPanel.Location = new System.Drawing.Point(3, 422);
            this.InventoryInfoPanel.Name = "InventoryInfoPanel";
            this.InventoryInfoPanel.Size = new System.Drawing.Size(928, 54);
            this.InventoryInfoPanel.TabIndex = 4;
            // 
            // InventoryDataGridView
            // 
            this.InventoryDataGridView.AllowUserToAddRows = false;
            this.InventoryDataGridView.AllowUserToDeleteRows = false;
            this.InventoryDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.InventoryDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InventoryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.InventoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.InventoryDataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.InventoryDataGridView.Location = new System.Drawing.Point(3, 3);
            this.InventoryDataGridView.MultiSelect = false;
            this.InventoryDataGridView.Name = "InventoryDataGridView";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InventoryDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.InventoryDataGridView.Size = new System.Drawing.Size(1215, 401);
            this.InventoryDataGridView.TabIndex = 1;
            // 
            // HistoryTab
            // 
            this.HistoryTab.Controls.Add(this.HistoryUpdateButton);
            this.HistoryTab.Controls.Add(this.HistoryDataGridView);
            this.HistoryTab.Location = new System.Drawing.Point(4, 22);
            this.HistoryTab.Name = "HistoryTab";
            this.HistoryTab.Size = new System.Drawing.Size(1221, 479);
            this.HistoryTab.TabIndex = 3;
            this.HistoryTab.Text = "History";
            this.HistoryTab.UseVisualStyleBackColor = true;
            // 
            // HistoryUpdateButton
            // 
            this.HistoryUpdateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.HistoryUpdateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HistoryUpdateButton.Location = new System.Drawing.Point(1083, 421);
            this.HistoryUpdateButton.Name = "HistoryUpdateButton";
            this.HistoryUpdateButton.Size = new System.Drawing.Size(136, 55);
            this.HistoryUpdateButton.TabIndex = 28;
            this.HistoryUpdateButton.Text = "Update";
            this.HistoryUpdateButton.UseVisualStyleBackColor = true;
            this.HistoryUpdateButton.Click += new System.EventHandler(this.HistoryUpdateButton_Click);
            // 
            // HistoryDataGridView
            // 
            this.HistoryDataGridView.AllowUserToAddRows = false;
            this.HistoryDataGridView.AllowUserToDeleteRows = false;
            this.HistoryDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HistoryDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.HistoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HistoryDataGridView.DefaultCellStyle = dataGridViewCellStyle10;
            this.HistoryDataGridView.Location = new System.Drawing.Point(0, 0);
            this.HistoryDataGridView.MultiSelect = false;
            this.HistoryDataGridView.Name = "HistoryDataGridView";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.HistoryDataGridView.Size = new System.Drawing.Size(1218, 393);
            this.HistoryDataGridView.TabIndex = 1;
            // 
            // BudgetsTab
            // 
            this.BudgetsTab.Controls.Add(this.BudgetsTabControl);
            this.BudgetsTab.Location = new System.Drawing.Point(4, 22);
            this.BudgetsTab.Name = "BudgetsTab";
            this.BudgetsTab.Padding = new System.Windows.Forms.Padding(3);
            this.BudgetsTab.Size = new System.Drawing.Size(1221, 479);
            this.BudgetsTab.TabIndex = 6;
            this.BudgetsTab.Text = "Budgets";
            this.BudgetsTab.UseVisualStyleBackColor = true;
            // 
            // BudgetsTabControl
            // 
            this.BudgetsTabControl.Controls.Add(this.CostCentreSummaryTab);
            this.BudgetsTabControl.Controls.Add(this.ReagentSummaryTab);
            this.BudgetsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BudgetsTabControl.Location = new System.Drawing.Point(3, 3);
            this.BudgetsTabControl.Name = "BudgetsTabControl";
            this.BudgetsTabControl.SelectedIndex = 0;
            this.BudgetsTabControl.Size = new System.Drawing.Size(1215, 473);
            this.BudgetsTabControl.TabIndex = 0;
            // 
            // CostCentreSummaryTab
            // 
            this.CostCentreSummaryTab.Controls.Add(this.EmailUserReportsButton);
            this.CostCentreSummaryTab.Controls.Add(this.ExportMasterReportButton);
            this.CostCentreSummaryTab.Controls.Add(this.CostCentreSummaryDisplayButton);
            this.CostCentreSummaryTab.Controls.Add(this.groupBox2);
            this.CostCentreSummaryTab.Controls.Add(this.groupBox1);
            this.CostCentreSummaryTab.Controls.Add(this.BillingStatusGroupBox);
            this.CostCentreSummaryTab.Controls.Add(this.EndDateGroupBox);
            this.CostCentreSummaryTab.Controls.Add(this.StartDateGroupBox);
            this.CostCentreSummaryTab.Location = new System.Drawing.Point(4, 22);
            this.CostCentreSummaryTab.Name = "CostCentreSummaryTab";
            this.CostCentreSummaryTab.Padding = new System.Windows.Forms.Padding(3);
            this.CostCentreSummaryTab.Size = new System.Drawing.Size(1207, 447);
            this.CostCentreSummaryTab.TabIndex = 0;
            this.CostCentreSummaryTab.Text = "Cost Centre Summary";
            this.CostCentreSummaryTab.UseVisualStyleBackColor = true;
            // 
            // EmailUserReportsButton
            // 
            this.EmailUserReportsButton.Location = new System.Drawing.Point(25, 622);
            this.EmailUserReportsButton.Name = "EmailUserReportsButton";
            this.EmailUserReportsButton.Size = new System.Drawing.Size(250, 35);
            this.EmailUserReportsButton.TabIndex = 8;
            this.EmailUserReportsButton.Text = "Email User Reports";
            this.EmailUserReportsButton.UseVisualStyleBackColor = true;
            this.EmailUserReportsButton.Click += new System.EventHandler(this.EmailUserReportsButton_Click);
            // 
            // ExportMasterReportButton
            // 
            this.ExportMasterReportButton.Location = new System.Drawing.Point(25, 580);
            this.ExportMasterReportButton.Name = "ExportMasterReportButton";
            this.ExportMasterReportButton.Size = new System.Drawing.Size(250, 35);
            this.ExportMasterReportButton.TabIndex = 7;
            this.ExportMasterReportButton.Text = "Export Master Report";
            this.ExportMasterReportButton.UseVisualStyleBackColor = true;
            this.ExportMasterReportButton.Click += new System.EventHandler(this.ExportMasterReportButton_Click);
            // 
            // CostCentreSummaryDisplayButton
            // 
            this.CostCentreSummaryDisplayButton.Location = new System.Drawing.Point(25, 541);
            this.CostCentreSummaryDisplayButton.Name = "CostCentreSummaryDisplayButton";
            this.CostCentreSummaryDisplayButton.Size = new System.Drawing.Size(250, 33);
            this.CostCentreSummaryDisplayButton.TabIndex = 6;
            this.CostCentreSummaryDisplayButton.Text = "Display";
            this.CostCentreSummaryDisplayButton.UseVisualStyleBackColor = true;
            this.CostCentreSummaryDisplayButton.Click += new System.EventHandler(this.CostCentreSummaryDisplayButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.CostCentreDetailsDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(765, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(436, 425);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Details";
            // 
            // CostCentreDetailsDataGridView
            // 
            this.CostCentreDetailsDataGridView.AllowUserToAddRows = false;
            this.CostCentreDetailsDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCentreDetailsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.CostCentreDetailsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CostCentreDetailsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventID,
            this.ItemName,
            this.UserName,
            this.Quantity,
            this.Date,
            this.Billed});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CostCentreDetailsDataGridView.DefaultCellStyle = dataGridViewCellStyle13;
            this.CostCentreDetailsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CostCentreDetailsDataGridView.Location = new System.Drawing.Point(3, 16);
            this.CostCentreDetailsDataGridView.MultiSelect = false;
            this.CostCentreDetailsDataGridView.Name = "CostCentreDetailsDataGridView";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCentreDetailsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.CostCentreDetailsDataGridView.Size = new System.Drawing.Size(430, 406);
            this.CostCentreDetailsDataGridView.TabIndex = 0;
            // 
            // EventID
            // 
            this.EventID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.EventID.HeaderText = "EventID";
            this.EventID.Name = "EventID";
            this.EventID.ReadOnly = true;
            this.EventID.Width = 71;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // UserName
            // 
            this.UserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UserName.HeaderText = "UserName";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.Width = 71;
            // 
            // Date
            // 
            this.Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 55;
            // 
            // Billed
            // 
            this.Billed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Billed.HeaderText = "Billed";
            this.Billed.Name = "Billed";
            this.Billed.Width = 38;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.CostCentreSummaryDataGridView);
            this.groupBox1.Location = new System.Drawing.Point(300, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 425);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Summary";
            // 
            // CostCentreSummaryDataGridView
            // 
            this.CostCentreSummaryDataGridView.AllowUserToAddRows = false;
            this.CostCentreSummaryDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCentreSummaryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.CostCentreSummaryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CostCentreSummaryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DebtorCostCentre,
            this.CreditorCostCentre,
            this.BillableAmount,
            this.BillButton,
            this.UnBillButton});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CostCentreSummaryDataGridView.DefaultCellStyle = dataGridViewCellStyle16;
            this.CostCentreSummaryDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CostCentreSummaryDataGridView.Location = new System.Drawing.Point(3, 16);
            this.CostCentreSummaryDataGridView.Name = "CostCentreSummaryDataGridView";
            this.CostCentreSummaryDataGridView.ReadOnly = true;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CostCentreSummaryDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.CostCentreSummaryDataGridView.Size = new System.Drawing.Size(411, 406);
            this.CostCentreSummaryDataGridView.TabIndex = 0;
            this.CostCentreSummaryDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CostCentreSummaryDataGridView_CellContentClick);
            // 
            // DebtorCostCentre
            // 
            this.DebtorCostCentre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DebtorCostCentre.HeaderText = "DebtorCostCentre";
            this.DebtorCostCentre.Name = "DebtorCostCentre";
            this.DebtorCostCentre.ReadOnly = true;
            this.DebtorCostCentre.Width = 116;
            // 
            // CreditorCostCentre
            // 
            this.CreditorCostCentre.HeaderText = "CreditorCostCentre";
            this.CreditorCostCentre.Name = "CreditorCostCentre";
            this.CreditorCostCentre.ReadOnly = true;
            // 
            // BillableAmount
            // 
            this.BillableAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BillableAmount.HeaderText = "BillableAmount";
            this.BillableAmount.Name = "BillableAmount";
            this.BillableAmount.ReadOnly = true;
            this.BillableAmount.Width = 101;
            // 
            // BillButton
            // 
            this.BillButton.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BillButton.HeaderText = "Bill";
            this.BillButton.Name = "BillButton";
            this.BillButton.ReadOnly = true;
            this.BillButton.Text = "Bill";
            this.BillButton.UseColumnTextForButtonValue = true;
            this.BillButton.Width = 26;
            // 
            // UnBillButton
            // 
            this.UnBillButton.HeaderText = "UnBill";
            this.UnBillButton.Name = "UnBillButton";
            this.UnBillButton.ReadOnly = true;
            this.UnBillButton.Text = "UnBill";
            this.UnBillButton.UseColumnTextForButtonValue = true;
            // 
            // BillingStatusGroupBox
            // 
            this.BillingStatusGroupBox.Controls.Add(this.CostCentreSummaryAllBillingStatusRadioButton);
            this.BillingStatusGroupBox.Controls.Add(this.CostCentreSummaryBilledBillingStatusRadioButton);
            this.BillingStatusGroupBox.Controls.Add(this.CostCentreSummaryUnbilledBillingStatusRadioButton);
            this.BillingStatusGroupBox.Location = new System.Drawing.Point(23, 439);
            this.BillingStatusGroupBox.Name = "BillingStatusGroupBox";
            this.BillingStatusGroupBox.Size = new System.Drawing.Size(252, 97);
            this.BillingStatusGroupBox.TabIndex = 3;
            this.BillingStatusGroupBox.TabStop = false;
            this.BillingStatusGroupBox.Text = "Billing Status";
            // 
            // CostCentreSummaryAllBillingStatusRadioButton
            // 
            this.CostCentreSummaryAllBillingStatusRadioButton.AutoSize = true;
            this.CostCentreSummaryAllBillingStatusRadioButton.Location = new System.Drawing.Point(196, 44);
            this.CostCentreSummaryAllBillingStatusRadioButton.Name = "CostCentreSummaryAllBillingStatusRadioButton";
            this.CostCentreSummaryAllBillingStatusRadioButton.Size = new System.Drawing.Size(36, 17);
            this.CostCentreSummaryAllBillingStatusRadioButton.TabIndex = 2;
            this.CostCentreSummaryAllBillingStatusRadioButton.Text = "All";
            this.CostCentreSummaryAllBillingStatusRadioButton.UseVisualStyleBackColor = true;
            // 
            // CostCentreSummaryBilledBillingStatusRadioButton
            // 
            this.CostCentreSummaryBilledBillingStatusRadioButton.AutoSize = true;
            this.CostCentreSummaryBilledBillingStatusRadioButton.Location = new System.Drawing.Point(114, 44);
            this.CostCentreSummaryBilledBillingStatusRadioButton.Name = "CostCentreSummaryBilledBillingStatusRadioButton";
            this.CostCentreSummaryBilledBillingStatusRadioButton.Size = new System.Drawing.Size(50, 17);
            this.CostCentreSummaryBilledBillingStatusRadioButton.TabIndex = 1;
            this.CostCentreSummaryBilledBillingStatusRadioButton.Text = "Billed";
            this.CostCentreSummaryBilledBillingStatusRadioButton.UseVisualStyleBackColor = true;
            // 
            // CostCentreSummaryUnbilledBillingStatusRadioButton
            // 
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.AutoSize = true;
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.Checked = true;
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.Location = new System.Drawing.Point(19, 44);
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.Name = "CostCentreSummaryUnbilledBillingStatusRadioButton";
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.Size = new System.Drawing.Size(63, 17);
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.TabIndex = 0;
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.TabStop = true;
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.Text = "Unbilled";
            this.CostCentreSummaryUnbilledBillingStatusRadioButton.UseVisualStyleBackColor = true;
            // 
            // EndDateGroupBox
            // 
            this.EndDateGroupBox.Controls.Add(this.CostCentreSummaryEndDateCalendar);
            this.EndDateGroupBox.Location = new System.Drawing.Point(23, 229);
            this.EndDateGroupBox.Name = "EndDateGroupBox";
            this.EndDateGroupBox.Size = new System.Drawing.Size(252, 204);
            this.EndDateGroupBox.TabIndex = 2;
            this.EndDateGroupBox.TabStop = false;
            this.EndDateGroupBox.Text = "End Date";
            // 
            // CostCentreSummaryEndDateCalendar
            // 
            this.CostCentreSummaryEndDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.CostCentreSummaryEndDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.CostCentreSummaryEndDateCalendar.MaxSelectionCount = 1;
            this.CostCentreSummaryEndDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.CostCentreSummaryEndDateCalendar.Name = "CostCentreSummaryEndDateCalendar";
            this.CostCentreSummaryEndDateCalendar.TabIndex = 0;
            // 
            // StartDateGroupBox
            // 
            this.StartDateGroupBox.Controls.Add(this.CostCentreSummaryStartDateCalendar);
            this.StartDateGroupBox.Location = new System.Drawing.Point(23, 19);
            this.StartDateGroupBox.Name = "StartDateGroupBox";
            this.StartDateGroupBox.Size = new System.Drawing.Size(252, 204);
            this.StartDateGroupBox.TabIndex = 1;
            this.StartDateGroupBox.TabStop = false;
            this.StartDateGroupBox.Text = "Start Date";
            // 
            // CostCentreSummaryStartDateCalendar
            // 
            this.CostCentreSummaryStartDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.CostCentreSummaryStartDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.CostCentreSummaryStartDateCalendar.MaxSelectionCount = 1;
            this.CostCentreSummaryStartDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.CostCentreSummaryStartDateCalendar.Name = "CostCentreSummaryStartDateCalendar";
            this.CostCentreSummaryStartDateCalendar.TabIndex = 0;
            // 
            // ReagentSummaryTab
            // 
            this.ReagentSummaryTab.Controls.Add(this.groupBox3);
            this.ReagentSummaryTab.Controls.Add(this.groupBox6);
            this.ReagentSummaryTab.Controls.Add(this.ReagentSummaryDisplayButton);
            this.ReagentSummaryTab.Controls.Add(this.groupBox4);
            this.ReagentSummaryTab.Controls.Add(this.groupBox5);
            this.ReagentSummaryTab.Location = new System.Drawing.Point(4, 22);
            this.ReagentSummaryTab.Name = "ReagentSummaryTab";
            this.ReagentSummaryTab.Padding = new System.Windows.Forms.Padding(3);
            this.ReagentSummaryTab.Size = new System.Drawing.Size(1207, 447);
            this.ReagentSummaryTab.TabIndex = 2;
            this.ReagentSummaryTab.Text = "Reagent Summary";
            this.ReagentSummaryTab.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.ReagentDetailsDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(647, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(554, 425);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Details";
            // 
            // ReagentDetailsDataGridView
            // 
            this.ReagentDetailsDataGridView.AllowUserToAddRows = false;
            this.ReagentDetailsDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReagentDetailsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.ReagentDetailsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReagentDetailsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn11,
            this.Value});
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReagentDetailsDataGridView.DefaultCellStyle = dataGridViewCellStyle19;
            this.ReagentDetailsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReagentDetailsDataGridView.Location = new System.Drawing.Point(3, 16);
            this.ReagentDetailsDataGridView.MultiSelect = false;
            this.ReagentDetailsDataGridView.Name = "ReagentDetailsDataGridView";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReagentDetailsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.ReagentDetailsDataGridView.Size = new System.Drawing.Size(548, 406);
            this.ReagentDetailsDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.HeaderText = "ItemName";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn11.HeaderText = "StockUnits";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 84;
            // 
            // Value
            // 
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            this.Value.ReadOnly = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.ReagentSummaryDataGridView);
            this.groupBox6.Location = new System.Drawing.Point(300, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(341, 425);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Summary";
            // 
            // ReagentSummaryDataGridView
            // 
            this.ReagentSummaryDataGridView.AllowUserToAddRows = false;
            this.ReagentSummaryDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReagentSummaryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.ReagentSummaryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReagentSummaryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.ReagentSummaryCostCentre,
            this.dataGridViewTextBoxColumn14});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReagentSummaryDataGridView.DefaultCellStyle = dataGridViewCellStyle22;
            this.ReagentSummaryDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReagentSummaryDataGridView.Location = new System.Drawing.Point(3, 16);
            this.ReagentSummaryDataGridView.MultiSelect = false;
            this.ReagentSummaryDataGridView.Name = "ReagentSummaryDataGridView";
            this.ReagentSummaryDataGridView.ReadOnly = true;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReagentSummaryDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.ReagentSummaryDataGridView.Size = new System.Drawing.Size(335, 406);
            this.ReagentSummaryDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "Category";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // ReagentSummaryCostCentre
            // 
            this.ReagentSummaryCostCentre.HeaderText = "CostCentre";
            this.ReagentSummaryCostCentre.Name = "ReagentSummaryCostCentre";
            this.ReagentSummaryCostCentre.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn14.HeaderText = "Value";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 59;
            // 
            // ReagentSummaryDisplayButton
            // 
            this.ReagentSummaryDisplayButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ReagentSummaryDisplayButton.Location = new System.Drawing.Point(23, 295);
            this.ReagentSummaryDisplayButton.Name = "ReagentSummaryDisplayButton";
            this.ReagentSummaryDisplayButton.Size = new System.Drawing.Size(250, 44);
            this.ReagentSummaryDisplayButton.TabIndex = 10;
            this.ReagentSummaryDisplayButton.Text = "Display";
            this.ReagentSummaryDisplayButton.UseVisualStyleBackColor = true;
            this.ReagentSummaryDisplayButton.Click += new System.EventHandler(this.ReagentSummaryDisplayButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.ReagentSummaryEndDateCalendar);
            this.groupBox4.Location = new System.Drawing.Point(23, 229);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(252, 60);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "End Date";
            // 
            // ReagentSummaryEndDateCalendar
            // 
            this.ReagentSummaryEndDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.ReagentSummaryEndDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.ReagentSummaryEndDateCalendar.MaxSelectionCount = 1;
            this.ReagentSummaryEndDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.ReagentSummaryEndDateCalendar.Name = "ReagentSummaryEndDateCalendar";
            this.ReagentSummaryEndDateCalendar.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ReagentSummaryStartDateCalendar);
            this.groupBox5.Location = new System.Drawing.Point(23, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(252, 204);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Start Date";
            // 
            // ReagentSummaryStartDateCalendar
            // 
            this.ReagentSummaryStartDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.ReagentSummaryStartDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.ReagentSummaryStartDateCalendar.MaxSelectionCount = 1;
            this.ReagentSummaryStartDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.ReagentSummaryStartDateCalendar.Name = "ReagentSummaryStartDateCalendar";
            this.ReagentSummaryStartDateCalendar.TabIndex = 0;
            // 
            // SignOutListTabPage
            // 
            this.SignOutListTabPage.Controls.Add(this.SignoutListExportButton);
            this.SignOutListTabPage.Controls.Add(this.SignOutListRevertButton);
            this.SignOutListTabPage.Controls.Add(this.SignOutListUpdateButton);
            this.SignOutListTabPage.Controls.Add(this.SignOutListDisplayButton);
            this.SignOutListTabPage.Controls.Add(this.groupBox11);
            this.SignOutListTabPage.Controls.Add(this.groupBox13);
            this.SignOutListTabPage.Controls.Add(this.groupBox14);
            this.SignOutListTabPage.Location = new System.Drawing.Point(4, 22);
            this.SignOutListTabPage.Name = "SignOutListTabPage";
            this.SignOutListTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SignOutListTabPage.Size = new System.Drawing.Size(1221, 479);
            this.SignOutListTabPage.TabIndex = 7;
            this.SignOutListTabPage.Text = "Sign Out List";
            this.SignOutListTabPage.UseVisualStyleBackColor = true;
            this.SignOutListTabPage.Click += new System.EventHandler(this.SignOutListTabPage_Click);
            // 
            // SignoutListExportButton
            // 
            this.SignoutListExportButton.Location = new System.Drawing.Point(8, 524);
            this.SignoutListExportButton.Name = "SignoutListExportButton";
            this.SignoutListExportButton.Size = new System.Drawing.Size(250, 44);
            this.SignoutListExportButton.TabIndex = 14;
            this.SignoutListExportButton.Text = "Export";
            this.SignoutListExportButton.UseVisualStyleBackColor = true;
            this.SignoutListExportButton.Click += new System.EventHandler(this.SignoutListExportButton_Click);
            // 
            // SignOutListRevertButton
            // 
            this.SignOutListRevertButton.Location = new System.Drawing.Point(8, 573);
            this.SignOutListRevertButton.Name = "SignOutListRevertButton";
            this.SignOutListRevertButton.Size = new System.Drawing.Size(250, 44);
            this.SignOutListRevertButton.TabIndex = 13;
            this.SignOutListRevertButton.Text = "Revert";
            this.SignOutListRevertButton.UseVisualStyleBackColor = true;
            this.SignOutListRevertButton.Click += new System.EventHandler(this.SignOutListDisplayButton_Click);
            // 
            // SignOutListUpdateButton
            // 
            this.SignOutListUpdateButton.Location = new System.Drawing.Point(8, 475);
            this.SignOutListUpdateButton.Name = "SignOutListUpdateButton";
            this.SignOutListUpdateButton.Size = new System.Drawing.Size(250, 44);
            this.SignOutListUpdateButton.TabIndex = 12;
            this.SignOutListUpdateButton.Text = "Update";
            this.SignOutListUpdateButton.UseVisualStyleBackColor = true;
            this.SignOutListUpdateButton.Click += new System.EventHandler(this.SignOutListUpdateButton_Click);
            // 
            // SignOutListDisplayButton
            // 
            this.SignOutListDisplayButton.Location = new System.Drawing.Point(8, 426);
            this.SignOutListDisplayButton.Name = "SignOutListDisplayButton";
            this.SignOutListDisplayButton.Size = new System.Drawing.Size(250, 44);
            this.SignOutListDisplayButton.TabIndex = 11;
            this.SignOutListDisplayButton.Text = "Display";
            this.SignOutListDisplayButton.UseVisualStyleBackColor = true;
            this.SignOutListDisplayButton.Click += new System.EventHandler(this.SignOutListDisplayButton_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.SignOutListDataGridView);
            this.groupBox11.Location = new System.Drawing.Point(283, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(974, 611);
            this.groupBox11.TabIndex = 10;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Summary";
            // 
            // SignOutListDataGridView
            // 
            this.SignOutListDataGridView.AllowUserToAddRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SignOutListDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.SignOutListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SignOutListDataGridView.DefaultCellStyle = dataGridViewCellStyle25;
            this.SignOutListDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SignOutListDataGridView.Location = new System.Drawing.Point(3, 16);
            this.SignOutListDataGridView.MultiSelect = false;
            this.SignOutListDataGridView.Name = "SignOutListDataGridView";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SignOutListDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.SignOutListDataGridView.Size = new System.Drawing.Size(968, 592);
            this.SignOutListDataGridView.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.SignoutListEndDateCalendar);
            this.groupBox13.Location = new System.Drawing.Point(6, 216);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(252, 204);
            this.groupBox13.TabIndex = 8;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "End Date";
            // 
            // SignoutListEndDateCalendar
            // 
            this.SignoutListEndDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.SignoutListEndDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.SignoutListEndDateCalendar.MaxSelectionCount = 1;
            this.SignoutListEndDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.SignoutListEndDateCalendar.Name = "SignoutListEndDateCalendar";
            this.SignoutListEndDateCalendar.TabIndex = 0;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.SignoutListStartDateCalendar);
            this.groupBox14.Location = new System.Drawing.Point(6, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(252, 204);
            this.groupBox14.TabIndex = 7;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Start Date";
            // 
            // SignoutListStartDateCalendar
            // 
            this.SignoutListStartDateCalendar.Location = new System.Drawing.Point(12, 25);
            this.SignoutListStartDateCalendar.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.SignoutListStartDateCalendar.MaxSelectionCount = 1;
            this.SignoutListStartDateCalendar.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.SignoutListStartDateCalendar.Name = "SignoutListStartDateCalendar";
            this.SignoutListStartDateCalendar.TabIndex = 0;
            // 
            // CategoriesTab
            // 
            this.CategoriesTab.Controls.Add(this.ReagentSetToolsGroupBox);
            this.CategoriesTab.Controls.Add(this.CategoryToolsGroupBox);
            this.CategoriesTab.Controls.Add(this.CategoriesInfoPanel);
            this.CategoriesTab.Location = new System.Drawing.Point(4, 22);
            this.CategoriesTab.Name = "CategoriesTab";
            this.CategoriesTab.Padding = new System.Windows.Forms.Padding(3);
            this.CategoriesTab.Size = new System.Drawing.Size(1221, 479);
            this.CategoriesTab.TabIndex = 4;
            this.CategoriesTab.Text = "Categories/Bundles";
            this.CategoriesTab.UseVisualStyleBackColor = true;
            // 
            // ReagentSetToolsGroupBox
            // 
            this.ReagentSetToolsGroupBox.Controls.Add(this.EditSetReagentsGroupBox);
            this.ReagentSetToolsGroupBox.Controls.Add(this.DeleteSetGroupBox);
            this.ReagentSetToolsGroupBox.Controls.Add(this.CreateSetGroupBox);
            this.ReagentSetToolsGroupBox.Location = new System.Drawing.Point(354, 6);
            this.ReagentSetToolsGroupBox.Name = "ReagentSetToolsGroupBox";
            this.ReagentSetToolsGroupBox.Size = new System.Drawing.Size(889, 541);
            this.ReagentSetToolsGroupBox.TabIndex = 5;
            this.ReagentSetToolsGroupBox.TabStop = false;
            this.ReagentSetToolsGroupBox.Text = "Reagent Bundle Tools";
            // 
            // EditSetReagentsGroupBox
            // 
            this.EditSetReagentsGroupBox.Controls.Add(this.EditSetDestinationDataGridView);
            this.EditSetReagentsGroupBox.Controls.Add(this.label24);
            this.EditSetReagentsGroupBox.Controls.Add(this.label23);
            this.EditSetReagentsGroupBox.Controls.Add(this.label22);
            this.EditSetReagentsGroupBox.Controls.Add(this.EditSetComboBox);
            this.EditSetReagentsGroupBox.Controls.Add(this.UpdateSetReagentButton);
            this.EditSetReagentsGroupBox.Controls.Add(this.RemoveSetReagentButton);
            this.EditSetReagentsGroupBox.Controls.Add(this.AddSetReagentButton);
            this.EditSetReagentsGroupBox.Controls.Add(this.EditSetSourceDataGridView);
            this.EditSetReagentsGroupBox.Location = new System.Drawing.Point(6, 109);
            this.EditSetReagentsGroupBox.Name = "EditSetReagentsGroupBox";
            this.EditSetReagentsGroupBox.Size = new System.Drawing.Size(877, 412);
            this.EditSetReagentsGroupBox.TabIndex = 2;
            this.EditSetReagentsGroupBox.TabStop = false;
            this.EditSetReagentsGroupBox.Text = "Add/Remove Bundle Reagents";
            // 
            // EditSetDestinationDataGridView
            // 
            this.EditSetDestinationDataGridView.AllowUserToAddRows = false;
            this.EditSetDestinationDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EditSetDestinationDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.EditSetDestinationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EditSetDestinationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditSetDestinationReagentTypeID,
            this.EditSetDestinationReagentName,
            this.EditSetDestinationReagentManufacturer,
            this.EditSetDestinationUnits});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.EditSetDestinationDataGridView.DefaultCellStyle = dataGridViewCellStyle28;
            this.EditSetDestinationDataGridView.Location = new System.Drawing.Point(468, 83);
            this.EditSetDestinationDataGridView.MultiSelect = false;
            this.EditSetDestinationDataGridView.Name = "EditSetDestinationDataGridView";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EditSetDestinationDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.EditSetDestinationDataGridView.Size = new System.Drawing.Size(372, 313);
            this.EditSetDestinationDataGridView.TabIndex = 9;
            // 
            // EditSetDestinationReagentTypeID
            // 
            this.EditSetDestinationReagentTypeID.HeaderText = "ReagentTypeID";
            this.EditSetDestinationReagentTypeID.Name = "EditSetDestinationReagentTypeID";
            this.EditSetDestinationReagentTypeID.Visible = false;
            // 
            // EditSetDestinationReagentName
            // 
            this.EditSetDestinationReagentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditSetDestinationReagentName.FillWeight = 60F;
            this.EditSetDestinationReagentName.HeaderText = "ReagentName";
            this.EditSetDestinationReagentName.Name = "EditSetDestinationReagentName";
            // 
            // EditSetDestinationReagentManufacturer
            // 
            this.EditSetDestinationReagentManufacturer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditSetDestinationReagentManufacturer.FillWeight = 20F;
            this.EditSetDestinationReagentManufacturer.HeaderText = "Manufacturer";
            this.EditSetDestinationReagentManufacturer.Name = "EditSetDestinationReagentManufacturer";
            // 
            // EditSetDestinationUnits
            // 
            this.EditSetDestinationUnits.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditSetDestinationUnits.FillWeight = 20F;
            this.EditSetDestinationUnits.HeaderText = "Units";
            this.EditSetDestinationUnits.Name = "EditSetDestinationUnits";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(465, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(127, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "Reagent Types in Bundle";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 67);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(126, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "Available Reagent Types";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 33);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Select Bundle";
            // 
            // EditSetComboBox
            // 
            this.EditSetComboBox.FormattingEnabled = true;
            this.EditSetComboBox.Location = new System.Drawing.Point(79, 30);
            this.EditSetComboBox.Name = "EditSetComboBox";
            this.EditSetComboBox.Size = new System.Drawing.Size(173, 21);
            this.EditSetComboBox.TabIndex = 5;
            // 
            // UpdateSetReagentButton
            // 
            this.UpdateSetReagentButton.Location = new System.Drawing.Point(387, 304);
            this.UpdateSetReagentButton.Name = "UpdateSetReagentButton";
            this.UpdateSetReagentButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateSetReagentButton.TabIndex = 4;
            this.UpdateSetReagentButton.Text = "Update";
            this.UpdateSetReagentButton.UseVisualStyleBackColor = true;
            this.UpdateSetReagentButton.Click += new System.EventHandler(this.UpdateSetReagentButton_Click);
            // 
            // RemoveSetReagentButton
            // 
            this.RemoveSetReagentButton.Location = new System.Drawing.Point(387, 240);
            this.RemoveSetReagentButton.Name = "RemoveSetReagentButton";
            this.RemoveSetReagentButton.Size = new System.Drawing.Size(75, 23);
            this.RemoveSetReagentButton.TabIndex = 2;
            this.RemoveSetReagentButton.Text = "<< Remove";
            this.RemoveSetReagentButton.UseVisualStyleBackColor = true;
            this.RemoveSetReagentButton.Click += new System.EventHandler(this.RemoveSetReagentButton_Click);
            // 
            // AddSetReagentButton
            // 
            this.AddSetReagentButton.Location = new System.Drawing.Point(387, 178);
            this.AddSetReagentButton.Name = "AddSetReagentButton";
            this.AddSetReagentButton.Size = new System.Drawing.Size(75, 23);
            this.AddSetReagentButton.TabIndex = 1;
            this.AddSetReagentButton.Text = "Add >>";
            this.AddSetReagentButton.UseVisualStyleBackColor = true;
            this.AddSetReagentButton.Click += new System.EventHandler(this.AddSetReagentButton_Click);
            // 
            // EditSetSourceDataGridView
            // 
            this.EditSetSourceDataGridView.AllowUserToAddRows = false;
            this.EditSetSourceDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EditSetSourceDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.EditSetSourceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EditSetSourceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditSetSourceReagentTypeID,
            this.EditSetSourceReagentName,
            this.EditSetSourceManufacturer});
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.EditSetSourceDataGridView.DefaultCellStyle = dataGridViewCellStyle31;
            this.EditSetSourceDataGridView.Location = new System.Drawing.Point(9, 83);
            this.EditSetSourceDataGridView.MultiSelect = false;
            this.EditSetSourceDataGridView.Name = "EditSetSourceDataGridView";
            this.EditSetSourceDataGridView.ReadOnly = true;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EditSetSourceDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.EditSetSourceDataGridView.Size = new System.Drawing.Size(372, 313);
            this.EditSetSourceDataGridView.TabIndex = 0;
            // 
            // EditSetSourceReagentTypeID
            // 
            this.EditSetSourceReagentTypeID.HeaderText = "ReagentTypeID";
            this.EditSetSourceReagentTypeID.Name = "EditSetSourceReagentTypeID";
            this.EditSetSourceReagentTypeID.ReadOnly = true;
            this.EditSetSourceReagentTypeID.Visible = false;
            // 
            // EditSetSourceReagentName
            // 
            this.EditSetSourceReagentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditSetSourceReagentName.FillWeight = 80F;
            this.EditSetSourceReagentName.HeaderText = "ReagentName";
            this.EditSetSourceReagentName.Name = "EditSetSourceReagentName";
            this.EditSetSourceReagentName.ReadOnly = true;
            // 
            // EditSetSourceManufacturer
            // 
            this.EditSetSourceManufacturer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditSetSourceManufacturer.FillWeight = 20F;
            this.EditSetSourceManufacturer.HeaderText = "Manufacturer";
            this.EditSetSourceManufacturer.Name = "EditSetSourceManufacturer";
            this.EditSetSourceManufacturer.ReadOnly = true;
            // 
            // DeleteSetGroupBox
            // 
            this.DeleteSetGroupBox.Controls.Add(this.DeleteSetComboBox);
            this.DeleteSetGroupBox.Controls.Add(this.label21);
            this.DeleteSetGroupBox.Controls.Add(this.DeleteSetButton);
            this.DeleteSetGroupBox.Location = new System.Drawing.Point(284, 19);
            this.DeleteSetGroupBox.Name = "DeleteSetGroupBox";
            this.DeleteSetGroupBox.Size = new System.Drawing.Size(272, 84);
            this.DeleteSetGroupBox.TabIndex = 1;
            this.DeleteSetGroupBox.TabStop = false;
            this.DeleteSetGroupBox.Text = "Delete Set";
            // 
            // DeleteSetComboBox
            // 
            this.DeleteSetComboBox.FormattingEnabled = true;
            this.DeleteSetComboBox.Location = new System.Drawing.Point(77, 23);
            this.DeleteSetComboBox.Name = "DeleteSetComboBox";
            this.DeleteSetComboBox.Size = new System.Drawing.Size(183, 21);
            this.DeleteSetComboBox.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 28);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Bundle Name";
            // 
            // DeleteSetButton
            // 
            this.DeleteSetButton.Location = new System.Drawing.Point(185, 50);
            this.DeleteSetButton.Name = "DeleteSetButton";
            this.DeleteSetButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteSetButton.TabIndex = 1;
            this.DeleteSetButton.Text = "Delete";
            this.DeleteSetButton.UseVisualStyleBackColor = true;
            this.DeleteSetButton.Click += new System.EventHandler(this.DeleteSetButton_Click);
            // 
            // CreateSetGroupBox
            // 
            this.CreateSetGroupBox.Controls.Add(this.label20);
            this.CreateSetGroupBox.Controls.Add(this.CreateSetButton);
            this.CreateSetGroupBox.Controls.Add(this.SetNameTextBox);
            this.CreateSetGroupBox.Location = new System.Drawing.Point(6, 19);
            this.CreateSetGroupBox.Name = "CreateSetGroupBox";
            this.CreateSetGroupBox.Size = new System.Drawing.Size(272, 84);
            this.CreateSetGroupBox.TabIndex = 0;
            this.CreateSetGroupBox.TabStop = false;
            this.CreateSetGroupBox.Text = "Create Set";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Bundle Name";
            // 
            // CreateSetButton
            // 
            this.CreateSetButton.Location = new System.Drawing.Point(185, 50);
            this.CreateSetButton.Name = "CreateSetButton";
            this.CreateSetButton.Size = new System.Drawing.Size(75, 23);
            this.CreateSetButton.TabIndex = 1;
            this.CreateSetButton.Text = "Create";
            this.CreateSetButton.UseVisualStyleBackColor = true;
            this.CreateSetButton.Click += new System.EventHandler(this.CreateSetButton_Click);
            // 
            // SetNameTextBox
            // 
            this.SetNameTextBox.Location = new System.Drawing.Point(78, 24);
            this.SetNameTextBox.Name = "SetNameTextBox";
            this.SetNameTextBox.Size = new System.Drawing.Size(185, 20);
            this.SetNameTextBox.TabIndex = 0;
            // 
            // CategoryToolsGroupBox
            // 
            this.CategoryToolsGroupBox.Controls.Add(this.SetCategoryRelGroupBox);
            this.CategoryToolsGroupBox.Controls.Add(this.AddCategoryGroupBox);
            this.CategoryToolsGroupBox.Controls.Add(this.groupBox7);
            this.CategoryToolsGroupBox.Controls.Add(this.ReagentCategoryRelGroupBox);
            this.CategoryToolsGroupBox.Location = new System.Drawing.Point(6, 6);
            this.CategoryToolsGroupBox.Name = "CategoryToolsGroupBox";
            this.CategoryToolsGroupBox.Size = new System.Drawing.Size(333, 541);
            this.CategoryToolsGroupBox.TabIndex = 4;
            this.CategoryToolsGroupBox.TabStop = false;
            this.CategoryToolsGroupBox.Text = "Category Tools";
            // 
            // SetCategoryRelGroupBox
            // 
            this.SetCategoryRelGroupBox.Controls.Add(this.SetCategoryLinkUnLinkButton);
            this.SetCategoryRelGroupBox.Controls.Add(this.SetCategoryLinkLinkButton);
            this.SetCategoryRelGroupBox.Controls.Add(this.SetCategoryLinkCategoryComboBox);
            this.SetCategoryRelGroupBox.Controls.Add(this.label25);
            this.SetCategoryRelGroupBox.Controls.Add(this.SetCategoryLinkSetComboBox);
            this.SetCategoryRelGroupBox.Controls.Add(this.label26);
            this.SetCategoryRelGroupBox.Location = new System.Drawing.Point(19, 294);
            this.SetCategoryRelGroupBox.Name = "SetCategoryRelGroupBox";
            this.SetCategoryRelGroupBox.Size = new System.Drawing.Size(301, 140);
            this.SetCategoryRelGroupBox.TabIndex = 4;
            this.SetCategoryRelGroupBox.TabStop = false;
            this.SetCategoryRelGroupBox.Text = "Bundle/Category Link";
            // 
            // SetCategoryLinkUnLinkButton
            // 
            this.SetCategoryLinkUnLinkButton.Location = new System.Drawing.Point(213, 87);
            this.SetCategoryLinkUnLinkButton.Name = "SetCategoryLinkUnLinkButton";
            this.SetCategoryLinkUnLinkButton.Size = new System.Drawing.Size(75, 23);
            this.SetCategoryLinkUnLinkButton.TabIndex = 9;
            this.SetCategoryLinkUnLinkButton.Text = "UnLink";
            this.SetCategoryLinkUnLinkButton.UseVisualStyleBackColor = true;
            this.SetCategoryLinkUnLinkButton.Click += new System.EventHandler(this.SetCategoryLinkUnLinkButton_Click);
            // 
            // SetCategoryLinkLinkButton
            // 
            this.SetCategoryLinkLinkButton.Location = new System.Drawing.Point(132, 87);
            this.SetCategoryLinkLinkButton.Name = "SetCategoryLinkLinkButton";
            this.SetCategoryLinkLinkButton.Size = new System.Drawing.Size(75, 23);
            this.SetCategoryLinkLinkButton.TabIndex = 8;
            this.SetCategoryLinkLinkButton.Text = "Link";
            this.SetCategoryLinkLinkButton.UseVisualStyleBackColor = true;
            this.SetCategoryLinkLinkButton.Click += new System.EventHandler(this.SetCategoryLinkLinkButton_Click);
            // 
            // SetCategoryLinkCategoryComboBox
            // 
            this.SetCategoryLinkCategoryComboBox.FormattingEnabled = true;
            this.SetCategoryLinkCategoryComboBox.Location = new System.Drawing.Point(93, 60);
            this.SetCategoryLinkCategoryComboBox.Name = "SetCategoryLinkCategoryComboBox";
            this.SetCategoryLinkCategoryComboBox.Size = new System.Drawing.Size(195, 21);
            this.SetCategoryLinkCategoryComboBox.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 63);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "Category";
            // 
            // SetCategoryLinkSetComboBox
            // 
            this.SetCategoryLinkSetComboBox.FormattingEnabled = true;
            this.SetCategoryLinkSetComboBox.Location = new System.Drawing.Point(92, 28);
            this.SetCategoryLinkSetComboBox.Name = "SetCategoryLinkSetComboBox";
            this.SetCategoryLinkSetComboBox.Size = new System.Drawing.Size(196, 21);
            this.SetCategoryLinkSetComboBox.TabIndex = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 31);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Bundle";
            // 
            // AddCategoryGroupBox
            // 
            this.AddCategoryGroupBox.Controls.Add(this.NewCategoryCreateButton);
            this.AddCategoryGroupBox.Controls.Add(this.NewCategoryParentComboBox);
            this.AddCategoryGroupBox.Controls.Add(this.NewCategoryParentLabel);
            this.AddCategoryGroupBox.Controls.Add(this.NewCategoryNameTextBox);
            this.AddCategoryGroupBox.Controls.Add(this.NewCategoryNameLabel);
            this.AddCategoryGroupBox.Location = new System.Drawing.Point(16, 19);
            this.AddCategoryGroupBox.Name = "AddCategoryGroupBox";
            this.AddCategoryGroupBox.Size = new System.Drawing.Size(301, 121);
            this.AddCategoryGroupBox.TabIndex = 0;
            this.AddCategoryGroupBox.TabStop = false;
            this.AddCategoryGroupBox.Text = "Create Category";
            // 
            // NewCategoryCreateButton
            // 
            this.NewCategoryCreateButton.Location = new System.Drawing.Point(215, 90);
            this.NewCategoryCreateButton.Name = "NewCategoryCreateButton";
            this.NewCategoryCreateButton.Size = new System.Drawing.Size(75, 23);
            this.NewCategoryCreateButton.TabIndex = 4;
            this.NewCategoryCreateButton.Text = "Create";
            this.NewCategoryCreateButton.UseVisualStyleBackColor = true;
            this.NewCategoryCreateButton.Click += new System.EventHandler(this.NewCategoryCreateButton_Click);
            // 
            // NewCategoryParentComboBox
            // 
            this.NewCategoryParentComboBox.FormattingEnabled = true;
            this.NewCategoryParentComboBox.Location = new System.Drawing.Point(92, 63);
            this.NewCategoryParentComboBox.Name = "NewCategoryParentComboBox";
            this.NewCategoryParentComboBox.Size = new System.Drawing.Size(198, 21);
            this.NewCategoryParentComboBox.TabIndex = 3;
            // 
            // NewCategoryParentLabel
            // 
            this.NewCategoryParentLabel.AutoSize = true;
            this.NewCategoryParentLabel.Location = new System.Drawing.Point(6, 66);
            this.NewCategoryParentLabel.Name = "NewCategoryParentLabel";
            this.NewCategoryParentLabel.Size = new System.Drawing.Size(50, 13);
            this.NewCategoryParentLabel.TabIndex = 2;
            this.NewCategoryParentLabel.Text = "Create In";
            // 
            // NewCategoryNameTextBox
            // 
            this.NewCategoryNameTextBox.Location = new System.Drawing.Point(92, 28);
            this.NewCategoryNameTextBox.MaxLength = 50;
            this.NewCategoryNameTextBox.Name = "NewCategoryNameTextBox";
            this.NewCategoryNameTextBox.Size = new System.Drawing.Size(198, 20);
            this.NewCategoryNameTextBox.TabIndex = 1;
            // 
            // NewCategoryNameLabel
            // 
            this.NewCategoryNameLabel.AutoSize = true;
            this.NewCategoryNameLabel.Location = new System.Drawing.Point(6, 31);
            this.NewCategoryNameLabel.Name = "NewCategoryNameLabel";
            this.NewCategoryNameLabel.Size = new System.Drawing.Size(80, 13);
            this.NewCategoryNameLabel.TabIndex = 0;
            this.NewCategoryNameLabel.Text = "Category Name";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DeleteCategoryButton);
            this.groupBox7.Controls.Add(this.DeleteCategoryComboBox);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Location = new System.Drawing.Point(19, 439);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(301, 92);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Delete Category";
            // 
            // DeleteCategoryButton
            // 
            this.DeleteCategoryButton.Location = new System.Drawing.Point(215, 62);
            this.DeleteCategoryButton.Name = "DeleteCategoryButton";
            this.DeleteCategoryButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteCategoryButton.TabIndex = 4;
            this.DeleteCategoryButton.Text = "Delete";
            this.DeleteCategoryButton.UseVisualStyleBackColor = true;
            this.DeleteCategoryButton.Click += new System.EventHandler(this.DeleteCategoryButton_Click);
            // 
            // DeleteCategoryComboBox
            // 
            this.DeleteCategoryComboBox.FormattingEnabled = true;
            this.DeleteCategoryComboBox.Location = new System.Drawing.Point(92, 28);
            this.DeleteCategoryComboBox.Name = "DeleteCategoryComboBox";
            this.DeleteCategoryComboBox.Size = new System.Drawing.Size(198, 21);
            this.DeleteCategoryComboBox.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Category";
            // 
            // ReagentCategoryRelGroupBox
            // 
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkUnLinkButton);
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkLinkButton);
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkCategoryComboBox);
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkCategoryLabel);
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkReagentComboBox);
            this.ReagentCategoryRelGroupBox.Controls.Add(this.ReagentCategoryLinkReagentLabel);
            this.ReagentCategoryRelGroupBox.Location = new System.Drawing.Point(16, 147);
            this.ReagentCategoryRelGroupBox.Name = "ReagentCategoryRelGroupBox";
            this.ReagentCategoryRelGroupBox.Size = new System.Drawing.Size(301, 140);
            this.ReagentCategoryRelGroupBox.TabIndex = 1;
            this.ReagentCategoryRelGroupBox.TabStop = false;
            this.ReagentCategoryRelGroupBox.Text = "Reagent/Category Link";
            // 
            // ReagentCategoryLinkUnLinkButton
            // 
            this.ReagentCategoryLinkUnLinkButton.Location = new System.Drawing.Point(213, 87);
            this.ReagentCategoryLinkUnLinkButton.Name = "ReagentCategoryLinkUnLinkButton";
            this.ReagentCategoryLinkUnLinkButton.Size = new System.Drawing.Size(75, 23);
            this.ReagentCategoryLinkUnLinkButton.TabIndex = 9;
            this.ReagentCategoryLinkUnLinkButton.Text = "UnLink";
            this.ReagentCategoryLinkUnLinkButton.UseVisualStyleBackColor = true;
            this.ReagentCategoryLinkUnLinkButton.Click += new System.EventHandler(this.ReagentCategoryLinkUnLinkButton_Click);
            // 
            // ReagentCategoryLinkLinkButton
            // 
            this.ReagentCategoryLinkLinkButton.Location = new System.Drawing.Point(132, 87);
            this.ReagentCategoryLinkLinkButton.Name = "ReagentCategoryLinkLinkButton";
            this.ReagentCategoryLinkLinkButton.Size = new System.Drawing.Size(75, 23);
            this.ReagentCategoryLinkLinkButton.TabIndex = 8;
            this.ReagentCategoryLinkLinkButton.Text = "Link";
            this.ReagentCategoryLinkLinkButton.UseVisualStyleBackColor = true;
            this.ReagentCategoryLinkLinkButton.Click += new System.EventHandler(this.ReagentCategoryLinkLinkButton_Click);
            // 
            // ReagentCategoryLinkCategoryComboBox
            // 
            this.ReagentCategoryLinkCategoryComboBox.FormattingEnabled = true;
            this.ReagentCategoryLinkCategoryComboBox.Location = new System.Drawing.Point(93, 60);
            this.ReagentCategoryLinkCategoryComboBox.Name = "ReagentCategoryLinkCategoryComboBox";
            this.ReagentCategoryLinkCategoryComboBox.Size = new System.Drawing.Size(195, 21);
            this.ReagentCategoryLinkCategoryComboBox.TabIndex = 7;
            // 
            // ReagentCategoryLinkCategoryLabel
            // 
            this.ReagentCategoryLinkCategoryLabel.AutoSize = true;
            this.ReagentCategoryLinkCategoryLabel.Location = new System.Drawing.Point(7, 63);
            this.ReagentCategoryLinkCategoryLabel.Name = "ReagentCategoryLinkCategoryLabel";
            this.ReagentCategoryLinkCategoryLabel.Size = new System.Drawing.Size(49, 13);
            this.ReagentCategoryLinkCategoryLabel.TabIndex = 6;
            this.ReagentCategoryLinkCategoryLabel.Text = "Category";
            // 
            // ReagentCategoryLinkReagentComboBox
            // 
            this.ReagentCategoryLinkReagentComboBox.FormattingEnabled = true;
            this.ReagentCategoryLinkReagentComboBox.Location = new System.Drawing.Point(92, 28);
            this.ReagentCategoryLinkReagentComboBox.Name = "ReagentCategoryLinkReagentComboBox";
            this.ReagentCategoryLinkReagentComboBox.Size = new System.Drawing.Size(196, 21);
            this.ReagentCategoryLinkReagentComboBox.TabIndex = 5;
            // 
            // ReagentCategoryLinkReagentLabel
            // 
            this.ReagentCategoryLinkReagentLabel.AutoSize = true;
            this.ReagentCategoryLinkReagentLabel.Location = new System.Drawing.Point(6, 31);
            this.ReagentCategoryLinkReagentLabel.Name = "ReagentCategoryLinkReagentLabel";
            this.ReagentCategoryLinkReagentLabel.Size = new System.Drawing.Size(48, 13);
            this.ReagentCategoryLinkReagentLabel.TabIndex = 4;
            this.ReagentCategoryLinkReagentLabel.Text = "Reagent";
            // 
            // CategoriesInfoPanel
            // 
            this.CategoriesInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CategoriesInfoPanel.AutoScroll = true;
            this.CategoriesInfoPanel.Location = new System.Drawing.Point(21, 553);
            this.CategoriesInfoPanel.Name = "CategoriesInfoPanel";
            this.CategoriesInfoPanel.Size = new System.Drawing.Size(1236, 64);
            this.CategoriesInfoPanel.TabIndex = 2;
            // 
            // ManageUsersTab
            // 
            this.ManageUsersTab.Controls.Add(this.tc_ManageUsers);
            this.ManageUsersTab.Controls.Add(this.groupBox9);
            this.ManageUsersTab.Controls.Add(this.groupBox8);
            this.ManageUsersTab.Location = new System.Drawing.Point(4, 22);
            this.ManageUsersTab.Name = "ManageUsersTab";
            this.ManageUsersTab.Padding = new System.Windows.Forms.Padding(3);
            this.ManageUsersTab.Size = new System.Drawing.Size(1221, 479);
            this.ManageUsersTab.TabIndex = 5;
            this.ManageUsersTab.Text = "Manage Users";
            this.ManageUsersTab.UseVisualStyleBackColor = true;
            // 
            // tc_ManageUsers
            // 
            this.tc_ManageUsers.Controls.Add(this.tab_Users);
            this.tc_ManageUsers.Controls.Add(this.tab_CostCentres);
            this.tc_ManageUsers.Controls.Add(this.tab_LabsInstitutes);
            this.tc_ManageUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_ManageUsers.Location = new System.Drawing.Point(3, 3);
            this.tc_ManageUsers.Name = "tc_ManageUsers";
            this.tc_ManageUsers.SelectedIndex = 0;
            this.tc_ManageUsers.Size = new System.Drawing.Size(1215, 473);
            this.tc_ManageUsers.TabIndex = 20;
            // 
            // tab_Users
            // 
            this.tab_Users.Controls.Add(this.gb_UserCredentials);
            this.tab_Users.Controls.Add(this.CreateUserGroupBox);
            this.tab_Users.Location = new System.Drawing.Point(4, 22);
            this.tab_Users.Name = "tab_Users";
            this.tab_Users.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Users.Size = new System.Drawing.Size(1207, 447);
            this.tab_Users.TabIndex = 0;
            this.tab_Users.Text = "Users";
            this.tab_Users.UseVisualStyleBackColor = true;
            this.tab_Users.Click += new System.EventHandler(this.tab_Users_Click);
            // 
            // gb_UserCredentials
            // 
            this.gb_UserCredentials.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_UserCredentials.Controls.Add(this.groupBox12);
            this.gb_UserCredentials.Controls.Add(this.gb_UserCredentialsExisting);
            this.gb_UserCredentials.Location = new System.Drawing.Point(347, 15);
            this.gb_UserCredentials.Name = "gb_UserCredentials";
            this.gb_UserCredentials.Size = new System.Drawing.Size(854, 418);
            this.gb_UserCredentials.TabIndex = 2;
            this.gb_UserCredentials.TabStop = false;
            this.gb_UserCredentials.Text = "User Credentials";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btn_UserCredentialAdd);
            this.groupBox12.Controls.Add(this.cmb_CreateCredentialApplication);
            this.groupBox12.Controls.Add(this.label3);
            this.groupBox12.Controls.Add(this.lb_ApplicationDescription);
            this.groupBox12.Controls.Add(this.cmb_CreateCredentialInstitute);
            this.groupBox12.Controls.Add(this.label6);
            this.groupBox12.Controls.Add(this.label4);
            this.groupBox12.Controls.Add(this.cmb_CreateCredentialRole);
            this.groupBox12.Controls.Add(this.cmb_CreateCredentialLab);
            this.groupBox12.Controls.Add(this.label5);
            this.groupBox12.Location = new System.Drawing.Point(6, 18);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(432, 171);
            this.groupBox12.TabIndex = 14;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "New Credential";
            // 
            // btn_UserCredentialAdd
            // 
            this.btn_UserCredentialAdd.Location = new System.Drawing.Point(287, 133);
            this.btn_UserCredentialAdd.Name = "btn_UserCredentialAdd";
            this.btn_UserCredentialAdd.Size = new System.Drawing.Size(121, 32);
            this.btn_UserCredentialAdd.TabIndex = 13;
            this.btn_UserCredentialAdd.Text = "Add Credential";
            this.btn_UserCredentialAdd.UseVisualStyleBackColor = true;
            this.btn_UserCredentialAdd.Click += new System.EventHandler(this.btn_UserCredentialAdd_Click);
            // 
            // cmb_CreateCredentialApplication
            // 
            this.cmb_CreateCredentialApplication.FormattingEnabled = true;
            this.cmb_CreateCredentialApplication.Location = new System.Drawing.Point(71, 19);
            this.cmb_CreateCredentialApplication.Name = "cmb_CreateCredentialApplication";
            this.cmb_CreateCredentialApplication.Size = new System.Drawing.Size(337, 21);
            this.cmb_CreateCredentialApplication.TabIndex = 1;
            this.cmb_CreateCredentialApplication.SelectedIndexChanged += new System.EventHandler(this.cmb_CreateCredentialApplication_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Application";
            // 
            // lb_ApplicationDescription
            // 
            this.lb_ApplicationDescription.AutoSize = true;
            this.lb_ApplicationDescription.Location = new System.Drawing.Point(414, 22);
            this.lb_ApplicationDescription.Name = "lb_ApplicationDescription";
            this.lb_ApplicationDescription.Size = new System.Drawing.Size(0, 13);
            this.lb_ApplicationDescription.TabIndex = 9;
            // 
            // cmb_CreateCredentialInstitute
            // 
            this.cmb_CreateCredentialInstitute.FormattingEnabled = true;
            this.cmb_CreateCredentialInstitute.Location = new System.Drawing.Point(71, 50);
            this.cmb_CreateCredentialInstitute.Name = "cmb_CreateCredentialInstitute";
            this.cmb_CreateCredentialInstitute.Size = new System.Drawing.Size(337, 21);
            this.cmb_CreateCredentialInstitute.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Role";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Institute";
            // 
            // cmb_CreateCredentialRole
            // 
            this.cmb_CreateCredentialRole.FormattingEnabled = true;
            this.cmb_CreateCredentialRole.Location = new System.Drawing.Point(71, 106);
            this.cmb_CreateCredentialRole.Name = "cmb_CreateCredentialRole";
            this.cmb_CreateCredentialRole.Size = new System.Drawing.Size(337, 21);
            this.cmb_CreateCredentialRole.TabIndex = 7;
            // 
            // cmb_CreateCredentialLab
            // 
            this.cmb_CreateCredentialLab.FormattingEnabled = true;
            this.cmb_CreateCredentialLab.Location = new System.Drawing.Point(71, 79);
            this.cmb_CreateCredentialLab.Name = "cmb_CreateCredentialLab";
            this.cmb_CreateCredentialLab.Size = new System.Drawing.Size(337, 21);
            this.cmb_CreateCredentialLab.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Lab";
            // 
            // gb_UserCredentialsExisting
            // 
            this.gb_UserCredentialsExisting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_UserCredentialsExisting.Controls.Add(this.pnl_UserCredentialsExisting);
            this.gb_UserCredentialsExisting.Location = new System.Drawing.Point(6, 195);
            this.gb_UserCredentialsExisting.Name = "gb_UserCredentialsExisting";
            this.gb_UserCredentialsExisting.Size = new System.Drawing.Size(842, 217);
            this.gb_UserCredentialsExisting.TabIndex = 10;
            this.gb_UserCredentialsExisting.TabStop = false;
            this.gb_UserCredentialsExisting.Text = "Existing Credentials";
            // 
            // pnl_UserCredentialsExisting
            // 
            this.pnl_UserCredentialsExisting.AutoScroll = true;
            this.pnl_UserCredentialsExisting.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.pnl_UserCredentialsExisting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_UserCredentialsExisting.Location = new System.Drawing.Point(3, 16);
            this.pnl_UserCredentialsExisting.Name = "pnl_UserCredentialsExisting";
            this.pnl_UserCredentialsExisting.Size = new System.Drawing.Size(836, 198);
            this.pnl_UserCredentialsExisting.TabIndex = 0;
            // 
            // CreateUserGroupBox
            // 
            this.CreateUserGroupBox.Controls.Add(this.label31);
            this.CreateUserGroupBox.Controls.Add(this.label30);
            this.CreateUserGroupBox.Controls.Add(this.btn_UserUpdatePassword);
            this.CreateUserGroupBox.Controls.Add(this.tb_UserUpdatePassword);
            this.CreateUserGroupBox.Controls.Add(this.label7);
            this.CreateUserGroupBox.Controls.Add(this.cmb_UserSelect);
            this.CreateUserGroupBox.Controls.Add(this.label2);
            this.CreateUserGroupBox.Controls.Add(this.cmb_UserInstitute);
            this.CreateUserGroupBox.Controls.Add(this.label1);
            this.CreateUserGroupBox.Controls.Add(this.btn_UserDelete);
            this.CreateUserGroupBox.Controls.Add(this.btn_UserUpdate);
            this.CreateUserGroupBox.Controls.Add(this.btn_UserCreate);
            this.CreateUserGroupBox.Controls.Add(this.cmb_UserLab);
            this.CreateUserGroupBox.Controls.Add(this.chk_IsLabHead);
            this.CreateUserGroupBox.Controls.Add(this.tb_UserExtension);
            this.CreateUserGroupBox.Controls.Add(this.tb_UserEmail);
            this.CreateUserGroupBox.Controls.Add(this.tb_UserLastName);
            this.CreateUserGroupBox.Controls.Add(this.tb_UserFirstName);
            this.CreateUserGroupBox.Controls.Add(this.lbl_IsLabHead);
            this.CreateUserGroupBox.Controls.Add(this.LabLabel);
            this.CreateUserGroupBox.Controls.Add(this.ExtLabel);
            this.CreateUserGroupBox.Controls.Add(this.EmailLabel);
            this.CreateUserGroupBox.Controls.Add(this.LastNameLabel);
            this.CreateUserGroupBox.Controls.Add(this.FirstNameLabel);
            this.CreateUserGroupBox.Location = new System.Drawing.Point(19, 15);
            this.CreateUserGroupBox.Name = "CreateUserGroupBox";
            this.CreateUserGroupBox.Size = new System.Drawing.Size(309, 418);
            this.CreateUserGroupBox.TabIndex = 1;
            this.CreateUserGroupBox.TabStop = false;
            this.CreateUserGroupBox.Text = "Create/Edit User";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(10, 68);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 23;
            this.label31.Text = "User Details";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 13);
            this.label30.TabIndex = 22;
            this.label30.Text = "Select User....";
            // 
            // btn_UserUpdatePassword
            // 
            this.btn_UserUpdatePassword.Location = new System.Drawing.Point(14, 367);
            this.btn_UserUpdatePassword.Name = "btn_UserUpdatePassword";
            this.btn_UserUpdatePassword.Size = new System.Drawing.Size(278, 35);
            this.btn_UserUpdatePassword.TabIndex = 21;
            this.btn_UserUpdatePassword.Text = "Update Password";
            this.btn_UserUpdatePassword.UseVisualStyleBackColor = true;
            this.btn_UserUpdatePassword.Click += new System.EventHandler(this.btn_UserUpdatePassword_Click);
            // 
            // tb_UserUpdatePassword
            // 
            this.tb_UserUpdatePassword.Location = new System.Drawing.Point(91, 337);
            this.tb_UserUpdatePassword.Name = "tb_UserUpdatePassword";
            this.tb_UserUpdatePassword.PasswordChar = '*';
            this.tb_UserUpdatePassword.Size = new System.Drawing.Size(202, 20);
            this.tb_UserUpdatePassword.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "New Password";
            // 
            // cmb_UserSelect
            // 
            this.cmb_UserSelect.FormattingEnabled = true;
            this.cmb_UserSelect.Location = new System.Drawing.Point(85, 42);
            this.cmb_UserSelect.Name = "cmb_UserSelect";
            this.cmb_UserSelect.Size = new System.Drawing.Size(208, 21);
            this.cmb_UserSelect.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "User";
            // 
            // cmb_UserInstitute
            // 
            this.cmb_UserInstitute.FormattingEnabled = true;
            this.cmb_UserInstitute.Location = new System.Drawing.Point(84, 201);
            this.cmb_UserInstitute.Name = "cmb_UserInstitute";
            this.cmb_UserInstitute.Size = new System.Drawing.Size(208, 21);
            this.cmb_UserInstitute.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Institute";
            // 
            // btn_UserDelete
            // 
            this.btn_UserDelete.Location = new System.Drawing.Point(202, 290);
            this.btn_UserDelete.Name = "btn_UserDelete";
            this.btn_UserDelete.Size = new System.Drawing.Size(90, 35);
            this.btn_UserDelete.TabIndex = 14;
            this.btn_UserDelete.Text = "Delete";
            this.btn_UserDelete.UseVisualStyleBackColor = true;
            this.btn_UserDelete.Click += new System.EventHandler(this.btn_UserDelete_Click);
            // 
            // btn_UserUpdate
            // 
            this.btn_UserUpdate.Location = new System.Drawing.Point(108, 290);
            this.btn_UserUpdate.Name = "btn_UserUpdate";
            this.btn_UserUpdate.Size = new System.Drawing.Size(90, 35);
            this.btn_UserUpdate.TabIndex = 13;
            this.btn_UserUpdate.Text = "Update";
            this.btn_UserUpdate.UseVisualStyleBackColor = true;
            this.btn_UserUpdate.Click += new System.EventHandler(this.btn_UserUpdate_Click);
            // 
            // btn_UserCreate
            // 
            this.btn_UserCreate.Location = new System.Drawing.Point(14, 290);
            this.btn_UserCreate.Name = "btn_UserCreate";
            this.btn_UserCreate.Size = new System.Drawing.Size(90, 35);
            this.btn_UserCreate.TabIndex = 12;
            this.btn_UserCreate.Text = "Create";
            this.btn_UserCreate.UseVisualStyleBackColor = true;
            this.btn_UserCreate.Click += new System.EventHandler(this.btn_UserCreate_Click);
            // 
            // cmb_UserLab
            // 
            this.cmb_UserLab.FormattingEnabled = true;
            this.cmb_UserLab.Location = new System.Drawing.Point(84, 229);
            this.cmb_UserLab.Name = "cmb_UserLab";
            this.cmb_UserLab.Size = new System.Drawing.Size(208, 21);
            this.cmb_UserLab.TabIndex = 11;
            // 
            // chk_IsLabHead
            // 
            this.chk_IsLabHead.AutoSize = true;
            this.chk_IsLabHead.Location = new System.Drawing.Point(84, 260);
            this.chk_IsLabHead.Name = "chk_IsLabHead";
            this.chk_IsLabHead.Size = new System.Drawing.Size(15, 14);
            this.chk_IsLabHead.TabIndex = 10;
            this.chk_IsLabHead.UseVisualStyleBackColor = true;
            // 
            // tb_UserExtension
            // 
            this.tb_UserExtension.Location = new System.Drawing.Point(84, 175);
            this.tb_UserExtension.Name = "tb_UserExtension";
            this.tb_UserExtension.Size = new System.Drawing.Size(208, 20);
            this.tb_UserExtension.TabIndex = 9;
            // 
            // tb_UserEmail
            // 
            this.tb_UserEmail.Location = new System.Drawing.Point(84, 146);
            this.tb_UserEmail.Name = "tb_UserEmail";
            this.tb_UserEmail.Size = new System.Drawing.Size(208, 20);
            this.tb_UserEmail.TabIndex = 8;
            // 
            // tb_UserLastName
            // 
            this.tb_UserLastName.Location = new System.Drawing.Point(84, 117);
            this.tb_UserLastName.Name = "tb_UserLastName";
            this.tb_UserLastName.Size = new System.Drawing.Size(208, 20);
            this.tb_UserLastName.TabIndex = 7;
            // 
            // tb_UserFirstName
            // 
            this.tb_UserFirstName.Location = new System.Drawing.Point(84, 88);
            this.tb_UserFirstName.Name = "tb_UserFirstName";
            this.tb_UserFirstName.Size = new System.Drawing.Size(208, 20);
            this.tb_UserFirstName.TabIndex = 6;
            // 
            // lbl_IsLabHead
            // 
            this.lbl_IsLabHead.AutoSize = true;
            this.lbl_IsLabHead.Location = new System.Drawing.Point(10, 261);
            this.lbl_IsLabHead.Name = "lbl_IsLabHead";
            this.lbl_IsLabHead.Size = new System.Drawing.Size(54, 13);
            this.lbl_IsLabHead.TabIndex = 5;
            this.lbl_IsLabHead.Text = "Lab Head";
            // 
            // LabLabel
            // 
            this.LabLabel.AutoSize = true;
            this.LabLabel.Location = new System.Drawing.Point(10, 232);
            this.LabLabel.Name = "LabLabel";
            this.LabLabel.Size = new System.Drawing.Size(25, 13);
            this.LabLabel.TabIndex = 4;
            this.LabLabel.Text = "Lab";
            // 
            // ExtLabel
            // 
            this.ExtLabel.AutoSize = true;
            this.ExtLabel.Location = new System.Drawing.Point(10, 178);
            this.ExtLabel.Name = "ExtLabel";
            this.ExtLabel.Size = new System.Drawing.Size(53, 13);
            this.ExtLabel.TabIndex = 3;
            this.ExtLabel.Text = "Extension";
            // 
            // EmailLabel
            // 
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Location = new System.Drawing.Point(10, 149);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(32, 13);
            this.EmailLabel.TabIndex = 2;
            this.EmailLabel.Text = "Email";
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(10, 120);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(58, 13);
            this.LastNameLabel.TabIndex = 1;
            this.LastNameLabel.Text = "Last Name";
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(10, 92);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(57, 13);
            this.FirstNameLabel.TabIndex = 0;
            this.FirstNameLabel.Text = "First Name";
            // 
            // tab_CostCentres
            // 
            this.tab_CostCentres.Controls.Add(this.groupBox10);
            this.tab_CostCentres.Location = new System.Drawing.Point(4, 22);
            this.tab_CostCentres.Name = "tab_CostCentres";
            this.tab_CostCentres.Size = new System.Drawing.Size(1207, 447);
            this.tab_CostCentres.TabIndex = 2;
            this.tab_CostCentres.Text = "Cost Centres";
            this.tab_CostCentres.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label32);
            this.groupBox10.Controls.Add(this.label33);
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Controls.Add(this.chkb_CostCentreRetired);
            this.groupBox10.Controls.Add(this.btn_CostCentreDelete);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this.cmb_CostCentre);
            this.groupBox10.Controls.Add(this.btn_CostCentreUpdate);
            this.groupBox10.Controls.Add(this.label8);
            this.groupBox10.Controls.Add(this.cmb_CostCentreInstitute);
            this.groupBox10.Controls.Add(this.tb_CostCentreDescription);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.tb_CostCentreCode);
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.cmb_CostCentreLab);
            this.groupBox10.Controls.Add(this.btn_CostCentreCreate);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Location = new System.Drawing.Point(9, 14);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(302, 280);
            this.groupBox10.TabIndex = 19;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Create Cost Centre";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(7, 71);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(97, 13);
            this.label32.TabIndex = 29;
            this.label32.Text = "Cost Centre Details";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(7, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(107, 13);
            this.label33.TabIndex = 28;
            this.label33.Text = "Select Cost Centre....";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Retired";
            // 
            // chkb_CostCentreRetired
            // 
            this.chkb_CostCentreRetired.AutoSize = true;
            this.chkb_CostCentreRetired.Location = new System.Drawing.Point(71, 196);
            this.chkb_CostCentreRetired.Name = "chkb_CostCentreRetired";
            this.chkb_CostCentreRetired.Size = new System.Drawing.Size(15, 14);
            this.chkb_CostCentreRetired.TabIndex = 26;
            this.chkb_CostCentreRetired.UseVisualStyleBackColor = true;
            // 
            // btn_CostCentreDelete
            // 
            this.btn_CostCentreDelete.Location = new System.Drawing.Point(197, 216);
            this.btn_CostCentreDelete.Name = "btn_CostCentreDelete";
            this.btn_CostCentreDelete.Size = new System.Drawing.Size(93, 35);
            this.btn_CostCentreDelete.TabIndex = 25;
            this.btn_CostCentreDelete.Text = "Delete";
            this.btn_CostCentreDelete.UseVisualStyleBackColor = true;
            this.btn_CostCentreDelete.Click += new System.EventHandler(this.btn_CostCentreDelete_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Cost Centre";
            // 
            // cmb_CostCentre
            // 
            this.cmb_CostCentre.FormattingEnabled = true;
            this.cmb_CostCentre.Location = new System.Drawing.Point(72, 43);
            this.cmb_CostCentre.Name = "cmb_CostCentre";
            this.cmb_CostCentre.Size = new System.Drawing.Size(218, 21);
            this.cmb_CostCentre.TabIndex = 23;
            this.cmb_CostCentre.SelectedIndexChanged += new System.EventHandler(this.cmb_CostCentre_SelectedIndexChanged);
            // 
            // btn_CostCentreUpdate
            // 
            this.btn_CostCentreUpdate.Location = new System.Drawing.Point(101, 216);
            this.btn_CostCentreUpdate.Name = "btn_CostCentreUpdate";
            this.btn_CostCentreUpdate.Size = new System.Drawing.Size(93, 35);
            this.btn_CostCentreUpdate.TabIndex = 22;
            this.btn_CostCentreUpdate.Text = "Update";
            this.btn_CostCentreUpdate.UseVisualStyleBackColor = true;
            this.btn_CostCentreUpdate.Click += new System.EventHandler(this.btn_CostCentreUpdate_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Institute";
            // 
            // cmb_CostCentreInstitute
            // 
            this.cmb_CostCentreInstitute.FormattingEnabled = true;
            this.cmb_CostCentreInstitute.Location = new System.Drawing.Point(72, 89);
            this.cmb_CostCentreInstitute.Name = "cmb_CostCentreInstitute";
            this.cmb_CostCentreInstitute.Size = new System.Drawing.Size(218, 21);
            this.cmb_CostCentreInstitute.TabIndex = 20;
            this.cmb_CostCentreInstitute.SelectedIndexChanged += new System.EventHandler(this.cmb_CostCentreInstitute_SelectedIndexChanged);
            // 
            // tb_CostCentreDescription
            // 
            this.tb_CostCentreDescription.Location = new System.Drawing.Point(71, 169);
            this.tb_CostCentreDescription.Name = "tb_CostCentreDescription";
            this.tb_CostCentreDescription.Size = new System.Drawing.Size(219, 20);
            this.tb_CostCentreDescription.TabIndex = 19;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 171);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Description";
            // 
            // tb_CostCentreCode
            // 
            this.tb_CostCentreCode.Location = new System.Drawing.Point(71, 143);
            this.tb_CostCentreCode.Name = "tb_CostCentreCode";
            this.tb_CostCentreCode.Size = new System.Drawing.Size(219, 20);
            this.tb_CostCentreCode.TabIndex = 17;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "Lab";
            // 
            // cmb_CostCentreLab
            // 
            this.cmb_CostCentreLab.FormattingEnabled = true;
            this.cmb_CostCentreLab.Location = new System.Drawing.Point(72, 116);
            this.cmb_CostCentreLab.Name = "cmb_CostCentreLab";
            this.cmb_CostCentreLab.Size = new System.Drawing.Size(218, 21);
            this.cmb_CostCentreLab.TabIndex = 15;
            // 
            // btn_CostCentreCreate
            // 
            this.btn_CostCentreCreate.Location = new System.Drawing.Point(11, 216);
            this.btn_CostCentreCreate.Name = "btn_CostCentreCreate";
            this.btn_CostCentreCreate.Size = new System.Drawing.Size(85, 35);
            this.btn_CostCentreCreate.TabIndex = 13;
            this.btn_CostCentreCreate.Text = "Create";
            this.btn_CostCentreCreate.UseVisualStyleBackColor = true;
            this.btn_CostCentreCreate.Click += new System.EventHandler(this.btn_CostCentreCreate_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 145);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Code";
            // 
            // tab_LabsInstitutes
            // 
            this.tab_LabsInstitutes.Controls.Add(this.groupBox16);
            this.tab_LabsInstitutes.Controls.Add(this.groupBox15);
            this.tab_LabsInstitutes.Controls.Add(this.CreateLabGroupBox);
            this.tab_LabsInstitutes.Location = new System.Drawing.Point(4, 22);
            this.tab_LabsInstitutes.Name = "tab_LabsInstitutes";
            this.tab_LabsInstitutes.Size = new System.Drawing.Size(1207, 447);
            this.tab_LabsInstitutes.TabIndex = 3;
            this.tab_LabsInstitutes.Text = "Labs/Institutes";
            this.tab_LabsInstitutes.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox16.Controls.Add(this.pnl_LabMembers);
            this.groupBox16.Location = new System.Drawing.Point(10, 224);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(315, 205);
            this.groupBox16.TabIndex = 20;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Lab Members";
            // 
            // pnl_LabMembers
            // 
            this.pnl_LabMembers.AutoScroll = true;
            this.pnl_LabMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_LabMembers.Location = new System.Drawing.Point(3, 16);
            this.pnl_LabMembers.Name = "pnl_LabMembers";
            this.pnl_LabMembers.Size = new System.Drawing.Size(309, 186);
            this.pnl_LabMembers.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label34);
            this.groupBox15.Controls.Add(this.label35);
            this.groupBox15.Controls.Add(this.cmb_LabSelect);
            this.groupBox15.Controls.Add(this.btn_LabDelete);
            this.groupBox15.Controls.Add(this.btn_LabUpdate);
            this.groupBox15.Controls.Add(this.btn_LabCreate);
            this.groupBox15.Controls.Add(this.label14);
            this.groupBox15.Controls.Add(this.label12);
            this.groupBox15.Controls.Add(this.cmb_LabInstitute);
            this.groupBox15.Controls.Add(this.tb_LabName);
            this.groupBox15.Controls.Add(this.label13);
            this.groupBox15.Location = new System.Drawing.Point(10, 24);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(315, 191);
            this.groupBox15.TabIndex = 19;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Lab";
            this.groupBox15.Enter += new System.EventHandler(this.groupBox15_Enter);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(20, 65);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "Lab Details";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(20, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(70, 13);
            this.label35.TabIndex = 36;
            this.label35.Text = "Select Lab....";
            // 
            // cmb_LabSelect
            // 
            this.cmb_LabSelect.FormattingEnabled = true;
            this.cmb_LabSelect.Location = new System.Drawing.Point(84, 36);
            this.cmb_LabSelect.Name = "cmb_LabSelect";
            this.cmb_LabSelect.Size = new System.Drawing.Size(188, 21);
            this.cmb_LabSelect.TabIndex = 35;
            this.cmb_LabSelect.SelectedIndexChanged += new System.EventHandler(this.cmb_LabSelect_SelectedIndexChanged);
            // 
            // btn_LabDelete
            // 
            this.btn_LabDelete.Location = new System.Drawing.Point(207, 142);
            this.btn_LabDelete.Name = "btn_LabDelete";
            this.btn_LabDelete.Size = new System.Drawing.Size(90, 35);
            this.btn_LabDelete.TabIndex = 34;
            this.btn_LabDelete.Text = "Delete";
            this.btn_LabDelete.UseVisualStyleBackColor = true;
            this.btn_LabDelete.Click += new System.EventHandler(this.btn_LabDelete_Click);
            // 
            // btn_LabUpdate
            // 
            this.btn_LabUpdate.Location = new System.Drawing.Point(111, 142);
            this.btn_LabUpdate.Name = "btn_LabUpdate";
            this.btn_LabUpdate.Size = new System.Drawing.Size(90, 35);
            this.btn_LabUpdate.TabIndex = 33;
            this.btn_LabUpdate.Text = "Update";
            this.btn_LabUpdate.UseVisualStyleBackColor = true;
            this.btn_LabUpdate.Click += new System.EventHandler(this.btn_LabUpdate_Click);
            // 
            // btn_LabCreate
            // 
            this.btn_LabCreate.Location = new System.Drawing.Point(15, 142);
            this.btn_LabCreate.Name = "btn_LabCreate";
            this.btn_LabCreate.Size = new System.Drawing.Size(90, 35);
            this.btn_LabCreate.TabIndex = 32;
            this.btn_LabCreate.Text = "Create";
            this.btn_LabCreate.UseVisualStyleBackColor = true;
            this.btn_LabCreate.Click += new System.EventHandler(this.btn_LabCreate_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Lab";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 87);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Institute";
            // 
            // cmb_LabInstitute
            // 
            this.cmb_LabInstitute.FormattingEnabled = true;
            this.cmb_LabInstitute.Location = new System.Drawing.Point(84, 84);
            this.cmb_LabInstitute.Name = "cmb_LabInstitute";
            this.cmb_LabInstitute.Size = new System.Drawing.Size(188, 21);
            this.cmb_LabInstitute.TabIndex = 22;
            // 
            // tb_LabName
            // 
            this.tb_LabName.Location = new System.Drawing.Point(84, 111);
            this.tb_LabName.Name = "tb_LabName";
            this.tb_LabName.Size = new System.Drawing.Size(188, 20);
            this.tb_LabName.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Lab Name";
            // 
            // CreateLabGroupBox
            // 
            this.CreateLabGroupBox.Controls.Add(this.label36);
            this.CreateLabGroupBox.Controls.Add(this.label37);
            this.CreateLabGroupBox.Controls.Add(this.tb_InstituteDeliveryAddress);
            this.CreateLabGroupBox.Controls.Add(this.label29);
            this.CreateLabGroupBox.Controls.Add(this.tb_InstituteBillingAddress);
            this.CreateLabGroupBox.Controls.Add(this.label28);
            this.CreateLabGroupBox.Controls.Add(this.btn_InstituteDelete);
            this.CreateLabGroupBox.Controls.Add(this.btn_InstituteUpdate);
            this.CreateLabGroupBox.Controls.Add(this.label27);
            this.CreateLabGroupBox.Controls.Add(this.chk_InstituteCurrentSite);
            this.CreateLabGroupBox.Controls.Add(this.label11);
            this.CreateLabGroupBox.Controls.Add(this.cmb_InstituteSelect);
            this.CreateLabGroupBox.Controls.Add(this.btn_InstituteCreate);
            this.CreateLabGroupBox.Controls.Add(this.tb_InstituteName);
            this.CreateLabGroupBox.Controls.Add(this.CreateLabLabNameLabel);
            this.CreateLabGroupBox.Location = new System.Drawing.Point(342, 24);
            this.CreateLabGroupBox.Name = "CreateLabGroupBox";
            this.CreateLabGroupBox.Size = new System.Drawing.Size(304, 322);
            this.CreateLabGroupBox.TabIndex = 18;
            this.CreateLabGroupBox.TabStop = false;
            this.CreateLabGroupBox.Text = "Institute";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(9, 65);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 13);
            this.label36.TabIndex = 37;
            this.label36.Text = "Institute Details";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 19);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 13);
            this.label37.TabIndex = 36;
            this.label37.Text = "Select Institute....";
            // 
            // tb_InstituteDeliveryAddress
            // 
            this.tb_InstituteDeliveryAddress.Location = new System.Drawing.Point(95, 175);
            this.tb_InstituteDeliveryAddress.Multiline = true;
            this.tb_InstituteDeliveryAddress.Name = "tb_InstituteDeliveryAddress";
            this.tb_InstituteDeliveryAddress.Size = new System.Drawing.Size(188, 60);
            this.tb_InstituteDeliveryAddress.TabIndex = 35;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(10, 178);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 13);
            this.label29.TabIndex = 34;
            this.label29.Text = "Delivery Address";
            // 
            // tb_InstituteBillingAddress
            // 
            this.tb_InstituteBillingAddress.Location = new System.Drawing.Point(95, 109);
            this.tb_InstituteBillingAddress.Multiline = true;
            this.tb_InstituteBillingAddress.Name = "tb_InstituteBillingAddress";
            this.tb_InstituteBillingAddress.Size = new System.Drawing.Size(188, 60);
            this.tb_InstituteBillingAddress.TabIndex = 33;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(10, 112);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "Billing Address";
            // 
            // btn_InstituteDelete
            // 
            this.btn_InstituteDelete.Location = new System.Drawing.Point(208, 270);
            this.btn_InstituteDelete.Name = "btn_InstituteDelete";
            this.btn_InstituteDelete.Size = new System.Drawing.Size(90, 35);
            this.btn_InstituteDelete.TabIndex = 31;
            this.btn_InstituteDelete.Text = "Delete";
            this.btn_InstituteDelete.UseVisualStyleBackColor = true;
            this.btn_InstituteDelete.Click += new System.EventHandler(this.btn_InstituteDelete_Click);
            // 
            // btn_InstituteUpdate
            // 
            this.btn_InstituteUpdate.Location = new System.Drawing.Point(112, 270);
            this.btn_InstituteUpdate.Name = "btn_InstituteUpdate";
            this.btn_InstituteUpdate.Size = new System.Drawing.Size(90, 35);
            this.btn_InstituteUpdate.TabIndex = 30;
            this.btn_InstituteUpdate.Text = "Update";
            this.btn_InstituteUpdate.UseVisualStyleBackColor = true;
            this.btn_InstituteUpdate.Click += new System.EventHandler(this.btn_InstituteUpdate_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(21, 243);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 13);
            this.label27.TabIndex = 29;
            this.label27.Text = "Current Site";
            // 
            // chk_InstituteCurrentSite
            // 
            this.chk_InstituteCurrentSite.AutoSize = true;
            this.chk_InstituteCurrentSite.Location = new System.Drawing.Point(95, 243);
            this.chk_InstituteCurrentSite.Name = "chk_InstituteCurrentSite";
            this.chk_InstituteCurrentSite.Size = new System.Drawing.Size(15, 14);
            this.chk_InstituteCurrentSite.TabIndex = 28;
            this.chk_InstituteCurrentSite.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Institute";
            // 
            // cmb_InstituteSelect
            // 
            this.cmb_InstituteSelect.FormattingEnabled = true;
            this.cmb_InstituteSelect.Location = new System.Drawing.Point(95, 36);
            this.cmb_InstituteSelect.Name = "cmb_InstituteSelect";
            this.cmb_InstituteSelect.Size = new System.Drawing.Size(188, 21);
            this.cmb_InstituteSelect.TabIndex = 22;
            this.cmb_InstituteSelect.SelectedIndexChanged += new System.EventHandler(this.cmb_InstituteSelect_SelectedIndexChanged);
            // 
            // btn_InstituteCreate
            // 
            this.btn_InstituteCreate.Location = new System.Drawing.Point(16, 270);
            this.btn_InstituteCreate.Name = "btn_InstituteCreate";
            this.btn_InstituteCreate.Size = new System.Drawing.Size(90, 35);
            this.btn_InstituteCreate.TabIndex = 13;
            this.btn_InstituteCreate.Text = "Create";
            this.btn_InstituteCreate.UseVisualStyleBackColor = true;
            this.btn_InstituteCreate.Click += new System.EventHandler(this.btn_InstituteCreate_Click);
            // 
            // tb_InstituteName
            // 
            this.tb_InstituteName.Location = new System.Drawing.Point(95, 83);
            this.tb_InstituteName.Name = "tb_InstituteName";
            this.tb_InstituteName.Size = new System.Drawing.Size(188, 20);
            this.tb_InstituteName.TabIndex = 1;
            // 
            // CreateLabLabNameLabel
            // 
            this.CreateLabLabNameLabel.AutoSize = true;
            this.CreateLabLabNameLabel.Location = new System.Drawing.Point(10, 86);
            this.CreateLabLabNameLabel.Name = "CreateLabLabNameLabel";
            this.CreateLabLabNameLabel.Size = new System.Drawing.Size(75, 13);
            this.CreateLabLabNameLabel.TabIndex = 0;
            this.CreateLabLabNameLabel.Text = "Institute Name";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.DeleteCostCentreComboBox);
            this.groupBox9.Controls.Add(this.DeleteCostCentreButton);
            this.groupBox9.Controls.Add(this.DeleteCostCentreLabel);
            this.groupBox9.Location = new System.Drawing.Point(311, 484);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(277, 121);
            this.groupBox9.TabIndex = 19;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Delete Cost Centre";
            this.groupBox9.Visible = false;
            // 
            // DeleteCostCentreComboBox
            // 
            this.DeleteCostCentreComboBox.FormattingEnabled = true;
            this.DeleteCostCentreComboBox.Location = new System.Drawing.Point(75, 34);
            this.DeleteCostCentreComboBox.Name = "DeleteCostCentreComboBox";
            this.DeleteCostCentreComboBox.Size = new System.Drawing.Size(187, 21);
            this.DeleteCostCentreComboBox.TabIndex = 14;
            // 
            // DeleteCostCentreButton
            // 
            this.DeleteCostCentreButton.Location = new System.Drawing.Point(13, 71);
            this.DeleteCostCentreButton.Name = "DeleteCostCentreButton";
            this.DeleteCostCentreButton.Size = new System.Drawing.Size(250, 35);
            this.DeleteCostCentreButton.TabIndex = 13;
            this.DeleteCostCentreButton.Text = "Delete";
            this.DeleteCostCentreButton.UseVisualStyleBackColor = true;
            // 
            // DeleteCostCentreLabel
            // 
            this.DeleteCostCentreLabel.AutoSize = true;
            this.DeleteCostCentreLabel.Location = new System.Drawing.Point(10, 37);
            this.DeleteCostCentreLabel.Name = "DeleteCostCentreLabel";
            this.DeleteCostCentreLabel.Size = new System.Drawing.Size(65, 13);
            this.DeleteCostCentreLabel.TabIndex = 0;
            this.DeleteCostCentreLabel.Text = "Cost Centre:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.DeleteLabComboBox);
            this.groupBox8.Controls.Add(this.DeleteLabButton);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Location = new System.Drawing.Point(17, 484);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(277, 121);
            this.groupBox8.TabIndex = 17;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Delete Lab";
            this.groupBox8.Visible = false;
            // 
            // DeleteLabComboBox
            // 
            this.DeleteLabComboBox.FormattingEnabled = true;
            this.DeleteLabComboBox.Location = new System.Drawing.Point(75, 34);
            this.DeleteLabComboBox.Name = "DeleteLabComboBox";
            this.DeleteLabComboBox.Size = new System.Drawing.Size(187, 21);
            this.DeleteLabComboBox.TabIndex = 14;
            // 
            // DeleteLabButton
            // 
            this.DeleteLabButton.Location = new System.Drawing.Point(13, 71);
            this.DeleteLabButton.Name = "DeleteLabButton";
            this.DeleteLabButton.Size = new System.Drawing.Size(250, 35);
            this.DeleteLabButton.TabIndex = 13;
            this.DeleteLabButton.Text = "Delete";
            this.DeleteLabButton.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Lab Name:";
            // 
            // ConfigTabPage
            // 
            this.ConfigTabPage.Controls.Add(this.groupBox18);
            this.ConfigTabPage.Controls.Add(this.DBConfigGroupBox);
            this.ConfigTabPage.Location = new System.Drawing.Point(4, 22);
            this.ConfigTabPage.Name = "ConfigTabPage";
            this.ConfigTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ConfigTabPage.Size = new System.Drawing.Size(1221, 479);
            this.ConfigTabPage.TabIndex = 8;
            this.ConfigTabPage.Text = "Config";
            this.ConfigTabPage.UseVisualStyleBackColor = true;
            this.ConfigTabPage.Enter += new System.EventHandler(this.tab_Config_Enter);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label39);
            this.groupBox18.Controls.Add(this.tb_SMTPPort);
            this.groupBox18.Controls.Add(this.btn_SMTPUpdate);
            this.groupBox18.Controls.Add(this.label38);
            this.groupBox18.Controls.Add(this.tb_SMTPServer);
            this.groupBox18.Location = new System.Drawing.Point(8, 84);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(251, 100);
            this.groupBox18.TabIndex = 10;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "SMTP Settings";
            // 
            // btn_SMTPUpdate
            // 
            this.btn_SMTPUpdate.Location = new System.Drawing.Point(161, 71);
            this.btn_SMTPUpdate.Name = "btn_SMTPUpdate";
            this.btn_SMTPUpdate.Size = new System.Drawing.Size(75, 23);
            this.btn_SMTPUpdate.TabIndex = 2;
            this.btn_SMTPUpdate.Text = "Update";
            this.btn_SMTPUpdate.UseVisualStyleBackColor = true;
            this.btn_SMTPUpdate.Click += new System.EventHandler(this.btn_SMTPUpdate_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "SMTP Server";
            // 
            // tb_SMTPServer
            // 
            this.tb_SMTPServer.Location = new System.Drawing.Point(76, 19);
            this.tb_SMTPServer.Name = "tb_SMTPServer";
            this.tb_SMTPServer.Size = new System.Drawing.Size(160, 20);
            this.tb_SMTPServer.TabIndex = 0;
            // 
            // DBConfigGroupBox
            // 
            this.DBConfigGroupBox.Controls.Add(this.DatabaseConfigEditButton);
            this.DBConfigGroupBox.Location = new System.Drawing.Point(8, 4);
            this.DBConfigGroupBox.Name = "DBConfigGroupBox";
            this.DBConfigGroupBox.Size = new System.Drawing.Size(251, 54);
            this.DBConfigGroupBox.TabIndex = 0;
            this.DBConfigGroupBox.TabStop = false;
            this.DBConfigGroupBox.Text = "Database Configuration";
            // 
            // DatabaseConfigEditButton
            // 
            this.DatabaseConfigEditButton.Location = new System.Drawing.Point(6, 19);
            this.DatabaseConfigEditButton.Name = "DatabaseConfigEditButton";
            this.DatabaseConfigEditButton.Size = new System.Drawing.Size(230, 23);
            this.DatabaseConfigEditButton.TabIndex = 8;
            this.DatabaseConfigEditButton.Text = "Edit Database Configuration";
            this.DatabaseConfigEditButton.UseVisualStyleBackColor = true;
            this.DatabaseConfigEditButton.Click += new System.EventHandler(this.DatabaseConfigEditButton_Click);
            // 
            // AdministrationExitButton
            // 
            this.AdministrationExitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AdministrationExitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdministrationExitButton.Location = new System.Drawing.Point(1096, 514);
            this.AdministrationExitButton.Name = "AdministrationExitButton";
            this.AdministrationExitButton.Size = new System.Drawing.Size(136, 55);
            this.AdministrationExitButton.TabIndex = 1;
            this.AdministrationExitButton.Text = "Exit";
            this.AdministrationExitButton.UseVisualStyleBackColor = true;
            this.AdministrationExitButton.Click += new System.EventHandler(this.AdministrationExitButton_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(12, 25);
            this.monthCalendar1.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.monthCalendar1.MaxSelectionCount = 1;
            this.monthCalendar1.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewCheckBoxColumn1});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 16);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridView1.Size = new System.Drawing.Size(590, 550);
            this.dataGridView1.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn1.HeaderText = "EventID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ItemName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "UserName";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.HeaderText = "Date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Billed";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewButtonColumn1,
            this.dataGridViewButtonColumn2});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridView2.Size = new System.Drawing.Size(335, 550);
            this.dataGridView2.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.HeaderText = "CostCentre";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.HeaderText = "BillableAmount";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewButtonColumn1.HeaderText = "Bill";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.ReadOnly = true;
            this.dataGridViewButtonColumn1.Text = "Bill";
            this.dataGridViewButtonColumn1.UseColumnTextForButtonValue = true;
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.HeaderText = "UnBill";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            this.dataGridViewButtonColumn2.ReadOnly = true;
            this.dataGridViewButtonColumn2.Text = "UnBill";
            this.dataGridViewButtonColumn2.UseColumnTextForButtonValue = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewButtonColumn3,
            this.dataGridViewButtonColumn4});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(3, 16);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridView3.Size = new System.Drawing.Size(453, 550);
            this.dataGridView3.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn8.HeaderText = "DebtorCostCentre";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "CreditorCostCentre";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn12.HeaderText = "BillableAmount";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewButtonColumn3
            // 
            this.dataGridViewButtonColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewButtonColumn3.HeaderText = "Bill";
            this.dataGridViewButtonColumn3.Name = "dataGridViewButtonColumn3";
            this.dataGridViewButtonColumn3.ReadOnly = true;
            this.dataGridViewButtonColumn3.Text = "Bill";
            this.dataGridViewButtonColumn3.UseColumnTextForButtonValue = true;
            // 
            // dataGridViewButtonColumn4
            // 
            this.dataGridViewButtonColumn4.HeaderText = "UnBill";
            this.dataGridViewButtonColumn4.Name = "dataGridViewButtonColumn4";
            this.dataGridViewButtonColumn4.ReadOnly = true;
            this.dataGridViewButtonColumn4.Text = "UnBill";
            this.dataGridViewButtonColumn4.UseColumnTextForButtonValue = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(196, 44);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(36, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "All";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(114, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(50, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Billed";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(19, 44);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(63, 17);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Unbilled";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // monthCalendar2
            // 
            this.monthCalendar2.Location = new System.Drawing.Point(12, 25);
            this.monthCalendar2.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.monthCalendar2.MaxSelectionCount = 1;
            this.monthCalendar2.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.monthCalendar2.Name = "monthCalendar2";
            this.monthCalendar2.TabIndex = 0;
            // 
            // monthCalendar3
            // 
            this.monthCalendar3.Location = new System.Drawing.Point(12, 25);
            this.monthCalendar3.MaxDate = new System.DateTime(2025, 1, 1, 0, 0, 0, 0);
            this.monthCalendar3.MaxSelectionCount = 1;
            this.monthCalendar3.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.monthCalendar3.Name = "monthCalendar3";
            this.monthCalendar3.TabIndex = 0;
            // 
            // ShowIDCheckBox
            // 
            this.ShowIDCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowIDCheckBox.AutoSize = true;
            this.ShowIDCheckBox.Location = new System.Drawing.Point(12, 518);
            this.ShowIDCheckBox.Name = "ShowIDCheckBox";
            this.ShowIDCheckBox.Size = new System.Drawing.Size(37, 17);
            this.ShowIDCheckBox.TabIndex = 2;
            this.ShowIDCheckBox.Text = "ID";
            this.ShowIDCheckBox.UseVisualStyleBackColor = true;
            this.ShowIDCheckBox.CheckedChanged += new System.EventHandler(this.ShowIDCheckBox_CheckedChanged);
            // 
            // ShowNameCheckBox
            // 
            this.ShowNameCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowNameCheckBox.AutoSize = true;
            this.ShowNameCheckBox.Location = new System.Drawing.Point(12, 535);
            this.ShowNameCheckBox.Name = "ShowNameCheckBox";
            this.ShowNameCheckBox.Size = new System.Drawing.Size(54, 17);
            this.ShowNameCheckBox.TabIndex = 3;
            this.ShowNameCheckBox.Text = "Name";
            this.ShowNameCheckBox.UseVisualStyleBackColor = true;
            this.ShowNameCheckBox.CheckedChanged += new System.EventHandler(this.ShowNameCheckBox_CheckedChanged);
            // 
            // ShowManufacturerCheckBox
            // 
            this.ShowManufacturerCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowManufacturerCheckBox.AutoSize = true;
            this.ShowManufacturerCheckBox.Location = new System.Drawing.Point(12, 552);
            this.ShowManufacturerCheckBox.Name = "ShowManufacturerCheckBox";
            this.ShowManufacturerCheckBox.Size = new System.Drawing.Size(89, 17);
            this.ShowManufacturerCheckBox.TabIndex = 4;
            this.ShowManufacturerCheckBox.Text = "Manufacturer";
            this.ShowManufacturerCheckBox.UseVisualStyleBackColor = true;
            this.ShowManufacturerCheckBox.CheckedChanged += new System.EventHandler(this.ShowManufacturerCheckBox_CheckedChanged);
            // 
            // ShowLastOrderedCheckBox
            // 
            this.ShowLastOrderedCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowLastOrderedCheckBox.AutoSize = true;
            this.ShowLastOrderedCheckBox.Location = new System.Drawing.Point(124, 552);
            this.ShowLastOrderedCheckBox.Name = "ShowLastOrderedCheckBox";
            this.ShowLastOrderedCheckBox.Size = new System.Drawing.Size(87, 17);
            this.ShowLastOrderedCheckBox.TabIndex = 7;
            this.ShowLastOrderedCheckBox.Text = "Last Ordered";
            this.ShowLastOrderedCheckBox.UseVisualStyleBackColor = true;
            this.ShowLastOrderedCheckBox.CheckedChanged += new System.EventHandler(this.ShowLastOrderedCheckBox_CheckedChanged);
            // 
            // ShowPartNumberCheckBox
            // 
            this.ShowPartNumberCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowPartNumberCheckBox.AutoSize = true;
            this.ShowPartNumberCheckBox.Location = new System.Drawing.Point(124, 535);
            this.ShowPartNumberCheckBox.Name = "ShowPartNumberCheckBox";
            this.ShowPartNumberCheckBox.Size = new System.Drawing.Size(85, 17);
            this.ShowPartNumberCheckBox.TabIndex = 6;
            this.ShowPartNumberCheckBox.Text = "Part Number";
            this.ShowPartNumberCheckBox.UseVisualStyleBackColor = true;
            this.ShowPartNumberCheckBox.CheckedChanged += new System.EventHandler(this.ShowPartNumberCheckBox_CheckedChanged);
            // 
            // ShowSupplierCheckBox
            // 
            this.ShowSupplierCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowSupplierCheckBox.AutoSize = true;
            this.ShowSupplierCheckBox.Location = new System.Drawing.Point(124, 518);
            this.ShowSupplierCheckBox.Name = "ShowSupplierCheckBox";
            this.ShowSupplierCheckBox.Size = new System.Drawing.Size(64, 17);
            this.ShowSupplierCheckBox.TabIndex = 5;
            this.ShowSupplierCheckBox.Text = "Supplier";
            this.ShowSupplierCheckBox.UseVisualStyleBackColor = true;
            this.ShowSupplierCheckBox.CheckedChanged += new System.EventHandler(this.ShowSupplierCheckBox_CheckedChanged);
            // 
            // ShowUnitofSaleCheckBox
            // 
            this.ShowUnitofSaleCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowUnitofSaleCheckBox.AutoSize = true;
            this.ShowUnitofSaleCheckBox.Location = new System.Drawing.Point(335, 552);
            this.ShowUnitofSaleCheckBox.Name = "ShowUnitofSaleCheckBox";
            this.ShowUnitofSaleCheckBox.Size = new System.Drawing.Size(81, 17);
            this.ShowUnitofSaleCheckBox.TabIndex = 13;
            this.ShowUnitofSaleCheckBox.Text = "Unit of Sale";
            this.ShowUnitofSaleCheckBox.UseVisualStyleBackColor = true;
            this.ShowUnitofSaleCheckBox.CheckedChanged += new System.EventHandler(this.ShowUnitofSaleCheckBox_CheckedChanged);
            // 
            // ShowCPUCheckBox
            // 
            this.ShowCPUCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowCPUCheckBox.AutoSize = true;
            this.ShowCPUCheckBox.Location = new System.Drawing.Point(335, 535);
            this.ShowCPUCheckBox.Name = "ShowCPUCheckBox";
            this.ShowCPUCheckBox.Size = new System.Drawing.Size(88, 17);
            this.ShowCPUCheckBox.TabIndex = 12;
            this.ShowCPUCheckBox.Text = "Cost Per Unit";
            this.ShowCPUCheckBox.UseVisualStyleBackColor = true;
            this.ShowCPUCheckBox.CheckedChanged += new System.EventHandler(this.ShowCPUCheckBox_CheckedChanged);
            // 
            // ShowCostCheckBox
            // 
            this.ShowCostCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowCostCheckBox.AutoSize = true;
            this.ShowCostCheckBox.Location = new System.Drawing.Point(335, 518);
            this.ShowCostCheckBox.Name = "ShowCostCheckBox";
            this.ShowCostCheckBox.Size = new System.Drawing.Size(47, 17);
            this.ShowCostCheckBox.TabIndex = 11;
            this.ShowCostCheckBox.Text = "Cost";
            this.ShowCostCheckBox.UseVisualStyleBackColor = true;
            this.ShowCostCheckBox.CheckedChanged += new System.EventHandler(this.ShowCostCheckBox_CheckedChanged);
            // 
            // ShowUnitsPerItemCheckBox
            // 
            this.ShowUnitsPerItemCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowUnitsPerItemCheckBox.AutoSize = true;
            this.ShowUnitsPerItemCheckBox.Location = new System.Drawing.Point(223, 552);
            this.ShowUnitsPerItemCheckBox.Name = "ShowUnitsPerItemCheckBox";
            this.ShowUnitsPerItemCheckBox.Size = new System.Drawing.Size(92, 17);
            this.ShowUnitsPerItemCheckBox.TabIndex = 10;
            this.ShowUnitsPerItemCheckBox.Text = "Units Per Item";
            this.ShowUnitsPerItemCheckBox.UseVisualStyleBackColor = true;
            this.ShowUnitsPerItemCheckBox.CheckedChanged += new System.EventHandler(this.ShowUnitsPerItemCheckBox_CheckedChanged);
            // 
            // ShowOrderThresholdCheckBox
            // 
            this.ShowOrderThresholdCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowOrderThresholdCheckBox.AutoSize = true;
            this.ShowOrderThresholdCheckBox.Location = new System.Drawing.Point(223, 535);
            this.ShowOrderThresholdCheckBox.Name = "ShowOrderThresholdCheckBox";
            this.ShowOrderThresholdCheckBox.Size = new System.Drawing.Size(102, 17);
            this.ShowOrderThresholdCheckBox.TabIndex = 9;
            this.ShowOrderThresholdCheckBox.Text = "Order Threshold";
            this.ShowOrderThresholdCheckBox.UseVisualStyleBackColor = true;
            this.ShowOrderThresholdCheckBox.CheckedChanged += new System.EventHandler(this.ShowOrderThresholdCheckBox_CheckedChanged);
            // 
            // ShowStatusCheckBox
            // 
            this.ShowStatusCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowStatusCheckBox.AutoSize = true;
            this.ShowStatusCheckBox.Location = new System.Drawing.Point(223, 518);
            this.ShowStatusCheckBox.Name = "ShowStatusCheckBox";
            this.ShowStatusCheckBox.Size = new System.Drawing.Size(56, 17);
            this.ShowStatusCheckBox.TabIndex = 8;
            this.ShowStatusCheckBox.Text = "Status";
            this.ShowStatusCheckBox.UseVisualStyleBackColor = true;
            this.ShowStatusCheckBox.CheckedChanged += new System.EventHandler(this.ShowStatusCheckBox_CheckedChanged);
            // 
            // ShowReceiveDateCheckBox
            // 
            this.ShowReceiveDateCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowReceiveDateCheckBox.AutoSize = true;
            this.ShowReceiveDateCheckBox.Location = new System.Drawing.Point(547, 552);
            this.ShowReceiveDateCheckBox.Name = "ShowReceiveDateCheckBox";
            this.ShowReceiveDateCheckBox.Size = new System.Drawing.Size(92, 17);
            this.ShowReceiveDateCheckBox.TabIndex = 19;
            this.ShowReceiveDateCheckBox.Text = "Receive Date";
            this.ShowReceiveDateCheckBox.UseVisualStyleBackColor = true;
            this.ShowReceiveDateCheckBox.CheckedChanged += new System.EventHandler(this.ShowReceiveDateCheckBox_CheckedChanged);
            // 
            // ShowOrderedByCheckBox
            // 
            this.ShowOrderedByCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowOrderedByCheckBox.AutoSize = true;
            this.ShowOrderedByCheckBox.Location = new System.Drawing.Point(547, 535);
            this.ShowOrderedByCheckBox.Name = "ShowOrderedByCheckBox";
            this.ShowOrderedByCheckBox.Size = new System.Drawing.Size(79, 17);
            this.ShowOrderedByCheckBox.TabIndex = 18;
            this.ShowOrderedByCheckBox.Text = "Ordered By";
            this.ShowOrderedByCheckBox.UseVisualStyleBackColor = true;
            this.ShowOrderedByCheckBox.CheckedChanged += new System.EventHandler(this.ShowOrderedByCheckBox_CheckedChanged);
            // 
            // ShowOrderDateCheckBox
            // 
            this.ShowOrderDateCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowOrderDateCheckBox.AutoSize = true;
            this.ShowOrderDateCheckBox.Location = new System.Drawing.Point(547, 518);
            this.ShowOrderDateCheckBox.Name = "ShowOrderDateCheckBox";
            this.ShowOrderDateCheckBox.Size = new System.Drawing.Size(78, 17);
            this.ShowOrderDateCheckBox.TabIndex = 17;
            this.ShowOrderDateCheckBox.Text = "Order Date";
            this.ShowOrderDateCheckBox.UseVisualStyleBackColor = true;
            this.ShowOrderDateCheckBox.CheckedChanged += new System.EventHandler(this.ShowOrderDateCheckBox_CheckedChanged);
            // 
            // ShowUnitsRemainingCheckBox
            // 
            this.ShowUnitsRemainingCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowUnitsRemainingCheckBox.AutoSize = true;
            this.ShowUnitsRemainingCheckBox.Location = new System.Drawing.Point(435, 552);
            this.ShowUnitsRemainingCheckBox.Name = "ShowUnitsRemainingCheckBox";
            this.ShowUnitsRemainingCheckBox.Size = new System.Drawing.Size(103, 17);
            this.ShowUnitsRemainingCheckBox.TabIndex = 16;
            this.ShowUnitsRemainingCheckBox.Text = "Units Remaining";
            this.ShowUnitsRemainingCheckBox.UseVisualStyleBackColor = true;
            this.ShowUnitsRemainingCheckBox.CheckedChanged += new System.EventHandler(this.ShowUnitsRemainingCheckBox_CheckedChanged);
            // 
            // ShowExpiryDateCheckBox
            // 
            this.ShowExpiryDateCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowExpiryDateCheckBox.AutoSize = true;
            this.ShowExpiryDateCheckBox.Location = new System.Drawing.Point(435, 535);
            this.ShowExpiryDateCheckBox.Name = "ShowExpiryDateCheckBox";
            this.ShowExpiryDateCheckBox.Size = new System.Drawing.Size(80, 17);
            this.ShowExpiryDateCheckBox.TabIndex = 15;
            this.ShowExpiryDateCheckBox.Text = "Expiry Date";
            this.ShowExpiryDateCheckBox.UseVisualStyleBackColor = true;
            this.ShowExpiryDateCheckBox.CheckedChanged += new System.EventHandler(this.ShowExpiryDateCheckBox_CheckedChanged);
            // 
            // ShowLotNumberCheckBox
            // 
            this.ShowLotNumberCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowLotNumberCheckBox.AutoSize = true;
            this.ShowLotNumberCheckBox.Location = new System.Drawing.Point(435, 518);
            this.ShowLotNumberCheckBox.Name = "ShowLotNumberCheckBox";
            this.ShowLotNumberCheckBox.Size = new System.Drawing.Size(81, 17);
            this.ShowLotNumberCheckBox.TabIndex = 14;
            this.ShowLotNumberCheckBox.Text = "Lot Number";
            this.ShowLotNumberCheckBox.UseVisualStyleBackColor = true;
            this.ShowLotNumberCheckBox.CheckedChanged += new System.EventHandler(this.ShowLotNumberCheckBox_CheckedChanged);
            // 
            // ShowPCCCheckBox
            // 
            this.ShowPCCCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowPCCCheckBox.AutoSize = true;
            this.ShowPCCCheckBox.Location = new System.Drawing.Point(760, 518);
            this.ShowPCCCheckBox.Name = "ShowPCCCheckBox";
            this.ShowPCCCheckBox.Size = new System.Drawing.Size(129, 17);
            this.ShowPCCCheckBox.TabIndex = 23;
            this.ShowPCCCheckBox.Text = "Purchase Cost Centre";
            this.ShowPCCCheckBox.UseVisualStyleBackColor = true;
            this.ShowPCCCheckBox.CheckedChanged += new System.EventHandler(this.ShowPCCCheckBox_CheckedChanged);
            // 
            // ShowCommentCheckBox
            // 
            this.ShowCommentCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowCommentCheckBox.AutoSize = true;
            this.ShowCommentCheckBox.Location = new System.Drawing.Point(648, 552);
            this.ShowCommentCheckBox.Name = "ShowCommentCheckBox";
            this.ShowCommentCheckBox.Size = new System.Drawing.Size(75, 17);
            this.ShowCommentCheckBox.TabIndex = 22;
            this.ShowCommentCheckBox.Text = "Comments";
            this.ShowCommentCheckBox.UseVisualStyleBackColor = true;
            this.ShowCommentCheckBox.CheckedChanged += new System.EventHandler(this.ShowCommentCheckBox_CheckedChanged);
            // 
            // ShowFinishDateCheckBox
            // 
            this.ShowFinishDateCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowFinishDateCheckBox.AutoSize = true;
            this.ShowFinishDateCheckBox.Location = new System.Drawing.Point(648, 535);
            this.ShowFinishDateCheckBox.Name = "ShowFinishDateCheckBox";
            this.ShowFinishDateCheckBox.Size = new System.Drawing.Size(79, 17);
            this.ShowFinishDateCheckBox.TabIndex = 21;
            this.ShowFinishDateCheckBox.Text = "Finish Date";
            this.ShowFinishDateCheckBox.UseVisualStyleBackColor = true;
            this.ShowFinishDateCheckBox.CheckedChanged += new System.EventHandler(this.ShowFinishDateCheckBox_CheckedChanged);
            // 
            // ShowReceivedByCheckBox
            // 
            this.ShowReceivedByCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowReceivedByCheckBox.AutoSize = true;
            this.ShowReceivedByCheckBox.Location = new System.Drawing.Point(648, 518);
            this.ShowReceivedByCheckBox.Name = "ShowReceivedByCheckBox";
            this.ShowReceivedByCheckBox.Size = new System.Drawing.Size(87, 17);
            this.ShowReceivedByCheckBox.TabIndex = 20;
            this.ShowReceivedByCheckBox.Text = "Received By";
            this.ShowReceivedByCheckBox.UseVisualStyleBackColor = true;
            this.ShowReceivedByCheckBox.CheckedChanged += new System.EventHandler(this.ShowReceivedByCheckBox_CheckedChanged);
            // 
            // ShowInfiniteResourceCheckBox
            // 
            this.ShowInfiniteResourceCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowInfiniteResourceCheckBox.AutoSize = true;
            this.ShowInfiniteResourceCheckBox.Location = new System.Drawing.Point(760, 535);
            this.ShowInfiniteResourceCheckBox.Name = "ShowInfiniteResourceCheckBox";
            this.ShowInfiniteResourceCheckBox.Size = new System.Drawing.Size(106, 17);
            this.ShowInfiniteResourceCheckBox.TabIndex = 24;
            this.ShowInfiniteResourceCheckBox.Text = "Infinite Resource";
            this.ShowInfiniteResourceCheckBox.UseVisualStyleBackColor = true;
            this.ShowInfiniteResourceCheckBox.CheckedChanged += new System.EventHandler(this.ShoeInfiniteResourceCheckBox_CheckedChanged);
            // 
            // ShowMarkUpCheckBox
            // 
            this.ShowMarkUpCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowMarkUpCheckBox.AutoSize = true;
            this.ShowMarkUpCheckBox.Location = new System.Drawing.Point(932, 518);
            this.ShowMarkUpCheckBox.Name = "ShowMarkUpCheckBox";
            this.ShowMarkUpCheckBox.Size = new System.Drawing.Size(67, 17);
            this.ShowMarkUpCheckBox.TabIndex = 25;
            this.ShowMarkUpCheckBox.Text = "Mark Up";
            this.ShowMarkUpCheckBox.UseVisualStyleBackColor = true;
            this.ShowMarkUpCheckBox.CheckedChanged += new System.EventHandler(this.ShowMarkUpCheckBox_CheckedChanged);
            // 
            // ShowDefaultPCCCheckBox
            // 
            this.ShowDefaultPCCCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowDefaultPCCCheckBox.AutoSize = true;
            this.ShowDefaultPCCCheckBox.Location = new System.Drawing.Point(760, 552);
            this.ShowDefaultPCCCheckBox.Name = "ShowDefaultPCCCheckBox";
            this.ShowDefaultPCCCheckBox.Size = new System.Drawing.Size(166, 17);
            this.ShowDefaultPCCCheckBox.TabIndex = 26;
            this.ShowDefaultPCCCheckBox.Text = "Default Purchase Cost Centre";
            this.ShowDefaultPCCCheckBox.UseVisualStyleBackColor = true;
            this.ShowDefaultPCCCheckBox.CheckedChanged += new System.EventHandler(this.ShowDefaultPCCCheckBox_CheckedChanged);
            // 
            // ShowUseAsCheckBox
            // 
            this.ShowUseAsCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowUseAsCheckBox.AutoSize = true;
            this.ShowUseAsCheckBox.Location = new System.Drawing.Point(932, 535);
            this.ShowUseAsCheckBox.Name = "ShowUseAsCheckBox";
            this.ShowUseAsCheckBox.Size = new System.Drawing.Size(60, 17);
            this.ShowUseAsCheckBox.TabIndex = 27;
            this.ShowUseAsCheckBox.Text = "Use As";
            this.ShowUseAsCheckBox.UseVisualStyleBackColor = true;
            this.ShowUseAsCheckBox.CheckedChanged += new System.EventHandler(this.UseAsCheckBox_CheckedChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(4, 48);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 13);
            this.label39.TabIndex = 4;
            this.label39.Text = "SMTP Port";
            // 
            // tb_SMTPPort
            // 
            this.tb_SMTPPort.Location = new System.Drawing.Point(77, 45);
            this.tb_SMTPPort.Name = "tb_SMTPPort";
            this.tb_SMTPPort.Size = new System.Drawing.Size(35, 20);
            this.tb_SMTPPort.TabIndex = 3;
            // 
            // AdministrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 574);
            this.Controls.Add(this.ShowUseAsCheckBox);
            this.Controls.Add(this.ShowDefaultPCCCheckBox);
            this.Controls.Add(this.ShowMarkUpCheckBox);
            this.Controls.Add(this.ShowInfiniteResourceCheckBox);
            this.Controls.Add(this.ShowPCCCheckBox);
            this.Controls.Add(this.ShowCommentCheckBox);
            this.Controls.Add(this.ShowFinishDateCheckBox);
            this.Controls.Add(this.ShowReceivedByCheckBox);
            this.Controls.Add(this.ShowReceiveDateCheckBox);
            this.Controls.Add(this.ShowOrderedByCheckBox);
            this.Controls.Add(this.ShowOrderDateCheckBox);
            this.Controls.Add(this.ShowUnitsRemainingCheckBox);
            this.Controls.Add(this.ShowExpiryDateCheckBox);
            this.Controls.Add(this.ShowLotNumberCheckBox);
            this.Controls.Add(this.ShowUnitofSaleCheckBox);
            this.Controls.Add(this.ShowCPUCheckBox);
            this.Controls.Add(this.ShowCostCheckBox);
            this.Controls.Add(this.ShowUnitsPerItemCheckBox);
            this.Controls.Add(this.ShowOrderThresholdCheckBox);
            this.Controls.Add(this.ShowStatusCheckBox);
            this.Controls.Add(this.ShowLastOrderedCheckBox);
            this.Controls.Add(this.ShowPartNumberCheckBox);
            this.Controls.Add(this.ShowSupplierCheckBox);
            this.Controls.Add(this.ShowManufacturerCheckBox);
            this.Controls.Add(this.ShowNameCheckBox);
            this.Controls.Add(this.ShowIDCheckBox);
            this.Controls.Add(this.AdministrationExitButton);
            this.Controls.Add(this.AdministrationTabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdministrationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.AdministrationTabs.ResumeLayout(false);
            this.ReagentTypesTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReagentTypesDataGridView)).EndInit();
            this.OrdersTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersDataGridView)).EndInit();
            this.InventoryTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InventoryDataGridView)).EndInit();
            this.HistoryTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDataGridView)).EndInit();
            this.BudgetsTab.ResumeLayout(false);
            this.BudgetsTabControl.ResumeLayout(false);
            this.CostCentreSummaryTab.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreDetailsDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreSummaryDataGridView)).EndInit();
            this.BillingStatusGroupBox.ResumeLayout(false);
            this.BillingStatusGroupBox.PerformLayout();
            this.EndDateGroupBox.ResumeLayout(false);
            this.StartDateGroupBox.ResumeLayout(false);
            this.ReagentSummaryTab.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReagentDetailsDataGridView)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReagentSummaryDataGridView)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.SignOutListTabPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SignOutListDataGridView)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.CategoriesTab.ResumeLayout(false);
            this.ReagentSetToolsGroupBox.ResumeLayout(false);
            this.EditSetReagentsGroupBox.ResumeLayout(false);
            this.EditSetReagentsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditSetDestinationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditSetSourceDataGridView)).EndInit();
            this.DeleteSetGroupBox.ResumeLayout(false);
            this.DeleteSetGroupBox.PerformLayout();
            this.CreateSetGroupBox.ResumeLayout(false);
            this.CreateSetGroupBox.PerformLayout();
            this.CategoryToolsGroupBox.ResumeLayout(false);
            this.SetCategoryRelGroupBox.ResumeLayout(false);
            this.SetCategoryRelGroupBox.PerformLayout();
            this.AddCategoryGroupBox.ResumeLayout(false);
            this.AddCategoryGroupBox.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ReagentCategoryRelGroupBox.ResumeLayout(false);
            this.ReagentCategoryRelGroupBox.PerformLayout();
            this.ManageUsersTab.ResumeLayout(false);
            this.tc_ManageUsers.ResumeLayout(false);
            this.tab_Users.ResumeLayout(false);
            this.gb_UserCredentials.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.gb_UserCredentialsExisting.ResumeLayout(false);
            this.CreateUserGroupBox.ResumeLayout(false);
            this.CreateUserGroupBox.PerformLayout();
            this.tab_CostCentres.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tab_LabsInstitutes.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.CreateLabGroupBox.ResumeLayout(false);
            this.CreateLabGroupBox.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ConfigTabPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.DBConfigGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl AdministrationTabs;
        private System.Windows.Forms.TabPage ReagentTypesTab;
        private System.Windows.Forms.TabPage OrdersTab;
        private System.Windows.Forms.DataGridView ReagentTypesDataGridView;
        private System.Windows.Forms.Button ReagentTypesUpdateButton;
        private System.Windows.Forms.Button ReagentTypesRevertButton;
        private System.Windows.Forms.Button AdministrationExitButton;
        private System.Windows.Forms.TabPage InventoryTab;
        private System.Windows.Forms.TabPage HistoryTab;
        private System.Windows.Forms.DataGridView OrdersDataGridView;
        private System.Windows.Forms.DataGridView InventoryDataGridView;
        private System.Windows.Forms.DataGridView HistoryDataGridView;
        private System.Windows.Forms.Panel ReagentTypesInfoPanel;
        private System.Windows.Forms.Panel OrdersInfoPanel;
        private System.Windows.Forms.Button InventoryRevertButton;
        private System.Windows.Forms.Button InventoryUpdateButton;
        private System.Windows.Forms.Panel InventoryInfoPanel;
        private System.Windows.Forms.TabPage CategoriesTab;
        private System.Windows.Forms.GroupBox AddCategoryGroupBox;
        private System.Windows.Forms.ComboBox NewCategoryParentComboBox;
        private System.Windows.Forms.Label NewCategoryParentLabel;
        private System.Windows.Forms.TextBox NewCategoryNameTextBox;
        private System.Windows.Forms.Label NewCategoryNameLabel;
        private System.Windows.Forms.Button NewCategoryCreateButton;
        private System.Windows.Forms.GroupBox ReagentCategoryRelGroupBox;
        private System.Windows.Forms.ComboBox ReagentCategoryLinkCategoryComboBox;
        private System.Windows.Forms.Label ReagentCategoryLinkCategoryLabel;
        private System.Windows.Forms.ComboBox ReagentCategoryLinkReagentComboBox;
        private System.Windows.Forms.Label ReagentCategoryLinkReagentLabel;
        private System.Windows.Forms.Button ReagentCategoryLinkUnLinkButton;
        private System.Windows.Forms.Button ReagentCategoryLinkLinkButton;
        private System.Windows.Forms.Panel CategoriesInfoPanel;
        private System.Windows.Forms.Button OrdersRevertButton;
        private System.Windows.Forms.Button OrdersUpdateButton;
        private System.Windows.Forms.TabPage ManageUsersTab;
        private System.Windows.Forms.TabPage BudgetsTab;
        private System.Windows.Forms.TabControl BudgetsTabControl;
        private System.Windows.Forms.TabPage CostCentreSummaryTab;
        private System.Windows.Forms.TabPage ReagentSummaryTab;
        private System.Windows.Forms.GroupBox EndDateGroupBox;
        private System.Windows.Forms.MonthCalendar CostCentreSummaryEndDateCalendar;
        private System.Windows.Forms.GroupBox StartDateGroupBox;
        private System.Windows.Forms.MonthCalendar CostCentreSummaryStartDateCalendar;
        private System.Windows.Forms.GroupBox BillingStatusGroupBox;
        private System.Windows.Forms.RadioButton CostCentreSummaryBilledBillingStatusRadioButton;
        private System.Windows.Forms.RadioButton CostCentreSummaryUnbilledBillingStatusRadioButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView CostCentreSummaryDataGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView CostCentreDetailsDataGridView;
        private System.Windows.Forms.Button CostCentreSummaryDisplayButton;
        private System.Windows.Forms.RadioButton CostCentreSummaryAllBillingStatusRadioButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Billed;
        private System.Windows.Forms.Button ReagentSummaryDisplayButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MonthCalendar ReagentSummaryEndDateCalendar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.MonthCalendar ReagentSummaryStartDateCalendar;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView ReagentDetailsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView ReagentSummaryDataGridView;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button DeleteCategoryButton;
        private System.Windows.Forms.ComboBox DeleteCategoryComboBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button DeleteLabButton;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox DeleteLabComboBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox DeleteCostCentreComboBox;
        private System.Windows.Forms.Button DeleteCostCentreButton;
        private System.Windows.Forms.Label DeleteCostCentreLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReagentSummaryCostCentre;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebtorCostCentre;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditorCostCentre;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillableAmount;
        private System.Windows.Forms.DataGridViewButtonColumn BillButton;
        private System.Windows.Forms.DataGridViewButtonColumn UnBillButton;
        private System.Windows.Forms.TabPage SignOutListTabPage;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn3;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.MonthCalendar monthCalendar2;
        private System.Windows.Forms.MonthCalendar monthCalendar3;
        private System.Windows.Forms.Button SignOutListDisplayButton;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView SignOutListDataGridView;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.MonthCalendar SignoutListEndDateCalendar;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.MonthCalendar SignoutListStartDateCalendar;
        private System.Windows.Forms.Button SignOutListRevertButton;
        private System.Windows.Forms.Button SignOutListUpdateButton;
        private System.Windows.Forms.GroupBox ReagentSetToolsGroupBox;
        private System.Windows.Forms.GroupBox CategoryToolsGroupBox;
        private System.Windows.Forms.GroupBox DeleteSetGroupBox;
        private System.Windows.Forms.ComboBox DeleteSetComboBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button DeleteSetButton;
        private System.Windows.Forms.GroupBox CreateSetGroupBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button CreateSetButton;
        private System.Windows.Forms.TextBox SetNameTextBox;
        private System.Windows.Forms.GroupBox EditSetReagentsGroupBox;
        private System.Windows.Forms.Button RemoveSetReagentButton;
        private System.Windows.Forms.Button AddSetReagentButton;
        private System.Windows.Forms.DataGridView EditSetSourceDataGridView;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox EditSetComboBox;
        private System.Windows.Forms.Button UpdateSetReagentButton;
        private System.Windows.Forms.DataGridView EditSetDestinationDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetDestinationReagentTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetDestinationReagentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetDestinationReagentManufacturer;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetDestinationUnits;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetSourceReagentTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetSourceReagentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EditSetSourceManufacturer;
        private System.Windows.Forms.GroupBox SetCategoryRelGroupBox;
        private System.Windows.Forms.Button SetCategoryLinkUnLinkButton;
        private System.Windows.Forms.Button SetCategoryLinkLinkButton;
        private System.Windows.Forms.ComboBox SetCategoryLinkCategoryComboBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox SetCategoryLinkSetComboBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button ExportMasterReportButton;
        private System.Windows.Forms.CheckBox ShowIDCheckBox;
        private System.Windows.Forms.CheckBox ShowNameCheckBox;
        private System.Windows.Forms.CheckBox ShowManufacturerCheckBox;
        private System.Windows.Forms.CheckBox ShowLastOrderedCheckBox;
        private System.Windows.Forms.CheckBox ShowPartNumberCheckBox;
        private System.Windows.Forms.CheckBox ShowSupplierCheckBox;
        private System.Windows.Forms.CheckBox ShowUnitofSaleCheckBox;
        private System.Windows.Forms.CheckBox ShowCPUCheckBox;
        private System.Windows.Forms.CheckBox ShowCostCheckBox;
        private System.Windows.Forms.CheckBox ShowUnitsPerItemCheckBox;
        private System.Windows.Forms.CheckBox ShowOrderThresholdCheckBox;
        private System.Windows.Forms.CheckBox ShowStatusCheckBox;
        private System.Windows.Forms.CheckBox ShowReceiveDateCheckBox;
        private System.Windows.Forms.CheckBox ShowOrderedByCheckBox;
        private System.Windows.Forms.CheckBox ShowOrderDateCheckBox;
        private System.Windows.Forms.CheckBox ShowUnitsRemainingCheckBox;
        private System.Windows.Forms.CheckBox ShowExpiryDateCheckBox;
        private System.Windows.Forms.CheckBox ShowLotNumberCheckBox;
        private System.Windows.Forms.CheckBox ShowPCCCheckBox;
        private System.Windows.Forms.CheckBox ShowCommentCheckBox;
        private System.Windows.Forms.CheckBox ShowFinishDateCheckBox;
        private System.Windows.Forms.CheckBox ShowReceivedByCheckBox;
        private System.Windows.Forms.CheckBox ShowInfiniteResourceCheckBox;
        private System.Windows.Forms.CheckBox ShowMarkUpCheckBox;
        private System.Windows.Forms.CheckBox ShowDefaultPCCCheckBox;
        private System.Windows.Forms.Button EmailUserReportsButton;
        private System.Windows.Forms.CheckBox ShowUseAsCheckBox;
        private System.Windows.Forms.Button HistoryUpdateButton;
        private System.Windows.Forms.TabPage ConfigTabPage;
        private System.Windows.Forms.GroupBox DBConfigGroupBox;
        private System.Windows.Forms.Button DatabaseConfigEditButton;
        private System.Windows.Forms.SaveFileDialog ExportMasterFileDialogBox;
        private System.Windows.Forms.Button SignoutListExportButton;
        private System.Windows.Forms.SaveFileDialog ExportSignoutListFileDialogBox;
        private System.Windows.Forms.TabControl tc_ManageUsers;
        private System.Windows.Forms.TabPage tab_Users;
        private System.Windows.Forms.TabPage tab_CostCentres;
        private System.Windows.Forms.TabPage tab_LabsInstitutes;
        private System.Windows.Forms.GroupBox gb_UserCredentials;
        private System.Windows.Forms.Button btn_UserCredentialAdd;
        private System.Windows.Forms.GroupBox gb_UserCredentialsExisting;
        private System.Windows.Forms.Label lb_ApplicationDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmb_CreateCredentialRole;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmb_CreateCredentialLab;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmb_CreateCredentialInstitute;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_CreateCredentialApplication;
        private System.Windows.Forms.GroupBox CreateUserGroupBox;
        private System.Windows.Forms.Button btn_UserUpdatePassword;
        private System.Windows.Forms.TextBox tb_UserUpdatePassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmb_UserSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_UserInstitute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_UserDelete;
        private System.Windows.Forms.Button btn_UserUpdate;
        private System.Windows.Forms.Button btn_UserCreate;
        private System.Windows.Forms.ComboBox cmb_UserLab;
        private System.Windows.Forms.CheckBox chk_IsLabHead;
        private System.Windows.Forms.TextBox tb_UserExtension;
        private System.Windows.Forms.TextBox tb_UserEmail;
        private System.Windows.Forms.TextBox tb_UserLastName;
        private System.Windows.Forms.TextBox tb_UserFirstName;
        private System.Windows.Forms.Label lbl_IsLabHead;
        private System.Windows.Forms.Label LabLabel;
        private System.Windows.Forms.Label ExtLabel;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkb_CostCentreRetired;
        private System.Windows.Forms.Button btn_CostCentreDelete;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_CostCentre;
        private System.Windows.Forms.Button btn_CostCentreUpdate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmb_CostCentreInstitute;
        private System.Windows.Forms.TextBox tb_CostCentreDescription;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_CostCentreCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmb_CostCentreLab;
        private System.Windows.Forms.Button btn_CostCentreCreate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox CreateLabGroupBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmb_InstituteSelect;
        private System.Windows.Forms.Button btn_InstituteCreate;
        private System.Windows.Forms.TextBox tb_InstituteName;
        private System.Windows.Forms.Label CreateLabLabNameLabel;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmb_LabInstitute;
        private System.Windows.Forms.TextBox tb_LabName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btn_InstituteDelete;
        private System.Windows.Forms.Button btn_InstituteUpdate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox chk_InstituteCurrentSite;
        private System.Windows.Forms.ComboBox cmb_LabSelect;
        private System.Windows.Forms.Button btn_LabDelete;
        private System.Windows.Forms.Button btn_LabUpdate;
        private System.Windows.Forms.Button btn_LabCreate;
        private System.Windows.Forms.Panel pnl_UserCredentialsExisting;
        private System.Windows.Forms.TextBox tb_InstituteDeliveryAddress;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tb_InstituteBillingAddress;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Panel pnl_LabMembers;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Button btn_SMTPUpdate;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tb_SMTPServer;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_SMTPPort;
    }
}