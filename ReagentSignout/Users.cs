
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Cryptography;


public class Users
{

    public static string ConnectionString { get; set; }
    

    ///////////////////////////////////////////////////////////////
    ///////////////          Classes       ////////////////////////
    ///////////////////////////////////////////////////////////////

    public class UserProfile
    {
        private Guid _userid;
        private List<Credential> _credentials;
        private string _firstname;
        private string _lastname;
        private string _fullname;
        private string _extension;
        private string _email;
        private string _rfid;
        private Laboratory _lab;
        private string _preferences;
        private bool _externaluser;
        private bool _labhead;

        public Guid UserID { get { return _userid; } set { _userid = value; } }
        public List<Credential> Credentials { get { return _credentials; } set { _credentials = value; } }
        public string FirstName { get { return _firstname; } set { _firstname = value; } }
        public string LastName { get { return _lastname; } set { _lastname = value; } }
        public string FullName { get { return _fullname; } set { _fullname = value; } }
        public string Extension { get { return _extension; } set { _extension = value; } }
        public string Email { get { return _email; } set { _email = value; } }
        public string RFID { get { return _rfid; } set { _rfid = value; } }
        //public string LabID { get { return _labid; } set { _labid = value; } }
        public Laboratory Lab { get { return _lab; } set { _lab = value; } }
        public string Preferences { get { return _preferences; } set { _preferences = value; } }
        public bool IsExternalUser { get { return _externaluser; } set { _externaluser = value; } }
        public bool IsLabHead { get { return _labhead; } set { _labhead = value; } }

        public UserProfile()
        {
            Credentials = new List<Credential>();
        }

        public UserProfile(Guid UserID)
        {
            this.UserID = UserID;

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {

                try
                {
                    Connection.Open();

                    GetAccountDetails(Connection);

                    this.Credentials = GetCredentials(Connection);
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }
        }

        public UserProfile(Guid UserID, ClientApplication Application)
        {
            this.UserID = UserID;

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {

                try
                {
                    Connection.Open();

                    GetAccountDetails(Connection);

                    this.Credentials = GetCredentials(Connection, Application);

                    this.Preferences = GetPreferences(Connection, Application);
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }
        }

        private string GetPreferences(SqlConnection Connection, ClientApplication Application)
        {
            SqlCommand Command = new SqlCommand("SELECT Preferences FROM user_application_preferences WHERE UserID=@UserID AND ApplicationID=@ApplicationID", Connection);
            Command.Parameters.AddWithValue("@UserID", this.UserID);
            Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);

            using (SqlDataReader Reader = Command.ExecuteReader())
            {
                if (Reader.HasRows)
                {
                    Reader.Read();
                    return (Reader.IsDBNull(0)) ? String.Empty : Reader.GetSqlXml(0).ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        private List<Credential> GetCredentials(SqlConnection Connection, ClientApplication Application)
        {
            List<Credential> ReturnList = new List<Credential>();

            SqlCommand Command = new SqlCommand("SELECT LabID, RoleID, ApplicationID FROM user_app_lab_role_rel WHERE UserID=@UserID AND ApplicationID=@ApplicationID", Connection);
            Command.Parameters.AddWithValue("@UserID", this.UserID);
            Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);

            using (SqlDataReader Reader = Command.ExecuteReader())
            {
                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        ReturnList.Add(new Credential(Reader.GetByte(0), Reader.GetInt32(1), Reader.GetByte(2)));
                    }
                }
            }
            return ReturnList;
        }


        private void GetAccountDetails(SqlConnection Connection)
        {
            SqlCommand Command = new SqlCommand("SELECT FirstName, LastName, Extension, Email, RFID, LabID, IsLabHead FROM Users WHERE UserID=@UserID", Connection);
            Command.Parameters.AddWithValue("@UserID", UserID);

            using (SqlDataReader Reader = Command.ExecuteReader())
            {

                if (Reader.HasRows)
                {
                    Reader.Read();

                    this.UserID = UserID;
                    this.FirstName = Reader.IsDBNull(0) ? String.Empty : Reader.GetString(0);
                    this.LastName = Reader.IsDBNull(1) ? String.Empty : Reader.GetString(1);
                    this.FullName = FirstName + " " + LastName;
                    this.Extension = Reader.IsDBNull(2) ? String.Empty : Reader.GetString(2);
                    this.Email = Reader.IsDBNull(3) ? String.Empty : Reader.GetString(3);
                    this.RFID = Reader.IsDBNull(4) ? String.Empty : Reader.GetString(4);
                    this.Lab = new Laboratory(Reader.GetByte(5));
                    this.IsLabHead = Reader.GetBoolean(6);
                    this.IsExternalUser = !Lab.Institute.IsCurrentSite;
                }
                else
                {
                    throw new Exception("Get account details request returned no rows based on UserID:"+UserID.ToString());
                }
            }
        }

        private List<Credential> GetCredentials(SqlConnection Connection)
        {
            List<Credential> ReturnList = new List<Credential>();

            SqlCommand Command = new SqlCommand("SELECT LabID, RoleID, ApplicationID FROM user_app_lab_role_rel WHERE UserID=@UserID");
            Command.Parameters.AddWithValue("@UserID", this.UserID);
            Command.Connection = Connection;

            using (SqlDataReader Reader = Command.ExecuteReader())
            {
                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        ReturnList.Add(new Credential(Reader.GetByte(0), Reader.GetInt32(1), Reader.GetByte(2)));
                    }
                }
            }
            return ReturnList;
        }

        public void Write(string HashedPassword)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {

                SqlCommand Command = new SqlCommand("EXEC p_AddUser @FirstName,@LastName,@Password,@Email, @RFID, @IsLabHead, @LabID,@Extension;");
                Command.Parameters.AddWithValue("@FirstName", this.FirstName.Trim());
                Command.Parameters.AddWithValue("@LastName", this.LastName.Trim());
                Command.Parameters.AddWithValue("@Password", HashedPassword);
                Command.Parameters.AddWithValue("@Email", string.IsNullOrEmpty(this.Email) ? (object)DBNull.Value : this.Email.Trim());
                Command.Parameters.AddWithValue("@RFID", string.IsNullOrEmpty(this.RFID) ? (object)DBNull.Value : this.RFID.Trim());
                Command.Parameters.AddWithValue("@Extension", string.IsNullOrEmpty(this.Extension) ? (object)DBNull.Value : this.Extension.Trim());
                Command.Parameters.AddWithValue("@IsLabHead", this.IsLabHead);
                Command.Parameters.AddWithValue("@LabID", this.Lab.LabID);

                Command.Connection = Connection;

                Connection.Open();
                Command.ExecuteNonQuery();

            }
        }

        internal void Write(string HashedPassword, string ApplicationName, string RoleID)
        {

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {

                SqlCommand Command = new SqlCommand("EXEC p_AddUser @FirstName,@LastName,@Password,@Email, @RFID, @IsLabHead, @LabID,@Extension,@ApplicationName,@RoleID;");
                Command.Parameters.AddWithValue("@FirstName", this.FirstName.Trim());
                Command.Parameters.AddWithValue("@LastName", this.LastName.Trim());
                Command.Parameters.AddWithValue("@Password", HashedPassword);
                Command.Parameters.AddWithValue("@Email", string.IsNullOrEmpty(this.Email) ? (object)DBNull.Value : this.Email.Trim());
                Command.Parameters.AddWithValue("@RFID", string.IsNullOrEmpty(this.RFID) ? (object)DBNull.Value : this.RFID.Trim());
                Command.Parameters.AddWithValue("@Extension", string.IsNullOrEmpty(this.Extension) ? (object)DBNull.Value : this.Extension.Trim());
                Command.Parameters.AddWithValue("@IsLabHead", this.IsLabHead);
                Command.Parameters.AddWithValue("@LabID", this.Lab.LabID);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Parameters.AddWithValue("@RoleID", RoleID);
                Command.Connection = Connection;

                Connection.Open();
                Command.ExecuteNonQuery();
            }
        }

        internal bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE users SET FirstName=@FirstName, LastName=@LastName,Email=@Email, RFID=@RFID, Extension=@Extension, IsLabHead=@IsLabHead, LabID=@LabID WHERE UserID=@UserID");
                Command.Parameters.AddWithValue("@UserID", this.UserID);
                Command.Parameters.AddWithValue("@FirstName", this.FirstName);
                Command.Parameters.AddWithValue("@LastName", this.LastName);
                Command.Parameters.AddWithValue("@Email", string.IsNullOrEmpty(this.Email) ? (object)DBNull.Value : this.Email);
                Command.Parameters.AddWithValue("@RFID", string.IsNullOrEmpty(this.RFID) ? (object)DBNull.Value : this.RFID);
                Command.Parameters.AddWithValue("@Extension", string.IsNullOrEmpty(this.Extension) ? (object)DBNull.Value : this.Extension);
                Command.Parameters.AddWithValue("@LabID", this.Lab.LabID);
                Command.Parameters.AddWithValue("@IsLabHead", this.IsLabHead);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        internal bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM users WHERE UserID=@UserID", Connection);
                Command.Parameters.AddWithValue("@UserID", this.UserID);
                Connection.Open();

                try
                {
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while deleting user profile", Err);
                }
            }
        }

        internal bool Exists()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT * FROM users WHERE FirstName=@FirstName AND LastName=@LastName", Connection);
                Command.Parameters.AddWithValue("@FirstName", this.FirstName);
                Command.Parameters.AddWithValue("@LastName", this.LastName);
                Connection.Open();

                try
                {
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while checking if a with that name user is registered", Err);
                }
            }
        }

        public static UserProfile Authenticate(string UserName, string Password, Users.ClientApplication App)
        {

            System.Text.RegularExpressions.Regex Matcher = new System.Text.RegularExpressions.Regex(@"\w+\s\w+");
            string FirstName;
            string LastName;

            if (Matcher.IsMatch(UserName))
            {

                FirstName = UserName.Split(' ')[0];
                LastName = UserName.Split(' ')[1];
            }
            else
            {
                throw new Exception("Bad Username Format");
            }

            string HashedPassword = GetHash(Password);
            string MasterPassword = App.GetHashedMasterPassword();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand();
                if (MasterPassword == HashedPassword)
                {
                    Command.CommandText = "SELECT UserID FROM AuthenticationView WHERE FirstName=@FirstName AND LastName=@LastName AND ApplicationName=@ApplicationName";
                }
                else
                {
                    Command.CommandText = "SELECT UserID FROM AuthenticationView WHERE FirstName=@FirstName AND LastName=@LastName AND Password=@Password AND ApplicationName=@ApplicationName";
                }

                Command.Parameters.AddWithValue("@FirstName", FirstName);
                Command.Parameters.AddWithValue("@LastName", LastName);
                Command.Parameters.AddWithValue("@Password", HashedPassword);
                Command.Parameters.AddWithValue("@ApplicationName", App.Name);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return new UserProfile(Reader.GetGuid(0), App);
                        }
                        else
                        {
                            throw new Exception("Invalid UserName/Password Combination");
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }

        }

        //Checks user tables for a RFID match
        public static UserProfile AuthenticateByRFID(string RFID, Users.ClientApplication App)
        {

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT UserID FROM AuthenticationView WHERE RFID=@RFID AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@RFID", RFID);
                Command.Parameters.AddWithValue("@ApplicationName", App.Name);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return new UserProfile(Reader.GetGuid(0),App);
                        }
                        else
                        {
                            throw new Exception("RFID not registered to a user account.");
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }

        }

        //Change a users password
        public bool ChangePassword(string NewPassword)
        {
            SqlConnection UpdateConnection = new SqlConnection(ConnectionString);
            SqlCommand UpdateCommand = new SqlCommand("UPDATE users SET Password=@Password WHERE UserID=@UserID");
            UpdateCommand.Parameters.AddWithValue("@Password", GetHash(NewPassword));
            UpdateCommand.Parameters.AddWithValue("@UserID", UserID);

            UpdateCommand.Connection = UpdateConnection;
            UpdateConnection.Open();

            if (UpdateCommand.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //Reset a users password to random and email new password
        public bool ResetPassword(string Email)
        {
            string Password = PasswordGenerator();
            SqlConnection UpdateConnection = new SqlConnection(ConnectionString);
            SqlCommand UpdateCommand = new SqlCommand("UPDATE users SET Password=@Password WHERE Email=@Email");
            UpdateCommand.Parameters.AddWithValue("@Password", GetHash(Password));
            UpdateCommand.Parameters.AddWithValue("@Email", Email);

            try
            {

                UpdateCommand.Connection = UpdateConnection;
                UpdateConnection.Open();

                if (UpdateCommand.ExecuteNonQuery() == 1)
                {
                    string MessageFrom = Settings.GetSetting("SystemAdminEmail", ConnectionString);
                    string  MessageSubject = "Password Reset";
                    bool MessageIsBodyHtml = true;

                    System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                    MessageBody.Append("Hi, <br/><br/> ");
                    MessageBody.AppendFormat("Your Password has been reset to <b>{0}</b> <br/><br/>", Password);
                    MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");

                    try
                    {
                        Users.SendMail(MessageSubject, Email, MessageFrom, MessageBody.ToString(), MessageIsBodyHtml);
                    }
                    catch (Exception Err)
                    {
                        throw new Exception("Failed to send email, account creation successful.", Err);
                    }
                        return true;
                }
                else
                {
                    throw new Exception("Reset Password Query Returned Wrong Number of Affected Rows");
                }
            }
            catch (Exception Err)
            {
                throw new Exception ("An error occurred while performing password reset",Err);
            }
        }

        //Retrieves a users password in hashed form.
        public string GetHashedPassword(Guid UserID)
        {
            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT Password FROM users WHERE UserID=@UserID");
            Command.Parameters.AddWithValue("@UserID", UserID);
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    return Reader.GetString(0);
                }
                else
                {
                    return "Invalid UserID";
                }
            }
            catch (Exception Err)
            {
                throw new Exception("An error occured while retrieving password", Err);
            }
        }

        //Retrieves a list of all Users as User Profiles
        public static List<UserProfile> GetAllUsers()
        {

            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT UserID FROM Users ORDER BY FirstName, LastName");

            Command.Connection = Connection;

            try
            {
                Connection.Open();


                using (SqlDataReader Reader = Command.ExecuteReader())
                {

                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            Users.Add(new Users.UserProfile(Reader.GetGuid(0)));
                        }
                    }
                }

                return Users;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching all users", Err);
            }


        }


    }

    public class ClientApplication
    {
        public int ApplicationID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DefaultPreferences { get; set; }
        public List<Role> Roles { get; set; }

        public override bool Equals(object obj)
        {
            ClientApplication App = obj as ClientApplication;
            if (App == null) return false;
            return App.ApplicationID == ApplicationID && App.Name == Name;
        }

        public override int GetHashCode()
        {
            return ApplicationID.GetHashCode() & Name.GetHashCode();
        }

        public ClientApplication()
        {
            Roles = new List<Role>();
        }

        public ClientApplication(int ApplicationID)
        {
            this.ApplicationID = ApplicationID;
            Roles = new List<Role>();
            FetchDetails();
        }

        public ClientApplication(string ApplicationName)
        {
            this.ApplicationID = GetApplicationIDFromName(ApplicationName);
            Roles = new List<Role>();
            FetchDetails();
        }

        public static bool Create(string Name, string Description, string DefaultPreferences, string MasterPassword)
        {
            ClientApplication Application = new ClientApplication();
            Application.Name = Name;
            Application.Description = Description;
            Application.DefaultPreferences = DefaultPreferences;

            return Application.Write(MasterPassword);
        }

        private int GetApplicationIDFromName(string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT ApplicationID FROM Applications WHERE ApplicationName=@ApplicationName", Connection);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Connection.Open();

                return Convert.ToInt32(Command.ExecuteScalar());
            }
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT ApplicationName, DescriptiveName, DefaultPreferences FROM Applications WHERE ApplicationID=@ApplicationID", Connection);
                Command.Parameters.AddWithValue("@ApplicationID", ApplicationID);
                Connection.Open();
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        Name = Reader.GetString(0);
                        Description = Reader.GetString(1);
                        DefaultPreferences = Reader.IsDBNull(2) ? String.Empty : Reader.GetSqlXml(2).Value;
                    }
                }

                Command.CommandText = "SELECT Roles.RoleID, RoleName, Description FROM app_role_relations INNER JOIN Roles ON app_role_relations.RoleID=Roles.RoleID WHERE ApplicationID=@ApplicationID";

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            Roles.Add(new Role(Reader.GetInt32(0), Reader.GetString(1), Reader.GetString(2)));
                        }
                    }
                }
            }
        }

        private bool Write(string MasterPassword)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO Applications (ApplicationName, DescriptiveName, DefaultPreferences, MasterPassword) VALUES (@ApplicationName, @DescriptiveName, @DefaultPreferences, @MasterPassword)", Connection);
                Command.Parameters.AddWithValue("@ApplicationName", this.Name);
                Command.Parameters.AddWithValue("@DescriptiveName", this.Description);
                Command.Parameters.AddWithValue("@DefaultPreferences", String.IsNullOrEmpty(this.DefaultPreferences) ? (object)DBNull.Value : this.DefaultPreferences);
                Command.Parameters.AddWithValue("@MasterPassword", String.IsNullOrEmpty(MasterPassword) ? (object)DBNull.Value : GetHash(MasterPassword));
                Connection.Open();

                try
                {
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occured while writing application details to database", Err);
                }
            }
        }

        public bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE Applications SET ApplicationName=@ApplicationName, DescriptiveName=@DescriptiveName, DefaultPreferences=@DefaultPreferences WHERE ApplicationID=@ApplicationID", Connection);
                Command.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
                Command.Parameters.AddWithValue("@ApplicationName", this.Name);
                Command.Parameters.AddWithValue("@DescriptiveName", this.Description);
                Command.Parameters.AddWithValue("@DefaultPreferences", String.IsNullOrEmpty(this.DefaultPreferences) ? (object)DBNull.Value : this.DefaultPreferences);
                Connection.Open();

                try
                {
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while updating application information", Err);
                }
            }
        }

        public bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM Applications WHERE ApplicationID=@ApplicationID", Connection);
                Command.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
                Connection.Open();
                try
                {
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occured while deleting application", Err);
                }
            }
        }

        public bool UpdateMasterPassword(string CurrentMasterPassword, string NewMasterPassword)
        {
            string TrueCurrentMasterPassword = this.GetHashedMasterPassword();
            string SuppliedCurrentMasterPassword = GetHash(CurrentMasterPassword);

            if (TrueCurrentMasterPassword != SuppliedCurrentMasterPassword)
            {
                throw new Exception("The current master password supplied is incorrect");
            }

            string HashedNewMasterPassword = GetHash(NewMasterPassword);

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE Applications SET MasterPassword=@MasterPassword WHERE ApplicationID=@ApplicationID", Connection);
                Command.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
                Command.Parameters.AddWithValue("@MasterPassword", String.IsNullOrEmpty(HashedNewMasterPassword) ? (object)DBNull.Value : HashedNewMasterPassword);
                Connection.Open();

                try
                {
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }
        }

        public string GetHashedMasterPassword()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT MasterPassword FROM Applications WHERE ApplicationID=@ApplicationID", Connection);
                Command.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
                Connection.Open();
                try
                {
                    object MasterPassword = Command.ExecuteScalar();
                    if (MasterPassword == null || MasterPassword == DBNull.Value)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return (string)MasterPassword;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while retrieving master password", Err);
                }
            }
        }

        //Retrieves a list of the Roles available to an application
        public List<string[]> GetRoles()
        {
            List<string[]> ReturnList = new List<string[]>();

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT roles.RoleID, roles.RoleName FROM applications INNER JOIN app_role_relations ON applications.ApplicationID=app_role_relations.ApplicationID INNER JOIN roles ON app_role_relations.RoleID=roles.RoleID WHERE ApplicationID=@ApplicationID");
                Command.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(new string[] { Reader.GetInt32(0).ToString(), Reader.GetString(1) });
                            }
                        }

                        return ReturnList;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while retrieving a list of roles for the application", Err);
                }
            }
        }

        //Retrieves a list of all the UserIDs and UserNames associated with an application
        public List<UserProfile> GetUsers()
        {
            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection UserConnection = new SqlConnection(ConnectionString);
            SqlCommand UserCommand = new SqlCommand("SELECT DISTINCT users.UserID, FirstName FROM users INNER JOIN user_app_lab_role_rel ON users.UserID=user_app_lab_role_rel.UserID INNER JOIN applications ON user_app_lab_role_rel.ApplicationID=applications.ApplicationID WHERE applications.ApplicationID=@ApplicationID ORDER BY FirstName");
            UserCommand.Parameters.AddWithValue("@ApplicationID", this.ApplicationID);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();

                SqlDataReader Reader = UserCommand.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        UserProfile User = new UserProfile(Reader.GetGuid(0));
                        Users.Add(User);
                    }
                }
                return Users;
            }
            catch (Exception Err)
            {
                throw Err;
            }


        }

        //Retrieves a list of all the UserIDs and UserNames with provided Last Name
        public List<UserProfile> GetUsersByLastName(string LastName)
        {

            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand InternalUserCommand = new SqlCommand("SELECT DISTINCT UserID, (FirstName+' '+LastName) AS Name FROM UserProfileView WHERE ApplicationName=@ApplicationName AND LastName LIKE @LastName ORDER BY Name");
            InternalUserCommand.Parameters.AddWithValue("@LastName", LastName);
            InternalUserCommand.Parameters.AddWithValue("@ApplicationName", this.Name);
            InternalUserCommand.Connection = Connection;

            try
            {
                Connection.Open();


                using (SqlDataReader Reader = InternalUserCommand.ExecuteReader())
                {

                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            Users.Add(new UserProfile(Reader.GetGuid(0)));
                        }
                    }
                }

                return Users;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching users by surname", Err);
            }


        }

        //Returns boolean value indicating if User has a credential with specified role
        public bool UserHasRole(UserProfile User, Role UserRole)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT CASE WHEN EXISTS (SELECT * FROM UserProfileView WHERE UserID=@UserID AND RoleName=@RoleName AND ApplicationName=@ApplicationName) THEN CAST(1 as bit) ELSE CAST(0 as bit) END");
                Command.Parameters.AddWithValue("@UserID", User.UserID);
                Command.Parameters.AddWithValue("@RoleName", UserRole.Name);
                Command.Parameters.AddWithValue("@ApplicationName", this.Name);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {

                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return Reader.GetBoolean(0);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while checking user roles", Err);
                }
            }
        }

        //Retrieves a list of Email addresses for administrators associated with an application
        public List<string> GetAdminEmailAddresses()
        {
            List<string> AddressCollection = new List<string>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT Email FROM UserProfileView WHERE RoleName='Administrator' AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@ApplicationName", this.Name);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                AddressCollection.Add(Reader["Email"].ToString());
                            }
                        }
                    }
                    return AddressCollection;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching administrator email addresses", Err);
                }
            }
        }

        //Retrieves a list of Email addresses for Users associated with an application
        public List<string> GetUserEmailAddresses()
        {
            List<string> AddressCollection = new List<string>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT Email FROM UserProfileView WHERE RoleName<>'Administrator' AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@ApplicationName", this.Name);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                AddressCollection.Add(Reader["Email"].ToString());
                            }
                        }
                    }
                    return AddressCollection;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while getting user email address list", Err);
                }
            }
        }

        //Returns the descriptive name for the supplied application ID
        public string GetApplicationDescriptiveName(int ApplicationID)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT DescriptiveName FROM Applications WHERE ApplicationID=@ApplicationID");
                Command.Parameters.AddWithValue("@ApplicationID", ApplicationID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    return (string)Command.ExecuteScalar();
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        private string GetApplicationDescriptiveName(string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT DescriptiveName FROM Applications WHERE ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    return (string)Command.ExecuteScalar();
                }
                catch
                {
                    return ApplicationName;
                }
            }
        }

        
        public static List<ClientApplication> GetApplications()
        {

            List<ClientApplication> Applications = new List<ClientApplication>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT ApplicationID FROM Applications");
            Command.Connection = Connection;

            try
            {
                Connection.Open();


                using (SqlDataReader Reader = Command.ExecuteReader())
                {

                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            Applications.Add(new ClientApplication(Reader.GetByte(0)));
                        }
                    }
                }

                return Applications;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching list of applications", Err);
            }


        }

    }

    public class Role
    {
        public int RoleID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Role()
        {
            Name = string.Empty;
        }

        public Role(int RoleID)
        {
            this.RoleID = RoleID;
            FetchDetails();
        }

        public Role(int RoleID, string RoleName, string Description)
        {
            this.RoleID = RoleID;
            this.Name = RoleName;
            this.Description = Description;
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT RoleName FROM Roles WHERE RoleID=@RoleID", Connection);
                Command.Parameters.AddWithValue("@RoleID", RoleID);
                Connection.Open();
                this.Name = (string)Command.ExecuteScalar();
            }
        }

        //Create Role
        public bool Create(string RoleName)
        {
            Role NewRole = new Role();
            NewRole.Name = RoleName;
            return NewRole.Write();
        }

        private bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO Roles (RoleName) VALUES (@RoleName)");
                Command.Parameters.AddWithValue("@RoleName", this.Name);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while writing role to database", Err);
                }
            }
        }

        public bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM Roles WHERE RoleID=@RoleID");
                Command.Parameters.AddWithValue("@RoleID", this.RoleID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while deleting role", Err);
                }
            }
        }

        public bool LinkToApplication(ClientApplication Application, string Description)
        {

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO app_role_relations (RoleID, ApplicationID, Description) VALUES (@RoleID, @ApplicationID, @Description)");
                Command.Parameters.AddWithValue("@RoleID", this.RoleID);
                Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);
                Command.Parameters.AddWithValue("@Description", Description);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while linking role to application", Err);
                }
            }
        }

        public bool UnLinkFromApplication(ClientApplication Application)
        {

            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM app_role_relations WHERE RoleID=@RoleID AND ApplicationID=@ApplicationID");
                Command.Parameters.AddWithValue("@RoleID", this.RoleID);
                Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while unlinking role from application", Err);
                }
            }
        }

        public static int GetIDFromName(string RoleName)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT RoleID FROM Roles WHERE RoleName=@RoleName");
                Command.Parameters.AddWithValue("@RoleName", RoleName);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    object RoleID = Command.ExecuteScalar();
                    if (RoleID != null)
                    {
                        return Convert.ToInt32(RoleID);
                    }
                    else
                    {
                        throw new Exception("The supplied rolename returned no matching entries while fetching RoleID");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while getting RoleID from role name", Err);
                }
            }
        }

        //Gets a list of registered roles
        public static List<Role> GetRoles()
        {
            List<Role> ReturnList = new List<Role>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT RoleID, RoleName FROM Roles", Connection);
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ReturnList.Add(new Role(Reader.GetInt32(0), Reader.GetString(1), String.Empty));
                        }
                    }
                }
            }
            return ReturnList;
        }

        //Retrieves the description of a Role for an application
        public string GetDescription(string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT app_role_relations.[Description] FROM applications INNER JOIN app_role_relations ON applications.ApplicationID=app_role_relations.ApplicationID INNER JOIN roles ON app_role_relations.RoleID=roles.RoleID WHERE ApplicationName=@ApplicationName AND Roles.RoleID=@RoleID");
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Parameters.AddWithValue("@RoleID", RoleID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            return Reader.GetString(0);
                        }

                        return String.Empty;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching role descriptions", Err);
                }
            }
        }

    }

    public class Credential
    {
        public Laboratory Lab { get; set; }
        public Role UserRole { get; set; }
        public ClientApplication Application { get; set; }

        public override bool Equals(object obj)
        {
            Credential C = obj as Credential;
            if (C == null || C.Application == null || C.Lab == null || C.UserRole == null) return false;
            return C.Application.ApplicationID == Application.ApplicationID && C.Lab.LabID == Lab.LabID && C.UserRole.RoleID == UserRole.RoleID;
        }

        public override int GetHashCode()
        {
            try
            {
                return Application.ApplicationID.GetHashCode() & Lab.LabID.GetHashCode() & UserRole.RoleID.GetHashCode();
            }
            catch (Exception Err)
            {
                throw new Exception("Calculating object hash for credential failed.", Err);

            }
       }

        public Credential()
        {

        }

        public Credential(int LabID, int RoleID, int ApplicationID)
        {
            Lab = new Laboratory(LabID);
            UserRole = new Role(RoleID);
            Application = new ClientApplication(ApplicationID);
        }

        public Credential(Laboratory Lab, Role Role, ClientApplication Application)
        {
            this.Lab = Lab;
            this.UserRole = Role;
            this.Application = Application;
        }

        //Subscribe a user to an application
        public bool AddOrUpdate(UserProfile User, bool SendMail)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand Command = new SqlCommand("EXEC p_AddCredential @UserID, @LabID, @RoleID, @ApplicationID;", Connection);
                Command.Parameters.AddWithValue("@UserID", User.UserID);
                Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);
                Command.Parameters.AddWithValue("@RoleID", UserRole.RoleID);
                Command.Parameters.AddWithValue("@LabID", Lab.LabID);

                try
                {

                    Command.ExecuteNonQuery();

                    if (!SendMail)
                    {
                        return true;
                    }
                    else
                    {
                        try
                        {

                            List<string> AdminEmails = Application.GetAdminEmailAddresses();
                            string FirstAdmin = string.Empty;
                            if (AdminEmails != null) FirstAdmin = AdminEmails[0];
                            string MessageFrom = String.IsNullOrEmpty(FirstAdmin) ? Settings.GetSetting("SystemAdminEmail", Users.ConnectionString) : FirstAdmin;
                            string  MessageSubject = "New Account";
                            bool MessageIsBodyHtml = true;

                            System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                            MessageBody.AppendFormat("Hi {0} <br/><br/> ", User.FirstName);
                            MessageBody.AppendFormat("Credentials have been created for you for the {0} <br/><br/>", Application.Description);
                            MessageBody.Append("You may now login using your existing username and password.");
                            MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");

                            //Message.To.Add(User.Email);
                            //SMTPsender.Send(Message);

                            try
                            {
                                Users.SendMail(MessageSubject, User.Email, MessageFrom, MessageBody.ToString(), MessageIsBodyHtml);
                            }
                            catch (Exception Err)
                            {
                                throw new Exception("Failed to send email, credential successfully added", Err);
                            }

                                return true;
                        }
                        catch (Exception Err)
                        {
                            throw new Exception("User account created successfully, however, notification email failed to send", Err);
                        }
                    }

                }
                catch (SqlException SQLErr)
                {
                    throw new Exception("A database error occurred while writing user credential",SQLErr);
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }
        }

        //UnSubscribe a user from an application
        public bool RemoveUserCredential(UserProfile User)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM user_app_lab_role_rel WHERE UserID=@UserID AND ApplicationID=@ApplicationID AND LabID=@LabID;", Connection);
                Command.Parameters.AddWithValue("@UserID", User.UserID);
                Command.Parameters.AddWithValue("@ApplicationID", Application.ApplicationID);
                Command.Parameters.AddWithValue("@LabID", Lab.LabID);
                Connection.Open();

                try
                {
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Remove Credential Query Returned Wrong Number of Rows");
                    }
                }
                catch (Exception Err)
                {
                    throw Err;
                }
            }
        }

        

        //Returns boolean value indicating if User has a credential with specified role for specified lab	
        public bool UserHasRoleForLab(UserProfile User, Role UserRole, Laboratory Lab, ClientApplication Application)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT CASE WHEN EXISTS (SELECT * FROM UserProfileView WHERE UserID=@UserID AND RoleID=@RoleID AND LabID=@LabID AND ApplicationName=@ApplicationName) THEN CAST(1 as bit) ELSE CAST(0 as bit) END");
                Command.Parameters.AddWithValue("@UserID", User.UserID);
                Command.Parameters.AddWithValue("@RoleID", UserRole.RoleID);
                Command.Parameters.AddWithValue("@ApplicationName", Application.Name);
                Command.Parameters.AddWithValue("@LabID", Lab.LabID);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {

                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return Reader.GetBoolean(0);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while checking if the user has the specified role in the specified lab for the specified application", Err);
                }
            }
        }


    }

    public class Laboratory
    {
        public int LabID { get; set; }
        public string Name { get; set; }
        public string LabHeadID { get; set; }
        //public List<string> CostCentres { get; set; }

        public ResearchInstitute Institute { get; set; }

        public override bool Equals(object obj)
        {
            Laboratory Lab = obj as Laboratory;
            if (Lab == null) return false;
            return Lab.LabID == LabID && Lab.Name == Name;
        }

        public override int GetHashCode()
        {
            return LabID.GetHashCode() & Name.GetHashCode();
        }

        public Laboratory()
        {
            //CostCentres = new List<string>();
        }

        public Laboratory(int LabID)
        {
            this.LabID = LabID;
            //CostCentres = new List<string>();
            FetchDetails();
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("SELECT LabID, LabName, InstituteID FROM labs WHERE labs.LabID=@LabID");
                    Command.Parameters.AddWithValue("@LabID", LabID);
                    Command.Connection = Connection;

                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            Name = Reader.GetString(1);
                            Institute = new ResearchInstitute(Reader.GetByte(2));
                        }
                    }

                    /*Command.CommandText = "SELECT CostCentreID FROM Cost_Centres WHERE LabID=@LabID AND Retired=0";

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                CostCentres.Add(Reader.GetString(0));
                            }
                        }
                    }*/

                    Command.CommandText = "SELECT UserID FROM Users WHERE LabID=@LabID AND IsLabHead=1";

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            LabHeadID = Reader.GetGuid(0).ToString();
                        }
                        else
                        {
                            LabHeadID = string.Empty;
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching lab details", Err);
                }
            }
        }

        
        public void Write()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO labs (LabName, InstituteID) VALUES (@LabName, @InstituteID)");
                Command.Parameters.AddWithValue("@LabName", this.Name);
                Command.Parameters.AddWithValue("@InstituteID", this.Institute.InstituteID);
                Command.Connection = Connection;
                Connection.Open();

                if (Command.ExecuteNonQuery() != 1)
                {
                    throw new Exception("Insert Lab query returned wrong number of affected rows");
                }
            }
        }

        public bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE labs SET LabName=@LabName, InstituteID=@InstituteID WHERE LabID=@LabID");
                Command.Parameters.AddWithValue("@LabID", LabID);
                Command.Parameters.AddWithValue("@LabName", Name);
                Command.Parameters.AddWithValue("@InstituteID", this.Institute.InstituteID);

                Command.Connection = Connection;
                Connection.Open();

                try
                {
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Update Lab query returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while updating lab details", Err);
                }

            }
        }


        public bool Delete()
        {
            try
            {
                if (LabHasUsers())
                {
                    return false;
                }
                else
                {
                    using (SqlConnection Connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand Command = new SqlCommand("DELETE FROM labs WHERE LabID=@LabID");
                        Command.Parameters.AddWithValue("@LabID", LabID);

                        Command.Connection = Connection;
                        Connection.Open();


                        if (Command.ExecuteNonQuery() == 1)
                        {
                            return true;
                        }
                        else
                        {
                            throw new Exception("Delete Lab query returned wrong number of affected rows");
                        }


                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while deleing lab", Err);
            }
        }


        private bool LabHasUsers()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT UserID FROM UserProfileView WHERE LabID=@LabID");
                Command.Parameters.AddWithValue("@LabID", LabID);
                Command.Connection = Connection;
                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    return Reader.HasRows;
                }
            }
        }


        //Retrieves a list of all the Users in a lab with a credential for given application
        public List<UserProfile> GetCredentialedUsers(ClientApplication App)
        {

            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection UserConnection = new SqlConnection(ConnectionString);
            SqlCommand UserCommand = new SqlCommand("SELECT DISTINCT UserID, FirstName FROM UserProfileView WHERE ApplicationName=@ApplicationName AND LabID=@LabID ORDER BY FirstName");
            UserCommand.Parameters.AddWithValue("@LabID", this.LabID);
            UserCommand.Parameters.AddWithValue("@ApplicationName", App.Name);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();


                SqlDataReader Reader = UserCommand.ExecuteReader();


                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Users.Add(new UserProfile(Reader.GetGuid(0)));
                    }
                }

                return Users;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while retirieving user profiles for specified lab and application", Err);
            }


        }

        //Retrieves a list of all the Users with a credential for any application in this lab
        public List<UserProfile> GetCredentialedUsers()
        {

            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection UserConnection = new SqlConnection(ConnectionString);
            SqlCommand UserCommand = new SqlCommand("SELECT DISTINCT UserID, FirstName FROM UserProfileView WHERE LabID=@LabID ORDER BY FirstName");
            UserCommand.Parameters.AddWithValue("@LabID", this.LabID);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();


                SqlDataReader Reader = UserCommand.ExecuteReader();


                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Users.Add(new UserProfile(Reader.GetGuid(0)));
                    }
                }

                return Users;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while retirieving user profiles for specified lab", Err);
            }


        }

        //Get a list of users that are assinged this lab as their primary lab
        public List<UserProfile> GetPrimaryMembers()
        {

            List<UserProfile> Users = new List<UserProfile>();

            SqlConnection UserConnection = new SqlConnection(ConnectionString);
            SqlCommand UserCommand = new SqlCommand("SELECT DISTINCT UserID, FirstName FROM Users WHERE LabID=@LabID ORDER BY FirstName");
            UserCommand.Parameters.AddWithValue("@LabID", this.LabID);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();


                SqlDataReader Reader = UserCommand.ExecuteReader();


                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Users.Add(new UserProfile(Reader.GetGuid(0)));
                    }
                }

                return Users;
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while retirieving user profiles for specified lab", Err);
            }
            
        }

        //Retrieves a list of LabIDs and Lab Names for give institute
        public static List<Laboratory> GetLabs(int InstituteID)
        {
            List<Laboratory> Labs = new List<Laboratory>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT LabID FROM labs WHERE InstituteID=@InstituteID ORDER BY LabName");
            Command.Parameters.AddWithValue("@InstituteID", InstituteID);
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Labs.Add(new Laboratory(Reader.GetByte(0)));
                    }
                }
                return Labs;
            }
            catch
            {
                throw;
            }

        }

        public static List<Laboratory> GetLabs()
        {
            List<Laboratory> Labs = new List<Laboratory>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT LabID FROM labs ORDER BY LabName");
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Labs.Add(new Laboratory(Reader.GetByte(0)));
                    }
                }
                return Labs;
            }
            catch (Exception Err)
            {
                throw;
            }

        }


    }

    public class ResearchInstitute
    {
        public int InstituteID { get; set; }
        public string Name { get; set; }
        public string BillingAddress { get; set; }
        public string DeliveryAddress { get; set; }
        public bool IsCurrentSite { get; set; }

        public override bool Equals(object obj)
        {
            ResearchInstitute Institute = obj as ResearchInstitute;
            if (Institute == null) return false;
            
            return Institute.InstituteID == InstituteID && Institute.Name == Name;
            
        }

        public override int GetHashCode()
        {
            return InstituteID.GetHashCode() & Name.GetHashCode();
        }

        public ResearchInstitute()
        {

        }

        public ResearchInstitute(string Name)
        {
            this.Name = Name;
            this.InstituteID = -1;
        }

        public ResearchInstitute(int InstituteID)
        {
            this.InstituteID = InstituteID;
            FetchDetails();
        }
        

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("SELECT Name, DeliveryAddress, BillingAddress, IsCurrentSite FROM Institutes WHERE Institutes.InstituteID=@InstituteID");
                    Command.Parameters.AddWithValue("@InstituteID", InstituteID);
                    Command.Connection = Connection;

                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            Name = Reader.IsDBNull(0) ? String.Empty : Reader.GetString(0);
                            DeliveryAddress = Reader.IsDBNull(1) ? String.Empty : Reader.GetString(1);
                            BillingAddress = Reader.IsDBNull(2) ? String.Empty : Reader.GetString(2);
                            IsCurrentSite = Reader.IsDBNull(3) ? false : Reader.GetBoolean(3);
                        }
                    }


                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching institute details", Err);
                }
            }
        }

        public bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("INSERT INTO Institutes (Name, DeliveryAddress, BillingAddress, IsCurrentSite) VALUES (@Name, @DeliveryAddress, @BillingAddress, @IsCurrentSite)");
                    Command.Parameters.AddWithValue("@Name", Name);
                    Command.Parameters.AddWithValue("@DeliveryAddress", String.IsNullOrEmpty(DeliveryAddress) ? (object)DBNull.Value : DeliveryAddress);
                    Command.Parameters.AddWithValue("@BillingAddress", String.IsNullOrEmpty(BillingAddress) ? (object)DBNull.Value : BillingAddress);
                    Command.Parameters.AddWithValue("@IsCurrentSite", IsCurrentSite);
                    Command.Connection = Connection;

                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while writing institute", Err);
                }
            }
        }

        public bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("UPDATE Institutes SET Name=@Name, DeliveryAddress=@DeliveryAddress, BillingAddress=@BillingAddress, IsCurrentSite=@IsCurrentSite WHERE InstituteID=@InstituteID");
                    Command.Parameters.AddWithValue("@InstituteID", InstituteID);
                    Command.Parameters.AddWithValue("@Name", Name);
                    Command.Parameters.AddWithValue("@DeliveryAddress", String.IsNullOrEmpty(DeliveryAddress) ? (object)DBNull.Value : DeliveryAddress);
                    Command.Parameters.AddWithValue("@BillingAddress", String.IsNullOrEmpty(BillingAddress) ? (object)DBNull.Value : BillingAddress);
                    Command.Parameters.AddWithValue("@IsCurrentSite", IsCurrentSite);
                    Command.Connection = Connection;

                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while updating institute", Err);
                }
            }
        }

        public bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("DELETE FROM Institutes WHERE InstituteID=@InstituteID", Connection);
                    Command.Parameters.AddWithValue("@InstituteID", InstituteID);
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    return true;

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while deleting institute", Err);
                }
            }
        }

        internal static ResearchInstitute GetCurrentSite()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("SELECT InstituteID FROM Institutes WHERE IsCurrentSite=1");
                    Command.Connection = Connection;
                    Connection.Open();

                    object InstituteID = Command.ExecuteScalar();

                    if (InstituteID != null && InstituteID != DBNull.Value)
                    {
                        return new ResearchInstitute(Convert.ToInt32(InstituteID));
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while getting current site", Err);
                }
            }
        }


        //Retrieves a list of registered Institutes
        public static List<ResearchInstitute> GetInstitutes()
        {
            List<ResearchInstitute> Institutes = new List<ResearchInstitute>();

            SqlConnection Connection = new SqlConnection(ConnectionString);
            SqlCommand Command = new SqlCommand("SELECT InstituteID FROM Institutes");
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        Institutes.Add(new ResearchInstitute(Reader.GetByte(0)));
                    }
                }
                return Institutes;
            }
            catch
            {
                return null;
            }

        }
    }

    public class CostCentre
    {
        public string CostCentreID { get; set; }
        public string Description { get; set; }
        public bool Retired { get; set; }
        public int LabID { get; set; }

        public CostCentre() { }

        public CostCentre(string CostCentreID)
        {
            this.CostCentreID = CostCentreID.Trim();
            FetchDetails();
        }

        private void FetchDetails()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand Command = new SqlCommand("SELECT Description, Retired, LabID FROM Cost_Centres WHERE CostCentreID=@CostCentreID");
                    Command.Parameters.AddWithValue("@CostCentreID", CostCentreID);
                    Command.Connection = Connection;

                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            Description = Reader.GetString(0);
                            Retired = Reader.GetBoolean(1);
                            LabID = Reader.GetByte(2);
                        }
                        else
                        {
                            throw new ArgumentException("Supplied CostCentreID returned no rows");
                        }
                    }

                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching cost centre details", Err);
                }
            }
        }


        internal bool Write()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO cost_centres (CostCentreID, Description, LabID, Retired) VALUES (@CostCentreID, @Description, @LabID, @Retired)");
                Command.Parameters.AddWithValue("@CostCentreID", this.CostCentreID);
                Command.Parameters.AddWithValue("@Description", this.Description);
                Command.Parameters.AddWithValue("@LabID", this.LabID);
                Command.Parameters.AddWithValue("@Retired", this.Retired);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Insert new cost centre returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while writing cost centre", Err);
                }
            }
        }

        internal bool Update()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE cost_centres SET Description=@Description, LabID=@LabID, Retired=@Retired WHERE CostCentreID=@CostCentreID");
                Command.Parameters.AddWithValue("@CostCentreID", this.CostCentreID);
                Command.Parameters.AddWithValue("@Description", this.Description);
                Command.Parameters.AddWithValue("@LabID", this.LabID);
                Command.Parameters.AddWithValue("@Retired", this.Retired);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Update cost centre returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while updating cost centre details", Err);
                }
            }
        }

        internal bool Delete()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("DELETE FROM cost_centres WHERE CostCentreID=@CostCentreID");
                Command.Parameters.AddWithValue("@CostCentreID", this.CostCentreID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    if (Command.ExecuteNonQuery() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("Delete cost centre returned wrong number of affected rows");
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while deleting cost centre", Err);
                }
            }
        }

        public bool CreateCostCentre(string CostCentreID, string Description, string LabID)
        {
            CostCentre CC = new CostCentre();
            CC.CostCentreID = CostCentreID;
            CC.Description = Description;
            CC.LabID = int.Parse(LabID);
            CC.Retired = false;
            return CC.Write();
        }


        //Return a list of cost centres associated with a Lab
        public static List<CostCentre> GetCostCentresByLabID(int LabID, bool ActiveOnly)
        {
            List<CostCentre> CostCentres = new List<CostCentre>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT CostCentreID FROM cost_centres WHERE LabID=@LabID" + ((ActiveOnly) ? " AND Retired=0" : string.Empty));
                Command.Parameters.AddWithValue("@LabID", LabID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                CostCentres.Add(new CostCentre(Reader.GetString(0)));
                            }
                            return CostCentres;
                        }
                        else { return CostCentres; }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching cost centres for specified lab", Err);
                }
            }

        }

        //Return a list of cost centres associated with a User
        public static List<CostCentre> GetCostCentresByUserID(Guid UserID, string ApplicationName, bool ActiveOnly)
        {
            UserProfile User = new UserProfile(UserID);
            if (User.IsExternalUser)
            {
                CostCentre C = new CostCentre();
                C.CostCentreID = UserID.ToString();
                C.Description = "External User Cost Centre";
                C.LabID = User.Lab.LabID;
                return new List<CostCentre> { C };
            }

            List<CostCentre> CostCentres = new List<CostCentre>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT DISTINCT CostCentreID FROM [UserProfileView] INNER JOIN cost_centres ON [UserProfileView].LabID = cost_centres.LabID WHERE ApplicationName=@ApplicationName AND UserID=@UserID" + (ActiveOnly ? " AND Retired=0" : String.Empty));
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Parameters.AddWithValue("@UserID", UserID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                CostCentres.Add(new CostCentre(Reader.GetString(0)));
                            }
                            return CostCentres;
                        }
                        else { return CostCentres; }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching cost centres for specified user", Err);
                }
            }

        }

        //Return a LabID associated with a cost centre
        /*public int GetLabIDFromCostCentre(string CostCentreID)
        {

            try
            {
                CostCentre CC = new CostCentre(CostCentreID);
                return CC.LabID;
            }
            catch (Exception Err)
            {
                System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetLabIDForCostCentre; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 5);
                return -1;
            }


        }*/

        //Return a list of cost centres
        public static List<CostCentre> GetCostCentres()
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT CostCentreID FROM cost_centres");
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            List<CostCentre> CostCentres = new List<CostCentre>();
                            while (Reader.Read())
                            {
                                CostCentres.Add(new CostCentre(Reader.GetString(0)));
                            }
                            return CostCentres;
                        }
                        else { return null; }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An error occurred while fetching cost centres", Err);
                }
            }

        }

    }

    ///////////////////////////////////////////////////////////////
    ///////////////          Methods       ////////////////////////
    ///////////////////////////////////////////////////////////////


    /////////////////////
    ////  Helpers  //////
    /////////////////////

    //Send an Email
    public static bool SendMail(string Subject, string Recipients, string Sender, string MessageBody, bool IsBodyHtml)
    {
        System.Net.Mail.SmtpClient SMTPsender = new System.Net.Mail.SmtpClient();
        SMTPsender.Host = Settings.GetSetting("SMTPServer",Users.ConnectionString);
        SMTPsender.Port = Int32.Parse(Settings.GetSetting("SMTPPort", Users.ConnectionString));
        System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
        if (String.IsNullOrEmpty(Sender))
        {
            Message.From = new System.Net.Mail.MailAddress(Settings.GetSetting("SystemAdminEmail", Users.ConnectionString));
        }
        else
        {
            Message.From = new System.Net.Mail.MailAddress(Sender);
        }

        Message.Subject = Subject;
        Message.IsBodyHtml = IsBodyHtml;

        Message.Body = MessageBody;
        Message.To.Add(Recipients);

        try
        {
            SMTPsender.Send(Message);
            return true;
        }
        catch (Exception Err)
        {
            throw new Exception("An error occurred while sending email. Check SMTP server settings are correct.", Err);
        }
    }

    

    //Generates a 40 character hash for password encryption
    public static string GetHash(string Input)
    {
        SHA1 SHAEncryptor = new SHA1CryptoServiceProvider();
        byte[] bHashedInput = SHAEncryptor.ComputeHash(System.Text.Encoding.Default.GetBytes(Input.ToCharArray()));
        return BitConverter.ToString(bHashedInput).Replace("-", string.Empty);
    }

    private static string PasswordGenerator()
    {
        string Password = "";
        Random Generator = new Random();
        for (int i = 0; i < 5; i++)
        {
            Password += Generator.Next(0, 9).ToString() + ((char)(Generator.Next(65, 90))).ToString();
        }

        return Password;
    }


}

//Authentication
//Checks user tables for a username/password match


//Profile/List Retrieval


/*

//Retrieves a list of all Users as User Profiles
public List<UserProfile> GetUserListAsProfiles()
{

    List<UserProfile> Users = new List<UserProfile>();

    SqlConnection Connection = new SqlConnection(ConnectionString);
    SqlCommand Command = new SqlCommand("SELECT UserID FROM Users");

    Command.Connection = Connection;

    try
    {
        Connection.Open();


        using (SqlDataReader Reader = Command.ExecuteReader())
        {

            if (Reader.HasRows)
            {
                while (Reader.Read())
                {
                    Users.Add(new UserProfile(Reader.GetGuid(0)));
                }
            }
        }

        return Users;
    }
    catch (Exception Err)
    {
        System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetUsers; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 2);
        return null;
    }
}


//Retrieves a list of all the Users in a lab
public List<string[]> GetUsersByLabIDAsIDNamePairs(string LabID, string ApplicationName)
{

    List<string[]> Users = new List<string[]>();

    SqlConnection UserConnection = new SqlConnection(ConnectionString);
    SqlCommand UserCommand = new SqlCommand("SELECT UserID, FirstName, LastName FROM UserProfileView WHERE ApplicationName=@ApplicationName AND LabID=@LabID");
    UserCommand.Parameters.AddWithValue("@LabID", LabID);
    UserCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
    UserCommand.Connection = UserConnection;

    try
    {
        UserConnection.Open();


        SqlDataReader Reader = UserCommand.ExecuteReader();


        if (Reader.HasRows)
        {
            while (Reader.Read())
            {
                Users.Add(new string[] { Reader.GetGuid(0).ToString(), Reader.GetString(1) + " " + Reader.GetString(2) });
            }
        }

        return Users;
    }
    catch (Exception Err)
    {
        System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetUsersByLabIDAsIDNamePairs; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 2);
        return null;
    }


}

//Retrieves a userprofile object from a UserID
public UserProfile GetUserByIDForApplication(Guid UserID, string ApplicationName)
{
    using (SqlConnection Connection = new SqlConnection(ConnectionString))
    {

        try
        {
            return new UserProfile(UserID, new ClientApplication(ApplicationName));
        }
        catch (Exception Err)
        {
            System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetProfileByID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 2);
            return null;
        }

    }

}

//Retrieves a userprofile object from a UserID
public UserProfile GetUserByID(Guid UserID)
{
    using (SqlConnection Connection = new SqlConnection(ConnectionString))
    {
        try
        {
            return new UserProfile(UserID);
        }
        catch (Exception Err)
        {
            System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetProfileByID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 2);
            return null;
        }
    }

}

//Gets a Users ID from their User Name
public string GetUserIDFromUserName(string UserName, string ApplicationName)
{

    System.Text.RegularExpressions.Regex Matcher = new System.Text.RegularExpressions.Regex(@"\w+\s\w+");
    string FirstName;
    string LastName;

    if (Matcher.IsMatch(UserName))
    {

        FirstName = UserName.Split(' ')[0];
        LastName = UserName.Split(' ')[1];
    }
    else
    {
        return "-1:BadLogin";
    }

    ClientApplication App;

    try
    {
        App = new ClientApplication(ApplicationName);
    }
    catch
    {
        return "-2:Bad Application";
    }


    using (SqlConnection Connection = new SqlConnection(ConnectionString))
    {
        SqlCommand Command = new SqlCommand();
        Command.CommandText = "SELECT UserID FROM AuthenticationView WHERE FirstName=@FirstName AND LastName=@LastName AND ApplicationName=@ApplicationName";

        Command.Parameters.AddWithValue("@FirstName", FirstName);
        Command.Parameters.AddWithValue("@LastName", LastName);
        Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);

        Command.Connection = Connection;

        try
        {
            Connection.Open();
            using (SqlDataReader Reader = Command.ExecuteReader())
            {
                if (Reader.HasRows)
                {
                    Reader.Read();

                    return Reader.GetGuid(0).ToString();
                }
                else
                {
                    return "-1:BadLogin";
                }
            }
        }
        catch (Exception Err)
        {
            return "-2:" + Err.Message;
        }
    }

}

//Create Account
//Create a new user account without subscribing to an application
public string CreateUserAccount(string FirstName, string LastName, int LabID, string Extension, string Email, string RFID, bool IsLabHead, bool SendEmail)
{

    try
    {
        string Password = string.Empty;

        if (SendEmail)
        {
            Password = PasswordGenerator();
        }
        else
        { 
            Password = "password";
        }
        string HashedPassword = GetHash(Password.Trim());

        UserProfile User = new UserProfile();
        User.FirstName = FirstName;
        User.LastName = LastName;
        User.Lab = new Laboratory(LabID);
        User.Extension = Extension;
        User.RFID = RFID;
        User.IsLabHead = IsLabHead;
        User.Email = Email;


        User.Write(HashedPassword);



        if (SendEmail)
        {
            try
            {
                System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                MessageBody.AppendFormat("Hi {0} <br/><br/> ", FirstName);
                MessageBody.AppendFormat("A new account has been created for you. <br/><br/>");
                MessageBody.Append("This account will remain inactive until you are subscribed to one or more applications.<br/>");
                MessageBody.AppendFormat("<table><tr><th>User Name:</th><td>{0} {1}</td></tr><tr><th>Password:</th><td>{2}</td></tr></table>", FirstName, LastName, Password);
                MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");

                if (!SendMail("New Account", Email, Settings.GetSetting("SystemAdminEmail"), MessageBody.ToString(), true))
                {
                    throw new Exception("Mail Error");
                }
            }
            catch (Exception Err)
            {
                return "MailError:" + Err + "," + Settings.GetSetting("SMTPServer") + "-" + Password;
            }
        }

        return "Success";
    }
    catch (SqlException Err)
    {


        if (Err.Message == "AccountExists")
        {
            return "A User Account With The Supplied Details Already Exists For This Application";
        }
        else if (Err.Message == "InvalidApplicationName")
        {
            return "The supplied application name was not a recognised application";
        }
        else
        {
            return "SQLException:"+Err.Message;
        }
    }
    catch (Exception Err)
    {

        string returnmessage = "An error occurred while creating user account:";
        while (Err != null)
        {
            returnmessage += Err.Message;
            Err = Err.InnerException;
        }
        return returnmessage;
    }

}

//Create a new user account and/or subscribe user to the calling application
public string CreateUserAccountForApplication(string FirstName, string LastName, int LabID, string Extension, string Email, string RFID, bool IsLabHead, string RoleID, Clien ApplicationName, bool SendEmail)
    {

        string Password = string.Empty;

        if (SendEmail)
        {
            Password = PasswordGenerator();
        }
        else
        {
            Password = "password";
        } 
        string HashedPassword = GetHash(Password.Trim());

        UserProfile User = new UserProfile();
        User.FirstName = FirstName;
        User.LastName = LastName;
        User.Lab = new Laboratory(LabID);
        User.Extension = Extension;
        User.Email = Email;
        User.RFID = RFID;
        User.IsLabHead = IsLabHead;

       try
        {
            bool ExistingUser = User.Exists();

            User.Write(HashedPassword, ApplicationName, RoleID);

            if (SendEmail)
            {
                try
                {
                    string MessageFrom;
                    List<string> AdminEmails = GetAdminEmailAddresses(ApplicationName);
                    string FirstAdmin = string.Empty;
                    if (AdminEmails != null) FirstAdmin = AdminEmails[0];
                    MessageFrom = (String.IsNullOrEmpty(FirstAdmin) ? Settings.GetSetting("SystemAdminEmail") : FirstAdmin);

                    System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();

                    if (!ExistingUser)
                    {
                        MessageBody.AppendFormat("Hi {0} <br/><br/> ", FirstName);
                        MessageBody.AppendFormat("A new account has been created for you for the {0} <br/><br/>", GetApplicationDescriptiveName(ApplicationName));
                        MessageBody.Append("You may already have a previous account for another application using this login system, if so you can login using you existing details. Otherwise, You can login using the following Username/Password <br/>");
                        MessageBody.AppendFormat("<table><tr><th>User Name:</th><td>{0} {1}</td></tr><tr><th>Password:</th><td>{2}</td></tr></table>", FirstName, LastName, Password);
                        MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");
                    }
                    else
                    {
                        MessageBody.AppendFormat("Hi {0} <br/><br/> ", User.FirstName);
                        MessageBody.AppendFormat("Credentials have been created for you for the {0} <br/><br/>", GetApplicationDescriptiveName(ApplicationName));
                        MessageBody.Append("You may now login using your existing username and password.");
                        MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");
                    }

                    if (!SendMail("New Account", Email, MessageFrom, MessageBody.ToString(), true))
                    {
                        throw new Exception("Mail Error");
                    }
                }
                catch (Exception Err)
                {
                    return "MailError:" + Err + "," + Settings.GetSetting("SMTPServer") + "-" + Password;
                }
            }

            return "Success";
        }
        catch (SqlException Err)
        {
            if (Err.Message == "AccountExists")
            {
                return "A User Account With The Supplied Details Already Exists For This Application";
            }
            else if (Err.Message == "InvalidApplicationName")
            {
                return "The supplied application name was not a recognised application";
            }
            else
            {
                throw Err;
            }
        }
        catch (Exception Err)
        {

            string returnmessage = "An error occurred while creating user account:";
            while (Err != null)
            {
                returnmessage += Err.Message;
                Err = Err.InnerException;
            }
            return returnmessage;
        }

    }

    //Delete Account
    //Permanantly delete a User Profile
    public bool DeleteUser(UserProfile User)
    {
        return User.Delete();
    }

//Revoke a users access to an application using their UserID
public int DeleteUserByID(Guid UserID, string ApplicationName, int LabID)
    {
        using (SqlConnection DeleteConnection = new SqlConnection(ConnectionString))
        {
            SqlCommand DeleteCommand = new SqlCommand("DELETE FROM user_app_lab_role_rel WHERE UserID=@UserID AND LabID=@LabID AND ApplicationID=(SELECT ApplicationID FROM applications WHERE ApplicationName=@ApplicationName)");
            DeleteCommand.Parameters.AddWithValue("@UserID", UserID);
            DeleteCommand.Parameters.AddWithValue("@LabID", LabID);
            DeleteCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
            DeleteCommand.Connection = DeleteConnection;
            try
            {
                DeleteConnection.Open();
                if (DeleteCommand.ExecuteNonQuery() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception Err)
            {
                System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:DeleteUserByID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 7);
                return -1;
            }
        }
    }

    //Revoke a users access to an application using their email address
    public int DeleteUserByEmail(string Email, string ApplicationName)
    {
        SqlConnection DeleteConnection = new SqlConnection(ConnectionString);
        SqlCommand DeleteCommand = new SqlCommand("DELETE FROM user_app_lab_role_rel WHERE UserID=(SELECT UserID FROM users WHERE Email=@Email) AND ApplicationID=(SELECT ApplicationID FROM applications WHERE ApplicationName=@ApplicationName)");
        DeleteCommand.Parameters.AddWithValue("@Email", Email);
        DeleteCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
        DeleteCommand.Connection = DeleteConnection;

        try
        {
            DeleteConnection.Open();
            if (DeleteCommand.ExecuteNonQuery() == 1)
            {
                return 1;
            }
            else
            {
                DeleteConnection.Close();
                return 0;
            }
        }
        catch (Exception Err)
        {
            System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:DeleteUserByEmail; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 8);
            return -1;
        }

}
//Credentials
        //Update
        //Update a Users Details
        public bool UpdateUserInfo(UserProfile UserInfo)
        {
            try
            {
                return UserInfo.Update();
            }
            catch
            {
                return false;
            }
        }

*/
