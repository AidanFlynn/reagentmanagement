﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace ReagentSignout
{
    public partial class DatabaseConfig : Form
    {
        public static string ApplicationName = string.Empty;
        public static string Host = string.Empty;
        public static string DatabaseName = string.Empty;
        public static string UserName = string.Empty;
        public static string Password = string.Empty;
        
        public DatabaseConfig()
        {
            InitializeComponent();
            AppNameTextBox.Text = ApplicationName;
            DatabaseHostTextBox.Text = Host;
            DatabaseNameTextBox.Text =DatabaseName;
            DatabaseUsernameTextBox.Text = UserName;
            DatabasePasswordTextBox.Text = Password;
        }

        private void DatabaseConfigUpdateButton_Click(object sender, EventArgs e)
        {
            string _applicationname = AppNameTextBox.Text;
            string _host = DatabaseHostTextBox.Text;
            string _dbName = DatabaseNameTextBox.Text;
            string _username = DatabaseUsernameTextBox.Text;
            string _password = DatabasePasswordTextBox.Text;

            if (String.IsNullOrEmpty(_applicationname) || String.IsNullOrEmpty(_host) || String.IsNullOrEmpty(_dbName) || String.IsNullOrEmpty(_username) || String.IsNullOrEmpty(_password))
            {
                MessageBox.Show("You must supply a value for all fields", "Missing Field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    ApplicationName = _applicationname;
                    Host = _host;
                    DatabaseName = _dbName;
                    UserName = _username;
                    Password = _password;

                    UpdateConfigFile(ApplicationName, Host, DatabaseName, UserName, Password);

                    MessageBox.Show("Database configuration updated. Software will now restart.", "Restart Required", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Restart();
                    this.Close();
                }
                catch (Exception Err)
                {
                    string ErrorMessage = string.Empty;
                    while (Err != null)
                    {
                        ErrorMessage += Err.Message + "; ";
                        Err = Err.InnerException;
                    }
                    MessageBox.Show("An error occurred while updating database configuration" + ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
             
            }
        }

        public bool SetApplicationNameField(string ApplicationName)
        {
            AppNameTextBox.Text = ApplicationName;
            return true;
        }

        public bool SetDatabaseHostField(string DatabaseHost)
        {
            DatabaseHostTextBox.Text = DatabaseHost;
            return true;
        }

        public bool SetDatabaseNameField(string DatabaseName)
        {
            DatabaseNameTextBox.Text = DatabaseName;
            return true;
        }

        public bool SetDatabaseUserNameField(string DatabaseUsername)
        {
            DatabaseUsernameTextBox.Text = DatabaseUsername;
            return true;
        }

        public bool SetDatabasePasswordField(string DatabasePassword)
        {
            DatabasePasswordTextBox.Text = DatabasePassword;
            return true;
        }

        public static void UpdateConfigFile(string ApplicationName, string Host, string DatabaseName, string UserName, string Password)
        {
            using (StreamWriter ConfigWriter = new StreamWriter("config.cfg",false))
            {
                Encryption.TripleDESStringEncryptor Encrypter = new Encryption.TripleDESStringEncryptor();

                ConfigWriter.WriteLine(ApplicationName);
                ConfigWriter.WriteLine(Host);
                ConfigWriter.WriteLine(DatabaseName);
                ConfigWriter.WriteLine(UserName);
                ConfigWriter.Write(Encrypter.EncryptString(Password));
            }
        }

        public static string GetConnectionString()
        {
            return @"Data Source=" + Host + ";Initial Catalog=" + DatabaseName + ";User Id=" + UserName + ";Password=" + Password;        
        }

        public static string GetUsersConnectionString()
        {
            return @"Data Source=" + Host + ";Initial Catalog=Users;User Id=" + UserName + ";Password=" + Password;
        }

        public static void LoadConfig()
        {
            if (!File.Exists("config.cfg"))
            {
                MessageBox.Show("No database configuration file was found. Click OK to configure application...", "Missing Config File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DatabaseConfig DBConfig = new DatabaseConfig();
                DBConfig.ShowDialog();
            }
            else
            {
                using (StreamReader ConfigReader = new StreamReader("config.cfg"))
                {
                    Encryption.TripleDESStringEncryptor Decrypter = new Encryption.TripleDESStringEncryptor();
                    string _applicationname = ConfigReader.ReadLine();
                    string _host = ConfigReader.ReadLine();
                    string _dbName = ConfigReader.ReadLine();
                    string _username = ConfigReader.ReadLine();
                    string _encryptedpassword = ConfigReader.ReadToEnd();

                    string _password = Decrypter.DecryptString(_encryptedpassword);

                    if (String.IsNullOrEmpty(_applicationname) || String.IsNullOrEmpty(_host) || String.IsNullOrEmpty(_dbName) || String.IsNullOrEmpty(_username) || String.IsNullOrEmpty(_encryptedpassword))
                    {
                        MessageBox.Show("One or more database configuration fields are missing, you must supply this information before continuing", "Missing Field", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DatabaseConfig DBConfig = new DatabaseConfig();
                        DBConfig.SetApplicationNameField(_applicationname);
                        DBConfig.SetDatabaseHostField(_host);
                        DBConfig.SetDatabaseNameField(_dbName);
                        DBConfig.SetDatabaseUserNameField(_username);
                        DBConfig.SetDatabasePasswordField(_password);
                        DBConfig.ShowDialog();
                    }
                    else
                    {
                        SetApplicationsName(_applicationname);
                        SetConnectionStringFields(_host, _dbName, _username, _password);
                        
                    }
                }
            }
        }

        private static void SetApplicationsName(string _applicationname)
        {
            ApplicationName = _applicationname;
        }

        private static void SetConnectionStringFields(string host, string dbName, string username, string password)
        {
            Host = host;
            DatabaseName = dbName;
            UserName = username;
            Password = password;
        }

        internal static bool Test(out Exception Err)
        {
            using (SqlConnection Connection = new SqlConnection(GetConnectionString()))
            {
                try
                {
                    Connection.Open();
                    Err = null;
                    return true;
                }
                catch (Exception Error)
                {
                    Err = Error;
                    return false;
                }
            }
        }
    }
}
