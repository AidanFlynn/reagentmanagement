﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ReagentSignout
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void Help_Load(object sender, EventArgs e)
        {
            if (File.Exists(".\\Help.txt"))
            {
                using (StreamReader HelpReader = new StreamReader(".\\Help.txt"))
                {
                    HelpLabel.Text = HelpReader.ReadToEnd();
                }
            }
            else
            {
                HelpLabel.Text = "No Help File Found";
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
