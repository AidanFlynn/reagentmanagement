﻿namespace ReagentSignout
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PasswordChangeCurrentTextBox = new System.Windows.Forms.TextBox();
            this.PasswordChangeNewTextBox = new System.Windows.Forms.TextBox();
            this.PasswordChangeConfirmTextBox = new System.Windows.Forms.TextBox();
            this.PasswordChangeConfirmButton = new System.Windows.Forms.Button();
            this.PasswordChangeCancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Current Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "New Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Confirm Password";
            // 
            // PasswordChangeCurrentTextBox
            // 
            this.PasswordChangeCurrentTextBox.Location = new System.Drawing.Point(108, 18);
            this.PasswordChangeCurrentTextBox.Name = "PasswordChangeCurrentTextBox";
            this.PasswordChangeCurrentTextBox.PasswordChar = '*';
            this.PasswordChangeCurrentTextBox.Size = new System.Drawing.Size(164, 20);
            this.PasswordChangeCurrentTextBox.TabIndex = 3;
            // 
            // PasswordChangeNewTextBox
            // 
            this.PasswordChangeNewTextBox.Location = new System.Drawing.Point(108, 50);
            this.PasswordChangeNewTextBox.Name = "PasswordChangeNewTextBox";
            this.PasswordChangeNewTextBox.PasswordChar = '*';
            this.PasswordChangeNewTextBox.Size = new System.Drawing.Size(164, 20);
            this.PasswordChangeNewTextBox.TabIndex = 4;
            // 
            // PasswordChangeConfirmTextBox
            // 
            this.PasswordChangeConfirmTextBox.Location = new System.Drawing.Point(108, 82);
            this.PasswordChangeConfirmTextBox.Name = "PasswordChangeConfirmTextBox";
            this.PasswordChangeConfirmTextBox.PasswordChar = '*';
            this.PasswordChangeConfirmTextBox.Size = new System.Drawing.Size(164, 20);
            this.PasswordChangeConfirmTextBox.TabIndex = 5;
            // 
            // PasswordChangeConfirmButton
            // 
            this.PasswordChangeConfirmButton.Location = new System.Drawing.Point(108, 108);
            this.PasswordChangeConfirmButton.Name = "PasswordChangeConfirmButton";
            this.PasswordChangeConfirmButton.Size = new System.Drawing.Size(77, 31);
            this.PasswordChangeConfirmButton.TabIndex = 6;
            this.PasswordChangeConfirmButton.Text = "Confirm";
            this.PasswordChangeConfirmButton.UseVisualStyleBackColor = true;
            this.PasswordChangeConfirmButton.Click += new System.EventHandler(this.PasswordChangeConfirmButton_Click);
            // 
            // PasswordChangeCancelButton
            // 
            this.PasswordChangeCancelButton.Location = new System.Drawing.Point(195, 108);
            this.PasswordChangeCancelButton.Name = "PasswordChangeCancelButton";
            this.PasswordChangeCancelButton.Size = new System.Drawing.Size(77, 31);
            this.PasswordChangeCancelButton.TabIndex = 7;
            this.PasswordChangeCancelButton.Text = "Cancel";
            this.PasswordChangeCancelButton.UseVisualStyleBackColor = true;
            this.PasswordChangeCancelButton.Click += new System.EventHandler(this.PasswordChangeCancelButton_Click);
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 147);
            this.ControlBox = false;
            this.Controls.Add(this.PasswordChangeCancelButton);
            this.Controls.Add(this.PasswordChangeConfirmButton);
            this.Controls.Add(this.PasswordChangeConfirmTextBox);
            this.Controls.Add(this.PasswordChangeNewTextBox);
            this.Controls.Add(this.PasswordChangeCurrentTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChangePassword";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PasswordChangeCurrentTextBox;
        private System.Windows.Forms.TextBox PasswordChangeNewTextBox;
        private System.Windows.Forms.TextBox PasswordChangeConfirmTextBox;
        private System.Windows.Forms.Button PasswordChangeConfirmButton;
        private System.Windows.Forms.Button PasswordChangeCancelButton;
    }
}