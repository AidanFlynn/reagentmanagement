﻿using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for Settings
/// </summary>
public static class Settings
{
    public static string GetSetting(string SettingName, string ConnectionString)
    {
        try
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT Value FROM settings WHERE SettingName=@SettingName");
                Command.Parameters.AddWithValue("@SettingName", SettingName);
                Command.Connection = Connection;

                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        return Reader["Value"].ToString();
                    }
                    else
                    {
                        throw new Exception("Supplied SettingName returned no rows");
                    }
                }
            }
        }
        catch (Exception Err)
        {
            throw new Exception("Fetching setting failed", Err);
        }

    }

    public static bool SettingExists(string SettingName, string ConnectionString)
    {
        try
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("SELECT Value FROM settings WHERE SettingName=@SettingName");
                Command.Parameters.AddWithValue("@SettingName", SettingName);
                Command.Connection = Connection;

                Connection.Open();

                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    return Reader.HasRows;

                }
            }
        }
        catch (Exception Err)
        {
            throw new Exception("Fetching setting failed", Err);
        }

    }


    public static bool SetSetting(string SettingName, string Value, string ConnectionString)
    {
        try
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("UPDATE settings SET Value=@Value WHERE SettingName=@SettingName");
                Command.Parameters.AddWithValue("@Value", Value);
                Command.Parameters.AddWithValue("@SettingName", SettingName);
                Command.Connection = Connection;

                Connection.Open();


                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Update query returned no affected rows");
                }

            }

        }
        catch (Exception Err)
        {
            throw new Exception("Updating setting failed", Err);
        }
    }

    public static bool AddSetting(string SettingName, string Value, string ConnectionString)
    {
        try
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                SqlCommand Command = new SqlCommand("INSERT INTO settings (SettingName, Value) VALUES (@SettingName, @Value)");
                Command.Parameters.AddWithValue("@Value", Value);
                Command.Parameters.AddWithValue("@SettingName", SettingName);
                Command.Connection = Connection;

                Connection.Open();


                if (Command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Setting Insert query returned no affected rows");
                }

            }

        }
        catch (Exception Err)
        {
            throw new Exception("Adding setting failed", Err);
        }
    }
}
