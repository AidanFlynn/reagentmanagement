﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

    /// <summary>
    /// Summary description for PMCI_Users
    /// </summary>
    [WebService(Namespace = "http://petermac.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PMCI_Users : System.Web.Services.WebService
    {

        public PMCI_Users()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
            if (!System.Diagnostics.EventLog.SourceExists("AuthorisationWebService"))
            {
                System.Diagnostics.EventLog.CreateEventSource("AuthorisationWebService", "Application");
            }

        }

        private string USERS_CON_STRING = WebConfigurationManager.ConnectionStrings["Users"].ConnectionString;

        [WebMethod(Description = "Checks user tables for a username/password match")]
        public string Authenticate(string UserName, string Password, string ApplicationName)
        {

            System.Text.RegularExpressions.Regex Matcher = new System.Text.RegularExpressions.Regex(@"\w+\s\w+");
            string FirstName;
            string LastName;

            if (Matcher.IsMatch(UserName))
            {

                FirstName = UserName.Split(' ')[0];
                LastName = UserName.Split(' ')[1];
            }
            else
            {
                return "-1";
            }

            string HashedPassword = GetHash(Password);

            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT UserID FROM AuthenticationView WHERE FirstName=@FirstName AND LastName=@LastName AND Password=@Password AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@FirstName", FirstName);
                Command.Parameters.AddWithValue("@LastName", LastName);
                Command.Parameters.AddWithValue("@Password", HashedPassword);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return Reader.GetGuid(0).ToString();
                        }
                        else
                        {
                            return "-1";
                        }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:Authenticate; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 1);
                    return "-2";
                }
            }

        }

        [WebMethod(Description = "Checks user tables for a RFID match")]
        public string AuthenticateByRFID(string RFID, string ApplicationName)
        {

            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT UserID FROM AuthenticationView WHERE RFID=@RFID AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@RFID", RFID);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            return Reader.GetGuid(0).ToString();
                        }
                        else
                        {
                            return "-1";
                        }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:AuthenticateRFID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 1);
                    return "-2";
                }
            }

        }

        //Generates a 40 character hash for password encryption
        private string GetHash(string Input)
        {
            SHA1 SHAEncryptor = new SHA1CryptoServiceProvider();
            byte[] bHashedInput = SHAEncryptor.ComputeHash(System.Text.Encoding.Default.GetBytes(Input.ToCharArray()));
            return BitConverter.ToString(bHashedInput).Replace("-", string.Empty);
        }

        [WebMethod(Description = "Retrieves a userprofile object from a UserID")]
        public UserProfile GetProfileByID(Guid UserID, string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT FirstName, LastName, Extension, Email, HomeLab, LabID, LabName, RoleID FROM UserProfileView WHERE UserProfileView.UserID=@UserID AND UserProfileView.ApplicationName=@ApplicationName;");
                Command.Parameters.AddWithValue("@UserID", UserID);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);

                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {

                        if (Reader.HasRows)
                        {
                            Reader.Read();

                            UserProfile Profile = new UserProfile();

                            Profile.UserID = UserID;
                            Profile.FirstName = Reader.GetString(0);
                            Profile.LastName = Reader.GetString(1);
                            Profile.Extension = Reader.GetString(2);
                            Profile.Email = Reader.GetString(3);
                            Profile.LabID = Reader.GetByte(4).ToString();

                            do
                            {
                                UserProfile.Credential C = new UserProfile.Credential();
                                C.LabID = Reader.GetByte(5).ToString();
                                C.LabName = Reader.GetString(6);
                                C.RoleID = Reader.GetInt32(7).ToString();
                                Profile.Credentials.Add(C);
                            }
                            while (Reader.Read());

                            return Profile;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetProfileByID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 2);
                    return null;
                }
            }

        }

        [WebMethod(Description = "Retrieves a list of Email addresses for administrators associated with an application")]
        public List<string> GetAdminEmailAddresses(string ApplicationName)
        {
            List<string> AddressCollection = new List<string>();
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT Email FROM UserProfileView WHERE RoleName='Administrator' AND ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                AddressCollection.Add(Reader["Email"].ToString());
                            }
                        }
                    }
                    return AddressCollection;
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetAdminEmailAddresses; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 3);
                    return null;
                }
            }
        }


        [WebMethod(Description = "Retrieves a list of Email addresses for Users associated with an application")]
        public List<string> GetUserEmailAddresses(string ApplicationName)
        {
            List<string> AddressCollection = new List<string>();
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT Email FROM UserProfileView WHERE RoleName<>'Administrator' AND ApplicationName=@ApplicationName");
                Command.Connection = Connection;

                try
                {
                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                AddressCollection.Add(Reader["Email"].ToString());
                            }
                        }
                    }
                    return AddressCollection;
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetUserEmailAddresses; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 4);
                    return null;
                }
            }
        }


        [WebMethod(Description = "Return a list of cost centres associated with a Lab")]
        public List<string> GetCostCentresByLabID(int LabID)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT CostCentreID FROM cost_centres WHERE LabID=@LabID");
                Command.Parameters.AddWithValue("@LabID", LabID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            List<string> CostCentres = new List<string>();
                            while (Reader.Read())
                            {
                                CostCentres.Add(Reader["CostCentreID"].ToString());
                            }
                            return CostCentres;
                        }
                        else { return null; }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:GetCostCentresByLabID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 5);
                    return null;
                }
            }

        }

        [WebMethod(Description = "Create a new user account and/or subscribe user to the calling application")]
        public string CreateUserAccount(string FirstName, string LastName, string LabID, string Extension, string Email, string RoleID, string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                string Password = GetHash(PasswordGenerator());

                SqlCommand Command = new SqlCommand("EXEC p_AddUser @FirstName,@LastName,@Password,@Email,@LabID,@Extension,@ApplicationName,@RoleID;");
                Command.Parameters.AddWithValue("@FirstName", FirstName);
                Command.Parameters.AddWithValue("@LastName", LastName);
                Command.Parameters.AddWithValue("@Password", Password);
                Command.Parameters.AddWithValue("@Email", Email);
                Command.Parameters.AddWithValue("@LabID", LabID);
                Command.Parameters.AddWithValue("@Extension", Extension);
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Parameters.AddWithValue("@RoleID", RoleID);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                   
                    try
                    {
                        System.Net.Mail.SmtpClient SMTPsender = new System.Net.Mail.SmtpClient();
                        SMTPsender.Host = GetSetting("SMTPServer");
                        SMTPsender.Port = 25;
                        System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
                        string FirstAdmin = GetAdminEmailAddresses(ApplicationName)[0];
                        Message.From = new System.Net.Mail.MailAddress(String.IsNullOrEmpty(FirstAdmin) ? "UserDatabase@petermac.org" : FirstAdmin);
                        Message.Subject = "New Account";
                        Message.IsBodyHtml = true;

                        System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                        MessageBody.AppendFormat("Hi {0} <br/><br/> ", FirstName);
                        MessageBody.AppendFormat("A new account has been created for you for the {0} <br/><br/>", GetApplicationDescriptiveName(ApplicationName));
                        MessageBody.Append("You can login using the following Username/Password <br/>");
                        MessageBody.AppendFormat("<table><tr><th>User Name:</th><td>{0} {1}</td></tr><tr><th>Password:</th><td>{2}</td></tr></table>", FirstName, LastName, Password);
                        MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");


                        Message.Body = MessageBody.ToString();
                        Message.To.Add(Email);
                        SMTPsender.Send(Message);
                    }
                    catch
                    {
                        return "MailError-" + Password;
                    }
                    
                    return "Success";
                }
                catch (SqlException Err)
                {
                    if (Err.Message == "AccountExists")
                    {
                        return "A User Account With The Supplied Details Already Exists For This Application";
                    }
                    else if (Err.Message == "InvalidApplicationName")
                    {
                        return "The supplied application name was not a recognised application";
                    }
                    else
                    {
                        throw Err;
                    }
                }
                catch
                { 
                    return "An error occurred while creating user account";
                }
            }
        }
        
        /*
        public string CreateUserAccount(string FirstName, string LastName, string LabID, string Extension, string Email, string RoleID, string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlConnection TransactionConnection = new SqlConnection(USERS_CON_STRING);
                SqlTransaction Transaction;

                //Get Application Details
                SqlCommand AppCommand = new SqlCommand("SELECT ApplicationID, DescriptiveName FROM applications WHERE ApplicationName=@ApplicationName");
                AppCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                AppCommand.Connection = Connection;

                int ApplicationID = -1;
                string DescriptiveName = "";
                String Password = "Existing Custom Password";

                try
                {

                    Connection.Open();
                    using (SqlDataReader Reader = AppCommand.ExecuteReader())
                    {

                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            ApplicationID = int.Parse(Reader[0].ToString());
                            DescriptiveName = Reader.GetString(1);
                        }
                        else
                        {
                            throw new Exception("Invalid Application Name");
                        }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:CreateUserAccount; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 6);
                    return "Error";
                }


                try
                {

                    //Check if the User already exists
                    SqlCommand UserExistsCommand = new SqlCommand("SELECT UserID FROM users WHERE Email=@Email");
                    UserExistsCommand.Parameters.AddWithValue("@Email", Email);
                    UserExistsCommand.Connection = Connection;

                    //Prepare SQL for User/Application Relation
                    SqlCommand InsertUserAppRelCommand = new SqlCommand("INSERT INTO user_app_lab_role_rel (UserID,ApplicationID,LabID,RoleID) VALUES (@UserID,@ApplicationID,@LabID,@RoleID)");
                    InsertUserAppRelCommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);
                    InsertUserAppRelCommand.Parameters.AddWithValue("@RoleID", RoleID);
                    InsertUserAppRelCommand.Parameters.AddWithValue("@LabID", LabID);
                    InsertUserAppRelCommand.Connection = Connection;


                    SqlDataReader Reader = UserExistsCommand.ExecuteReader();

                    if (Reader.HasRows)//if User exists
                    {
                        Reader.Read();
                        Guid UserID = Reader.GetGuid(0);
                        Reader.Close(); //Opened by UserExistsCommand

                        SqlCommand UserAppRelExistsCommand = new SqlCommand("SELECT * FROM user_app_lab_role_rel WHERE ApplicationID=@ApplicationID AND UserID=@UserID");
                        UserAppRelExistsCommand.Parameters.AddWithValue("@ApplicationID", ApplicationID);
                        UserAppRelExistsCommand.Parameters.AddWithValue("@UserID", UserID);
                        UserAppRelExistsCommand.Connection = Connection;

                        Reader = UserAppRelExistsCommand.ExecuteReader();

                        if (!Reader.HasRows)
                        {
                            Reader.Close();//Opened by UserAppRelExistsCommand

                            InsertUserAppRelCommand.Parameters.AddWithValue("@UserID", UserID);


                            if (InsertUserAppRelCommand.ExecuteNonQuery() != 1)
                            {
                                return "Application/User Relationship Insertion Fail";
                            }
                        }
                        else
                        {
                            return "User Account Exists";
                        }


                    }
                    else //if user doesn't exist
                    {
                        Reader.Close(); //Opened by UserExistsCommand
                        Password = PasswordGenerator();

                        try
                        {
                            TransactionConnection.Open();
                            Transaction = TransactionConnection.BeginTransaction();

                            SqlCommand InsertUserCommand = new SqlCommand("INSERT INTO users (FirstName,LastName,LabID,Extension,Email,Password) VALUES (@FirstName,@LastName,@LabID,@Extension,@Email,@Password)");
                            InsertUserCommand.Parameters.AddWithValue("@FirstName", FirstName);
                            InsertUserCommand.Parameters.AddWithValue("@LastName", LastName);
                            InsertUserCommand.Parameters.AddWithValue("@LabID", LabID);
                            InsertUserCommand.Parameters.AddWithValue("@Extension", Extension);
                            InsertUserCommand.Parameters.AddWithValue("@Email", Email);
                            InsertUserCommand.Parameters.AddWithValue("@Password", GetHash(Password));
                            InsertUserCommand.Connection = TransactionConnection;
                            InsertUserCommand.Transaction = Transaction;


                            try
                            {
                                if (InsertUserCommand.ExecuteNonQuery() == 1)
                                {

                                    UserExistsCommand.Connection = TransactionConnection;
                                    UserExistsCommand.Transaction = Transaction;

                                    Reader = UserExistsCommand.ExecuteReader();
                                    Reader.Read();
                                    Guid UserID = Reader.GetGuid(0);
                                    Reader.Close();//Opened by UserExistsCommand

                                    InsertUserAppRelCommand.Parameters.AddWithValue("@UserID", UserID);
                                    InsertUserAppRelCommand.Connection = TransactionConnection;
                                    InsertUserAppRelCommand.Transaction = Transaction;

                                    if (InsertUserAppRelCommand.ExecuteNonQuery() == 1)
                                    {
                                        Transaction.Commit();
                                    }
                                    else
                                    {
                                        Transaction.Rollback();
                                        return "Application/User Relationship Insertion Fail";
                                    }

                                }
                                else
                                {
                                    Connection.Close();
                                    return "User Insertion Fail";
                                }
                            }
                            catch (Exception Err)
                            {
                                Transaction.Rollback();
                                System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:CreateUserAccount; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 6);
                                return "Error";
                            }
                        }
                        catch (Exception Err)
                        {
                            System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:CreateUserAccount; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 6);
                            return "Error";
                        }
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:CreateUserAccount; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 6);
                    return "Error";
                }


                try
                {
                    System.Net.Mail.SmtpClient SMTPsender = new System.Net.Mail.SmtpClient();
                    //SMTPsender.Host = Settings.GetSetting("SMTPServer");
			SMTPsender.Host = "mail.tpg.com.au";
                    SMTPsender.Port = 25;
                    System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
                    string FirstAdmin = GetAdminEmailAddresses(ApplicationName)[0];
                    Message.From = new System.Net.Mail.MailAddress(String.IsNullOrEmpty(FirstAdmin) ? "UserDatabase@petermac.org" : FirstAdmin);
                    Message.Subject = "New Account";
                    Message.IsBodyHtml = true;

                    System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                    MessageBody.AppendFormat("Hi {0} <br/><br/> ", FirstName);
                    MessageBody.AppendFormat("A new account has been created for you for the {0} <br/><br/>", DescriptiveName);
                    MessageBody.Append("You can login using the following Username/Password <br/>");
                    MessageBody.AppendFormat("<table><tr><th>User Name:</th><td>{0} {1}</td></tr><tr><th>Password:</th><td>{2}</td></tr></table>", FirstName, LastName, Password);
                    MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");


                    Message.Body = MessageBody.ToString();
                    Message.To.Add(Email);
                    SMTPsender.Send(Message);
                }
                catch
                {
                    return "MailError-" + Password;
                }

                return "Success";
            }
        }
        */

        private string PasswordGenerator()
        {
            string Password = "";
            Random Generator = new Random();
            for (int i = 0; i < 5; i++)
            {
                Password += Generator.Next(0, 9).ToString() + ((char)(Generator.Next(65, 90))).ToString();
            }

            return Password;
        }

        public class UserProfile
        {
            public class Credential
            {
                public string LabName { get; set; }
                public string LabID { get; set; }
                public string RoleID { get; set; }

                public Credential()
                {
                    LabName = String.Empty;
                    LabID = String.Empty;
                    RoleID = String.Empty;
                }
            }

            public UserProfile()
            {
                Credentials = new List<Credential>();
            }

            private Guid _userid;
            private List<Credential> _credentials;
            private string _firstname;
            private string _lastname;
            private string _extension;
            private string _email;
            private string _labid;

            public Guid UserID { get { return _userid; } set { _userid = value; } }
            public List<Credential> Credentials { get { return _credentials; } set { _credentials = value; } }
            public string FirstName { get { return _firstname; } set { _firstname = value; } }
            public string LastName { get { return _lastname; } set { _lastname = value; } }
            public string Extension { get { return _extension; } set { _extension = value; } }
            public string Email { get { return _email; } set { _email = value; } }
            public string LabID { get { return _labid; } set { _labid = value; } }
        }

        private string GetApplicationDescriptiveName(string ApplicationName)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("SELECT DescriptiveName FROM Applications WHERE ApplicationName=@ApplicationName");
                Command.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    return (string)Command.ExecuteScalar();
                }
                catch
                {
                    return ApplicationName;
                }
            }
        }

        [WebMethod(Description = "Revoke a users access to an application using their UserID")]
        public int DeleteUserByID(int UserID, string ApplicationName, string LabID)
        {
            using (SqlConnection DeleteConnection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand DeleteCommand = new SqlCommand("DELETE FROM user_app_lab_role_rel WHERE UserID=@UserID AND LabID=@LabID AND ApplicationID=(SELECT ApplicationID FROM applications WHERE ApplicationName=@ApplicationName)");
                DeleteCommand.Parameters.AddWithValue("@UserID", UserID);
                DeleteCommand.Parameters.AddWithValue("@LabID", LabID);
                DeleteCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                DeleteCommand.Connection = DeleteConnection;
                try
                {
                    DeleteConnection.Open();
                    if (DeleteCommand.ExecuteNonQuery() == 1)
                    {  
                        SqlCommand AppCheckCommand = new SqlCommand("SELECT COUNT(user_app_lab_role_rel.UserID) as SubscriptionCount FROM user_app_lab_role_rel WHERE user_app_lab_role_rel.UserID=@UserID");
                        AppCheckCommand.Parameters.AddWithValue("@UserID", UserID);
                        AppCheckCommand.Connection = DeleteConnection;
                        
                        SqlDataReader Reader = AppCheckCommand.ExecuteReader();

                        if (Reader.HasRows)
                        {
                            Reader.Read();
                            int SubscriptionCount = Reader.GetInt32(0);

                            if (SubscriptionCount == 0)
                            {
                                DeleteConnection.Close();
                                SqlCommand UserDeleteCommand = new SqlCommand("DELETE FROM users WHERE UserID=@UserID");
                                UserDeleteCommand.Parameters.AddWithValue("@UserID", UserID);
                                UserDeleteCommand.Connection = DeleteConnection;
                                DeleteConnection.Open();
                                return UserDeleteCommand.ExecuteNonQuery();

                            }
                            else
                            {
                                return 1;
                            }
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (Exception Err)
                {
                    System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:DeleteUserByID; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 7);
                    return -1;
                }
            }
        }

        [WebMethod(Description = "Revoke a users access to an application using their email address")]
        public int DeleteUserByEmail(string Email, string ApplicationName)
        {
            SqlConnection DeleteConnection = new SqlConnection(USERS_CON_STRING);
            SqlCommand DeleteCommand = new SqlCommand("DELETE FROM user_app_lab_role_rel WHERE UserID=(SELECT UserID FROM users WHERE Email=@Email) AND ApplicationID=(SELECT ApplicationID FROM applications WHERE ApplicationName=@ApplicationName)");
            DeleteCommand.Parameters.AddWithValue("@Email", Email);
            DeleteCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
            DeleteCommand.Connection = DeleteConnection;

            try
            {
                DeleteConnection.Open();
                if (DeleteCommand.ExecuteNonQuery() == 1)
                {
                    DeleteConnection.Close();

                    SqlCommand AppCheckCommand = new SqlCommand("SELECT users.UserID AS UID, COUNT(user_app_lab_role_rel.UserID) as SubscriptionCount FROM users LEFT JOIN user_app_lab_role_rel ON users.UserID=user_app_lab_role_rel.UserID WHERE Email=@Email GROUP BY users.UserID");
                    AppCheckCommand.Parameters.AddWithValue("@Email", Email);
                    AppCheckCommand.Connection = DeleteConnection;

                    DeleteConnection.Open();
                    SqlDataReader Reader = AppCheckCommand.ExecuteReader();

                    if (Reader.HasRows)
                    {
                        Reader.Read();
                        int UserID = int.Parse(Reader["UID"].ToString());
                        int SubscriptionCount = int.Parse(Reader["SubscriptionCount"].ToString());

                        if (SubscriptionCount == 0)
                        {
                            DeleteConnection.Close();
                            SqlCommand UserDeleteCommand = new SqlCommand("DELETE FROM users WHERE UserID=@UserID");
                            UserDeleteCommand.Parameters.AddWithValue("@UserID", UserID);
                            UserDeleteCommand.Connection = DeleteConnection;
                            DeleteConnection.Open();
                            return UserDeleteCommand.ExecuteNonQuery();

                        }
                        else
                        {
                            DeleteConnection.Close();
                            return 1;
                        }
                    }
                    else
                    {
                        DeleteConnection.Close();
                        return 0;
                    }
                }
                else
                {
                    DeleteConnection.Close();
                    return 0;
                }
            }
            catch (Exception Err)
            {
                System.Diagnostics.EventLog.WriteEntry("AuthorisationWebService", "Method:DeleteUserByEmail; Source:" + Err.Source + "; Error:" + Err.Message, System.Diagnostics.EventLogEntryType.Error, 8);
                return -1;
            }

        }

        [WebMethod(Description = "Update a Users Details")]
        public bool UpdateUserInfo(UserProfile UserInfo, string ApplicationName)
        {
            SqlConnection UpdateConnection = new SqlConnection(USERS_CON_STRING);
            SqlTransaction Transaction;
            SqlCommand UserUpdateCommand = new SqlCommand("UPDATE users SET FirstName=@FirstName, LastName=@LastName,Email=@Email,Extension=@Extension, LabID=@LabID WHERE UserID=@UserID");
            UserUpdateCommand.Parameters.AddWithValue("@FirstName", UserInfo.FirstName);
            UserUpdateCommand.Parameters.AddWithValue("@LastName", UserInfo.LastName);
            UserUpdateCommand.Parameters.AddWithValue("@Email", UserInfo.Email);
            UserUpdateCommand.Parameters.AddWithValue("@Extension", UserInfo.Extension);
            UserUpdateCommand.Parameters.AddWithValue("@LabID", UserInfo.LabID);
            UserUpdateCommand.Parameters.AddWithValue("@UserID", UserInfo.UserID);
            UserUpdateCommand.Connection = UpdateConnection;

            SqlCommand AppRelUpdateCommand = new SqlCommand("UPDATE user_app_lab_role_rel SET RoleID=@RoleID WHERE UserID=@UserID AND LabID=@LabID AND ApplicationID=(SELECT ApplicationID FROM applications WHERE ApplicationName=@ApplicationName)");
            AppRelUpdateCommand.Connection = UpdateConnection;
            
            try
            {
                UpdateConnection.Open();
                Transaction = UpdateConnection.BeginTransaction();

                try
                {
                    UserUpdateCommand.Transaction = Transaction;
                    AppRelUpdateCommand.Transaction = Transaction;

                    if (UserUpdateCommand.ExecuteNonQuery() == 1)
                    {
                        foreach (UserProfile.Credential C in UserInfo.Credentials)
                        {
                            AppRelUpdateCommand.Parameters.Clear();
                            AppRelUpdateCommand.Parameters.AddWithValue("@RoleID", C.RoleID);
                            AppRelUpdateCommand.Parameters.AddWithValue("@LabID", C.LabID);
                            AppRelUpdateCommand.Parameters.AddWithValue("@UserID", UserInfo.UserID);
                            AppRelUpdateCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);

                            AppRelUpdateCommand.ExecuteNonQuery();
                        }
                        
                        Transaction.Commit();
                        return true;
                    }
                    else
                    {
                        Transaction.Rollback();
                        return false;
                    }
                }
                catch
                {
                    Transaction.Rollback();
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        [WebMethod(Description = "Change a users password")]
        public bool ChangePassword(int UserID, string NewPassword)
        {
            SqlConnection UpdateConnection = new SqlConnection(USERS_CON_STRING);
            SqlCommand UpdateCommand = new SqlCommand("UPDATE users SET Password=@Password WHERE UserID=@UserID");
            UpdateCommand.Parameters.AddWithValue("@Password", GetHash(NewPassword));
            UpdateCommand.Parameters.AddWithValue("@UserID", UserID);

            UpdateCommand.Connection = UpdateConnection;
            UpdateConnection.Open();

            if (UpdateCommand.ExecuteNonQuery() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        [WebMethod(Description = "Reset a users password to random and email new password")]
        public bool ResetPassword(string Email)
        {
            string Password = PasswordGenerator();
            SqlConnection UpdateConnection = new SqlConnection(USERS_CON_STRING);
            SqlCommand UpdateCommand = new SqlCommand("UPDATE users SET Password=@Password WHERE Email=@Email");
            UpdateCommand.Parameters.AddWithValue("@Password", GetHash(Password));
            UpdateCommand.Parameters.AddWithValue("@Email", Email);

            try
            {

                UpdateCommand.Connection = UpdateConnection;
                UpdateConnection.Open();

                if (UpdateCommand.ExecuteNonQuery() == 1)
                {
                    System.Net.Mail.SmtpClient SMTPsender = new System.Net.Mail.SmtpClient();
                    //SMTPsender.Host = Settings.GetSetting("SMTPServer");
			SMTPsender.Host = "mail.tpg.com.au";
                    SMTPsender.Port = 25;
                    System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
                    //Message.From = new System.Net.Mail.MailAddress(Settings.GetSetting("SenderAddress"));
			Message.From = new System.Net.Mail.MailAddress("AidanFlynn@tpg.com.au");
                    Message.Subject = "Password Reset";
                    Message.IsBodyHtml = true;

                    System.Text.StringBuilder MessageBody = new System.Text.StringBuilder();
                    MessageBody.Append("Hi, <br/><br/> ");
                    MessageBody.AppendFormat("Your Password has been reset to <b>{0}</b> <br/><br/>", Password);
                    MessageBody.Append("<br/><br/>Thanks<br/> The User Management System<br/>**This is an automated email**");


                    Message.Body = MessageBody.ToString();
                    Message.To.Add(Email);
                    SMTPsender.Send(Message);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        [WebMethod(Description = "Retrieves a list of all the UserIDs and UserNames associated with an application")]
        public List<string[]> GetUsersByApplicationName(string ApplicationName)
        {
            List<string[]> Users = new List<string[]>();

            SqlConnection UserConnection = new SqlConnection(USERS_CON_STRING);
            SqlCommand UserCommand = new SqlCommand("SELECT users.UserID, (FirstName+' '+LastName) AS Name FROM users INNER JOIN user_app_lab_role_rel ON users.UserID=user_app_lab_role_rel.UserID INNER JOIN applications ON user_app_lab_role_rel.ApplicationID=applications.ApplicationID WHERE ApplicationName=@ApplicationName");
            UserCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();

                SqlDataReader Reader = UserCommand.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        string[] User = { Reader.GetGuid(0).ToString(), Reader.GetString(1) };
                        Users.Add(User);
                    }
                }
                return Users;
            }
            catch
            {
                return null;
            }


        }

        [WebMethod(Description = "Retrieves a list of all the UserIDs and UserNames with provided Last Name")]
        public List<string[]> GetUsersByLastName(string LastName, string ApplicationName)
        {
            List<string[]> Users = new List<string[]>();

            SqlConnection UserConnection = new SqlConnection(USERS_CON_STRING);
            SqlCommand UserCommand = new SqlCommand("SELECT UserID, (FirstName+' '+LastName) AS Name FROM UserProfileView WHERE ApplicationName=@ApplicationName AND LastName LIKE @LastName");
            UserCommand.Parameters.AddWithValue("@LastName", LastName);
            UserCommand.Parameters.AddWithValue("@ApplicationName", ApplicationName);
            UserCommand.Connection = UserConnection;

            try
            {
                UserConnection.Open();

                SqlDataReader Reader = UserCommand.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        string[] User = { Reader.GetGuid(0).ToString(), Reader.GetString(1) };
                        Users.Add(User);
                    }
                }
                return Users;
            }
            catch (Exception Err)
            {
                return new List<string[]> {new string [] {Err.Message,"0"}};
            }


        }

        [WebMethod(Description = "Retrieves a list of LabIDs and Lab Names")]
        public List<string[]> GetLabs()
        {
            List<string[]> Labs = new List<string[]>();

            SqlConnection Connection = new SqlConnection(USERS_CON_STRING);
            SqlCommand Command = new SqlCommand("SELECT LabID, LabName FROM labs");
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        string[] Lab = { Reader.GetByte(0).ToString(), Reader.GetString(1) };
                        Labs.Add(Lab);
                    }
                }
                return Labs;
            }
            catch
            {
                return null;
            }



        }

        [WebMethod(Description = "Retrieves a users password in hashed form.")]
        public string GetHashedPassword(string UserID)
        {
            SqlConnection Connection = new SqlConnection(USERS_CON_STRING);
            SqlCommand Command = new SqlCommand("SELECT Password FROM users WHERE UserID=@UserID");
            Command.Parameters.AddWithValue("@UserID", UserID);
            Command.Connection = Connection;

            try
            {
                Connection.Open();

                SqlDataReader Reader = Command.ExecuteReader();

                if (Reader.HasRows)
                {
                    Reader.Read();

                    return Reader.GetString(0);
                }
                else
                {
                    return "Invalid UserID";
                }
            }
            catch
            {
                return "Error";
            }
        }

        private string GetSetting(string SettingName)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("EXEC p_GetSetting @Name");
                Command.Parameters.AddWithValue("@Name", SettingName);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    return (string)Command.ExecuteScalar();
                }
                catch (SqlException Err)
                {
                    if (Err.Message == "InvalidSettingName")
                    {
                        throw new Exception("Invalid Setting Name Supplied");
                    }
                    else
                    {
                        throw Err;
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("Error Getting Setting Value",Err);
                }
            }
        }

        private void SetSetting(string SettingName, string Value)
        {
            using (SqlConnection Connection = new SqlConnection(USERS_CON_STRING))
            {
                SqlCommand Command = new SqlCommand("EXEC p_SetSetting @Name @Value");
                Command.Parameters.AddWithValue("@Name", SettingName);
                Command.Parameters.AddWithValue("@Value", Value);
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    Command.ExecuteNonQuery();
                }
                catch (Exception Err)
                {
                    throw new Exception("Error Setting Value", Err);
                }
            }
        }
    }

