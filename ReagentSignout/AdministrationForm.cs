﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace ReagentSignout
{
    public partial class AdministrationForm : Form
    {
        DgvFilterPopup.DgvFilterManager ReagentTypesFilterManager = new DgvFilterPopup.DgvFilterManager();
        DgvFilterPopup.DgvFilterManager OrdersFilterManager = new DgvFilterPopup.DgvFilterManager();
        DgvFilterPopup.DgvFilterManager InventoryFilterManager = new DgvFilterPopup.DgvFilterManager();
        DgvFilterPopup.DgvFilterManager HistoryFilterManager = new DgvFilterPopup.DgvFilterManager();
        SqlDataAdapter ReagentTypesAdapter;
        SqlDataAdapter OrdersAdapter;
        SqlDataAdapter InventoryAdapter;
        SqlDataAdapter HistoryAdapter;
        SqlDataAdapter SignOutListAdapter;
        DataTable ReagentTypesTable = new DataTable();
        DataTable OrdersTable = new DataTable();
        DataTable InventoryTable = new DataTable();
        DataTable HistoryTable = new DataTable();
        DataTable SignOutListTable = new DataTable();
        Dictionary<string, int> CategoryList = new Dictionary<string, int>();
        Dictionary<string, int> LabsList = new Dictionary<string, int>();
        Dictionary<string, Guid> UsersList = new Dictionary<string, Guid>();
        Timer InventoryFlashUpdateTimer = new Timer();
        Timer OrderFlashUpdateTimer = new Timer();
        Timer ReagentTypesFlashUpdateTimer = new Timer();
        Timer SignOutListFlashUpdateTimer = new Timer();
        bool SuppressCellChangedEvent = false;
        public static Regex IsGuidRegex = new Regex("[AaBbCcDdEeFf0-9]{8}-[AaBbCcDdEeFf0-9]{4}-[AaBbCcDdEeFf0-9]{4}-[AaBbCcDdEeFf0-9]{4}-[AaBbCcDdEeFf0-9]{12}");
        public Users.ClientApplication App;

        public AdministrationForm()
        {
            this.VisibleChanged += new EventHandler(AdministrationForm_VisibleChanged);
            InitializeComponent();
            //try
            //{
                ReagentTypesFilterManager.ColumnFilterAdding += new DgvFilterPopup.ColumnFilterEventHandler(FilterManager_ColumnFilterAdding);
                ReagentTypesFilterManager.PopupShowing += new DgvFilterPopup.ColumnFilterEventHandler(FilterManager_PopupShowing);

                OrdersFilterManager.ColumnFilterAdding += new DgvFilterPopup.ColumnFilterEventHandler(FilterManager_ColumnFilterAdding);
                InventoryFilterManager.ColumnFilterAdding += new DgvFilterPopup.ColumnFilterEventHandler(FilterManager_ColumnFilterAdding);
                HistoryFilterManager.ColumnFilterAdding += new DgvFilterPopup.ColumnFilterEventHandler(FilterManager_ColumnFilterAdding);

                ReagentTypesFilterManager.DataGridView = ReagentTypesDataGridView;
                OrdersFilterManager.DataGridView = OrdersDataGridView;
                InventoryFilterManager.DataGridView = InventoryDataGridView;
                HistoryFilterManager.DataGridView = HistoryDataGridView;

                ReagentTypesDataGridView.DefaultValuesNeeded += new DataGridViewRowEventHandler(ReagentTypesDataGridView_DefaultValuesNeeded);
                AdministrationTabs.SelectedIndexChanged += new EventHandler(AdministrationTabs_SelectedIndexChanged);

                EditSetComboBox.SelectedIndexChanged += new EventHandler(EditSetComboBox_SelectedIndexChanged);

            //User Management Form Events
            //Tabs
            ManageUsersTab.Enter += new EventHandler(tab_Users_Enter);
            tab_Users.Enter += new EventHandler(tab_Users_Enter);
            tab_CostCentres.Enter += new EventHandler(tab_CostCentres_Enter);
            tab_LabsInstitutes.Enter += new EventHandler(Tab_LabsInstitutes_Enter);
            

            //Combo boxes
            cmb_UserSelect.SelectedIndexChanged += new EventHandler(Cmb_UserSelect_SelectedIndexChanged);
            cmb_UserInstitute.SelectedIndexChanged += new EventHandler(Cmb_UserInstitute_SelectedIndexChanged);
            cmb_CreateCredentialInstitute.SelectedIndexChanged += new EventHandler(Cmb_CreateCredentialInstitute_SelectedIndexChanged);
            
                PrepareReagentTypes();
                PrepareOrders();
                PrepareInventory();
                PrepareHistory();
                PrepareCategories();
                PrepareReagentSets();
                PrepareCostCentres();
                SetDefaultVisibleColumns("ReagentTypes");

                ReagentTypesAdapter.RowUpdating += new SqlRowUpdatingEventHandler(ReagentTypesAdapter_RowUpdating);

                ReagentTypesDataGridView.CellClick += new DataGridViewCellEventHandler(ReagentTypesDataGridView_CellClick);
                OrdersDataGridView.CellClick += new DataGridViewCellEventHandler(OrdersDataGridView_CellClick);
                InventoryDataGridView.CellClick += new DataGridViewCellEventHandler(InventoryDataGridView_CellClick);
                NewCategoryParentComboBox.KeyPress += new KeyPressEventHandler(NewCategoryParentComboBox_KeyPress);

                ReagentTypesDataGridView.CellValueChanged += new DataGridViewCellEventHandler(ReagentTypesDataGridView_CellValueChanged);
                ReagentTypesDataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DataGridView_RowsRemoved);
                ReagentTypesDataGridView.RowsAdded += new DataGridViewRowsAddedEventHandler(DataGridView_RowsAdded);
                InventoryDataGridView.CellValueChanged += new DataGridViewCellEventHandler(InventoryDataGridView_CellValueChanged);
                InventoryDataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DataGridView_RowsRemoved);
                InventoryDataGridView.RowsAdded += new DataGridViewRowsAddedEventHandler(DataGridView_RowsAdded);
                OrdersDataGridView.CellValueChanged += new DataGridViewCellEventHandler(OrdersDataGridView_CellValueChanged);
                OrdersDataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DataGridView_RowsRemoved);
                OrdersDataGridView.RowsAdded += new DataGridViewRowsAddedEventHandler(DataGridView_RowsAdded);
                SignOutListDataGridView.CellValueChanged += new DataGridViewCellEventHandler(SignOutListDataGridView_CellValueChanged);
                SignOutListDataGridView.RowsRemoved += new DataGridViewRowsRemovedEventHandler(SignOutListDataGridView_RowsRemoved);
                ReagentSummaryDataGridView.CellClick += new DataGridViewCellEventHandler(ReagentSummaryDataGridView_CellClick);

                
                CostCentreSummaryDataGridView.CellClick += new DataGridViewCellEventHandler(CostCentreSummaryDataGridView_CellClick);
                CostCentreDetailsDataGridView.CellClick += new DataGridViewCellEventHandler(CostCentreDetailsDataGridView_CellClick);

                InventoryFlashUpdateTimer.Interval = 1500;
                InventoryFlashUpdateTimer.Tick += new EventHandler(InventoryFlashUpdateTimer_Tick);
                OrderFlashUpdateTimer.Interval = 1500;
                OrderFlashUpdateTimer.Tick += new EventHandler(OrderFlashUpdateTimer_Tick);
                ReagentTypesFlashUpdateTimer.Interval = 1500;
                ReagentTypesFlashUpdateTimer.Tick += new EventHandler(ReagentTypesFlashUpdateTimer_Tick);
                SignOutListFlashUpdateTimer.Interval = 1500;
                SignOutListFlashUpdateTimer.Tick += new EventHandler(SignOutListFlashUpdateTimer_Tick);

                //Supress Row Deletes
                OrdersAdapter.RowUpdating += new SqlRowUpdatingEventHandler(SuppressRowDelete);
                InventoryAdapter.RowUpdating += new SqlRowUpdatingEventHandler(SuppressRowDelete);
                HistoryAdapter.RowUpdating += new SqlRowUpdatingEventHandler(SuppressRowDelete);
           /* }
            catch (Exception Err)
            {
                string ErrorMessage = "An Error Occurred While Initialising Administration Interface. Error Stack:";
                while (Err != null)
                {
                    ErrorMessage += Err.Message + ";";
                    Err = Err.InnerException;
                }
                MessageBox.Show(ErrorMessage, "Initialisation Error", MessageBoxButtons.OK);
                
            }*/
            


        }

        private void Tab_LabsInstitutes_Enter(object sender, EventArgs e)
        {
            cmb_LabSelect.DataSource = PrepareLabs(true, null);
            cmb_LabSelect.DisplayMember = "Name";
        }

        private void Cmb_CreateCredentialInstitute_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmb_CreateCredentialLab.DataSource = PrepareLabs(false, (Users.ResearchInstitute)cmb_CreateCredentialInstitute.SelectedItem);
            cmb_CreateCredentialLab.DisplayMember = "Name";
        }

        private void Cmb_UserInstitute_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmb_UserLab.DataSource = PrepareLabs(false, (Users.ResearchInstitute)cmb_UserInstitute.SelectedItem);
            cmb_UserLab.DisplayMember = "Name";
        }

        

        private void PopulateUsers()
        {
            Users.UserProfile NewUserDummy = new Users.UserProfile();
            NewUserDummy.FullName = "<New>";
            List<Users.UserProfile> UserList = new List<Users.UserProfile>();
            UserList.Add(NewUserDummy);
            UserList.AddRange(Users.UserProfile.GetAllUsers());

            cmb_UserSelect.DataSource = UserList;
            cmb_UserSelect.DisplayMember = "FullName";

        }

        private List<Users.Laboratory> PrepareLabs(bool AllowNew, Users.ResearchInstitute Institute)
        {
            List<Users.Laboratory> Labs = new List<Users.Laboratory>();

            if (AllowNew)
            {
                Users.Laboratory NewLabDummy = new Users.Laboratory();
                NewLabDummy.Name = "<New>";
                Labs.Add(NewLabDummy);

            }

            if (Institute == null)
            {
                Labs.AddRange(Users.Laboratory.GetLabs());                
            }
            else
            { 
                Labs.AddRange(Users.Laboratory.GetLabs(Institute.InstituteID));
            }
            return Labs;
        }

        private void PopulateInstitutes()
        {
            
            List<Users.ResearchInstitute> Institutes = Users.ResearchInstitute.GetInstitutes();

            List<Users.ResearchInstitute> InstitutesWithNew = new List<Users.ResearchInstitute>();
            Users.ResearchInstitute NewInstituteDummy = new Users.ResearchInstitute();
            NewInstituteDummy.Name = "<New>";
            InstitutesWithNew.Add(NewInstituteDummy);
            InstitutesWithNew.AddRange(Institutes);
             
            cmb_UserInstitute.DataSource = Institutes;
            cmb_UserInstitute.DisplayMember = "Name";

            cmb_CreateCredentialInstitute.BindingContext = new BindingContext();
            cmb_CreateCredentialInstitute.DataSource = Institutes;
            cmb_CreateCredentialInstitute.DisplayMember = "Name";

            cmb_LabInstitute.BindingContext = new BindingContext();
            cmb_LabInstitute.DataSource = Institutes;
            cmb_LabInstitute.DisplayMember = "Name";

            cmb_InstituteSelect.BindingContext = new BindingContext();
            cmb_InstituteSelect.DataSource = InstitutesWithNew;
            cmb_InstituteSelect.DisplayMember = "Name";

        }


        private void Cmb_UserSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;
            pnl_UserCredentialsExisting.Controls.Clear();

            if (User.FullName == "<New>")
            {
                btn_UserCreate.Enabled = true;
                btn_UserUpdate.Enabled = false;
                btn_UserDelete.Enabled = false;
                btn_UserUpdatePassword.Enabled = false;

                tb_UserFirstName.Text = string.Empty;
                tb_UserLastName.Text = string.Empty;
                tb_UserEmail.Text = string.Empty;
                tb_UserExtension.Text = string.Empty;
                tb_UserUpdatePassword.Text = string.Empty;
                
            }
            else
            {
                btn_UserCreate.Enabled = false;
                btn_UserUpdate.Enabled = true;
                btn_UserDelete.Enabled = true;
                btn_UserUpdatePassword.Enabled = true;

                tb_UserFirstName.Text = User.FirstName;
                tb_UserLastName.Text = User.LastName;
                tb_UserEmail.Text = User.Email;
                tb_UserExtension.Text = User.Extension;
                tb_UserUpdatePassword.Text = string.Empty;
                cmb_UserInstitute.SelectedItem = User.Lab.Institute;
                cmb_UserLab.SelectedItem = User.Lab;

                PopulateCredentials(User);
            }
        }

        private void PopulateCredentials(Users.UserProfile User)
        {
            pnl_UserCredentialsExisting.Controls.Clear();

            int xPos = 10;
            int yPos = 10;
            int MaxHeight = 20;
            foreach (Users.Credential C in User.Credentials)
            {
                Label lbl_AppName = new Label();
                lbl_AppName.Text = C.Application.Name;
                lbl_AppName.Top = yPos;
                lbl_AppName.Left = xPos;
                lbl_AppName.AutoSize = true;
                lbl_AppName.MaximumSize = new Size(150, MaxHeight);
                pnl_UserCredentialsExisting.Controls.Add(lbl_AppName);

                Label lbl_AppDesc = new Label();
                lbl_AppDesc.Text = C.Application.Description;
                lbl_AppDesc.Top = yPos;
                lbl_AppDesc.Left = lbl_AppName.Left + lbl_AppName.MaximumSize.Width + 10;
                lbl_AppDesc.AutoSize = true;
                lbl_AppDesc.MaximumSize = new Size(200, MaxHeight);
                pnl_UserCredentialsExisting.Controls.Add(lbl_AppDesc);


                Label lbl_LabName = new Label();
                lbl_LabName.Text = C.Lab.Name;
                lbl_LabName.Top = yPos;
                lbl_LabName.Left = lbl_AppDesc.Left + lbl_AppDesc.MaximumSize.Width + 10;
                lbl_LabName.AutoSize = true;
                lbl_LabName.MaximumSize = new Size(300, MaxHeight);
                pnl_UserCredentialsExisting.Controls.Add(lbl_LabName);

                Label lbl_RoleName = new Label();
                lbl_RoleName.Text = C.UserRole.Name;
                lbl_RoleName.Top = yPos;
                lbl_RoleName.Left = lbl_LabName.Left + lbl_LabName.MaximumSize.Width + 10;
                lbl_RoleName.AutoSize = true;
                lbl_RoleName.MaximumSize = new Size(100, MaxHeight);
                pnl_UserCredentialsExisting.Controls.Add(lbl_RoleName);

                LinkLabel lnk_DeleteCredential = new LinkLabel();
                lnk_DeleteCredential.Text = "Delete";
                lnk_DeleteCredential.Tag = C;
                lnk_DeleteCredential.Click += lnk_DeleteCredential_Click;
                lnk_DeleteCredential.Top = yPos;
                lnk_DeleteCredential.Left = lbl_RoleName.Left + lbl_RoleName.MaximumSize.Width + 10;
                lnk_DeleteCredential.AutoSize = true;
                pnl_UserCredentialsExisting.Controls.Add(lnk_DeleteCredential);

                yPos += 30;
            }
        }

        private void lnk_DeleteCredential_Click(object sender, EventArgs e)
        {
            Users.Credential C = (Users.Credential)((LinkLabel)sender).Tag;
            Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;
            try
            {
                C.RemoveUserCredential(User);
            }
            catch (Exception Err)
            {
                MessageBox.Show("Something went wrong while removing credential. Specific Error:" + Err.Message);
            }
            User.Credentials.Remove(C);
            PopulateCredentials(User);
        }
        

        private void tab_CostCentres_Enter(object sender, EventArgs e)
        {
            cmb_CostCentreInstitute.DataSource = Users.ResearchInstitute.GetInstitutes();
            cmb_CostCentreInstitute.DisplayMember = "Name";

            PopulateCostCentres();
        }

        private void tab_Users_Enter(object sender, EventArgs e)
        {
            PopulateUsers();
            PopulateInstitutes();
            PopulateApplications();
            
        }

        private void PopulateRoles(Users.ClientApplication App)
        {
            cmb_CreateCredentialRole.DataSource = App.Roles;
            cmb_CreateCredentialRole.DisplayMember = "Name";

        }

        private void PopulateApplications()
        {
            List<Users.ClientApplication> Apps = Users.ClientApplication.GetApplications();
            
            cmb_CreateCredentialApplication.DataSource = Apps;
            cmb_CreateCredentialApplication.DisplayMember = "Name";

        }

        void ReagentTypesAdapter_RowUpdating(object sender, SqlRowUpdatingEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                if (e.Row["MarkUp"] == DBNull.Value)
                {
                    MessageBox.Show("You Must Supply a Markup value. Enter zero for no markup.");
                }

                if (e.Row["DefaultPurchaseCostCentre"] == DBNull.Value)
                {
                    if (DialogResult.No == MessageBox.Show("You should supply a default purchase cost centre. Do you want to continue to create item without a default PCC?", "Default PCC", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        e.Status = UpdateStatus.SkipCurrentRow;
                        //e.Row.RejectChanges();
                        ReagentTypesDataGridView.Tag = "ActionCancelled";
                    }
                }
            }
        }

        void ReagentTypesDataGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["InfiniteResource"].Value = false;
        }

        
        void AdministrationForm_VisibleChanged(object sender, EventArgs e)
        {
            ReagentTypesTable.Clear();
            ReagentTypesAdapter.Fill(ReagentTypesTable);

            OrdersTable.Clear();
            OrdersAdapter.Fill(OrdersTable);

            InventoryTable.Clear();
            InventoryAdapter.Fill(InventoryTable);

            HistoryTable.Clear();
            HistoryAdapter.Fill(HistoryTable);

            SetDefaultVisibleColumns("ReagentTypes");
            AdministrationTabs.SelectTab(ReagentTypesTab);
        }

        void AdministrationTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (AdministrationTabs.SelectedTab.Name)
            {
                case "ReagentTypesTab":
                    ReagentTypesTable.Clear();
                    ReagentTypesAdapter.Fill(ReagentTypesTable);
                    SetDefaultVisibleColumns("ReagentTypes");
                    break;

                case "OrdersTab":
                    OrdersTable.Clear();
                    OrdersAdapter.Fill(OrdersTable);
                    SetDefaultVisibleColumns("Orders");
                    break;

                case "InventoryTab":
                    InventoryTable.Clear();
                    InventoryAdapter.Fill(InventoryTable);
                    SetDefaultVisibleColumns("Inventory");
                    break;

                case "HistoryTab":
                    HistoryTable.Clear();
                    HistoryAdapter.Fill(HistoryTable);
                    SetDefaultVisibleColumns("History");
                    break;
            }
        }

        private void SetDefaultVisibleColumns(string ViewName)
        {
            switch (ViewName)
            { 
                case "ReagentTypes":
                    ShowIDCheckBox.Checked = false;
                    if (ReagentTypesDataGridView.Columns.Contains("ReagentTypeID")) ReagentTypesDataGridView.Columns["ReagentTypeID"].Visible = ShowIDCheckBox.Checked;
                    ShowCommentCheckBox.Checked = false;
                    ShowFinishDateCheckBox.Checked = false;
                    ShowReceivedByCheckBox.Checked = false;
                    ShowReceiveDateCheckBox.Checked = false;
                    ShowOrderedByCheckBox.Checked = false;
                    ShowOrderDateCheckBox.Checked = false;
                    ShowUnitsRemainingCheckBox.Checked = false;
                    ShowExpiryDateCheckBox.Checked = false;
                    ShowLotNumberCheckBox.Checked = false;
                    ShowUseAsCheckBox.Checked = false;
                    ShowUnitofSaleCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("UnitofSale")) ReagentTypesDataGridView.Columns["UnitofSale"].Visible = ShowUnitofSaleCheckBox.Checked;
                    ShowInfiniteResourceCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("InfiniteResource")) ReagentTypesDataGridView.Columns["InfiniteResource"].Visible = ShowInfiniteResourceCheckBox.Checked;
                    ShowCPUCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("CPU")) ReagentTypesDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
                    ShowCostCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("Cost")) ReagentTypesDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
                    ShowUnitsPerItemCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("UnitsPerItem")) ReagentTypesDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
                    ShowOrderThresholdCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("OrderThreshold")) ReagentTypesDataGridView.Columns["OrderThreshold"].Visible = ShowOrderThresholdCheckBox.Checked;
                    ShowLastOrderedCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("LastOrdered")) ReagentTypesDataGridView.Columns["LastOrdered"].Visible = ShowLastOrderedCheckBox.Checked;
                    ShowStatusCheckBox.Checked = false;
                    if (ReagentTypesDataGridView.Columns.Contains("Status")) ReagentTypesDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
                    ShowPartNumberCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("PartNumber")) ReagentTypesDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
                    ShowSupplierCheckBox.Checked = true; 
                    if (ReagentTypesDataGridView.Columns.Contains("Supplier")) ReagentTypesDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
                    ShowManufacturerCheckBox.Checked = true; 
                    if (ReagentTypesDataGridView.Columns.Contains("Manufacturer")) ReagentTypesDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
                    ShowNameCheckBox.Checked = true; 
                    if (ReagentTypesDataGridView.Columns.Contains("Name")) ReagentTypesDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
                    ShowPCCCheckBox.Checked = false;
                    ShowMarkUpCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("MarkUp")) ReagentTypesDataGridView.Columns["MarkUp"].Visible = ShowMarkUpCheckBox.Checked;
                    ShowDefaultPCCCheckBox.Checked = true;
                    if (ReagentTypesDataGridView.Columns.Contains("DefaultPurchaseCostCentre")) ReagentTypesDataGridView.Columns["DefaultPurchaseCostCentre"].Visible = ShowDefaultPCCCheckBox.Checked;

                    break;
                case "Orders":
                    ShowIDCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("ItemID")) OrdersDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;
                    ShowCommentCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("Comment")) OrdersDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;
                    ShowFinishDateCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("FinishDate")) OrdersDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;
                    ShowReceivedByCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("ReceivedBy")) OrdersDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
                    ShowReceiveDateCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("ReceiveDate")) OrdersDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
                    ShowOrderedByCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("OrderedBy")) OrdersDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;
                    ShowOrderDateCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("OrderDate")) OrdersDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;
                    ShowUnitsRemainingCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("UnitsRemaining")) OrdersDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;
                    ShowExpiryDateCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("ExpiryDate")) OrdersDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
                    ShowLotNumberCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("LotNumber")) OrdersDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;
                    ShowUnitofSaleCheckBox.Checked = false;
                    ShowInfiniteResourceCheckBox.Checked = false;
                    ShowCPUCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("CPU")) OrdersDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
                    ShowCostCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("Cost")) OrdersDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
                    ShowUnitsPerItemCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("UnitsPerItem")) OrdersDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
                    ShowOrderThresholdCheckBox.Checked = false;
                    ShowLastOrderedCheckBox.Checked = false;
                    ShowStatusCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("Status")) OrdersDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
                    ShowPartNumberCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("PartNumber")) OrdersDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
                    ShowSupplierCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("Supplier")) OrdersDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
                    ShowManufacturerCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("Manufacturer")) OrdersDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
                    ShowNameCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("Name")) OrdersDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
                    ShowPCCCheckBox.Checked = true;
                    if (OrdersDataGridView.Columns.Contains("PCC")) OrdersDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
                    ShowDefaultPCCCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("DefaultPurchaseCostCentre")) OrdersDataGridView.Columns["DefaultPurchaseCostCentre"].Visible = ShowDefaultPCCCheckBox.Checked;
                    ShowMarkUpCheckBox.Checked = false;
                    if (OrdersDataGridView.Columns.Contains("MarkUp")) OrdersDataGridView.Columns["MarkUp"].Visible = ShowMarkUpCheckBox.Checked;

                    break;
                case "Inventory":
                    ShowIDCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("ItemID")) InventoryDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;
                    ShowCommentCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("Comment")) InventoryDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;
                    ShowFinishDateCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("FinishDate")) InventoryDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;
                    ShowReceivedByCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("ReceivedBy")) InventoryDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
                    ShowReceiveDateCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("ReceiveDate")) InventoryDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
                    ShowOrderedByCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("OrderedBy")) InventoryDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;
                    ShowOrderDateCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("OrderDate")) InventoryDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;
                    ShowUnitsRemainingCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("UnitsRemaining")) InventoryDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;
                    ShowExpiryDateCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("ExpiryDate")) InventoryDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
                    ShowLotNumberCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("LotNumber")) InventoryDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;
                    ShowUnitofSaleCheckBox.Checked = false;
                    ShowInfiniteResourceCheckBox.Checked = false;
                    ShowCPUCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("CPU")) InventoryDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
                    ShowCostCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("Cost")) InventoryDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
                    ShowUnitsPerItemCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("UnitsPerItem")) InventoryDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
                    ShowOrderThresholdCheckBox.Checked = false;
                    ShowLastOrderedCheckBox.Checked = false;
                    ShowStatusCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("Status")) InventoryDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
                    ShowPartNumberCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("PartNumber")) InventoryDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
                    ShowSupplierCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("Supplier")) InventoryDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
                    ShowManufacturerCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("Manufacturer")) InventoryDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
                    ShowNameCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("Name")) InventoryDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
                    ShowPCCCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("PCC")) InventoryDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
                    ShowUseAsCheckBox.Checked = true;
                    if (InventoryDataGridView.Columns.Contains("UseAs")) InventoryDataGridView.Columns["UseAs"].Visible = ShowUseAsCheckBox.Checked;
                    ShowDefaultPCCCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("DefaultPurchaseCostCentre")) InventoryDataGridView.Columns["DefaultPurchaseCostCentre"].Visible = ShowDefaultPCCCheckBox.Checked;
                    ShowMarkUpCheckBox.Checked = false;
                    if (InventoryDataGridView.Columns.Contains("MarkUp")) InventoryDataGridView.Columns["MarkUp"].Visible = ShowMarkUpCheckBox.Checked;

                    break;
                case "History":
                    ShowUseAsCheckBox.Checked = false;
                    ShowIDCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("ItemID")) HistoryDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;
                    ShowCommentCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("Comment")) HistoryDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;
                    ShowFinishDateCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("FinishDate")) HistoryDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;
                    ShowReceivedByCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("ReceivedBy")) HistoryDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
                    ShowReceiveDateCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("ReceiveDate")) HistoryDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
                    ShowOrderedByCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("OrderedBy")) HistoryDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;
                    ShowOrderDateCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("OrderDate")) HistoryDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;
                    ShowUnitsRemainingCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("UnitsRemaining")) HistoryDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;
                    ShowExpiryDateCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("ExpiryDate")) HistoryDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
                    ShowLotNumberCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("LotNumber")) HistoryDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;
                    ShowUnitofSaleCheckBox.Checked = false;
                    ShowInfiniteResourceCheckBox.Checked = false;
                    ShowCPUCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("CPU")) HistoryDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
                    ShowCostCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("Cost")) HistoryDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
                    ShowUnitsPerItemCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("UnitsPerItem")) HistoryDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
                    ShowOrderThresholdCheckBox.Checked = false;
                    ShowLastOrderedCheckBox.Checked = false;
                    ShowStatusCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("Status")) HistoryDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
                    ShowPartNumberCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("PartNumber")) HistoryDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
                    ShowSupplierCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("Supplier")) HistoryDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
                    ShowManufacturerCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("Manufacturer")) HistoryDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
                    ShowNameCheckBox.Checked = true;
                    if (HistoryDataGridView.Columns.Contains("Name")) HistoryDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
                    ShowPCCCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("PCC")) HistoryDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
                    ShowDefaultPCCCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("DefaultPurchaseCostCentre")) HistoryDataGridView.Columns["DefaultPurchaseCostCentre"].Visible = ShowDefaultPCCCheckBox.Checked;
                    ShowMarkUpCheckBox.Checked = false;
                    if (HistoryDataGridView.Columns.Contains("MarkUp")) HistoryDataGridView.Columns["MarkUp"].Visible = ShowMarkUpCheckBox.Checked;
                    break;
            }
        }

        void DataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {   
            SuppressCellChangedEvent = true;
            
        }

        void DataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (e.RowCount > 1)
            {
                SuppressCellChangedEvent = true;
            }
        }

        void FilterManager_PopupShowing(object sender, DgvFilterPopup.ColumnFilterEventArgs e)
        {
            SuppressCellChangedEvent = true;
        }

        void FilterManager_ColumnFilterAdding(object sender, DgvFilterPopup.ColumnFilterEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Name":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;

                case "Manufacturer":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;

                case "Supplier":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;

                case "PartNumber":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;

                case "LastOrdered":
                    e.ColumnFilter = new DgvFilterPopup.DgvDateRangeColumnFilter();
                    break;

                case "Status":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;

                case "OrderThreshold":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;
                case "UnitsPerItem":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;

                case "UnitOfSale":
                    e.ColumnFilter = new DgvFilterPopup.DgvComboBoxColumnFilter();
                    break;

                case "Cost":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;

                case "ItemID":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;
                
                case "CPU":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;

                case "LotNumber":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;

                case "ExpiryDate":
                    e.ColumnFilter = new DgvFilterPopup.DgvDateRangeColumnFilter();
                    break;

                case "UnitsRemaining":
                    e.ColumnFilter = new DgvFilterPopup.DgvNumRangeColumnFilter();
                    break;

                case "OrderDate":
                    e.ColumnFilter = new DgvFilterPopup.DgvDateRangeColumnFilter();
                    break;

                case "OrderedBy":
                    e.ColumnFilter = new DgvFilterPopup.DgvComboBoxColumnFilter();
                    break;

                case "PCC":
                    e.ColumnFilter = new DgvFilterPopup.DgvComboBoxColumnFilter();
                    break;

                case "ReceiveDate":
                    e.ColumnFilter = new DgvFilterPopup.DgvDateRangeColumnFilter();
                    break;

                case "ReceivedBy":
                    e.ColumnFilter = new DgvFilterPopup.DgvComboBoxColumnFilter();
                    break;

                case "FinishDate":
                    e.ColumnFilter = new DgvFilterPopup.DgvDateRangeColumnFilter();
                    break;

                case "Comment":
                    e.ColumnFilter = new DgvFilterPopup.DgvTextBoxColumnFilter();
                    break;
            }
        }

        void SignOutListDataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (!SuppressCellChangedEvent)
            {
                SignOutListFlashUpdateTimer.Start();
            }
            SuppressCellChangedEvent = false;
        }

        void SignOutListDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!SuppressCellChangedEvent)
            {
                SignOutListFlashUpdateTimer.Start();
            }
            SuppressCellChangedEvent = false;
        }

        void SignOutListFlashUpdateTimer_Tick(object sender, EventArgs e)
        {
            SignOutListUpdateButton.BackColor = (SignOutListUpdateButton.BackColor == System.Drawing.Color.Transparent) ? System.Drawing.Color.Red : System.Drawing.Color.Transparent;            
        }

        void SuppressRowDelete(object sender, SqlRowUpdatingEventArgs e)
        {
            if(e.StatementType == StatementType.Delete)
            {
                e.Status = UpdateStatus.SkipCurrentRow;
            }
        }

        private void PrepareCostCentres()
        {
            DeleteCostCentreComboBox.Items.Clear();
            List<Users.CostCentre> CostCentres = Users.CostCentre.GetCostCentres();

            foreach (Users.CostCentre CostCentre in CostCentres)
            {
                DeleteCostCentreComboBox.Items.Add(CostCentre);
            }
        }

        void ReagentSummaryDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                ReagentDetailsDataGridView.Rows.Clear();

                string StartDate = ReagentSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
                string EndDate = ReagentSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");

                string Mode = String.Empty;
                string RowCategory = (string)ReagentSummaryDataGridView.Rows[e.RowIndex].Cells[0].Value;
                string PurchaseCostCentre = (string)ReagentSummaryDataGridView.Rows[e.RowIndex].Cells[1].Value;
                if (RowCategory == "Total Stock on Order")
                {
                    Mode = "Order";
                }
                else if (RowCategory == "Total Reagent in Stock")
                {
                    Mode = "Stock";
                }
                else if (RowCategory == "Total UnTracked Loss")
                {
                    Mode = "Loss";
                }

                if (Mode != String.Empty)
                {
                    try
                    {
                        Dictionary<string, decimal[]> ReagentDetails = ReagentManagement.GetReagentDetails(StartDate, EndDate, PurchaseCostCentre, Mode, DatabaseConfig.GetConnectionString());

                        foreach (KeyValuePair<string, decimal[]> KVP in ReagentDetails)
                        {
                            ReagentDetailsDataGridView.Rows.Add(new object[] { KVP.Key, KVP.Value[0], KVP.Value[1] });
                        }
                    }
                    catch (Exception Err)
                    {
                        string ErrorMessage = string.Empty;
                        while (Err != null)
                        {
                            ErrorMessage += Err.Message + "; ";
                            Err = Err.InnerException;
                        }
                        MessageBox.Show("An error occurred while getting reagent summary details: " + ErrorMessage);
                    }
                }
            }
        }

        void CostCentreDetailsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int BilledColumnIndex = CostCentreDetailsDataGridView.Columns["Billed"].Index;
            if (e.ColumnIndex == BilledColumnIndex)
            {
                int EventID = (int)CostCentreDetailsDataGridView.Rows[e.RowIndex].Cells[0].Value;
                ReagentManagement.SignOutEvent S = new ReagentManagement.SignOutEvent(EventID, DatabaseConfig.GetConnectionString());

                try
                {
                    if ((bool)CostCentreDetailsDataGridView.Rows[e.RowIndex].Cells[BilledColumnIndex].Value)
                    {
                        CostCentreDetailsDataGridView.Rows[e.RowIndex].Cells[BilledColumnIndex].Value = false;
                        S.MarkUnBilled();
                    }
                    else
                    {
                        CostCentreDetailsDataGridView.Rows[e.RowIndex].Cells[BilledColumnIndex].Value = true;
                        S.MarkBilled();
                    }
                }
                catch
                {
                    MessageBox.Show("An Error Occurred While Updating Billing State");
                }
            }
        }

        void CostCentreSummaryDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string DebtorCostCentre = (string)CostCentreSummaryDataGridView.Rows[e.RowIndex].Cells[0].Value;
                string CreditorCostCentre = (string)CostCentreSummaryDataGridView.Rows[e.RowIndex].Cells[1].Value;
                string StartDate = CostCentreSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
                string EndDate = CostCentreSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
                string BillingStatus = "All";
                if (CostCentreSummaryUnbilledBillingStatusRadioButton.Checked)
                {
                    BillingStatus = "UnBilled";
                }
                else if (CostCentreSummaryBilledBillingStatusRadioButton.Checked)
                {
                    BillingStatus = "Billed";
                }

                if (DebtorCostCentre != String.Empty)
                {
                    CostCentreDetailsDataGridView.Rows.Clear();

                    if (DebtorCostCentre.IndexOf("(") != -1)
                    {
                        DebtorCostCentre = DebtorCostCentre.Substring(DebtorCostCentre.IndexOf("(") + 1, DebtorCostCentre.IndexOf(")") - (DebtorCostCentre.IndexOf("(")) - 1);
                    }

                    List<ReagentManagement.SignOutEvent> CostCentreDetails = ReagentManagement.GetCostCentreDetails(DebtorCostCentre, CreditorCostCentre, StartDate, EndDate, BillingStatus, DatabaseConfig.GetConnectionString());
                                        

                    foreach (ReagentManagement.SignOutEvent S in CostCentreDetails)
                    {
                        if (e.ColumnIndex == CostCentreSummaryDataGridView.Columns["BillButton"].Index)
                        {
                            S.MarkBilled();
                        }
                        else if (e.ColumnIndex == CostCentreSummaryDataGridView.Columns["UnBillButton"].Index)
                        {
                            S.MarkUnBilled();                        
                        }

                        Users.UserProfile User = new Users.UserProfile(S.UserID, ((CatalogForm)this.Owner).App);

                        CostCentreDetailsDataGridView.Rows.Add(new object[] { S.EventID, S.Item.Name, User.FirstName + " " + User.LastName, S.Quantity, S.Date, S.Billed });
                    }
                }
            }
        }

        
        
        
        void ReagentTypesDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int LastOrderedColumnIndex = ReagentTypesDataGridView.Columns["LastOrdered"].Index;
            int OrderQuantityColumnIndex = ReagentTypesDataGridView.Columns["OrderQuantity"].Index;

            if (e.ColumnIndex != LastOrderedColumnIndex && e.ColumnIndex != OrderQuantityColumnIndex)
            {
                if (!SuppressCellChangedEvent) { ReagentTypesFlashUpdateTimer.Start(); }
                else { SuppressCellChangedEvent = false; }
            }
        }

        void OrdersDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int LotNumberColumnIndex = OrdersDataGridView.Columns["LotNumber"].Index;
            int ExpiryDateColumnIndex = OrdersDataGridView.Columns["ExpiryDate"].Index;
            
            if (e.ColumnIndex != LotNumberColumnIndex && e.ColumnIndex != ExpiryDateColumnIndex)
            {
                if (!SuppressCellChangedEvent) { OrderFlashUpdateTimer.Start(); }
                else { SuppressCellChangedEvent = false; }
            }
        }

        void ReagentTypesFlashUpdateTimer_Tick(object sender, EventArgs e)
        {
            ReagentTypesUpdateButton.BackColor = (ReagentTypesUpdateButton.BackColor == System.Drawing.Color.Transparent) ? System.Drawing.Color.Red : System.Drawing.Color.Transparent;            
        }

        void OrderFlashUpdateTimer_Tick(object sender, EventArgs e)
        {

            OrdersUpdateButton.BackColor = (OrdersUpdateButton.BackColor == System.Drawing.Color.Transparent) ? System.Drawing.Color.Red : System.Drawing.Color.Transparent;            
        }

        void InventoryDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int UsageUnitsColumnIndex = InventoryDataGridView.Columns["UsageUnits"].Index;
            int CostCentreColumnIndex = InventoryDataGridView.Columns["CostCentre"].Index;

            if (e.ColumnIndex != UsageUnitsColumnIndex && e.ColumnIndex != CostCentreColumnIndex)
            {
                if (!SuppressCellChangedEvent) { InventoryFlashUpdateTimer.Start(); }
                else { SuppressCellChangedEvent = false; }
            }
        }

        void InventoryFlashUpdateTimer_Tick(object sender, EventArgs e)
        {
            InventoryUpdateButton.BackColor = (InventoryUpdateButton.BackColor == System.Drawing.Color.Transparent) ? System.Drawing.Color.Red : System.Drawing.Color.Transparent;
        }


        private void PrepareCategories()
        {
            CategoryList.Clear();
            List<ReagentManagement.Category> Categories = ReagentManagement.Category.GetTopLevelCategories(DatabaseConfig.GetConnectionString());
            CategoryList.Add("None",-1);
            foreach (ReagentManagement.Category C in Categories)
            {
                AddCategory(C,null);
            }

            NewCategoryParentComboBox.Items.Clear();
            ReagentCategoryLinkCategoryComboBox.Items.Clear();
            SetCategoryLinkCategoryComboBox.Items.Clear();
            DeleteCategoryComboBox.Items.Clear();
            foreach (KeyValuePair<string, int> KVP in CategoryList)
            {
                NewCategoryParentComboBox.Items.Add(KVP.Key); 
                ReagentCategoryLinkCategoryComboBox.Items.Add(KVP.Key);
                SetCategoryLinkCategoryComboBox.Items.Add(KVP.Key);
                DeleteCategoryComboBox.Items.Add(KVP.Key);
            }
            
        }

        private void AddCategory(ReagentManagement.Category Category, string ParentCategoryDescription)
        {

            CategoryList.Add((ParentCategoryDescription == null) ? Category.Description : Category.Description + " (in " + ParentCategoryDescription + ")", Category.CategoryID);
            List<ReagentManagement.Category> ChildCategories = Category.GetChildCategories();
            
            foreach (ReagentManagement.Category C in ChildCategories)
            {
                AddCategory(C, Category.Description);
            }
        }

        void NewCategoryParentComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        void InventoryDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                int UnitsRemainingColumnIndex = InventoryDataGridView.Columns["UnitsRemaining"].Index;
                int StatusColumnIndex = InventoryDataGridView.Columns["Status"].Index;
                int OpenColumnIndex = InventoryDataGridView.Columns["Open"].Index;
                int UseAsColumnIndex = InventoryDataGridView.Columns["UseAs"].Index;
                int UsageCostCentreColumnIndex = InventoryDataGridView.Columns["CostCentre"].Index;
                int UsageUnitsColumnIndex = InventoryDataGridView.Columns["UsageUnits"].Index;
                int UseColumnIndex = InventoryDataGridView.Columns["Use"].Index;
                int FinishColumnIndex = InventoryDataGridView.Columns["Finish"].Index;
                int FaultyColumnIndex = InventoryDataGridView.Columns["Faulty"].Index;
                int ItemIDColumnIndex = InventoryDataGridView.Columns["ItemID"].Index;
                string ItemID = InventoryDataGridView.Rows[e.RowIndex].Cells[ItemIDColumnIndex].Value.ToString();

                if (e.ColumnIndex == OpenColumnIndex)
                {
                    SuppressCellChangedEvent = true;

                    if (ItemID != String.Empty)
                    {
                        try
                        {
                            ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                            R.Status = ReagentManagement.ReagentStatus.Open;
                            R.Update();
                            InventoryDataGridView.Rows[e.RowIndex].Cells[StatusColumnIndex].Value = R.Status;

                            AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + R.Name + " (ItemID:" + R.ItemID + ", " + "Lot:" + R.LotNumber + ")" + " opened.", ref InventoryInfoPanel);

                        }
                        catch (Exception Err)
                        {
                            SuppressCellChangedEvent = false;
                            AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while setting status to open:" + Err.Message, ref InventoryInfoPanel);
                        }
                    }
                }
                else if (e.ColumnIndex == UseAsColumnIndex || e.ColumnIndex == UsageCostCentreColumnIndex)
                {
                    using (CostCentreSearch CSF = new CostCentreSearch())
                    {
                        CSF.ShowDialog(this);
                        if (CSF.DialogResult == DialogResult.OK)
                        {
                            Guid UserGUID_LocalCopy = CSF.UserID;
                            InventoryDataGridView.Rows[e.RowIndex].Cells[UseAsColumnIndex].Value = UserGUID_LocalCopy.ToString();
                            InventoryDataGridView.Rows[e.RowIndex].Cells[UsageCostCentreColumnIndex].Value = CSF.CostCentre;
                        }
                    }
                }
                else if (e.ColumnIndex == UseColumnIndex)
                {
                    string UsageUnits = InventoryDataGridView.Rows[e.RowIndex].Cells[UsageUnitsColumnIndex].Value.ToString();
                    string SuppliedCostCentre = (string)InventoryDataGridView.Rows[e.RowIndex].Cells[UsageCostCentreColumnIndex].Value;
                    string SuppliedUserID = (string)InventoryDataGridView.Rows[e.RowIndex].Cells[UseAsColumnIndex].Value;

                    if (UsageUnits == String.Empty)
                    {
                        MessageBox.Show("You Must Supply the Number of Units to Use");
                    }
                    else
                    {
                        double UsageUnitsDouble;
                        if (double.TryParse(UsageUnits, out UsageUnitsDouble))
                        {
                            ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                            try
                            {
                                Guid SignOutUserID = ((String.IsNullOrEmpty(SuppliedUserID)) ? ((CatalogForm)this.Owner).ShoppingCart.User.UserID : new Guid(SuppliedUserID));
                                string SignOutCostCentre = ((String.IsNullOrEmpty(SuppliedCostCentre)) ? ((CatalogForm)this.Owner).ShoppingCart.CostCentre : SuppliedCostCentre);
                                if (R.SignOut(SignOutUserID, SignOutCostCentre, UsageUnitsDouble, ((CatalogForm)this.Owner).App.Name))
                                {
                                    AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] Used " + UsageUnitsDouble.ToString() + " units of " + R.Name + " (ItemID:" + R.ItemID + ")", ref InventoryInfoPanel);
                                    if (R.Status == ReagentManagement.ReagentStatus.Finished)
                                    {
                                        InventoryDataGridView.Rows.RemoveAt(e.RowIndex);
                                        HistoryTable.Clear();
                                        HistoryAdapter.Fill(HistoryTable);
                                    }
                                    else
                                    {
                                        SuppressCellChangedEvent = true;
                                        InventoryDataGridView.Rows[e.RowIndex].Cells[StatusColumnIndex].Value = R.Status;
                                        SuppressCellChangedEvent = true;
                                        InventoryDataGridView.Rows[e.RowIndex].Cells[UnitsRemainingColumnIndex].Value = R.UnitsRemaining;
                                    }

                                    InventoryDataGridView.Rows[e.RowIndex].Cells[UseAsColumnIndex].Value = string.Empty;
                                    InventoryDataGridView.Rows[e.RowIndex].Cells[UsageCostCentreColumnIndex].Value = string.Empty;
                                    InventoryDataGridView.Rows[e.RowIndex].Cells[UsageUnitsColumnIndex].Value = string.Empty;

                                }
                                else
                                {
                                    AddStatusLabel(false, "Usage command returned false, this may indicate a problem.", ref InventoryInfoPanel);
                                }
                            }
                            catch (ArgumentOutOfRangeException Err)
                            {
                                MessageBox.Show("An error occurred while executing use command. The most likely cause is that you have attempted to use more units than remain for the item. Full Error:" + Err.Message);
                            }
                            catch (Exception Err)
                            {
                                if (Err.Message == "Stock Level Check Failed")
                                {
                                    AddStatusLabel(false, "Stock Level Check Failed, Please Manually Check Stock Levels.", ref InventoryInfoPanel);
                                    AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] Used " + UsageUnitsDouble.ToString() + " units of " + R.Name + " (ItemID:" + R.ItemID + ")", ref InventoryInfoPanel);

                                }
                                else
                                {
                                    MessageBox.Show("An error occurred while executing use command.");
                                }
                            }

                        }
                    }
                }
                else if (e.ColumnIndex == FinishColumnIndex)
                {

                    if (ItemID != String.Empty)
                    {
                        try
                        {
                            ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                            R.Status = ReagentManagement.ReagentStatus.Finished;
                            R.FinishDate = DateTime.Now.ToString("dd/MM/yyyy");
                            R.Update();
                            InventoryDataGridView.Rows.RemoveAt(e.RowIndex);
                            HistoryTable.Clear();
                            HistoryAdapter.Fill(HistoryTable);

                            AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + R.Name + " (ItemID:" + R.ItemID + ", " + "Lot:" + R.LotNumber + ")" + " finished.", ref InventoryInfoPanel);


                        }
                        catch (Exception Err)
                        {
                            AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while setting status to finished:" + Err.Message, ref InventoryInfoPanel);
                        }
                    }
                }
                else if (e.ColumnIndex == FaultyColumnIndex)
                {

                    if (ItemID != String.Empty)
                    {
                        try
                        {
                            ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                            R.Status = ReagentManagement.ReagentStatus.Faulty;
                            R.Update();
                            InventoryDataGridView.Rows.RemoveAt(e.RowIndex);
                            HistoryTable.Clear();
                            HistoryAdapter.Fill(HistoryTable);

                            AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + R.Name + " (ItemID:" + R.ItemID + ", " + "Lot:" + R.LotNumber + ")" + " faulty.", ref InventoryInfoPanel);
                        }
                        catch (Exception Err)
                        {
                            AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while setting status to faulty:" + Err.Message, ref InventoryInfoPanel);
                        }
                    }
                }
            }            
        }

        void OrdersDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                int ReceiveColumnIndex = OrdersDataGridView.Columns["Receive"].Index;
                int CancelColumnIndex = OrdersDataGridView.Columns["Cancel"].Index;
                int ItemIDColumnIndex = OrdersDataGridView.Columns["ItemID"].Index;
                int LotNumberColumnIndex = OrdersDataGridView.Columns["LotNumber"].Index;
                int ExpiryDateColumnIndex = OrdersDataGridView.Columns["ExpiryDate"].Index;
                

                string LotNumber = OrdersDataGridView.Rows[e.RowIndex].Cells[LotNumberColumnIndex].Value.ToString();
                string ExpiryDate = OrdersDataGridView.Rows[e.RowIndex].Cells[ExpiryDateColumnIndex].Value.ToString();
                string ItemID = OrdersDataGridView.Rows[e.RowIndex].Cells[ItemIDColumnIndex].Value.ToString();

                if (e.ColumnIndex == ReceiveColumnIndex)
                {

                    if (ItemID != String.Empty)
                    {
                        if (LotNumber != String.Empty)
                        {

                            try
                            {
                                ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                                R.LotNumber = LotNumber;
                                R.ExpiryDate = ExpiryDate;
                                R.Receive(((CatalogForm)this.Owner).ShoppingCart.User.FullName);
                                OrdersDataGridView.Rows.RemoveAt(e.RowIndex);
                                InventoryTable.Clear();
                                InventoryAdapter.Fill(InventoryTable);

                                AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + R.Name + " (ItemID:" + R.ItemID + ", " + "Lot:" + R.LotNumber + ")" + " received.", ref OrdersInfoPanel);

                            }
                            catch (Exception Err)
                            {
                                AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while receiving item:" + Err.Message, ref OrdersInfoPanel);
                            }
                        }
                        else
                        {
                            MessageBox.Show("You Must Supply a Lot Number to Receive An Item");
                        }

                    }
                }
                else if (e.ColumnIndex == CancelColumnIndex)
                {
                    if (ItemID != String.Empty)
                    {
                        try
                        {
                            ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                            ReagentManagement.Reagent.CancelOrder(R.ItemID, DatabaseConfig.GetConnectionString());
                            OrdersDataGridView.Rows.RemoveAt(e.RowIndex);                            

                            AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + R.Name + " (ItemID:" + R.ItemID + ")" + " order cancelled.", ref OrdersInfoPanel);
                        }
                        catch (Exception Err)
                        {

                            AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while cancelling order:" + Err.Message, ref OrdersInfoPanel);
                        }
                    }
                }
            }
        }

        void ReagentTypesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                int OrderQuantityColumnIndex = ReagentTypesDataGridView.Columns["OrderQuantity"].Index;
                int OrderColumnIndex = ReagentTypesDataGridView.Columns["Order"].Index;
                int ReagentTypeIDColumnIndex = ReagentTypesDataGridView.Columns["ReagentTypeID"].Index;
                int LastOrderedColumnIndex = ReagentTypesDataGridView.Columns["LastOrdered"].Index;


                if (e.ColumnIndex == OrderColumnIndex)
                {
                    string ReagentTypeID = ReagentTypesDataGridView.Rows[e.RowIndex].Cells[ReagentTypeIDColumnIndex].Value.ToString();
                    int OrderQuanity;
                    if (ReagentTypeID != String.Empty)
                    {
                        string OQ = String.IsNullOrEmpty((string)ReagentTypesDataGridView.Rows[e.RowIndex].Cells[OrderQuantityColumnIndex].Value)?"":(string)ReagentTypesDataGridView.Rows[e.RowIndex].Cells[OrderQuantityColumnIndex].Value;
                        if (int.TryParse(OQ, out OrderQuanity))
                        {
                            SuppressCellChangedEvent = true;
                            try
                            {
                                ReagentManagement.ReagentType RT = new ReagentManagement.ReagentType(ReagentTypeID, DatabaseConfig.GetConnectionString());
                                string OrderCostCentre = String.IsNullOrEmpty(RT.DefaultPurchaseCostCentre) ? ((CatalogForm)this.Owner).ShoppingCart.CostCentre : RT.DefaultPurchaseCostCentre;
                                RT.Order(OrderQuanity, OrderCostCentre, ((CatalogForm)this.Owner).ShoppingCart.User.FullName);
                                ReagentTypesDataGridView.Rows[e.RowIndex].Cells[LastOrderedColumnIndex].Value = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                                OrdersTable.Clear();
                                OrdersAdapter.Fill(OrdersTable);

                                AddStatusLabel(true, "[" + DateTime.Now.ToString() + "] " + OrderQuanity.ToString() + " x " + RT.Name + " added to order list.", ref ReagentTypesInfoPanel);

                            }
                            catch (Exception Err)
                            {
                                SuppressCellChangedEvent = false;
                                AddStatusLabel(false, "[" + DateTime.Now.ToString() + "] An error occurred while adding item to order list:" + Err.Message, ref ReagentTypesInfoPanel);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error Parsing Order Quantity. Please verify you have supplied a valid numeric integer.");
                        }
                    }
                }
            }
        }


        private void PrepareReagentTypes()
        {
            ReagentTypesAdapter = ReagentManagement.ReagentType.GetDataAdapter(DatabaseConfig.GetConnectionString());
            ReagentTypesAdapter.Fill(ReagentTypesTable);
            ReagentTypesDataGridView.DataSource = ReagentTypesTable;
            if (ReagentTypesDataGridView.Columns.Count > 0)
            {
                ReagentTypesDataGridView.Columns[0].ReadOnly = true;

                if (ReagentTypesDataGridView.Columns.Contains("LastOrdered"))
                {
                    ReagentTypesDataGridView.Columns["LastOrdered"].ReadOnly = true;
                }

                if (ReagentTypesDataGridView.Columns.Contains("CostPerUnit"))
                {
                    ReagentTypesDataGridView.Columns["CostPerUnit"].ReadOnly = true;
                }

                DataGridViewTextBoxColumn OrderQuantityColumn = new DataGridViewTextBoxColumn();
                OrderQuantityColumn.Name = "OrderQuantity";
                OrderQuantityColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                ReagentTypesDataGridView.Columns.Insert(ReagentTypesDataGridView.Columns.Count, OrderQuantityColumn);     

                DataGridViewButtonColumn OrderButtonColumn = new DataGridViewButtonColumn();
                OrderButtonColumn.Text = "Order";
                OrderButtonColumn.Name = "Order";
                OrderButtonColumn.UseColumnTextForButtonValue = true;
                OrderButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                ReagentTypesDataGridView.Columns.Insert(ReagentTypesDataGridView.Columns.Count, OrderButtonColumn);     
            }

            ReagentCategoryLinkReagentComboBox.DataSource = ReagentTypesTable;
            ReagentCategoryLinkReagentComboBox.DisplayMember = "Name";
            ReagentCategoryLinkReagentComboBox.ValueMember = "ReagentTypeID";

            SetColumnWidths(ReagentTypesDataGridView);
        }

        private void SetColumnWidths(DataGridView DGV)
        {
            foreach (DataGridViewColumn Column in DGV.Columns)
            {
                switch (Column.Name)
                {
                    case "ReagentTypeID":
                    case "ItemID":
                    case "FinishDate":
                    case "ReceivedBy":
                    case "ReceiveDate":
                    case "OrderedBy":
                    case "OrderDate":
                    case "UnitsRemaining":
                    case "UnitofSale":
                    case "CPU":
                    case "Cost":
                    case "Status":
                    case "LastOrdered":
                    case "PartNumber":
                    case "Supplier":
                    case "Manufacturer":
                    case "Name":
                    case "PCC":
                        Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
                        break;
                    case "UnitsPerItem":
                    case "OrderThreshold":
                    case "InfiniteResource":
                        Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                        break;
                    case "Comment":
                        Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        break;
                    case "ExpiryDate":
                    case "LotNumber":
                        Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        break;
                }
            }
        }

        private void ReagentTypesUpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                ReagentTypesAdapter.Update(ReagentTypesTable);
                if (ReagentTypesDataGridView.Tag != null && (ReagentTypesDataGridView.Tag.GetType() == typeof(string)) && (string)ReagentTypesDataGridView.Tag != "ActionCancelled")
                {
                    ReagentTypesTable.Clear();
                    ReagentTypesAdapter.Fill(ReagentTypesTable);

                    ReagentTypesUpdateButton.BackColor = System.Drawing.Color.Transparent;
                    ReagentTypesFlashUpdateTimer.Stop();
                }
                else
                {
                    ReagentTypesDataGridView.Tag = null;
                }
            }
            catch (SqlException SQLErr)
            {
                if (SQLErr.Number == 2627)
                {
                    MessageBox.Show("SQL Error 2627: Unique Key Constraint Violation.  This error is usually caused by trying to create an already existing reagent type.");
                }
                else
                {
                    MessageBox.Show("An SQL error occurred while updating table: " + SQLErr.Message);
                }
            }
            catch (Exception Err)
            {
                string ErrorMessage = string.Empty;
                while (Err != null)
                {
                    ErrorMessage += Err.Message + ";";
                    Err = Err.InnerException;
                }
                MessageBox.Show("An error occurred while updating table: " + ErrorMessage);
            }
        }

        private void ReagentTypesRevertButton_Click(object sender, EventArgs e)
        {
            ReagentTypesTable.Clear();
            ReagentTypesAdapter.Fill(ReagentTypesTable);
            ReagentTypesUpdateButton.BackColor = System.Drawing.Color.Transparent;
            ReagentTypesFlashUpdateTimer.Stop();
        }

        private void PrepareOrders()
        {
            OrdersAdapter = ReagentManagement.Reagent.GetDataAdaptor("reagent_inventory.Status = 0", DatabaseConfig.GetConnectionString());
            OrdersAdapter.Fill(OrdersTable);
            OrdersDataGridView.DataSource = OrdersTable;
            if (OrdersDataGridView.Columns.Count > 0)
            {
                OrdersDataGridView.Columns["ItemID"].ReadOnly = true;
                OrdersDataGridView.Columns["ItemID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                OrdersDataGridView.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                OrdersDataGridView.Columns["Status"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                OrdersDataGridView.Columns["Comment"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                OrdersDataGridView.Columns["CPU"].ReadOnly = true;

                DataGridViewButtonColumn ReceiveButtonColumn = new DataGridViewButtonColumn();
                ReceiveButtonColumn.Text = "Receive";
                ReceiveButtonColumn.Name = "Receive";
                ReceiveButtonColumn.UseColumnTextForButtonValue = true;
                ReceiveButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                OrdersDataGridView.Columns.Add(ReceiveButtonColumn);

                DataGridViewButtonColumn CancelButtonColumn = new DataGridViewButtonColumn();
                CancelButtonColumn.Text = "Cancel";
                CancelButtonColumn.Name = "Cancel";
                CancelButtonColumn.UseColumnTextForButtonValue = true;
                CancelButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                OrdersDataGridView.Columns.Add(CancelButtonColumn);

                SetColumnWidths(OrdersDataGridView);
            }
        }

        private void PrepareInventory()
        {
            InventoryAdapter = ReagentManagement.Reagent.GetDataAdaptor("reagent_inventory.Status = 1 OR reagent_inventory.Status = 2", DatabaseConfig.GetConnectionString());
            InventoryAdapter.Fill(InventoryTable);
            InventoryDataGridView.DataSource = InventoryTable;
            
            
            if (InventoryDataGridView.Columns.Count > 0)
            {
                InventoryDataGridView.Columns["ItemID"].ReadOnly = true;
                InventoryDataGridView.Columns["ItemID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                InventoryDataGridView.Columns["Manufacturer"].Visible = false;
                InventoryDataGridView.Columns["Supplier"].Visible = false; ;
                InventoryDataGridView.Columns["PartNumber"].Visible = false;
                InventoryDataGridView.Columns["Status"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                InventoryDataGridView.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                InventoryDataGridView.Columns["Comment"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                InventoryDataGridView.Columns["CPU"].ReadOnly = true;

                
                
                DataGridViewButtonColumn OpenButtonColumn = new DataGridViewButtonColumn();
                OpenButtonColumn.Text = "Open";
                OpenButtonColumn.Name = "Open";
                OpenButtonColumn.UseColumnTextForButtonValue = true;
                OpenButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(OpenButtonColumn);

                DataGridViewTextBoxColumn UseAsColumn = new DataGridViewTextBoxColumn();
                UseAsColumn.Name = "UseAs";
                UseAsColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(UseAsColumn);


                DataGridViewTextBoxColumn UsageCostCentreColumn = new DataGridViewTextBoxColumn();
                UsageCostCentreColumn.Name = "CostCentre";
                UsageCostCentreColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(UsageCostCentreColumn);

                DataGridViewTextBoxColumn UsageUnitsColumn = new DataGridViewTextBoxColumn();
                UsageUnitsColumn.Name = "UsageUnits";
                UsageUnitsColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                
                InventoryDataGridView.Columns.Add(UsageUnitsColumn);

                DataGridViewButtonColumn UseButtonColumn = new DataGridViewButtonColumn();
                UseButtonColumn.Text = "Use";
                UseButtonColumn.Name = "Use";
                UseButtonColumn.UseColumnTextForButtonValue = true;
                UseButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(UseButtonColumn);

                DataGridViewButtonColumn FinishButtonColumn = new DataGridViewButtonColumn();
                FinishButtonColumn.Text = "Finish";
                FinishButtonColumn.Name = "Finish";
                FinishButtonColumn.UseColumnTextForButtonValue = true;
                FinishButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(FinishButtonColumn);

                DataGridViewButtonColumn FaultyButtonColumn = new DataGridViewButtonColumn();
                FaultyButtonColumn.Text = "Faulty";
                FaultyButtonColumn.Name = "Faulty";
                FaultyButtonColumn.UseColumnTextForButtonValue = true;
                FaultyButtonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

                InventoryDataGridView.Columns.Add(FaultyButtonColumn);

                SetColumnWidths(InventoryDataGridView);
            }
        }

        private void PrepareHistory()
        {
            HistoryAdapter = ReagentManagement.Reagent.GetDataAdaptor("reagent_inventory.Status = 3 OR reagent_inventory.Status = 4", DatabaseConfig.GetConnectionString());
            HistoryAdapter.Fill(HistoryTable);
            HistoryDataGridView.DataSource = HistoryTable;
            //HistoryDataGridView.ReadOnly = true;
            HistoryDataGridView.Columns["ItemID"].ReadOnly = true;
            HistoryDataGridView.Columns["Name"].ReadOnly = true;
            HistoryDataGridView.Columns["Manufacturer"].ReadOnly = true;
            HistoryDataGridView.Columns["Supplier"].ReadOnly = true;
            HistoryDataGridView.Columns["PartNumber"].ReadOnly = true;
            HistoryDataGridView.Columns["OrderDate"].ReadOnly = true;
            HistoryDataGridView.Columns["OrderedBy"].ReadOnly = true;
            HistoryDataGridView.Columns["ReceivedBy"].ReadOnly = true;
            HistoryDataGridView.Columns["CPU"].ReadOnly = true;
            SetColumnWidths(HistoryDataGridView);
        }

        private void NewCategoryCreateButton_Click(object sender, EventArgs e)
        {
            ReagentManagement.Category NewCategory = new ReagentManagement.Category(DatabaseConfig.GetConnectionString());
            NewCategory.Description = NewCategoryNameTextBox.Text;
            if ((string)NewCategoryParentComboBox.SelectedItem != "none"
                && (string)NewCategoryParentComboBox.SelectedItem != String.Empty
                && (string)NewCategoryParentComboBox.SelectedItem != null)
            {
                NewCategory.ParentCategoryID = CategoryList[(string)NewCategoryParentComboBox.SelectedItem];
            }
            else
            {
                NewCategory.ParentCategoryID = -1;
            }

            try
            {
                NewCategory.Write();
                if (NewCategory.ParentCategoryID == -1)
                {
                    AddStatusLabel(true, "New category " + NewCategory.Description + " successfully created.", ref CategoriesInfoPanel);
                    AddCategory(NewCategory, null);
                    NewCategoryParentComboBox.Items.Add(NewCategory.Description);
                    ReagentCategoryLinkCategoryComboBox.Items.Add(NewCategory.Description);
                }
                else
                {
                    ReagentManagement.Category Parent = new ReagentManagement.Category(NewCategory.ParentCategoryID, DatabaseConfig.GetConnectionString());
                    AddStatusLabel(true, "New category " + NewCategory.Description + " successfully created in "+Parent.Description + ".", ref CategoriesInfoPanel);
                    AddCategory(NewCategory, Parent.Description);
                    NewCategoryParentComboBox.Items.Add(NewCategory.Description + " (in " + Parent.Description + ")");
                    ReagentCategoryLinkCategoryComboBox.Items.Add(NewCategory.Description + " (in " + Parent.Description + ")");
                }
                PrepareCategories();

            }
            catch
            {
                AddStatusLabel(false,"An error occurred while creating category " + NewCategory.Description + ". This may be because a category of that name already exists.", ref CategoriesInfoPanel);
            }

        }

        private void ReagentCategoryLinkLinkButton_Click(object sender, EventArgs e)
        {
            
            if ((string)ReagentCategoryLinkCategoryComboBox.SelectedItem != "none"
                && (string)ReagentCategoryLinkCategoryComboBox.SelectedItem != String.Empty
                && (string)ReagentCategoryLinkCategoryComboBox.SelectedItem != null)
            {
                ReagentManagement.Category Category = new ReagentManagement.Category(CategoryList[(string)ReagentCategoryLinkCategoryComboBox.SelectedItem], DatabaseConfig.GetConnectionString());

                if (ReagentCategoryLinkReagentComboBox.SelectedValue != null)
                {
                    ReagentManagement.ReagentType ReagentType = new ReagentManagement.ReagentType(ReagentCategoryLinkReagentComboBox.SelectedValue.ToString(),DatabaseConfig.GetConnectionString());


                    try
                    {
                        Category.AddReagentType(ReagentType);
                        AddStatusLabel(true, ReagentType.Name + " added to " + Category.Description, ref CategoriesInfoPanel);
                    }
                    catch
                    {
                        AddStatusLabel(false, "An error occurred while adding " + ReagentType.Name + " to " + Category.Description, ref CategoriesInfoPanel);
                    }
                }
                else
                {
                    MessageBox.Show("You Must Select A Reagent Type");
                }
            }
            else
            {
                MessageBox.Show("You Must Select A Category");
            }


        }

        public void AddStatusLabel(bool Success, string Message, ref Panel DisplayPanel)
        {
                foreach (Control C in DisplayPanel.Controls)
                {
                    C.Top += 15;
                }

                Label StatusLabel = new Label();
                if (Success)
                {
                    StatusLabel.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    StatusLabel.ForeColor = System.Drawing.Color.Red;
                }
                StatusLabel.Text = Message;
                StatusLabel.AutoSize = true;
                StatusLabel.Top = 0;
                StatusLabel.Left = 15;
                DisplayPanel.Controls.Add(StatusLabel);
        }

        private void OrdersUpdateButton_Click(object sender, EventArgs e)
        {
            OrdersAdapter.Update(OrdersTable);
            OrdersUpdateButton.BackColor = System.Drawing.Color.Transparent;
            OrderFlashUpdateTimer.Stop();
        }

        private void OrdersRevertButton_Click(object sender, EventArgs e)
        {
            OrdersTable.Clear();
            OrdersAdapter.Fill(OrdersTable);
            OrdersUpdateButton.BackColor = System.Drawing.Color.Transparent;
            OrderFlashUpdateTimer.Stop();
        }

        private void InventoryUpdateButton_Click(object sender, EventArgs e)
        {
            InventoryAdapter.Update(InventoryTable);
            InventoryUpdateButton.BackColor = System.Drawing.Color.Transparent;
            InventoryFlashUpdateTimer.Stop();
        }

        private void InventoryRevertButton_Click(object sender, EventArgs e)
        {
            InventoryTable.Clear();
            InventoryAdapter.Fill(InventoryTable);
            InventoryUpdateButton.BackColor = System.Drawing.Color.Transparent;
            InventoryFlashUpdateTimer.Stop();
        }

        private void HistoryUpdateButton_Click(object sender, EventArgs e)
        {
            HistoryAdapter.Update(HistoryTable);
        }

        private void AdministrationExitButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void CostCentreSummaryDisplayButton_Click(object sender, EventArgs e)
        {
            string StartDate = CostCentreSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
            string EndDate = CostCentreSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
            string BillingStatus = "All";
            if (CostCentreSummaryUnbilledBillingStatusRadioButton.Checked)
            {
                BillingStatus = "UnBilled";
            }
            else if (CostCentreSummaryBilledBillingStatusRadioButton.Checked)
            {           
                BillingStatus = "Billed";
            }

            CostCentreSummaryDataGridView.Rows.Clear();

            List<object[]> CostCentreSummary = ReagentManagement.GetCostCentreSummary(StartDate, EndDate, BillingStatus, DatabaseConfig.GetConnectionString());
            decimal Total = 0;
            foreach (object [] ListItem in CostCentreSummary)
            {
                if (IsGuidRegex.IsMatch((string)ListItem[0]))
                {
                    Users.UserProfile User = new Users.UserProfile(new Guid((string)ListItem[0]),((CatalogForm)this.Owner).App);
                    ListItem[0] = User.FirstName + " " + User.LastName + "(" + ListItem[0] + ")";
                }
                CostCentreSummaryDataGridView.Rows.Add(ListItem);
                Total += (decimal)ListItem[2];
            }

            if (CostCentreSummary.Count > 0)
            {
                CostCentreSummaryDataGridView.Rows.Add(new object[] { "All","All", Total.ToString() });
            }
        }

        private void ReagentSummaryDisplayButton_Click(object sender, EventArgs e)
        {
            string StartDate = ReagentSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
            string EndDate = ReagentSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");

            Dictionary<string, Dictionary<string, decimal>> ReagentSummary = ReagentManagement.GetReagentSummary(StartDate, EndDate, DatabaseConfig.GetConnectionString());

            ReagentSummaryDataGridView.Rows.Clear();

            foreach (KeyValuePair<string, Dictionary<string, decimal>> RS in ReagentSummary)
            {
                foreach (KeyValuePair<string, decimal> RS2 in RS.Value)
                {
                    ReagentSummaryDataGridView.Rows.Add(new object[] { RS.Key, RS2.Key, RS2.Value });
                }
            }
         
        }

        private void DeleteCategoryButton_Click(object sender, EventArgs e)
        {   
            if ((string)DeleteCategoryComboBox.SelectedItem != String.Empty
                && (string)DeleteCategoryComboBox.SelectedItem != null)
            {
                ReagentManagement.Category Category = new ReagentManagement.Category(CategoryList[(string)DeleteCategoryComboBox.SelectedItem], DatabaseConfig.GetConnectionString());

                try
                {

                    if (Category.Delete())
                    {
                        AddStatusLabel(true, "Category " + Category.Description + " successfully deleted.", ref CategoriesInfoPanel);
                        CategoryList.Remove(Category.Description);
                        PrepareCategories();
                    }
                }
                catch
                {
                    AddStatusLabel(false, "An error occurred while deleteing category " + Category.Description + ".", ref CategoriesInfoPanel);
                }

            }
            else
            {
                MessageBox.Show("You Must Select a Category to Delete");
            }

            
        }

        private void ReagentCategoryLinkUnLinkButton_Click(object sender, EventArgs e)
        {
            if ((string)ReagentCategoryLinkCategoryComboBox.SelectedItem != "none"
                   && (string)ReagentCategoryLinkCategoryComboBox.SelectedItem != String.Empty
                   && (string)ReagentCategoryLinkCategoryComboBox.SelectedItem != null)
            {
                ReagentManagement.Category Category = new ReagentManagement.Category(CategoryList[(string)ReagentCategoryLinkCategoryComboBox.SelectedItem], DatabaseConfig.GetConnectionString());

                if (ReagentCategoryLinkReagentComboBox.SelectedValue != null)
                {
                    ReagentManagement.ReagentType ReagentType = new ReagentManagement.ReagentType(ReagentCategoryLinkReagentComboBox.SelectedValue.ToString(),DatabaseConfig.GetConnectionString());


                    try
                    {
                        Category.RemoveReagentType(ReagentType);
                        AddStatusLabel(true, ReagentType.Name + " removed from " + Category.Description, ref CategoriesInfoPanel);
                    }
                    catch
                    {
                        AddStatusLabel(false, "An error occurred while removing " + ReagentType.Name + " from" + Category.Description, ref CategoriesInfoPanel);
                    }
                }
                else
                {
                    MessageBox.Show("You Must Select A Reagent Type");
                }
            }
            else
            {
                MessageBox.Show("You Must Select A Category");
            }

        }

       
        private void CostCentreSummaryDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SignOutListTabPage_Click(object sender, EventArgs e)
        {
            
        }

        private void SignOutListDisplayButton_Click(object sender, EventArgs e)
        {
            SuppressCellChangedEvent = true;
            SignOutListFlashUpdateTimer.Stop();
            SignOutListUpdateButton.BackColor = System.Drawing.Color.Transparent;
            SignOutListTable.Rows.Clear();

            string StartDate = SignoutListStartDateCalendar.SelectionStart.ToString("MM/dd/yyyy");
            string EndDate = SignoutListEndDateCalendar.SelectionStart.ToString("MM/dd/yyyy");
            SignOutListAdapter = ReagentManagement.SignOutEvent.GetDataAdaptor(" Date > '" + StartDate + "' AND Date < '" + EndDate + "'", DatabaseConfig.GetConnectionString());

            SignOutListDataGridView.DataSource = SignOutListTable;
            SignOutListAdapter.Fill(SignOutListTable);

            if(!SignOutListTable.Columns.Contains("User Name")) SignOutListTable.Columns.Add(new DataColumn("User Name"));
            SuppressCellChangedEvent = true;
            if (!SignOutListTable.Columns.Contains("Reagent")) SignOutListTable.Columns.Add(new DataColumn("Reagent"));
            foreach (DataRow Row in SignOutListTable.Rows)
            {
                Users.UserProfile User = new Users.UserProfile((Guid)Row["UserID"], ((CatalogForm)this.Owner).App);
                SuppressCellChangedEvent = true;
                Row["User Name"] = User.FirstName + " " + User.LastName;
                ReagentManagement.Reagent R = new ReagentManagement.Reagent(Row["ItemID"].ToString(), DatabaseConfig.GetConnectionString());
                SuppressCellChangedEvent = true;                
                Row["Reagent"] = R.Name;
                if (IsGuidRegex.IsMatch((string)Row["CostCentreID"]))
                {
                    Users.UserProfile CostCentreUser = new Users.UserProfile(new Guid((string)Row["CostCentreID"]), ((CatalogForm)this.Owner).App);
                    Row["CostCentreID"] = CostCentreUser.FullName;
                }
            }

            SignOutListDataGridView.Columns["UserID"].Visible = false;
            SignOutListDataGridView.Columns["Reagent"].DisplayIndex = 2;
            SignOutListDataGridView.Columns["Reagent"].ReadOnly = true;
            SignOutListDataGridView.Columns["User Name"].DisplayIndex = 3;
            SignOutListDataGridView.Columns["User Name"].ReadOnly = true;
            if (SignOutListDataGridView.Columns.Contains("EventID")) SignOutListDataGridView.Columns["EventID"].ReadOnly = true; 
            if (SignOutListDataGridView.Columns.Contains("ItemID")) SignOutListDataGridView.Columns["ItemID"].ReadOnly = true; 
            if (SignOutListDataGridView.Columns.Contains("Date")) SignOutListDataGridView.Columns["Date"].ReadOnly = true;
            foreach (DataGridViewColumn C in SignOutListDataGridView.Columns)
            {
                C.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void SignOutListUpdateButton_Click(object sender, EventArgs e)
        {
            if (SignOutListAdapter != null)
            {
                SuppressCellChangedEvent = true;
                SignOutListAdapter.Update(SignOutListTable);
                SignOutListFlashUpdateTimer.Stop();
                SignOutListUpdateButton.BackColor = System.Drawing.Color.Transparent;
            }
        }

        private void CreateSetButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(SetNameTextBox.Text))
            {
                ReagentManagement.ReagentSet RS = new ReagentManagement.ReagentSet(DatabaseConfig.GetConnectionString());
                RS.Name = SetNameTextBox.Text;
                RS.Write();
                PrepareReagentSets();
            }
        }

        void EditSetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (EditSetComboBox.SelectedItem != null)
            {
                EditSetSourceDataGridView.Rows.Clear();
                EditSetDestinationDataGridView.Rows.Clear();
                List<ReagentManagement.ReagentType> RTs = ReagentManagement.ReagentType.GetReagentTypes(DatabaseConfig.GetConnectionString());
                foreach (ReagentManagement.ReagentType RT in RTs)
                {
                    EditSetSourceDataGridView.Rows.Add(RT.TypeID, RT.Name, RT.Manufacturer);
                }

                ReagentManagement.ReagentSet RS = (ReagentManagement.ReagentSet)EditSetComboBox.SelectedItem;
                
                foreach (KeyValuePair<ReagentManagement.ReagentType, decimal> RT in RS.Reagents)
                {
                    for (int i = EditSetSourceDataGridView.Rows.Count - 1; i >= 0; i--)
                    {

                        DataGridViewRow R = EditSetSourceDataGridView.Rows[i];
                        if ((string)R.Cells["EditSetSourceReagentTypeID"].Value == RT.Key.TypeID)
                        {
                            EditSetSourceDataGridView.Rows.RemoveAt(i);
                            DataGridViewTextBoxCell UnitsCell = new DataGridViewTextBoxCell();
                            UnitsCell.Value = RT.Value;
                            R.Cells.Add(UnitsCell);
                            EditSetDestinationDataGridView.Rows.Add(R);
                        }
                    }
                }


            }
        }

        private void PrepareReagentSets()
        {
            DeleteSetComboBox.DataSource = ReagentManagement.ReagentSet.GetSetList(DatabaseConfig.GetConnectionString());
            DeleteSetComboBox.DisplayMember = "Name";
            
            EditSetComboBox.DataSource = ReagentManagement.ReagentSet.GetSetList(DatabaseConfig.GetConnectionString());
            EditSetComboBox.DisplayMember = "Name";

            SetCategoryLinkSetComboBox.DataSource = ReagentManagement.ReagentSet.GetSetList(DatabaseConfig.GetConnectionString());
            SetCategoryLinkSetComboBox.DisplayMember = "Name";
        }

        private void AddSetReagentButton_Click(object sender, EventArgs e)
        {
            if (EditSetSourceDataGridView.SelectedCells.Count >= 1)
            {
                DataGridViewRow R = EditSetSourceDataGridView.Rows[EditSetSourceDataGridView.SelectedCells[0].RowIndex];
                EditSetSourceDataGridView.Rows.RemoveAt(R.Index);
                DataGridViewTextBoxCell UnitsCell = new DataGridViewTextBoxCell();
                UnitsCell.Value = 0;
                UnitsCell.ReadOnly = false;
                R.Cells.Add(UnitsCell);
                EditSetDestinationDataGridView.Rows.Add(R);
                
            }
            else
            {
                MessageBox.Show("You must select a reagent type from the source table.");
            }
        }

        private void RemoveSetReagentButton_Click(object sender, EventArgs e)
        {
            if (EditSetDestinationDataGridView.SelectedCells.Count >= 1)
            {
                DataGridViewRow R = EditSetDestinationDataGridView.Rows[EditSetDestinationDataGridView.SelectedCells[0].RowIndex];
                EditSetDestinationDataGridView.Rows.RemoveAt(R.Index);
                R.Cells.RemoveAt(R.Cells.Count - 1);
                EditSetSourceDataGridView.Rows.Add(R);
            }
            else
            {
                MessageBox.Show("You must select a reagent type to remove from the table.");
            }
        }

        private void UpdateSetReagentButton_Click(object sender, EventArgs e)
        {
            ReagentManagement.ReagentSet RS = (ReagentManagement.ReagentSet)EditSetComboBox.SelectedItem;

            Dictionary<ReagentManagement.ReagentType, decimal> ReagentList = new Dictionary<ReagentManagement.ReagentType, decimal>();
            
            foreach (DataGridViewRow R in EditSetDestinationDataGridView.Rows)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(R.Cells["EditSetDestinationUnits"].Value.ToString(), @"\d+(\.\d+)?"))
                {
                    decimal Units = decimal.Parse(R.Cells["EditSetDestinationUnits"].Value.ToString());
                    if (Units != 0)
                    {
                        ReagentList.Add(new ReagentManagement.ReagentType(R.Cells["EditSetDestinationReagentTypeID"].Value.ToString(),DatabaseConfig.GetConnectionString()), decimal.Parse(R.Cells["EditSetDestinationUnits"].Value.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("You have supplied a zero value for the units column of one or more reagents. Please supply a non-zero value.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("You have supplied a non-numeric value for the units column of one or more reagents. Please supply a numeric value.");
                    return;
                }
            }

            try
            {
                if (RS.LinkToReagents(ReagentList))
                {
                    MessageBox.Show("Reagent Set Successfully Updated.");
                }
                else
                {
                    MessageBox.Show("Reagent set update returned false. This may indcate a problem.");
                }
            }
            catch
            {
                MessageBox.Show("An error occurred while linking reagents to set.");            
            }
        }

        private void SetCategoryLinkLinkButton_Click(object sender, EventArgs e)
        {
            if ((string)SetCategoryLinkCategoryComboBox.SelectedItem != "none"
                && (string)SetCategoryLinkCategoryComboBox.SelectedItem != String.Empty
                && (string)SetCategoryLinkCategoryComboBox.SelectedItem != null)
            {
                ReagentManagement.Category Category = new ReagentManagement.Category(CategoryList[(string)SetCategoryLinkCategoryComboBox.SelectedItem], DatabaseConfig.GetConnectionString());

                if (SetCategoryLinkSetComboBox.SelectedItem != null)
                {
                    ReagentManagement.ReagentSet ReagentSet = (ReagentManagement.ReagentSet)SetCategoryLinkSetComboBox.SelectedItem;


                    try
                    {
                        Category.AddReagentSet(ReagentSet);
                        AddStatusLabel(true, ReagentSet.Name + " added to " + Category.Description, ref CategoriesInfoPanel);
                    }
                    catch
                    {
                        AddStatusLabel(false, "An error occurred while adding " + ReagentSet.Name + " to " + Category.Description, ref CategoriesInfoPanel);
                    }
                }
                else
                {
                    MessageBox.Show("You Must Select A Reagent Set");
                }
            }
            else
            {
                MessageBox.Show("You Must Select A Category");
            }
        }

        private void SetCategoryLinkUnLinkButton_Click(object sender, EventArgs e)
        {
            if ((string)SetCategoryLinkCategoryComboBox.SelectedItem != "none"
                && (string)SetCategoryLinkCategoryComboBox.SelectedItem != String.Empty
                && (string)SetCategoryLinkCategoryComboBox.SelectedItem != null)
            {
                ReagentManagement.Category Category = new ReagentManagement.Category(CategoryList[(string)SetCategoryLinkCategoryComboBox.SelectedItem], DatabaseConfig.GetConnectionString());

                if (SetCategoryLinkSetComboBox.SelectedItem != null)
                {
                    ReagentManagement.ReagentSet ReagentSet = (ReagentManagement.ReagentSet)SetCategoryLinkSetComboBox.SelectedItem;


                    try
                    {
                        Category.RemoveReagentSet(ReagentSet);
                        AddStatusLabel(true, ReagentSet.Name + " removed from " + Category.Description, ref CategoriesInfoPanel);
                    }
                    catch
                    {
                        AddStatusLabel(false, "An error occurred while removing " + ReagentSet.Name + " from " + Category.Description, ref CategoriesInfoPanel);
                    }
                }
                else
                {
                    MessageBox.Show("You Must Select A Reagent Bundle");
                }
            }
            else
            {
                MessageBox.Show("You Must Select A Category");
            }
        }

        private void DeleteSetButton_Click(object sender, EventArgs e)
        {
            if (DeleteSetComboBox.SelectedItem != null)
            {
                ReagentManagement.ReagentSet RS = (ReagentManagement.ReagentSet)DeleteSetComboBox.SelectedItem;

                try
                {
                    RS.Delete();
                    PrepareReagentSets();
                    AddStatusLabel(true, RS.Name + " Successfully Deleted", ref CategoriesInfoPanel);
                }
                catch
                {
                    MessageBox.Show("An Error Occurred While Deleting Bundle", "Delete Bundle Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You Must Select A Reagent Bundle");
            }
        }

        public static string AusToSQLDateConvert(string AusDate)
        {
            char[] DateSplitter = new char[] { '/', '\\', '-' };
            if (!String.IsNullOrEmpty(AusDate))
            {
                return AusDate.Split(DateSplitter)[1] + "/" + AusDate.Split(DateSplitter)[0] + "/" + AusDate.Split(DateSplitter)[2];
            }
            else
            {
                return String.Empty;
            }
        }


        internal static List<Guid> GetActiveUsers(string StartDate, string EndDate, string BillingStatus)
        {
            List<Guid> ReturnList = new List<Guid>();

            string SQL = "SELECT USERID ";
            SQL += " FROM ";
            SQL += " fn_GetActiveUsers(@StartDate,@EndDate, @BillingStatus)";

            List<Guid> UserIDList = new List<Guid>();

            using (SqlConnection Connection = new SqlConnection(DatabaseConfig.GetConnectionString()))
            {
                SqlCommand Command = new SqlCommand(SQL);
                Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
                Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
                switch (BillingStatus)
                {
                    case "All":
                        Command.Parameters.AddWithValue("@BillingStatus", DBNull.Value);
                        break;
                    case "Billed":
                        Command.Parameters.AddWithValue("@BillingStatus", 1);
                        break;
                    case "UnBilled":
                        Command.Parameters.AddWithValue("@BillingStatus", 0);
                        break;
                }
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(Reader.GetGuid(0));
                            }
                        }
                    }

                    return ReturnList;
                }
                catch (Exception Err)
                {
                    throw new Exception("An Error Occurred While Fetching Active User List", Err);
                }

            }
        }

        internal static List<object[]> GetCostCentreSummary(string StartDate, string EndDate, string BillingStatus)
        {
            string SQL = "SELECT DebtorCostCentre, CreditorCostCentre, SUM(Cost) FROM fn_GetCostCentreSummary(@StartDate,@EndDate,@BillingStatus)";
            SQL += " GROUP BY DebtorCostCentre, CreditorCostCentre";

            List<object[]> ReturnList = new List<object[]>();

            using (SqlConnection Connection = new SqlConnection(DatabaseConfig.GetConnectionString()))
            {
                SqlCommand Command = new SqlCommand(SQL);
                Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
                Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
                switch (BillingStatus)
                {
                    case "All":
                        Command.Parameters.AddWithValue("@BillingStatus", DBNull.Value);
                        break;
                    case "Billed":
                        Command.Parameters.AddWithValue("@BillingStatus", 1);
                        break;
                    case "UnBilled":
                        Command.Parameters.AddWithValue("@BillingStatus", 0);
                        break;
                }
                Command.Connection = Connection;

                try
                {
                    Connection.Open();
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(new object[] { Reader.GetString(0), Reader.GetString(1), Reader.GetDecimal(2) });
                            }
                            return ReturnList;
                        }
                        else
                        {
                            return ReturnList;
                        }
                    }
                }
                catch (Exception Err)
                {
                    throw new Exception("An Error Occurred While Fetching Cost Centre Summaries", Err);
                }
            }
        }

        internal class Charge
        {

            public int EventID { get; set; }
            public int ItemID { get; set; }
            public string Name { get; set; }
            public DateTime Date { get; set; }
            public string DebtorCostCentre { get; set; }
            public string CreditorCostCentre { get; set; }
            public decimal Quantity { get; set; }
            public decimal Cost { get; set; }
            public bool Billed { get; set; }

            public Charge()
            { }

            public Charge(int EventID, int ItemID, string Name, DateTime Date, string DebtorCostCentre, string CreditorCostCentre, decimal Quantity, decimal Cost, bool Billed)
            {
                this.EventID = EventID;
                this.ItemID = ItemID;
                this.Name = Name;
                this.Date = Date;
                this.DebtorCostCentre = DebtorCostCentre;
                this.CreditorCostCentre = CreditorCostCentre;
                this.Quantity = Quantity;
                this.Cost = Cost;
                this.Billed = Billed;
            }
        }

        internal static List<Charge> GetUserReagentCharges(Guid UserID, string StartDate, string EndDate, string BillingStatus)
        {

            List<Charge> ReturnList = new List<Charge>();
            try
            {
                using (SqlConnection Connection = new SqlConnection(DatabaseConfig.GetConnectionString()))
                {
                    string SQL = "SELECT EventID, ItemID, Name, [Date], DebtorCostCentre, CreditorCostCentre, Quantity, Cost, Billed FROM fn_GetUserCostDetails(@UserID, @StartDate, @EndDate, @BillingStatus)";

                    SqlCommand Command = new SqlCommand(SQL, Connection);
                    Command.Parameters.AddWithValue("@UserID", UserID);
                    Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
                    Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
                    if (BillingStatus == "All")
                    {
                        Command.Parameters.AddWithValue("@BillingStatus", DBNull.Value);
                    }
                    else if (BillingStatus == "Billed")
                    {
                        Command.Parameters.AddWithValue("@BillingStatus", 1);
                    }
                    if (BillingStatus == "UnBilled")
                    {
                        Command.Parameters.AddWithValue("@BillingStatus", 0);
                    }

                    Connection.Open();

                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                ReturnList.Add(new Charge(Reader.GetInt32(0), Reader.GetInt32(1), Reader.GetString(2), Reader.GetDateTime(3), Reader.GetString(4), Reader.GetString(5), Reader.GetDecimal(6), Reader.GetDecimal(7), Reader.GetBoolean(8)));
                            }
                        }
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("An error occurred while fetching user reagent charges", Err);
            }

            return ReturnList;
        }


        class UserReport
        {
            public string Title { get; set; }
            public Guid UserID {get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public List<string> CostCentres { get; set; }
            public DataTable SummaryTable { get; set; }
            public DataTable ReagentDetailTable { get; set; }
            public bool IsInternal { get; set; }

            public UserReport(Guid UserID, string StartDate, string EndDate, string BillingStatus, Users.ClientApplication App)
            {
                this.Title = App.Name + " Account (" + StartDate + "-" + EndDate + ")";

                this.UserID = UserID;

                this.StartDate = StartDate;
                this.EndDate = EndDate;

                IsInternal = !(new Users.UserProfile(UserID).IsExternalUser);

                CostCentres = new List<string>();

                SummaryTable = new DataTable();
                SummaryTable.Columns.Add("DebtorCostCentre");
                SummaryTable.Columns.Add("CreditorCostCentre");
                SummaryTable.Columns.Add("Amount");

                ReagentDetailTable = new DataTable();
                ReagentDetailTable.Columns.Add("ItemID");
                ReagentDetailTable.Columns.Add("ItemName");
                ReagentDetailTable.Columns.Add("Date");
                ReagentDetailTable.Columns.Add("DebtorCostCentre");
                ReagentDetailTable.Columns.Add("CreditorCostCentre");
                ReagentDetailTable.Columns.Add("Quantity");
                ReagentDetailTable.Columns.Add("Cost");

                PopulateReport(BillingStatus);
            }

            void PopulateReport(string BillingStatus)
            {
                Dictionary<string, decimal> SummaryDictionary = new Dictionary<string, decimal>();

                foreach (Charge C in GetUserReagentCharges(UserID, StartDate, EndDate, BillingStatus))
                {
                    ReagentDetailTable.Rows.Add(new object[] { C.ItemID, C.Name, C.Date, C.DebtorCostCentre, C.CreditorCostCentre, C.Quantity, C.Cost });

                    if (!SummaryDictionary.ContainsKey(C.DebtorCostCentre.Trim() + "/" + C.CreditorCostCentre.Trim()))
                    {
                        SummaryDictionary.Add(C.DebtorCostCentre.Trim() + "/" + C.CreditorCostCentre.Trim(), C.Cost);
                    }
                    else
                    {
                        SummaryDictionary[C.DebtorCostCentre.Trim() + "/" + C.CreditorCostCentre.Trim()] += C.Cost;
                    }

                    if (!CostCentres.Contains(C.DebtorCostCentre.Trim()))
                    {
                        CostCentres.Add(C.DebtorCostCentre.Trim());
                    }
                }

                foreach (KeyValuePair<string, decimal> KVP in SummaryDictionary)
                {
                    this.SummaryTable.Rows.Add(new object[] { KVP.Key.Split('/')[0], KVP.Key.Split('/')[1], KVP.Value });
                }
            }

            internal void Email(string SenderEmail)
            {
                if (ReagentDetailTable.Rows.Count == 0 && SummaryTable.Rows.Count == 0) return;
                Users.UserProfile User = new Users.UserProfile(UserID);
                string Content = "Dear " + User.FirstName + ",";
                Content += "<br/>";
                Content += "<br/>";
                Content += "Please find below an account of your service/reagent provision for the period of " + StartDate + " to " + EndDate + ". Please note that charges reflect services and consumables up until " + EndDate + " and may include part of an ongoing service, in which case the remaining charges for that service will appear in the following month's cost recovery.";
                Content += "<br/>";
                Content += "<br/>";

                if (!IsInternal)
                {
                    Content += "<table border=\"1\">";
                    Content += "<tr><td>Institute:</td><td>" + User.Lab.Institute.Name + "</td></tr>";
                    Content += "<tr><td>Lab:</td><td>" + User.Lab.Name + "</td></tr>";
                    Content += "<tr><td>Billing Address:</td><td>" + User.Lab.Institute.BillingAddress + "</td></tr>";
                    Content += "</table>";
                }
                else
                {
                    Content += "<table border=\"1\">";
                    Content += "<tr><td>Lab:</td><td>" + User.Lab.Name + "</td></tr>";
                    string CostCentreList = string.Empty;
                    foreach (string CC in this.CostCentres)
                    {
                        CostCentreList += CC + " ";
                    }
                    Content += "<tr><td>Cost Centres:</td><td>" + CostCentreList + "</td></tr>";
                    Content += "</table>";
                }


                Content += "<br/>";
                Content += "<br/>";


                Content += "<table border=\"1\">";
                Content += "<tr><td colspan=\"3\"><b>Summary</b></td></tr>";
                Content += "<tr><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Amount</td></tr>";
                foreach (DataRow R in this.SummaryTable.Rows)
                {
                    Content += "<tr>";
                    Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                    Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                    Content += "<td>" + R["Amount"] + "</td>";
                    Content += "</tr>";
                }
                Content += "</table>";

                Content += "<br/>";
                Content += "<br/>";

                Content += "<table border=\"1\">";
                Content += "<tr><th colspan=\"7\"><b>Reagents/Services</b></td></tr>";
                Content += "<tr><td colspan=\"7\"><b>Details</b></td></tr>";
                Content += "<tr><td>Item ID</td><td>Item Name</td><td>Date</td><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Quantity</td><td>Cost</td></tr>";
                foreach (DataRow R in this.ReagentDetailTable.Rows)
                {
                    Content += "<tr>";
                    Content += "<td>" + R["ItemID"] + "</td>";
                    Content += "<td>" + R["ItemName"] + "</td>";
                    Content += "<td>" + R["Date"] + "</td>";
                    Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                    Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                    Content += "<td>" + R["Quantity"] + "</td>";
                    Content += "<td>" + R["Cost"] + "</td>";
                    Content += "</tr>";
                }
                Content += "</table>";

                Content += "<br/>";
                Content += "<br/>";

                
                Content += "Please review the above list. Should there be any issues, please don't hesitate to contact us.";

                Content += "<br/>";
                Content += "<br/>";
                Content += "<i><b>**This is an automatically generated email**</b></i>";

                Users.SendMail(this.Title, User.Email, SenderEmail, Content, true);
            }
        }

        class MasterReport
        {
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public DataTable SummaryTable { get; set; }
            public List<UserReport> SubReports { get; set; }
            public string BillingStatus { get; set; }

            public Users.ClientApplication App { get; set; }

            public MasterReport(string StartDate, string EndDate, string BillingStatus, Users.ClientApplication App)
            {
                this.StartDate = StartDate;
                this.EndDate = EndDate;
                this.BillingStatus = BillingStatus;
                this.App = App;

                SummaryTable = new DataTable();
                SummaryTable.Columns.Add("DebtorCostCentre");
                SummaryTable.Columns.Add("CreditorCostCentre");
                SummaryTable.Columns.Add("Amount");

                SubReports = new List<UserReport>();

                PopulateReport();
            }

            private void PopulateReport()
            {
                foreach (Guid UserID in GetActiveUsers(StartDate, EndDate, BillingStatus))
                {
                    SubReports.Add(new UserReport(UserID, StartDate, EndDate, BillingStatus, App));
                }
            }

            public void Write(StreamWriter ReportWriter)
            {
                
                ReportWriter.WriteLine("Billing Period:\t" + StartDate + "-" + EndDate);
                ReportWriter.WriteLine("");
                ReportWriter.WriteLine("All Cost Centre Summary");
                ReportWriter.WriteLine("Debtor Cost Centre\tCreditorCostCentre\tAmount");

                foreach (object[] CostCentreDebt in GetCostCentreSummary(StartDate, EndDate, BillingStatus))
                {
                    if (IsGuidRegex.IsMatch(CostCentreDebt[0].ToString()))
                    {
                        try
                        {
                            CostCentreDebt[0] = new Users.UserProfile(new Guid(CostCentreDebt[0].ToString())).FullName;
                        }
                        catch
                        {
                            //ignore
                        }
                    }
                    ReportWriter.WriteLine(CostCentreDebt[0].ToString() + "\t" + CostCentreDebt[1].ToString() + "\t" + CostCentreDebt[2].ToString()); ;
                }

                ReportWriter.WriteLine("");
                ReportWriter.WriteLine("***********");
                ReportWriter.WriteLine("Per User Reports");
                ReportWriter.WriteLine("***********");
                ReportWriter.WriteLine("");

                foreach (UserReport R in SubReports)
                {
                    if (R.ReagentDetailTable.Rows.Count == 0 && R.SummaryTable.Rows.Count == 0) continue;
                    Users.UserProfile User = new Users.UserProfile(R.UserID);
                    ReportWriter.WriteLine("User:" + User.FullName);

                    if (!R.IsInternal)
                    {
                        ReportWriter.WriteLine("Institute:" + User.Lab.Institute.Name);
                        ReportWriter.WriteLine("Lab:" + User.Lab.Name);
                        ReportWriter.WriteLine("Billing Address:" + User.Lab.Institute.BillingAddress);
                    }
                    else
                    {
                        ReportWriter.WriteLine("Lab:" + User.Lab.Name);
                        string CostCentreList = string.Empty;
                        string CostCentres = string.Empty;
                        foreach (string CC in R.CostCentres)
                        {
                            if (IsGuidRegex.IsMatch(CC))
                            {
                                try
                                {
                                    CostCentres += (new Users.UserProfile(new Guid(CC))).FullName;
                                }
                                catch
                                {
                                    CostCentres += CC + " ";
                                }
                            }
                            else
                            {
                                CostCentres += CC + " ";
                            }
                        }
                        ReportWriter.WriteLine("CostCentres:" + CostCentres);
                    }

                    ReportWriter.WriteLine("");
                    ReportWriter.WriteLine("User Summary");
                    ReportWriter.WriteLine("Debtor Cost Centre\tCreditor Cost Centre\tAmount");
                    foreach (DataRow Rw in R.SummaryTable.Rows)
                    {
                        if (IsGuidRegex.IsMatch(Rw["DebtorCostCentre"].ToString()))
                        {
                            try
                            {
                                Rw["DebtorCostCentre"] = (new Users.UserProfile(new Guid(Rw["DebtorCostCentre"].ToString()))).FullName;
                            }
                            catch
                            {
                                //ignore
                            }
                        }

                        ReportWriter.WriteLine(Rw["DebtorCostCentre"] + "\t" + Rw["CreditorCostCentre"] + "\t" + Rw["Amount"]);
                    }

                    ReportWriter.WriteLine("");
                    ReportWriter.WriteLine("User Details");

                    if (R.ReagentDetailTable.Rows.Count > 0)
                    {
                        ReportWriter.WriteLine("Reagents/Library Prep/Microarray");
                        ReportWriter.WriteLine("Item ID\tItem Name\tDate\tDebtor Cost Centre\tCreditor Cost Centre\tQuantity\tCost");
                        foreach (DataRow Rw in R.ReagentDetailTable.Rows)
                        {
                            if (IsGuidRegex.IsMatch(Rw["DebtorCostCentre"].ToString()))
                            {
                                try
                                {
                                    Rw["DebtorCostCentre"] = (new Users.UserProfile(new Guid(Rw["DebtorCostCentre"].ToString()))).FullName;
                                }
                                catch
                                {
                                    //ignore
                                }
                            }
                            ReportWriter.WriteLine(Rw["ItemID"] + "\t" + Rw["ItemName"] + "\t" + Rw["Date"] + "\t" + Rw["DebtorCostCentre"] + "\t" + Rw["CreditorCostCentre"] + "\t" + Rw["Quantity"] + "\t" + Rw["Cost"]);
                        }
                        ReportWriter.WriteLine("");
                    }

                    ReportWriter.WriteLine("***********");
                    ReportWriter.WriteLine("");

                }
            }
        }

        private void ExportMasterReportButton_Click(object sender, EventArgs e)
        {
            string StartDate = CostCentreSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
            string EndDate = CostCentreSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
            string BillingStatus = "All";
            if (CostCentreSummaryUnbilledBillingStatusRadioButton.Checked)
            {
                BillingStatus = "UnBilled";
            }
            else if (CostCentreSummaryBilledBillingStatusRadioButton.Checked)
            {
                BillingStatus = "Billed";
            }

            ExportMasterFileDialogBox.FileName = "CostRecoveryReport_" + CostCentreSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy").Replace('/', '-') + "to" + CostCentreSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy").Replace('/', '-') + ".txt";
            if (ExportMasterFileDialogBox.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter ReportWriter = new StreamWriter(ExportMasterFileDialogBox.FileName))
                    {
                        MasterReport MR = new MasterReport(StartDate, EndDate, BillingStatus, ((CatalogForm)this.Owner).App);
                        MR.Write(ReportWriter);
                    }
                    if (DialogResult.Yes == MessageBox.Show("Master report complete. Do you wish to mark all items for this time period as billed?", "Mark Billed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        SetBilling(1, "All", "All", StartDate, EndDate);
                    }
                }
                catch (IOException Err)
                {
                    MessageBox.Show("An error occurred while writing to file.  Ensure file is not open or write protected. Full Error:" + Err.Message, "File IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception Err)
                {
                    MessageBox.Show("An unknown error occurred:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

        }

        private void SetBilling(int Billed, string DebtorCostCentre, string CreditorCostCentre, string StartDate, string EndDate)
        {
            try
            {
                using (SqlConnection ReagentConnection = new SqlConnection(DatabaseConfig.GetConnectionString()))
                {
                    ReagentConnection.Open();
                    SqlTransaction ReagentTransaction = ReagentConnection.BeginTransaction();

                    try
                    {
                        SetReagentsBillingStatus(Billed, DebtorCostCentre, CreditorCostCentre, StartDate, EndDate, ReagentConnection, ReagentTransaction);
                        ReagentTransaction.Commit();
                    }
                    catch (Exception Err)
                    {
                        ReagentTransaction.Rollback();
                        throw Err;
                    }
                }
            }
            catch (Exception Err)
            {
                throw new Exception("Marking Items as billed failed", Err);
            }

        }

        private void SetReagentsBillingStatus(int BillingStatus, string DebtorCostCentre, string CreditorCostCentre, string StartDate, string EndDate, SqlConnection Connection, SqlTransaction Transaction)
        {

            string SQL = "UPDATE ReagentSignOut SET Billed = @BillingStatus WHERE";
            SQL += " Date > @StartDate AND Date < @EndDate";
            if (DebtorCostCentre != "All")
            {
                SQL += " AND CostCentreID=@DebtorCostCentre";
            }

            if (CreditorCostCentre != "All")
            {
                SQL += " AND CreditorCostCentre=@CreditorCostCentre";
            }

            SqlCommand Command = new SqlCommand(SQL, Connection, Transaction);
            Command.Parameters.AddWithValue("@StartDate", AusToSQLDateConvert(StartDate));
            Command.Parameters.AddWithValue("@EndDate", AusToSQLDateConvert(EndDate));
            Command.Parameters.AddWithValue("@DebtorCostCentre", DebtorCostCentre);
            Command.Parameters.AddWithValue("@CreditorCostCentre", CreditorCostCentre);
            Command.Parameters.AddWithValue("@BillingStatus", BillingStatus);

            try
            {
                Command.ExecuteNonQuery();
            }
            catch (Exception Err)
            {
                throw new Exception("An error ocurred while updating reagent billing status", Err);
            }
        }

        private void ShowIDCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("ReagentTypeID")) ReagentTypesDataGridView.Columns["ReagentTypeID"].Visible = ShowIDCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("ItemID")) OrdersDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("ItemID")) InventoryDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("ItemID")) HistoryDataGridView.Columns["ItemID"].Visible = ShowIDCheckBox.Checked;            
        }

        private void ShowCommentCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("Comment")) OrdersDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Comment")) InventoryDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Comment")) HistoryDataGridView.Columns["Comment"].Visible = ShowCommentCheckBox.Checked;  
        }

        private void ShowFinishDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("FinishDate")) OrdersDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("FinishDate")) InventoryDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("FinishDate")) HistoryDataGridView.Columns["FinishDate"].Visible = ShowFinishDateCheckBox.Checked;            
        }

        private void ShowReceivedByCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("ReceivedBy")) OrdersDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("ReceivedBy")) InventoryDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("ReceivedBy")) HistoryDataGridView.Columns["ReceivedBy"].Visible = ShowReceivedByCheckBox.Checked;
        }

        private void ShowReceiveDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("ReceiveDate")) OrdersDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("ReceiveDate")) InventoryDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("ReceiveDate")) HistoryDataGridView.Columns["ReceiveDate"].Visible = ShowReceiveDateCheckBox.Checked;
        }

        private void ShowOrderedByCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("OrderedBy")) OrdersDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("OrderedBy")) InventoryDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("OrderedBy")) HistoryDataGridView.Columns["OrderedBy"].Visible = ShowOrderedByCheckBox.Checked;            
        }

        private void ShowOrderDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("OrderDate")) OrdersDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("OrderDate")) InventoryDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("OrderDate")) HistoryDataGridView.Columns["OrderDate"].Visible = ShowOrderDateCheckBox.Checked;            
        }

        private void ShowUnitsRemainingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("UnitsRemaining")) OrdersDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("UnitsRemaining")) InventoryDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("UnitsRemaining")) HistoryDataGridView.Columns["UnitsRemaining"].Visible = ShowUnitsRemainingCheckBox.Checked;            
        }

        private void ShowExpiryDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("ExpiryDate")) OrdersDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("ExpiryDate")) InventoryDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("ExpiryDate")) HistoryDataGridView.Columns["ExpiryDate"].Visible = ShowExpiryDateCheckBox.Checked;
        }

        private void ShowLotNumberCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("LotNumber")) OrdersDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("LotNumber")) InventoryDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("LotNumber")) HistoryDataGridView.Columns["LotNumber"].Visible = ShowLotNumberCheckBox.Checked;            
        }

        private void ShowUnitofSaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("UnitofSale")) ReagentTypesDataGridView.Columns["UnitofSale"].Visible = ShowUnitofSaleCheckBox.Checked;
        }

        private void ShowCPUCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("CPU")) ReagentTypesDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("CPU")) OrdersDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("CPU")) InventoryDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("CPU")) HistoryDataGridView.Columns["CPU"].Visible = ShowCPUCheckBox.Checked;            
        }

        private void ShowCostCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("Cost")) ReagentTypesDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("Cost")) OrdersDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Cost")) InventoryDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Cost")) HistoryDataGridView.Columns["Cost"].Visible = ShowCostCheckBox.Checked;            
        }

        private void ShowUnitsPerItemCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("UnitsPerItem")) ReagentTypesDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("UnitsPerItem")) OrdersDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("UnitsPerItem")) InventoryDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("UnitsPerItem")) HistoryDataGridView.Columns["UnitsPerItem"].Visible = ShowUnitsPerItemCheckBox.Checked;
        }

        private void ShowOrderThresholdCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("OrderThreshold")) ReagentTypesDataGridView.Columns["OrderThreshold"].Visible = ShowOrderThresholdCheckBox.Checked;
        }

        private void ShowStatusCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("Status")) ReagentTypesDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("Status")) OrdersDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Status")) InventoryDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Status")) HistoryDataGridView.Columns["Status"].Visible = ShowStatusCheckBox.Checked;
        }

        private void ShowLastOrderedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("LastOrdered")) ReagentTypesDataGridView.Columns["LastOrdered"].Visible = ShowLastOrderedCheckBox.Checked;
        }

        private void ShowPartNumberCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("PartNumber")) ReagentTypesDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("PartNumber")) OrdersDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("PartNumber")) InventoryDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("PartNumber")) HistoryDataGridView.Columns["PartNumber"].Visible = ShowPartNumberCheckBox.Checked;
        }

        private void ShowSupplierCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("Supplier")) ReagentTypesDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("Supplier")) OrdersDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Supplier")) InventoryDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Supplier")) HistoryDataGridView.Columns["Supplier"].Visible = ShowSupplierCheckBox.Checked;
        }

        private void ShowManufacturerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("Manufacturer")) ReagentTypesDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("Manufacturer")) OrdersDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Manufacturer")) InventoryDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Manufacturer")) HistoryDataGridView.Columns["Manufacturer"].Visible = ShowManufacturerCheckBox.Checked;
        }

        private void ShowNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("Name")) ReagentTypesDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
            if (OrdersDataGridView.Columns.Contains("Name")) OrdersDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("Name")) InventoryDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("Name")) HistoryDataGridView.Columns["Name"].Visible = ShowNameCheckBox.Checked;
        }

        private void ShowPCCCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OrdersDataGridView.Columns.Contains("PCC")) OrdersDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
            if (InventoryDataGridView.Columns.Contains("PCC")) InventoryDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
            if (HistoryDataGridView.Columns.Contains("PCC")) HistoryDataGridView.Columns["PCC"].Visible = ShowPCCCheckBox.Checked;
        }

        private void ShoeInfiniteResourceCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("InfiniteResource")) ReagentTypesDataGridView.Columns["InfiniteResource"].Visible = ShowInfiniteResourceCheckBox.Checked;
        }

        private void ShowDefaultPCCCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("DefaultPurchaseCostCentre")) ReagentTypesDataGridView.Columns["DefaultPurchaseCostCentre"].Visible = ShowDefaultPCCCheckBox.Checked;
        }

        private void ShowMarkUpCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReagentTypesDataGridView.Columns.Contains("MarkUp")) ReagentTypesDataGridView.Columns["MarkUp"].Visible = ShowMarkUpCheckBox.Checked;
        }
        private void UseAsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (InventoryDataGridView.Columns.Contains("UseAs")) InventoryDataGridView.Columns["UseAs"].Visible = ShowUseAsCheckBox.Checked;
        }



        private void EmailUserReportsButton_Click(object sender, EventArgs e)
        {
            DialogResult DR = MessageBox.Show("This will email summaries to all users with purchases in the specified time period. Do you wish to continue?", "Send Summaries?", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
            
            if (DR == DialogResult.Yes)
            { 
                string StartDate = CostCentreSummaryStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
                string EndDate = CostCentreSummaryEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy");
                string BillingStatus = "Any";
                if (CostCentreSummaryUnbilledBillingStatusRadioButton.Checked)
                {
                    BillingStatus = "UnBilled";
                }
                else if (CostCentreSummaryBilledBillingStatusRadioButton.Checked)
                {
                    BillingStatus = "Billed";
                }


                List<UserReport> UserReportsList = new List<UserReport>();

                List<Guid> ActiveUserIDList = ReagentManagement.SignOutEvent.GetActiveUsersList(StartDate, EndDate, DatabaseConfig.GetConnectionString());

                try
                {
                    foreach (Guid UserID in ActiveUserIDList)
                    {
                        UserReportsList.Add(new UserReport(UserID, StartDate, EndDate, BillingStatus, ((CatalogForm)this.Owner).App));
                    }
                }
                catch (Exception Err)
                { 
                    string ErrorMessage = "An error occurred while building user reports. Error: ";
                    while (Err != null)
                    {
                        ErrorMessage += Err.Message + ". ";
                        Err= Err.InnerException;
                    }
                    MessageBox.Show(ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    string SenderEmail = ((CatalogForm)this.Owner).ShoppingCart.User.Email;

                    foreach (UserReport UR in UserReportsList)
                    {
                        UR.Email(SenderEmail);
                    }
                }
                catch (Exception Err)
                {
                    string ErrorMessage = "An error occurred while emailing user reports. Error: ";
                    while (Err != null)
                    {
                        ErrorMessage += Err.Message + ". ";
                        Err = Err.InnerException;
                    }
                    MessageBox.Show(ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

        }

        private void DatabaseConfigEditButton_Click(object sender, EventArgs e)
        {
            DatabaseConfig DBConfig = new DatabaseConfig();
            DBConfig.ShowDialog(this);
        }

        private void SignoutListExportButton_Click(object sender, EventArgs e)
        {
            string StartDate = SignoutListStartDateCalendar.SelectionStart.ToString("MM/dd/yyyy");
            string EndDate = SignoutListEndDateCalendar.SelectionStart.ToString("MM/dd/yyyy");
            SignOutListAdapter = ReagentManagement.SignOutEvent.GetDataAdaptor(" Date > '" + StartDate + "' AND Date < '" + EndDate + "'", DatabaseConfig.GetConnectionString());
            DataTable SignOutTable = new DataTable();
            SignOutListAdapter.Fill(SignOutTable);

            if (!SignOutTable.Columns.Contains("User Name")) SignOutTable.Columns.Add(new DataColumn("User Name"));
            if (!SignOutTable.Columns.Contains("Reagent")) SignOutTable.Columns.Add(new DataColumn("Reagent"));
            foreach (DataRow Row in SignOutTable.Rows)
            {
                Users.UserProfile User = new Users.UserProfile((Guid)Row["UserID"], ((CatalogForm)this.Owner).App);
                SuppressCellChangedEvent = true;
                Row["User Name"] = User.FirstName + " " + User.LastName;
                ReagentManagement.Reagent R = new ReagentManagement.Reagent(Row["ItemID"].ToString(), DatabaseConfig.GetConnectionString());
                SuppressCellChangedEvent = true;
                Row["Reagent"] = R.Name;
                if (IsGuidRegex.IsMatch((string)Row["CostCentreID"]))
                {
                    Users.UserProfile CostCentreUser = new Users.UserProfile(new Guid((string)Row["CostCentreID"]), ((CatalogForm)this.Owner).App);
                    Row["CostCentreID"] = CostCentreUser.FirstName + " " + CostCentreUser.LastName;
                }
            }
            SignOutTable.Columns.Remove("UserID");
            SignOutTable.Columns["Reagent"].SetOrdinal(2);
            SignOutTable.Columns["User Name"].SetOrdinal(4);
            SignOutTable.Columns["Billed"].SetOrdinal(SignOutTable.Columns.Count-1);

            ExportSignoutListFileDialogBox.FileName = "SignoutList_" + SignoutListStartDateCalendar.SelectionStart.ToString("dd/MM/yyyy").Replace('/', '-') + "to" + SignoutListEndDateCalendar.SelectionStart.ToString("dd/MM/yyyy").Replace('/', '-') + ".txt";
            if (ExportSignoutListFileDialogBox.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter ReportWriter = new StreamWriter(ExportSignoutListFileDialogBox.FileName))
                    {
                        foreach (DataColumn C in SignOutTable.Columns)
                        {
                            ReportWriter.Write(C.ColumnName + "\t");
                        }
                        ReportWriter.Write("\r\n");

                        foreach (DataRow R in SignOutTable.Rows)
                        {
                            foreach (object V in R.ItemArray)
                            {
                                ReportWriter.Write(Convert.ToString(V) + "\t");
                            }
                            ReportWriter.Write("\r\n");
                        }
                    }                    
                }
                catch (IOException Err)
                {
                    MessageBox.Show("An error occurred while writing to file.  Ensure file is not open or write protected. Full Error:" + Err.Message, "File IO Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception Err)
                {
                    MessageBox.Show("An unknown error occurred:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }


        }

        private void btn_UserCreate_Click(object sender, EventArgs e)
        {
            if (tb_UserFirstName.Text != string.Empty && tb_UserLastName.Text != string.Empty && tb_UserEmail.Text != string.Empty)
            {
                Users.UserProfile NewUser = new Users.UserProfile();
                NewUser.FirstName = tb_UserFirstName.Text;
                NewUser.LastName = tb_UserLastName.Text;
                NewUser.Email = tb_UserEmail.Text;
                NewUser.Extension = tb_UserExtension.Text;
                NewUser.Lab = (Users.Laboratory)cmb_UserLab.SelectedItem;
                NewUser.IsLabHead = chk_IsLabHead.Checked;
                try
                {
                    NewUser.Write(Users.GetHash("password"));

                    MessageBox.Show("User profile successfully created. The password is set to 'password', set a custom password by selecting the user and using the update password function. Subscribe the user to an application using the add credentials function.", "Profile Created", MessageBoxButtons.OK);

                    ClearUserForm();
                    
                    PopulateUsers();

                }
                catch (Exception Err)
                {
                    MessageBox.Show("An error occurred while creating user. Error Message:" + Err.Message);
                }
            }

            else
            {
                MessageBox.Show("Please provide a first name, last name, and email address for the user", "Form Incomplete", MessageBoxButtons.OK);
            }
        }

        private void btn_UserUpdate_Click(object sender, EventArgs e)
        {
            Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;
            User.FirstName = tb_UserFirstName.Text;
            User.LastName = tb_UserLastName.Text;
            User.Email = tb_UserEmail.Text;
            User.Extension = tb_UserExtension.Text;
            User.Lab = (Users.Laboratory)cmb_UserLab.SelectedItem;
            User.IsLabHead = chk_IsLabHead.Checked;

            try
            {
                if (User.Update())
                {
                    MessageBox.Show("User profile successfully updated", "Profile Updated", MessageBoxButtons.OK);
                }
                else
                {
                    throw new Exception("Update call returned false");
                }

            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while updating user profile: " + Err.Message, "Error");
            }
        }

        private void ClearUserForm()
        {
            tb_UserFirstName.Text = string.Empty;
            tb_UserLastName.Text = string.Empty;
            tb_UserEmail.Text = string.Empty;
            tb_UserExtension.Text = string.Empty;
            tb_UserUpdatePassword.Text = string.Empty;
        }

        private void btn_UserDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deleting a user profile completely is not recommended. Doing so may create orphan records in subscriber applications. To unsubscribe a user from an application, delete the relevant credential. Do you wish to proceed with deletion?", "Delete User?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;
                try
                {
                    if (User.Delete())
                    {
                        MessageBox.Show("User profile successfully Deleted", "Profile Deleted", MessageBoxButtons.OK);
                        ClearUserForm();
                        cmb_UserSelect.SelectedIndex = 0;
                        pnl_UserCredentialsExisting.Controls.Clear();
                    }
                    else
                    {
                        throw new Exception("Update call returned false");
                    }
                    
                }
                catch (Exception Err)
                {
                    MessageBox.Show("An error occurred while deleting account. Error: " + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
             }
        }

        private void btn_UserUpdatePassword_Click(object sender, EventArgs e)
        {
            Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;
            try
            {
                if (User.ChangePassword(tb_UserUpdatePassword.Text))
                {
                    MessageBox.Show("Password successfully updated.");
                }
                else
                {
                    throw new Exception("Password update function returned false");
                }
            }
            catch(Exception Err)
            {
                MessageBox.Show("An error occurred while update password. Error message: " + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_CreateCredentialApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateRoles((Users.ClientApplication)(((ComboBox)sender).SelectedItem));
        }

        private void btn_UserCredentialAdd_Click(object sender, EventArgs e)
        {
            Users.Credential C = new global::Users.Credential(
                (Users.Laboratory)cmb_CreateCredentialLab.SelectedItem,                
                (Users.Role)cmb_CreateCredentialRole.SelectedItem,
                (Users.ClientApplication)cmb_CreateCredentialApplication.SelectedItem
                );

            Users.UserProfile User = (Users.UserProfile)cmb_UserSelect.SelectedItem;

            try
            {
                if (C.AddOrUpdate(User, false))
                {
                    User.Credentials.Add(C);
                    PopulateCredentials(User);
                }
                else
                {
                    throw new Exception("Add Credential function returned false");
                }

            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occured while adding credential. Message:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void cmb_CostCentreInstitute_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmb_CostCentreLab.DataSource = PrepareLabs(false, (Users.ResearchInstitute)cmb_CostCentreInstitute.SelectedItem);
            cmb_CostCentreLab.DisplayMember = "Name";
        }

        private void cmb_CostCentre_SelectedIndexChanged(object sender, EventArgs e)
        {
            Users.CostCentre C = (Users.CostCentre)cmb_CostCentre.SelectedItem;

            if (C.CostCentreID == "<New>")
            {
                cmb_CostCentreInstitute.SelectedIndex = 0;
                cmb_CostCentreLab.SelectedIndex = 0;
                tb_CostCentreCode.Text = string.Empty;
                tb_CostCentreDescription.Text = string.Empty;
                chkb_CostCentreRetired.Checked = false;
                btn_CostCentreCreate.Enabled = true;
                btn_CostCentreUpdate.Enabled = false; 
                btn_CostCentreDelete.Enabled = false;
            }
            else
            { 
                Users.Laboratory Lab = new Users.Laboratory(C.LabID);
                cmb_CostCentreInstitute.SelectedItem = Lab.Institute;
                cmb_CostCentreLab.SelectedItem = Lab;
                tb_CostCentreCode.Text = C.CostCentreID;
                tb_CostCentreDescription.Text = C.Description;
                btn_CostCentreCreate.Enabled = false;
                btn_CostCentreUpdate.Enabled = true;
                btn_CostCentreDelete.Enabled = true;
                chkb_CostCentreRetired.Checked = C.Retired;
            }
        }

        private void btn_CostCentreCreate_Click(object sender, EventArgs e)
        {
            Users.CostCentre NewCostCentre = new global::Users.CostCentre();
            NewCostCentre.CostCentreID = tb_CostCentreCode.Text;
            NewCostCentre.Description = tb_CostCentreDescription.Text;
            NewCostCentre.LabID = ((Users.Laboratory)cmb_CostCentreLab.SelectedItem).LabID;
            NewCostCentre.Retired = chkb_CostCentreRetired.Checked;

            try
            {
                if (NewCostCentre.Write())
                {
                    MessageBox.Show("Cost Centre successfully created.");
                    tb_CostCentreCode.Text = string.Empty;
                    tb_CostCentreDescription.Text = string.Empty;
                    chkb_CostCentreRetired.Checked = false;

                    PopulateCostCentres();
                }
                else
                {
                    throw new Exception("Cost centre write returned false");
                }

            }
            catch(Exception Err)
            {
                MessageBox.Show("An error occurred while creating cost centre. Message: " + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PopulateCostCentres()
        {
            List<Users.CostCentre> CCs = new List<Users.CostCentre>();
            Users.CostCentre NewCostCentre = new Users.CostCentre();
            NewCostCentre.CostCentreID = "<New>";
            CCs.Add(NewCostCentre);
            CCs.AddRange(Users.CostCentre.GetCostCentres());
            cmb_CostCentre.DataSource = CCs;
            cmb_CostCentre.DisplayMember = "CostCentreID";
        }

        private void btn_CostCentreUpdate_Click(object sender, EventArgs e)
        {
            Users.CostCentre C = (Users.CostCentre)cmb_CostCentre.SelectedItem;
            C.CostCentreID = tb_CostCentreCode.Text;
            C.Description = tb_CostCentreDescription.Text;
            C.LabID = ((Users.Laboratory)cmb_CostCentreLab.SelectedItem).LabID;
            C.Retired = chkb_CostCentreRetired.Checked;

            try
            {
                if (C.Update())
                {
                    MessageBox.Show("Cost centre successfully updated.");
                }
                else
                {
                    throw new Exception("Cost centre update returned false");
                }

            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while updating cost centre. Message: " + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_CostCentreDelete_Click(object sender, EventArgs e)
        {
            Users.CostCentre C = (Users.CostCentre)cmb_CostCentre.SelectedItem;
            
            try
            {
                if (C.Delete())
                {
                    MessageBox.Show("Cost centre successfully deleted.");
                    PopulateCostCentres();
                    cmb_CostCentre.SelectedIndex = 0;
                }
                else
                {
                    throw new Exception("Cost centre delete returned false");
                }

            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while updating cost centre. Message: " + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_LabSelect_SelectedIndexChanged(object sender, EventArgs e)
        {

            Users.Laboratory Lab = (Users.Laboratory)cmb_LabSelect.SelectedItem;
            if (Lab.Name == "<New>")
            {
                cmb_LabInstitute.SelectedIndex = 0;
                tb_LabName.Text = string.Empty;

                btn_LabCreate.Enabled = true;
                btn_LabUpdate.Enabled = false;
                btn_LabDelete.Enabled = false;
                pnl_LabMembers.Controls.Clear();
            }
            else
            {
                cmb_LabInstitute.SelectedItem = Lab.Institute;
                tb_LabName.Text = Lab.Name;

                btn_LabCreate.Enabled = false;
                btn_LabUpdate.Enabled = true;
                btn_LabDelete.Enabled = true;

                PopulateLabMemebers(Lab);
            }

        }

        private void PopulateLabMemebers(Users.Laboratory lab)
        {
            pnl_LabMembers.Controls.Clear();

            List<Users.UserProfile> PrimaryMemebers = lab.GetPrimaryMembers();
            List<Users.UserProfile> CredentialedUsers = lab.GetCredentialedUsers();

            int xPos = 10;
            int yPos = 10;
            int Increment = 25;

            Label lbl_PrimaryMembersHeader = new Label();
            lbl_PrimaryMembersHeader.Text = "Primary Members";
            lbl_PrimaryMembersHeader.Left = xPos;
            lbl_PrimaryMembersHeader.Top = yPos;
            pnl_LabMembers.Controls.Add(lbl_PrimaryMembersHeader);
            yPos += Increment;

            foreach (Users.UserProfile U in PrimaryMemebers)
            {
                Label lbl_UserName = new Label();
                lbl_UserName.Left = xPos;
                lbl_UserName.Top = yPos;
                lbl_UserName.Text = U.FullName;
                pnl_LabMembers.Controls.Add(lbl_UserName);
                yPos += Increment;
            }

            yPos += Increment;

            Label lbl_CredentialedUsersHeader = new Label();
            lbl_CredentialedUsersHeader.Text = "Credentialed Users";
            lbl_CredentialedUsersHeader.Left = xPos;
            lbl_CredentialedUsersHeader.Top = yPos;
            pnl_LabMembers.Controls.Add(lbl_CredentialedUsersHeader);
            yPos += Increment;

            foreach (Users.UserProfile U in CredentialedUsers)
            {
                Label lbl_UserName = new Label();
                lbl_UserName.Left = xPos;
                lbl_UserName.Top = yPos;
                lbl_UserName.Text = U.FullName;
                pnl_LabMembers.Controls.Add(lbl_UserName);
                yPos += Increment;
            }
        }

        private void btn_LabUpdate_Click(object sender, EventArgs e)
        {
            Users.Laboratory Lab = (Users.Laboratory)cmb_LabSelect.SelectedItem;
            Lab.Institute = (Users.ResearchInstitute)cmb_LabInstitute.SelectedItem;
            Lab.Name = tb_LabName.Text;

            try
            {
                Lab.Update();
                MessageBox.Show("Lab successfully updated.");
            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while updating lab. Message:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_LabDelete_Click(object sender, EventArgs e)
        {
            Users.Laboratory Lab = (Users.Laboratory)cmb_LabSelect.SelectedItem;

            int PrimaryMemberCount = Lab.GetPrimaryMembers().Count;
            int CredentialMemberCount = Lab.GetCredentialedUsers().Count;

            if (MessageBox.Show("This lab has " + PrimaryMemberCount.ToString() + " primary members and " + CredentialMemberCount.ToString() + " users with associated credentials. Deleting a lab with associated users will also delete those users. It is strongly recommended you do not delete a lab with associated users. Do you wish to continue with deletion?","WARNING!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            { 
                try
                {
                    Lab.Delete();
                    MessageBox.Show("Lab successfully updated.");
                    cmb_LabSelect.DataSource = PrepareLabs(true, null);
                    cmb_LabSelect.SelectedIndex = 0;
                }
                catch (Exception Err)
                {
                    MessageBox.Show("An error occurred while updating lab. Message:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void cmb_InstituteSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            Users.ResearchInstitute Institute = (Users.ResearchInstitute)cmb_InstituteSelect.SelectedItem;

            if (Institute.Name == "<New>")
            {
                tb_InstituteName.Text = string.Empty;
                tb_InstituteBillingAddress.Text = string.Empty;
                tb_InstituteDeliveryAddress.Text = string.Empty;
                chk_InstituteCurrentSite.Checked = false;
                btn_InstituteCreate.Enabled = true;
                btn_InstituteUpdate.Enabled = false;
                btn_InstituteDelete.Enabled = false;
            }
            else
            {
                tb_InstituteName.Text = Institute.Name;
                tb_InstituteBillingAddress.Text = Institute.BillingAddress;
                tb_InstituteDeliveryAddress.Text = Institute.DeliveryAddress;
                chk_InstituteCurrentSite.Checked = Institute.IsCurrentSite;
                btn_InstituteCreate.Enabled = false;
                btn_InstituteUpdate.Enabled = true;
                btn_InstituteDelete.Enabled = true;
            }
            
        }

        private void btn_InstituteCreate_Click(object sender, EventArgs e)
        {
            Users.ResearchInstitute Institute = new Users.ResearchInstitute();

            Institute.Name = tb_InstituteName.Text;
            Institute.BillingAddress = tb_InstituteBillingAddress.Text;
            Institute.DeliveryAddress = tb_InstituteDeliveryAddress.Text;
            Institute.IsCurrentSite = chk_InstituteCurrentSite.Checked;

            try
            {
                if (Institute.Write())
                {
                    MessageBox.Show("Institute successfully created");
                    tb_InstituteName.Text = string.Empty;
                    tb_InstituteBillingAddress.Text = string.Empty;
                    tb_InstituteDeliveryAddress.Text = string.Empty;
                    chk_InstituteCurrentSite.Checked = false;
                    PopulateInstitutes();
                }
                else
                {
                    throw new Exception("Institute writing function returned false");
                }

            }
            catch (Exception Err)
            {

                throw new Exception("An error occured while writing institute. Message: "+Err.Message);
            }


        }

        private void btn_InstituteUpdate_Click(object sender, EventArgs e)
        {
            Users.ResearchInstitute Institute = (Users.ResearchInstitute)cmb_InstituteSelect.SelectedItem;
            
            Institute.Name = tb_InstituteName.Text;
            Institute.BillingAddress = tb_InstituteBillingAddress.Text;
            Institute.DeliveryAddress = tb_InstituteDeliveryAddress.Text;
            Institute.IsCurrentSite = chk_InstituteCurrentSite.Checked;

            try
            {
                if (Institute.Update())
                {
                    MessageBox.Show("Institute successfully updated");
                }
                else
                {
                    throw new Exception("Institute update function returned false");
                }

            }
            catch (Exception Err)
            {
                throw new Exception("An error occured while updating institute. Message: " + Err.Message);
            }

        }

        private void btn_InstituteDelete_Click(object sender, EventArgs e)
        {
            Users.ResearchInstitute Institute = (Users.ResearchInstitute)cmb_InstituteSelect.SelectedItem;
            int AssociatedLabCount = Users.Laboratory.GetLabs(Institute.InstituteID).Count;

            if (AssociatedLabCount > 0)
            {
                MessageBox.Show("You cannot delete an institute with associated labs","Just can't do it, captain!",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                try
                {
                    if (Institute.Delete())
                    {
                        MessageBox.Show("Institute successfully deleted");
                        tb_InstituteName.Text = string.Empty;
                        tb_InstituteBillingAddress.Text = string.Empty;
                        tb_InstituteDeliveryAddress.Text = string.Empty;
                        chk_InstituteCurrentSite.Checked = false;
                        PopulateInstitutes();
                        cmb_InstituteSelect.SelectedIndex = 0;
                    }
                    else
                    {
                        throw new Exception("Institute deletion function returned false");
                    }

                }
                catch (Exception Err)
                {

                    throw new Exception("An error occured while deleting institute. Message: " + Err.Message);
                }
            }
        }

        private void tab_Users_Click(object sender, EventArgs e)
        {

        }

        private void groupBox15_Enter(object sender, EventArgs e)
        {

        }

        private void btn_LabCreate_Click(object sender, EventArgs e)
        {
            Users.Laboratory NewLab = new global::Users.Laboratory();

            NewLab.Name = tb_LabName.Text;
            NewLab.Institute = (Users.ResearchInstitute)cmb_LabInstitute.SelectedItem;

            try
            {
                NewLab.Write();
                MessageBox.Show("New Lab successfully created");
                cmb_LabSelect.DataSource = PrepareLabs(true, null);
                cmb_LabSelect.SelectedIndex = 0;
                tb_LabName.Text = string.Empty;
                
            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while writing lab", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tab_Config_Enter(object sender, EventArgs e)
        {
            try
            {
                if (!Settings.SettingExists("SMTPServer",Users.ConnectionString))
                {
                    Settings.AddSetting("SMTPServer", "Unspecified", Users.ConnectionString);
                }

                if (!Settings.SettingExists("SMTPPort", Users.ConnectionString))
                {
                    Settings.AddSetting("SMTPPort", "25", Users.ConnectionString);
                }

                tb_SMTPServer.Text = Settings.GetSetting("SMTPServer", Users.ConnectionString);
                tb_SMTPPort.Text = Settings.GetSetting("SMTPPort", Users.ConnectionString);
            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while fetching SMTP server settings. Message:" + Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_SMTPUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Settings.SetSetting("SMTPServer", tb_SMTPServer.Text, Users.ConnectionString);

                int Port = 0;
                if (Int32.TryParse(tb_SMTPPort.Text, out Port))
                {
                    Settings.SetSetting("SMTPPort", Port.ToString(), Users.ConnectionString);
                }
                else
                {
                    MessageBox.Show("The supplied SMTP port is not a valid number");
                }

                MessageBox.Show("SMTP settings updated");

            }
            catch (Exception Err)
            {
                MessageBox.Show("An error occurred while updating SMTP settings. Message: " + Err.Message);
            }
            
        }
    }

    internal class CostCentreReport
    {
        public string Title { get; set; }
        public Users.Laboratory Lab { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<string> CostCentres { get; set; }
        public DataTable CostCentreSummary { get; set; }
        public DataTable CostCentreDetails { get; set; }
        public bool IsMasterReport { get; set; }
        public bool IsInternal { get; set; }

        public CostCentreReport()
        {
            CostCentreSummary = new DataTable();
            CostCentreSummary.Columns.Add("DebtorCostCentre");
            CostCentreSummary.Columns.Add("CreditorCostCentre");
            CostCentreSummary.Columns.Add("Amount");

            CostCentreDetails = new DataTable();
            CostCentreDetails.Columns.Add("ItemName");
            CostCentreDetails.Columns.Add("UserName");
            CostCentreDetails.Columns.Add("Quantity");
            CostCentreDetails.Columns.Add("Date");
            CostCentreDetails.Columns.Add("DebtorCostCentre");
            CostCentreDetails.Columns.Add("CreditorCostCentre");
        }

        internal void Email(string RecipientEmail)
        {
            string Content = "Please find below cost recovery details for the period of " + StartDate + " to " + EndDate + ":";
            Content += "<br/>";
            Content += "<br/>";

            if (!IsInternal)
            {
                Content += "<table border=\"1\">";
                Content += "<tr><td>Institute:</td><td>" + this.Lab.Institute.Name + "</td></tr>";
                Content += "<tr><td>Lab:</td><td>" + this.Lab.Name + "</td></tr>";
                Content += "<tr><td>Billing Address:</td><td>" + this.Lab.Institute.BillingAddress + "</td></tr>";
                Content += "</table>";
            }
            else
            {
                Content += "<table border=\"1\">";
                Content += "<tr><td>Lab:</td><td>" + this.Lab.Name + "</td></tr>";
                string CostCentreList = string.Empty;
                foreach (string CC in this.CostCentres)
                {
                    CostCentreList += CC + " "; 
                }
                Content += "<tr><td>Cost Centres:</td><td>" + CostCentreList + "</td></tr>";
                Content += "</table>";
            }


            Content += "<br/>";
            Content += "<br/>";


            Content += "<table border=\"1\">";
            Content += "<tr><td colspan=\"3\"><b>Summary</b></td></tr>";
            Content += "<tr><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Amount</td></tr>";
            foreach (DataRow R in this.CostCentreSummary.Rows)
            {
                Content += "<tr>";
                Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                Content += "<td>" + R["Amount"] + "</td>";
                Content += "</tr>";
            }
            Content += "</table>";

            Content += "<br/>";
            Content += "<br/>";

            Content += "<table border=\"1\">";
            Content += "<tr><td colspan=\"6\"><b>Details</b></td></tr>";
            Content += "<tr><td>Item Name</td><td>User Name</td><td>Quantity</td><td>Date</td><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td></tr>";
            foreach (DataRow R in this.CostCentreDetails.Rows)
            {
                Content += "<tr>";
                Content += "<td>" + R["ItemName"] + "</td>";
                Content += "<td>" + R["UserName"] + "</td>";
                Content += "<td>" + R["Quantity"] + "</td>";
                Content += "<td>" + R["Date"] + "</td>";
                Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                Content += "</tr>";
            }
            Content += "</table>";
            
            Users.SendMail(this.Title, RecipientEmail, RecipientEmail, Content, true);
        }

        internal static void SendMasterReport(List<CostCentreReport> Reports, string EmailRecipient)
        {

            string Content = string.Empty;
            bool InternalContentAdded = false;
            bool ExternalContentAdded = false;

            Content += "<h2>Summary</h2>";

            string InternalReportsSummaryTable = "<table border=\"1\">";
            InternalReportsSummaryTable += "<tr><td colspan=\"4\"><b>Internal Cost Recovery Summary</b></td></tr>";
            InternalReportsSummaryTable += "<tr><td>Debtor Lab</td><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Amount</td></tr>";

            string ExternalReportsSummaryTable = "<table border=\"1\">";
            ExternalReportsSummaryTable += "<tr><td colspan=\"5\"><b>External Cost Recovery Summary</b></td></tr>";
            ExternalReportsSummaryTable += "<tr><td>Contact Name</td><td>Creditor Cost Centre</td><td>Amount</td><td>Institute</td><td>Billing Address</td></tr>";

            foreach (CostCentreReport CCR in Reports)
            {
                if (CCR.IsInternal)
                {
                    InternalContentAdded = true;
                    foreach (DataRow R in CCR.CostCentreSummary.Rows)
                    {

                        InternalReportsSummaryTable += "<tr>";
                        InternalReportsSummaryTable += "<td>" + CCR.Lab.Name + "</td>";
                        InternalReportsSummaryTable += "<td>" + R["DebtorCostCentre"] + "</td>";
                        InternalReportsSummaryTable += "<td>" + R["CreditorCostCentre"] + "</td>";
                        InternalReportsSummaryTable += "<td>" + R["Amount"] + "</td>";
                        InternalReportsSummaryTable += "</tr>";
                    }
                }
                else
                {
                    ExternalContentAdded = true;
                    bool InstituteInfoAdded = false;
                    foreach (DataRow R in CCR.CostCentreSummary.Rows)
                    {

                        ExternalReportsSummaryTable += "<tr>";
                        ExternalReportsSummaryTable += "<td>" + R["DebtorCostCentre"] + "</td>";
                        ExternalReportsSummaryTable += "<td>" + R["CreditorCostCentre"] + "</td>";
                        ExternalReportsSummaryTable += "<td>" + R["Amount"] + "</td>";
                        if (!InstituteInfoAdded)
                        {
                            ExternalReportsSummaryTable += "<td rowspan=\"" + CCR.CostCentreSummary.Rows.Count + "\">" + CCR.Lab.Institute.Name + "</td>";
                            ExternalReportsSummaryTable += "<td rowspan=\"" + CCR.CostCentreSummary.Rows.Count + "\">" + CCR.Lab.Institute.BillingAddress + "</td>";
                            InstituteInfoAdded = true;
                        }
                        ExternalReportsSummaryTable += "</tr>";
                    }
                }
            }
            InternalReportsSummaryTable += "</table>";
            ExternalReportsSummaryTable += "</table>";

            if (InternalContentAdded) Content += "<br/>" + InternalReportsSummaryTable;
            if (ExternalContentAdded) Content += "<br/>" + ExternalReportsSummaryTable;

            Content += "<br/><br/><h2>Details</h2>";

            foreach (CostCentreReport CCR in Reports)
            {
                Content += "<br/>-------------------------------------------<br/><br/>";

                if (CCR.IsInternal)
                {
                    Content += "Lab:<b>" + CCR.Lab.Name + "</b><br/>";
                }
                else
                {
                    Content += "Lab:<b>" + CCR.Lab.Name + "</b><br/>";
                    Content += "Institute:<b>" + CCR.Lab.Institute.Name + "</b><br/>";
                }

                Content += "<br/>";

                Content += "<table border=\"1\">";
                Content += "<tr><td colspan=\"6\"><b>Details</b></td></tr>";
                Content += "<tr><td>Item Name</td><td>User Name</td><td>Quantity</td><td>Date</td><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td></tr>";

                foreach (DataRow R in CCR.CostCentreDetails.Rows)
                {
                    Content += "<tr>";
                    Content += "<td>" + R["ItemName"] + "</td>";
                    Content += "<td>" + R["UserName"] + "</td>";
                    Content += "<td>" + R["Quantity"] + "</td>";
                    Content += "<td>" + R["Date"] + "</td>";
                    Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                    Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                    Content += "</tr>";
                }
                Content += "</table>";
            }

            Users.SendMail("Cost Recovery Master", EmailRecipient, EmailRecipient, Content, true);      
            
        }

        internal static string CompileMasterReport(List<CostCentreReport> Reports)
        {

            string Content = string.Empty;
            bool InternalContentAdded = false;
            bool ExternalContentAdded = false;

            Content += "Summary\n";

            string InternalReportsSummaryTable = "Internal Cost Recovery Summary\n";
            InternalReportsSummaryTable += "Debtor Lab\tDebtor Cost Centre\tCreditor Cost Centre\tAmount\n";

            string ExternalReportsSummaryTable = "External Cost Recovery Summary\n";
            ExternalReportsSummaryTable += "Contact Name\tCreditor Cost Centre\tAmount\tInstitute\tBilling Address\n";

            foreach (CostCentreReport CCR in Reports)
            {
                if (CCR.IsInternal)
                {
                    InternalContentAdded = true;
                    foreach (DataRow R in CCR.CostCentreSummary.Rows)
                    {

                        InternalReportsSummaryTable += CCR.Lab.Name + "\t";
                        InternalReportsSummaryTable += R["DebtorCostCentre"] + "\t";
                        InternalReportsSummaryTable += R["CreditorCostCentre"] + "\t";
                        InternalReportsSummaryTable += R["Amount"] + "\t";
                        InternalReportsSummaryTable += "\n";
                    }
                }
                else
                {
                    ExternalContentAdded = true;
                    bool InstituteInfoAdded = false;
                    foreach (DataRow R in CCR.CostCentreSummary.Rows)
                    {

                        ExternalReportsSummaryTable += R["DebtorCostCentre"] + "\t";
                        ExternalReportsSummaryTable += R["CreditorCostCentre"] + "\t";
                        ExternalReportsSummaryTable += R["Amount"] + "\t";
                        if (!InstituteInfoAdded)
                        {
                            ExternalReportsSummaryTable += CCR.Lab.Institute.Name + "\t";
                            ExternalReportsSummaryTable += CCR.Lab.Institute.BillingAddress + "\t";
                            InstituteInfoAdded = true;
                        }
                        ExternalReportsSummaryTable += "\n";
                    }
                }
            }

            if (InternalContentAdded) Content += "\n" + InternalReportsSummaryTable;
            if (ExternalContentAdded) Content += "\n" + ExternalReportsSummaryTable;

            Content += "\n\nDetails";

            foreach (CostCentreReport CCR in Reports)
            {
                Content += "\n-------------------------------------------\n\n";

                if (CCR.IsInternal)
                {
                    Content += "Lab:" + CCR.Lab.Name + "\n";
                }
                else
                {
                    Content += "Lab:" + CCR.Lab.Name + "\n";
                    Content += "Institute:" + CCR.Lab.Institute.Name + "\n";
                }

                Content += "\n";

                Content += "Details\t\n";
                Content += "Item Name\tUser Name\tQuantity\tDate\tDebtor Cost Centre\tCreditor Cost Centre\t\n";

                foreach (DataRow R in CCR.CostCentreDetails.Rows)
                {
                    Content += R["ItemName"] + "\t";
                    Content += R["UserName"] + "\t";
                    Content += R["Quantity"] + "\t";
                    Content += R["Date"] + "\t";
                    Content += R["DebtorCostCentre"] + "\t";
                    Content += R["CreditorCostCentre"] + "\t";
                    Content += "\n";
                }
            }

            return Content;
        }
    }

/*    internal class UserReport
    {
        public string Title { get; set; }
        public Users.UserProfile User { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<string> CostCentres { get; set; }
        public DataTable SummaryTable { get; set; }
        public DataTable DetailTable { get; set; }
        public bool IsInternal { get; set; }

        public UserReport(Guid UserID, string StartDate, string EndDate, string BillingStatus)
        {
            this.Title = ((CatalogForm)this.Owner).App + " Account (" + StartDate + "-" + EndDate + ")";

            Users.UserProfile User = new Users.UserProfile(UserID);

            this.StartDate = StartDate;
            this.EndDate = EndDate;

            IsInternal = !User.IsExternalUser;

            CostCentres = new List<string>();

            SummaryTable = new DataTable();
            SummaryTable.Columns.Add("DebtorCostCentre");
            SummaryTable.Columns.Add("CreditorCostCentre");
            SummaryTable.Columns.Add("Amount");

            DetailTable = new DataTable();
            DetailTable.Columns.Add("ItemName");
            DetailTable.Columns.Add("Quantity");
            DetailTable.Columns.Add("Date");
            DetailTable.Columns.Add("DebtorCostCentre");
            DetailTable.Columns.Add("CreditorCostCentre");
            DetailTable.Columns.Add("Cost");

            PopulateReport(BillingStatus);
        }

        private void PopulateReport(string BillingStatus)
        {

            Dictionary<string, decimal> SummaryDictionary = new Dictionary<string, decimal>();

            foreach (ReagentManagement.SignOutEvent SE in ReagentManagement.SignOutEvent.GetSignoutHistory(this.User.UserID, StartDate, EndDate, BillingStatus, DatabaseConfig.GetConnectionString()))
            {
                DetailTable.Rows.Add(new object[] { SE.Item.Name, SE.Quantity, SE.Date, SE.CostCentreID, SE.CreditorCostCentre, (SE.Item.PurchaseCostPerUnit*SE.Quantity) });
                
                if(!SummaryDictionary.ContainsKey(SE.CostCentreID+"/"+SE.CreditorCostCentre))
                {
                    SummaryDictionary.Add(SE.CostCentreID + "/" + SE.CreditorCostCentre, (SE.Item.PurchaseCostPerUnit * SE.Quantity));                
                }
                else
                {
                    SummaryDictionary[SE.CostCentreID + "/" + SE.CreditorCostCentre] += (SE.Item.PurchaseCostPerUnit * SE.Quantity);                
                }

                if (!CostCentres.Contains(SE.CostCentreID))
                {
                    CostCentres.Add(SE.CostCentreID);
                }
            }

            foreach (KeyValuePair<string, decimal> KVP in SummaryDictionary)
            {
                SummaryTable.Rows.Add(new object[] { KVP.Key.Split('/')[0], KVP.Key.Split('/')[1], KVP.Value });
            }
        }


        internal void Email(string SenderEmail)
        {
            if (DetailTable.Rows.Count == 0 && SummaryTable.Rows.Count == 0) return;

            string Content = "Dear " + this.User.FirstName + ",";
            Content += "<br/>";
            Content += "<br/>";
            Content += "Please find below an account of your service/reagent provision for the period of " + StartDate + " to " + EndDate + ":";
            Content += "<br/>";
            Content += "<br/>";

            if (!IsInternal)
            {
                Content += "<table border=\"1\">";
                Content += "<tr><td>Institute:</td><td>" + this.User.Lab.Institute.Name + "</td></tr>";
                Content += "<tr><td>Lab:</td><td>" + this.User.Lab.Name + "</td></tr>";
                Content += "<tr><td>Billing Address:</td><td>" + this.User.Lab.Institute.BillingAddress + "</td></tr>";
                Content += "</table>";
            }
            else
            {
                Content += "<table border=\"1\">";
                Content += "<tr><td>Lab:</td><td>" + this.User.Lab.Name + "</td></tr>";
                string CostCentreList = string.Empty;
                foreach (string CC in this.CostCentres)
                {
                    CostCentreList += CC + " "; 
                }
                Content += "<tr><td>Cost Centres:</td><td>" + CostCentreList + "</td></tr>";
                Content += "</table>";
            }


            Content += "<br/>";
            Content += "<br/>";


            Content += "<table border=\"1\">";
            Content += "<tr><td colspan=\"3\"><b>Summary</b></td></tr>";
            Content += "<tr><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Amount</td></tr>";
            foreach (DataRow R in this.SummaryTable.Rows)
            {
                Content += "<tr>";
                Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                Content += "<td>" + R["Amount"] + "</td>";
                Content += "</tr>";
            }
            Content += "</table>";

            Content += "<br/>";
            Content += "<br/>";

            Content += "<table border=\"1\">";
            Content += "<tr><td colspan=\"6\"><b>Details</b></td></tr>";
            Content += "<tr><td>Item Name</td><td>Quantity</td><td>Date</td><td>Debtor Cost Centre</td><td>Creditor Cost Centre</td><td>Cost</td></tr>";
            foreach (DataRow R in this.DetailTable.Rows)
            {
                Content += "<tr>";
                Content += "<td>" + R["ItemName"] + "</td>";
                Content += "<td>" + R["Quantity"] + "</td>";
                Content += "<td>" + R["Date"] + "</td>";
                Content += "<td>" + R["DebtorCostCentre"] + "</td>";
                Content += "<td>" + R["CreditorCostCentre"] + "</td>";
                Content += "<td>" + R["Cost"] + "</td>";
                Content += "</tr>";
            }
            Content += "</table>";

            Content += "<br/>";
            Content += "<br/>";
            Content += "Please review the above list. Should there be any issues, please don't hesitate to contact us.";

            Content += "<br/>";
            Content += "<br/>";
            Content += "<i><b>**This is an automatically generated email**</b></i>";

            pmci_users.PMCI_UsersSoapClient PersonnelTools = new ReagentSignout.pmci_users.PMCI_UsersSoapClient();
            PersonnelTools.SendMail(this.Title, this.User.Email, SenderEmail, Content, true);
        }
    }
*/
}
