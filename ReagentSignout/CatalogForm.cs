﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ReagentSignout
{
     
    public partial class CatalogForm : Form
    {
        public enum Roles
        {
            Administrator = 1,
            User = 2
        }

        public ReagentManagement.ShoppingCart ShoppingCart;
        public ReagentManagement.Category CurrentCategory;
        public Users.ClientApplication App;
        SignInForm Login;
        AdministrationForm Admin;
        
        public CatalogForm()
        {
            DatabaseConfig.LoadConfig();
            if (System.Globalization.CultureInfo.CurrentCulture.Name != "en-AU" && System.Globalization.CultureInfo.CurrentCulture.Name != "da-DK")
            {
                MessageBox.Show("Your region and date/time configuration is not set to Australian(English) or Denmark (Danish). Adjust your region and language setting and restart the application.");
                Environment.Exit(0);
            }


            Exception Err;
            if (!DatabaseConfig.Test(out Err))
            {
                string ErrorMessage = "An Error Occurred While Testing Database Connection. Error Stack: ";
                while (Err != null)
                {
                    ErrorMessage += Err.Message + "; ";
                    Err = Err.InnerException;
                }

                ErrorMessage = ErrorMessage.TrimEnd(new char[] { ';', ' ' }) + " \n\n Click OK to display database configuration or cancel to exit.";

                if (DialogResult.Cancel == MessageBox.Show(ErrorMessage, "Initialisation Error", MessageBoxButtons.OKCancel))
                {
                    Environment.Exit(0);
                }
                else
                {
                    DatabaseConfig DBConfig = new DatabaseConfig();
                    DBConfig.ShowDialog(this);
                }
            }

            Users.ConnectionString = DatabaseConfig.GetUsersConnectionString();

            this.App = new Users.ClientApplication(DatabaseConfig.ApplicationName);

            try
            {                
                this.Text = App.Description;
            }
            catch (Exception Error)
            {

                MessageBox.Show("An error occurred while accessing application details: " + Error.Message+". \n\n Application will exit.", "Initialisation Error", MessageBoxButtons.OK);
                Environment.Exit(0);

            }

            
            

            this.FormClosing += new FormClosingEventHandler(CatalogForm_FormClosing);

            Timer ExpiryDateCheckTimer = new Timer();
            ExpiryDateCheckTimer.Interval = 86400000; //1 Day
            ExpiryDateCheckTimer.Tick += new EventHandler(ExpiryDateCheckTimer_Tick);
            ExpiryDateCheckTimer.Start();

            Admin = new AdministrationForm();
            Admin.App = App;
            Admin.Hide();

            InitializeComponent();
            this.ResizeEnd += new EventHandler(CatalogForm_ResizeEnd);

            Login = new SignInForm();
            Login.FormClosing += new FormClosingEventHandler(Login_FormClosing);
            Login.ShowDialog(this);
            ShoppingCartDataGrid.CellClick += new DataGridViewCellEventHandler(ShoppingCartDataGrid_CellClick);
            ShoppingCartDataGrid.CellEndEdit += new DataGridViewCellEventHandler(ShoppingCartDataGrid_CellEndEdit);
            PopulateTopLevelCategories();
            //SetPermissions();

        }

        

        void CatalogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Users.Role Role = new Users.Role(Users.Role.GetIDFromName("Administrator"));
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (!App.UserHasRole(this.ShoppingCart.User, Role))
                {
                    MessageBox.Show("You are not authorised to exit the application");
                    e.Cancel = true;
                }
            }
        }

       

        public void PopulateRecentPurchases()
        {

            Size ButtonSize = new Size(100, 100);
            Font ButtonFont = new Font("Arial", 11);
            int VerticalOffset = 20;
            int HorizontalOffset = 20;
            int Spacing = 10;
            RecentItemGroupBox.Controls.Clear();

            List<ReagentManagement.ReagentType> RecentItems = ReagentManagement.ReagentType.GetRecentPurchaseItems(ShoppingCart.User.UserID, 60, DatabaseConfig.GetConnectionString());
            foreach (ReagentManagement.ReagentType ReagentType in RecentItems)
            {
                Button ReagentButton = new Button();
                if (File.Exists(".\\images\\" + ReagentType.TypeID + ".jpg"))
                {
                    Image ButtonImage = Image.FromFile(".\\images\\" + ReagentType.TypeID + ".jpg");

                    if (ButtonImage.Height == 100 || ButtonImage.Width == 100)
                    {
                        ReagentButton.BackgroundImage = ButtonImage;
                    }
                    else
                    {
                        MessageBox.Show("Product Image " + ReagentType.TypeID + ".jpg is not 100x100 pixels, resize or delete the image to stop seeing this message.");
                        ReagentButton.Text = ReagentButton.Name + " (Unit: " + ReagentType.UnitOfSale + ")";
                    }

                }
                else
                {
                    ReagentButton.Text = ReagentType.Name + " (Unit: " + ReagentType.UnitOfSale + ")";
                }
                ReagentButton.Font = ButtonFont;
                ReagentButton.BackColor = System.Drawing.Color.Ivory;
                ReagentButton.Size = ButtonSize;
                ReagentButton.Top = VerticalOffset;
                ReagentButton.Left = HorizontalOffset;
                HorizontalOffset += ButtonSize.Width + Spacing;
                //if (ChildReagent.Icon != null)
                //{
                //    ReagentButton.Image = ChildReagent.Icon;
                //}
                ReagentButton.Tag = ReagentType;
                ReagentButton.Click += new EventHandler(ReagentButton_Click);
                RecentItemGroupBox.Controls.Add(ReagentButton);
                
            }

        }

        void ExpiryDateCheckTimer_Tick(object sender, EventArgs e)
        {
            DateTime LastExpiryCheck = new DateTime();
            try
            {
                LastExpiryCheck = DateTime.Parse(Settings.GetSetting("LastExpiryCheck", DatabaseConfig.GetConnectionString()));
            }
            catch(Exception Err)
            {
                if (Err.InnerException != null && Err.InnerException.Message == "Supplied SettingName returned no rows")
                {
                    Settings.AddSetting("LastExpiryCheck", DateTime.Now.ToString(), DatabaseConfig.GetConnectionString());
                    LastExpiryCheck = DateTime.Now;
                }
                else
                { throw Err; }
            }
            DateTime CurrentDateTime = DateTime.Now;
            TimeSpan TimeSinceCheck = CurrentDateTime.Subtract(LastExpiryCheck);
            if (TimeSinceCheck.Days > 30)
            {
                ReagentManagement.CheckExpiryDates(CurrentDateTime.AddMonths(1), DatabaseConfig.GetConnectionString(), DatabaseConfig.ApplicationName);
                Settings.SetSetting("LastExpiryCheck", CurrentDateTime.ToString(), DatabaseConfig.GetConnectionString());         
            }
            
        }

        public void SetPermissions()
        {
            foreach (Users.Credential C in this.ShoppingCart.User.Credentials)
            {
                if (C.UserRole.RoleID == ((int)Roles.Administrator))
                {
                    AdministrationButton.Visible = true;
                    ExitButton.Visible = true;
                    break;
                }
                else
                {
                    AdministrationButton.Visible = false;
                    ExitButton.Visible = false;
                }
            }
            
        }

        void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        void CatalogForm_ResizeEnd(object sender, EventArgs e)
        {
            if (CurrentCategory == null)
            {
                PopulateTopLevelCategories();
            }
            else
            {
                CatalogPanel.Controls.AddRange(MakeButtons(CurrentCategory));                
            }
        }

        void ShoppingCartDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int ItemNameColumnIndex = ShoppingCartDataGrid.Columns["Item"].Index;
            int ItemIDsColumnIndex = ShoppingCartDataGrid.Columns["ItemIDs"].Index;
            int QuantityColumnIndex = ShoppingCartDataGrid.Columns["Quantity"].Index;
            int RemoveButtonColumnIndex = ShoppingCartDataGrid.Columns["Remove"].Index;

            // find out which column was clicked

            if (e.ColumnIndex == RemoveButtonColumnIndex)
            {
                //get the value which you want to display
                string ItemName = (string)ShoppingCartDataGrid.Rows[e.RowIndex].Cells[ItemNameColumnIndex].Value;
                decimal OrderUnits = decimal.Parse(ShoppingCartDataGrid.Rows[e.RowIndex].Cells[QuantityColumnIndex].Value.ToString());
                if (OrderUnits % 1 == 0)
                {
                    ShoppingCart.Remove(ItemName, 1);
                }
                else
                {
                    ShoppingCart.Remove(ItemName, OrderUnits % 1);                
                }
                FillShoppingCartDataGrid();
            }
            

        }

        void ShoppingCartDataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int ItemTypeColumnIndex = ShoppingCartDataGrid.Columns["Item"].Index;
            int ItemIDsColumnIndex = ShoppingCartDataGrid.Columns["ItemIDs"].Index;
            if (ShoppingCartDataGrid.Rows[e.RowIndex].Cells[ItemIDsColumnIndex].Value == null) return;

            if (e.ColumnIndex == ItemIDsColumnIndex)
            {
                string ItemTypeName = ShoppingCartDataGrid.Rows[e.RowIndex].Cells[ItemTypeColumnIndex].Value.ToString();

                int ItemCartIndex = -1;
                for (int i = 0; i < ShoppingCart.Items.Count; i++)
                {
                    if (ShoppingCart.Items[i].ItemType.Name == ItemTypeName)
                    {
                        ItemCartIndex = i;
                        break;
                    }
                }

                string SuppliedItemIDs = ShoppingCartDataGrid.Rows[e.RowIndex].Cells[ItemIDsColumnIndex].Value.ToString();
                
                if(!string.IsNullOrEmpty(SuppliedItemIDs))
                {
                    ShoppingCart.Items[ItemCartIndex].ItemIDs.Clear();

                    foreach (string ItemID in SuppliedItemIDs.Split(','))
                    {
                        ReagentManagement.Reagent R = new ReagentManagement.Reagent(ItemID, DatabaseConfig.GetConnectionString());
                        if (R.Name == ShoppingCart.Items[ItemCartIndex].ItemType.Name)
                        {
                            ShoppingCart.Items[ItemCartIndex].ItemIDs.Add(ItemID);
                        }
                        else
                        {
                            ShoppingCart.Items[ItemCartIndex].ItemIDs.Clear();
                            MessageBox.Show("One or more of the ItemIDs you supplied do not belong to items of the type specified.", "Bad ItemID", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                }
            }
        }

        private void PopulateTopLevelCategories()
        {
            CatalogPanel.Controls.Clear();
            List<ReagentManagement.Category> TopLevel = ReagentManagement.Category.GetTopLevelCategories(DatabaseConfig.GetConnectionString());
            Size ButtonSize = new Size(100,100);
            Font ButtonFont = new Font("Arial", 11);
            int VerticalOffset = 50;
            int HorizontalOffset = 50;
            int Spacing = 10;
            int MaxButtonCount = (int)Math.Floor((double)((CatalogPanel.ClientSize.Width - (2 * HorizontalOffset)) / (ButtonSize.Width + Spacing)));
            int ButtonCount = 0;
            foreach (ReagentManagement.Category Category in TopLevel)
            {
                Button CategoryButton = new Button();
                CategoryButton.Font = ButtonFont;
                CategoryButton.Size = ButtonSize;
                CategoryButton.Top = VerticalOffset;
                CategoryButton.Left = HorizontalOffset;
                HorizontalOffset += ButtonSize.Width + Spacing;
                CategoryButton.Text = Category.Description;
                if (Category.Icon != null)
                {
                    CategoryButton.Image = Category.Icon;
                }
                CategoryButton.Tag = Category;
                CategoryButton.Click += new EventHandler(CategoryButton_Click);
                CatalogPanel.Controls.Add(CategoryButton);
                if (++ButtonCount >= MaxButtonCount)
                {
                    VerticalOffset += ButtonSize.Height + Spacing;
                    HorizontalOffset = 50;
                    ButtonCount = 0;
                }
            }
        }

        private Button[] MakeButtons(ReagentManagement.Category Category)
        {
            List<Button> ReturnList = new List<Button>();
            Size ButtonSize = new Size(100, 100);
            Font ButtonFont = new Font("Arial", 11);
            int VerticalOffset = 50;
            int HorizontalOffset = 50;
            int Spacing = 10;
            int MaxButtonCount = (int)Math.Floor((double)((CatalogPanel.ClientSize.Width - (2 * HorizontalOffset)) / (ButtonSize.Width + Spacing)));
            int ButtonCount = 0;
            CatalogPanel.Controls.Clear();
            CurrentCategory = Category;
            List<ReagentManagement.Category> ChildCategories = Category.GetChildCategories();
            foreach (ReagentManagement.Category ChildCategory in ChildCategories)
            {
                Button CategoryButton = new Button();
                CategoryButton.BackColor = System.Drawing.Color.LightSteelBlue;
                CategoryButton.Font = ButtonFont;
                CategoryButton.Size = ButtonSize;
                CategoryButton.Top = VerticalOffset;
                CategoryButton.Left = HorizontalOffset;
                HorizontalOffset += ButtonSize.Width + Spacing;
                CategoryButton.Text = ChildCategory.Description;
                if (ChildCategory.Icon != null)
                {
                    CategoryButton.Image = ChildCategory.Icon;
                }
                CategoryButton.Tag = ChildCategory;
                CategoryButton.Click += new EventHandler(CategoryButton_Click);
                ReturnList.Add(CategoryButton);
                if (++ButtonCount >= MaxButtonCount)
                {
                    VerticalOffset += ButtonSize.Height + Spacing;
                    HorizontalOffset = 50;
                    ButtonCount = 0;
                }
            }

            List<ReagentManagement.ReagentType> ChildReagents = Category.GetChildReagents();
            foreach (ReagentManagement.ReagentType ChildReagent in ChildReagents)
            {
                Button ReagentButton = new Button();
                if (File.Exists(".\\images\\" + ChildReagent.TypeID + ".jpg"))
                {
                    Image ButtonImage = Image.FromFile(".\\images\\" + ChildReagent.TypeID + ".jpg");

                    if (ButtonImage.Height == 100 || ButtonImage.Width == 100)
                    {
                        ReagentButton.BackgroundImage = ButtonImage;
                    }
                    else
                    {
                        MessageBox.Show("Product Image " + ChildReagent.TypeID + ".jpg is not 100x100 pixels, resize or delete the image to stop seeing this message.");
                        ReagentButton.Text = ChildReagent.Name + " (Unit: " + ChildReagent.UnitOfSale + ")";
                    }

                }
                else
                {
                    ReagentButton.Text = ChildReagent.Name + " (Unit: " + ChildReagent.UnitOfSale + ")";                
                }
                ReagentButton.Font = ButtonFont;
                ReagentButton.BackColor = System.Drawing.Color.Honeydew;
                ReagentButton.Size = ButtonSize;
                ReagentButton.Top = VerticalOffset;
                ReagentButton.Left = HorizontalOffset;
                HorizontalOffset += ButtonSize.Width + Spacing;
                //if (ChildReagent.Icon != null)
                //{
                //    ReagentButton.Image = ChildReagent.Icon;
                //}
                ReagentButton.Tag = ChildReagent;
                ReagentButton.Click += new EventHandler(ReagentButton_Click);
                ReturnList.Add(ReagentButton);
                if (++ButtonCount >= MaxButtonCount)
                {
                    VerticalOffset += ButtonSize.Height + Spacing;
                    HorizontalOffset = 50;
                    ButtonCount = 0;
                }
            }

            List<ReagentManagement.ReagentSet> ChildReagentSets = Category.GetChildReagentSets();
            foreach (ReagentManagement.ReagentSet ChildReagentSet in ChildReagentSets)
            {
                Button ReagentSetButton = new Button();
                ReagentSetButton.BackColor = System.Drawing.Color.LightGreen;
                ReagentSetButton.Size = ButtonSize;
                ReagentSetButton.Top = VerticalOffset;
                ReagentSetButton.Left = HorizontalOffset;
                HorizontalOffset += ButtonSize.Width + Spacing;
                ReagentSetButton.Text = ChildReagentSet.Name + " (Unit: Bundle)";
                //if (ChildReagentSet.Icon != null)
                //{
                //    ReagentSetButton.Image = ChildReagentSet.Icon;
                //}
                ReagentSetButton.Tag = ChildReagentSet;
                ReagentSetButton.Click += new EventHandler(ReagentSetButton_Click);
                ReturnList.Add(ReagentSetButton);
                if (++ButtonCount >= MaxButtonCount)
                {
                    VerticalOffset += ButtonSize.Height + Spacing;
                    HorizontalOffset = 50;
                    ButtonCount = 0;
                }
            }

            Button BackButton = new Button();
            BackButton.Size = ButtonSize;
            BackButton.Top = VerticalOffset;
            BackButton.Left = HorizontalOffset;
            BackButton.Text = "Back";
            BackButton.Click += new EventHandler(BackButton_Click);
            ReturnList.Add(BackButton);

            return ReturnList.ToArray();
        }

        void ReagentSetButton_Click(object sender, EventArgs e)
        {
            if (ShoppingCart == null) SetLoggedOut();
            QuantityForm QF = new QuantityForm();
            QF.ReagentSet = (ReagentManagement.ReagentSet)((Button)sender).Tag;
            QF.ShowDialog(this);
            FillShoppingCartDataGrid();
        }

        void CategoryButton_Click(object sender, EventArgs e)
        {
            if (ShoppingCart == null) SetLoggedOut();
            ReagentManagement.Category Category = (ReagentManagement.Category)((Button)sender).Tag;
            CatalogPanel.Controls.AddRange(MakeButtons(Category));
        }

        void ReagentButton_Click(object sender, EventArgs e)
        {
            if (ShoppingCart == null) SetLoggedOut();
            QuantityForm QF = new QuantityForm();
            QF.Reagent = (ReagentManagement.ReagentType)((Button)sender).Tag;
            QF.ShowDialog(this);
            FillShoppingCartDataGrid();
            
        }

        private void FillShoppingCartDataGrid()
        {
            ShoppingCartDataGrid.Rows.Clear();
            foreach (ReagentManagement.ShoppingCart.ShoppingCartItem CartItem in ShoppingCart.Items)
            {
                DataGridViewRow ItemRow = new DataGridViewRow();
                DataGridViewTextBoxCell NameCell = new DataGridViewTextBoxCell();
                NameCell.Value = CartItem.ItemType.Name;
                
                DataGridViewTextBoxCell ItemIDsCell = new DataGridViewTextBoxCell();
                string ItemIDList = string.Empty;
                foreach (string ItemID in CartItem.ItemIDs)
                {
                    ItemIDList += ItemID + ",";
                }
                if (!string.IsNullOrEmpty(ItemIDList)) ItemIDList.TrimEnd(',');
                ItemIDsCell.Value = ItemIDList;

                DataGridViewTextBoxCell QuantityCell = new DataGridViewTextBoxCell();
                QuantityCell.Value = CartItem.Quantity;

                DataGridViewButtonCell RemoveButtonCell = new DataGridViewButtonCell();
                ItemRow.Height = 50;
                RemoveButtonCell.Value = "Remove";
                ItemRow.Cells.Add(NameCell);
                ItemRow.Cells.Add(ItemIDsCell);
                ItemRow.Cells.Add(QuantityCell);
                ItemRow.Cells.Add(RemoveButtonCell);
                //ItemRow.Cells[0].ReadOnly = true;
                //ItemRow.Cells[1].ReadOnly = true;
                ShoppingCartDataGrid.Rows.Add(ItemRow);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            if (CurrentCategory.ParentCategoryID != -1)
            {
                CatalogPanel.Controls.AddRange(MakeButtons(new ReagentManagement.Category(CurrentCategory.ParentCategoryID, DatabaseConfig.GetConnectionString())));
            }
            else
            {
                PopulateTopLevelCategories();
            }
        }

        private void SetLoggedOut()
        {
            ShoppingCart = null;
            CatalogPanel.Controls.Clear();
            PopulateTopLevelCategories();
            ShoppingCartDataGrid.Rows.Clear();
            this.Hide();
            Login.SetLoggedOut();
            Login.ShowDialog(this);
        }

        public void PopulateSignoutHistory()
        {
            CheckoutHistoryGroupBox.Controls.Clear();
            try
            {
                List<ReagentManagement.SignOutEvent> EventList = ReagentManagement.SignOutEvent.GetSignoutHistory(ShoppingCart.User.UserID, DatabaseConfig.GetConnectionString());
                int CurrentTop = 20;
                for(int i =0; i<=10 && i<EventList.Count; i++)
                {
                    Label L = new Label();
                    L.Top = CurrentTop;
                    L.Left = 10;
                    L.Width = CheckoutHistoryGroupBox.Width - 15;
                    string ReagentName = ((EventList[i].Item.Name.Length > 25) ? EventList[i].Item.Name.Substring(0, 25) + "..." : EventList[i].Item.Name) + "(" + EventList[i].Item.UnitOfSale+")";
                    L.Text = EventList[i].Quantity.ToString() + "x " + ReagentName + "(ID#:" + EventList[i].Item.ItemID + ") - " + EventList[i].Date;
                    CheckoutHistoryGroupBox.Controls.Add(L);
                    CurrentTop += 30;
                }
            }
            catch
            {
                Label L = new Label();
                L.Top = 15;
                L.Left = 10;
                L.Text = "Error Fetching Signout History";
                L.Width = CheckoutHistoryGroupBox.Width - 5;
                CheckoutHistoryGroupBox.Controls.Add(L);
            }
        }

        private void LogOutButton_Click(object sender, EventArgs e)
        {
            SetLoggedOut();
        }

        private void CheckOutButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShoppingCart.Items.Count == 0)
                {
                    SetLoggedOut();
                }
                else
                {
                    List<ReagentManagement.Reagent> UsedItems = ShoppingCart.CheckOut(DatabaseConfig.ApplicationName);

                    string ItemsMessage = "The following Items were checked out:\r\n";
                    foreach (ReagentManagement.Reagent R in UsedItems)
                    {
                        ItemsMessage += R.Name + "(ItemID#: " + R.ItemID + ")\r\n";
                    }

                    if (MessageBox.Show(ItemsMessage, "Items Used", MessageBoxButtons.OK)== DialogResult.OK)
                    {
                        SetLoggedOut();
                    }
                }
            }
            catch (ArgumentOutOfRangeException Err)
            {
                MessageBox.Show(Err.Message, Err.ParamName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception Err)
            {
                if (Err.Message == "Stock Level Check Failed")
                {
                   //Ignore                                   
                   SetLoggedOut();
                }
                else
                {
                    MessageBox.Show(Err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void AdministrationButton_Click(object sender, EventArgs e)
        {                        
            Admin.ShowDialog(this);
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePassword CP = new ChangePassword();
            CP.ShowDialog(this);

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to exit the application?","Exit",MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

       

    }
}
